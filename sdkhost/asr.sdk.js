var global = global || window;

((queue, config) => {

    // inject feature switch in doc

    var functions, 
        exec, 
        i;

    // SDK functions
    functions = {
        // try to inject the parent scope here
        widgets: {
            initialize: (a) => {
                widgetsInitialize (a);
            },
            create: (a) => {
                widgetsCreate(a);
            },
        },
        events: {
            resize: (a) => {
                resizeIframe(a[0]);
            },
            click: (a) => {
                switchToWidget(a[0]);
            },
        },
        tools: {
            log : (a) => {
                console.log (a);
            },
        }
    };

    exec = function (a) {
        if (typeof functions[a[0]] === 'object' && typeof functions[a[0]][a[1]] === 'function') {
            functions[a[0]][a[1]](a[2]);
        } else {
            console.log('%c%s', 'color: white; background: red; font-size: 24px;', '⚡ STOP ⚡');
            console.error('This is an SDK intended for developers, please consult your documentation for proper use.');
        }
    }

    window[gnsWidgetObject] = (x, y, z) => {
        z = Object.prototype.toString.call(z) === '[object Array]' ? z : z = [z];
        (global[gnsWidgetObject].q = global[gnsWidgetObject].q || []).push([x, y, z]);
        exec(global[gnsWidgetObject].q[global[gnsWidgetObject].q.length - 1]);
    };

    // reattach the initial queue and config to the function for history purposes
    window[gnsWidgetObject].q = queue;
    window[gnsWidgetObject].c = config;

    // execute the provided queue
    for (i in queue){
        exec(queue[i]);
    }

})(window[gnsWidgetObject].q, window[gnsWidgetObject].c);






function widgetsInitialize (a){

    let obj = window[gnsWidgetObject];

    obj.state = obj.state || {elements: window.document.getElementsByClassName(gnsWidgetObject)};
    
    obj.c = obj.c || {};
    obj.c.host = extractHostname(obj.c.l);
    obj.c.url = extractHostname(obj.c.l, true);
    obj.c.versions = returnVersions();

    
    if (!obj.c.featureSwitch){
        loadFeatureSwitches(obj.c.url, function (err, featureSwitchConfig) {
            if (err !== null) {
                console.error (e);
                return;
            }
            obj.c.featureSwitch = featureSwitchConfig;
        });
    }

    if (!global.document.addEventListener) {
        for (i = 0; i < obj.state.elements.length; i += 1) {
            let el = obj.state.elements[i];

            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }

            const p = window.document.createElement('p');
            p.className = 'browser-not-supported';
            p.innerHTML = 'Unfortunately the browser you are currently using is not supported by our widgets. Please update your browser.';
            el.appendChild(p);

        }
        return false;
    }

}






function widgetsCreate (a){

    if (window[gnsWidgetObject].state === undefined){
        widgetsInitialize(a);
    }

    const config = global[gnsWidgetObject].c;
    let state = global[gnsWidgetObject].state;
    let widgets = [];
    let i;

    for (i = 0; i < state.elements.length; i += 1) {

        let el = state.elements[i];
        let attributes = getScriptTagDataAttributes(el.attributes);


        loadWidgetConfig(config.host, attributes, function (e, widgetConfig) {

            var versionPath = '',
                iFrame = document.createElement('iframe');

            if (e !== null) {
                console.error (e);
                return;
            }

            widgetConfig.actualVersion = config.versions[widgetConfig.version];

            if (widgetConfig.actualVersion !== undefined && widgetConfig.actualVersion !== '') {
                versionPath = '/' + widgetConfig.actualVersion;
            }

            widgetConfig.iFrameId = generateId(6);
            widgetConfig.versionPath = versionPath;
            widgetConfig.featureSwitches = config.featureSwitch;
            widgetConfig.loaderDomain = config.host;
            widgetConfig.baseURL = config.url;
            widgetConfig.widgetUrl = 'widgets/' + (widgetConfig.theme || 'default') + '/' + widgetConfig.sport + '/' + widgetConfig.type + '.js?viewCount=1';

            if (widgetConfig.client_id !== undefined) {
                widgetConfig.widgetUrl += '&c=' + widgetConfig.client_id;
            }

            widgets.push (widgetConfig);

            setIFrameVariables(iFrame, widgetConfig.iFrameId);

            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }

            el.appendChild(iFrame, el);

            writeIFrameDocument(iFrame, widgetConfig.widgetUrl, widgetConfig);

            var eventMethod = global.addEventListener ? 'addEventListener' : 'attachEvent';
            var eventListener = global[eventMethod];
            var messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';

            eventListener(messageEvent, messageEventCallback, true);

        });


    };

    window[gnsWidgetObject].state.widgets = widgets;

};

