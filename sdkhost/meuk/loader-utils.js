const _gateway2 = {
    "default": {
        "default": {
            "baseURL": "https://og2018-datatest-api.sports.gracenote.com/svc/",
            "headers": {
                
            },
            "mocks": {
                "baseURL": "http://widgets.sports.labs.gracenote.com/mocks/",
                "headers": {
                    
                }
            },
            "configURL": "https://widgets.sports.gracenote.com/configs/"
        },
        "widget-builder.dev": {
            "baseURL": "https://og2018-datatest-api.sports.gracenote.com/svc/",
            "headers": {
                
            },
            "mocks": {
                "baseURL": "//widget-builder.dev:3000/mocks/",
                "headers": {
                    
                }
            },
            "configURL": "https://widgets.sports.gracenote.com/configs/"
        },
        "test.widgets.sports.labs.gracenote.com": {
            "baseURL": "https://og2018-datatest-api.sports.gracenote.com/svc/",
            "headers": {
                
            },
            "mocks": {
                "baseURL": "//test.widgets.sports.labs.gracenote.com/mocks/",
                "headers": {
                    
                }
            },
            "configURL": "https://widgets.sports.gracenote.com/configs/"
        },
        "staging.widgets.sports.gracenote.com": {
            "baseURL": "https://og2018-api.sports.gracenote.com/svc/",
            "headers": {
                
            },
            "mocks": {
                "baseURL": "//staging.widgets.sports.gracenote.com/mocks/",
                "headers": {
                    
                }
            },
            "configURL": "https://widgets.sports.gracenote.com/configs/"
        },
        "widgets.sports.gracenote.com": {
            "baseURL": "https://og2018-api.sports.gracenote.com/svc/",
            "headers": {
                
            },
            "mocks": {
                "baseURL": "//staging.widgets.sports.gracenote.com/mocks/",
                "headers": {
                    
                }
            },
            "configURL": "https://widgets.sports.gracenote.com/configs/"
        },
        "staging-widgets.nbcolympics.com": {
            "baseURL": "https://stgapi-gracenote.nbcolympics.com/svc/",
            "headers": {
                
            },
            "mocks": {
                "baseURL": "//staging-widgets.nbcolympics.com/mocks/",
                "headers": {
                    
                }
            },
            "configURL": "https://widgets.nbcolympics.com/configs/"
        },
        "widgets.nbcolympics.com": {
            "baseURL": "https://api-gracenote.nbcolympics.com/svc/",
            "headers": {
                
            },
            "mocks": {
                "baseURL": "//widgets.nbcolympics.com/mocks/",
                "headers": {
                    
                }
            },
            "configURL": "https://widgets.nbcolympics.com/configs/"
        }
    }
}





function cloneAttributes(element, attributes) {
    Object.keys(attributes).forEach(function (key) {
        var val = attributes[key] || "";
        if (val){
            element.setAttribute('data-' + key, val);
        }
    });
};





function extractHostname(url, keepProtocol = false) {
    let hostname;

    // find & remove protocol (http, ftp, etc.) and get hostname
    if (url.indexOf('//') > -1) {
        hostname = url.split('/')[2];
    } else {
        hostname = url.split('/')[0];
    }

    // find & remove port number
    const port = hostname.split(':')[1];
    hostname = hostname.split(':')[0];

    // find & remove "?"
    hostname = hostname.split('?')[0];

    if (url.indexOf('//') > -1 && keepProtocol) {
        hostname = `${url.split('/')[0]}//${hostname}`;
        if (port) {
            hostname = `${hostname}:${port}`;
        }
    }

    return hostname;
}






function returnVersions() {
    return {
        "latest": "latest",
        "v1.6": "v1.6",
        "v1.7": "v1.7",
        "v1.8": "v1.8",
        "v1.9": "v1.9",
        "v2.0.0": "v2.0.0",
        "v2.0": "v2.0.0",
        "v2.1.0": "v2.1.0",
        "v2.1.1": "v2.1.1",
        "v2.1.2": "v2.1.2",
        "v2.2.0": "v2.2.0",
        "v2.3.0": "v2.3.0",
        "v2.1": "v2.1.2",
        "v2.2": "v2.2.0",
        "v2.3": "v2.3.0"
    }
}










function getScriptTagDataAttributes(attributes) {
    var dataAttributes = {};

    [].forEach.call(attributes, function (attribute) {
        if (/^data-/.test(attribute.name)) {
            var parameter = attribute.name.substr(5);
            dataAttributes[parameter] = attribute.value;
        }
    });

    return dataAttributes;
};











function loadWidgetConfig(loaderDomain, config, callback) {
    if (config === undefined || config.widget_id === undefined) {
        callback('Error: could not load widget config', null);
    } else {
        var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
        global.__GNS_ANTI_CACHE = antiCache;

        // Generate widget configuration url
        var configURL = _gateway2.default.default.configURL;
        if (loaderDomain && _gateway2.default[loaderDomain]) {
            configURL = _gateway2.default[loaderDomain].configURL;
        }
        var configFile = '' + configURL + config.widget_id + '.json?v=' + antiCache;

        var data = {};

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function xhrResponse() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    try {
                        data = JSON.parse(xhr.responseText);
                    } catch (error) {
                        callback('Error: could not load configuration file for widget id: ' + config.widget_id, null);
                        return;
                    }
                } else {
                    callback('Error: could not load configuration file for widget id: ' + config.widget_id, null);
                    return;
                }
                dataHandler(data, config, callback);
            }
        };
        xhr.open('get', configFile);
        xhr.responseType = 'text';
        xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
        xhr.send();
    }
};










function dataHandler(data, config, callback) {
    config.customer_id = data.customer_id;

    if (data.allow_tags === 'yes') {
        if (data.allow_data_tags !== undefined) {
            var tags = data.allow_data_tags.map(function (item) {
                return item.trim();
            });

            // Loop through all given config attributes
            Object.keys(config).forEach(function (configTag) {
                if (tags.find(function (tag) {
                    return tag === configTag;
                })) {
                    // attribute exists in allow_data_tags and default config.
                    // so we may override the data attribute with given config attribute
                    data[configTag] = config[configTag];
                }

                if (data[configTag] === undefined) {
                    // here we add it if it doesn't exists in the default config
                    data[configTag] = config[configTag];
                }
            });
        } else {
            data = mergeObject({}, data, config);
        }
    } else {
        // Loop through all given config attributes
        Object.keys(config).forEach(function (configTag) {
            if (data[configTag] === undefined) {
                // here we add it if it doesn't exists in the default config
                data[configTag] = config[configTag];
            }
        });
    }

    // Add custom_css to the data
    if (config.custom_css !== undefined && config.custom_css !== '') {
        data.custom_css = config.custom_css;
    }

    // TODO: In the near future we're going to add a real domain check...
    // if (!checkDomain(data, document.referrer.toString())) {
    //     callback('Error: domain is not allowed.', null);
    // }

    if (data.type === 'tagging') {
        data.version = 'v2.0';
        data.language = 'en_GB';
        data.language_code = '2';
        data.theme = 'default';
    }

    callback(null, data);
}









function messageEventCallback(event) {
    var message = void 0;
    try {
        message = JSON.parse(event.data);
    } catch (error) {
        message = null;
    }

    if (message) {
        if (message.action) {
            switch (message.action) {
                case 'gns.resizeIFrame':
                    if (global[gnsWidgetObject]){
                        global[gnsWidgetObject]('events', 'resize', message);
                    } else {
                        resizeIframe(message);
                    }
                    break;
                case 'gns.switchToWidget':
                    if (global[gnsWidgetObject]){
                        global[gnsWidgetObject]('events', 'click', message);
                    } else {
                        switchToWidget(message);
                    }
                    break;
                default:
            }
        }
    }
}






function resizeIframe (message){

    console.log ('message', message);

    if (message !== undefined && message.iFrameId !== undefined) {
        
        var targetIFrame = document.getElementById(message.iFrameId);
        if (targetIFrame) {
            var parentZoomLevel = window.getComputedStyle(document.getElementsByTagName('html')[0]).zoom;
            if (parentZoomLevel < 1) {
                targetIFrame.height = Math.ceil(message.bounds.height / parentZoomLevel);
            } else {
                targetIFrame.height = message.bounds.height;
            }
        }
    } else {
        return false;
    }

}


function switchToWidget(message){

    if (message.settings.iFrameId !== undefined) {
        var _targetIFrame = document.getElementById(message.settings.iFrameId);
        if (_targetIFrame) {
            var widgetUrl = 'widgets/' + message.settings.theme + '/' + message.settings.sport + '/' + message.settings.type + '.js?viewCount=1';

            if (message.settings.customer_id !== undefined) {
                widgetUrl = widgetUrl + '&c=' + message.settings.customer_id;
            }

            switchIFrameContent(widgetUrl, message.settings);
        }
    } else {
        return false;
    }

}




function switchIFrameContent(url, config) {
    var selectedIFrame = document.getElementById(config.iFrameId);
    var placeHolder = document.createElement('div');
    placeHolder.id = generateId(12);
    selectedIFrame.parentNode.insertBefore(placeHolder, selectedIFrame);
    selectedIFrame.parentNode.removeChild(selectedIFrame);

    var newFrame = document.createElement('iframe');

    setIFrameVariables(newFrame, config.iFrameId);

    placeHolder.parentNode.insertBefore(newFrame, placeHolder);
    placeHolder.parentNode.removeChild(placeHolder);

    writeIFrameDocument(newFrame, url, config);
}





function loadFeatureSwitches(loaderURL, callback) {
    var featureSwitches = {};
    
	var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
    global.__GNS_ANTI_CACHE = antiCache;
    var configFileURL = loaderURL + '/feature_switches.json?v=' + antiCache;

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function xhrResponse() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                try {
					featureSwitches = JSON.parse(xhr.responseText)
                } catch (error) {
                    callback('Error: could not parse feature switches config file', null);
                }
            } else {
                callback('Error: could not load feature switches config file', null);
			}
			callback(null, featureSwitches);
        }
    };

    xhr.open('get', configFileURL);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
    xhr.send();
};






function generateId(length) {
    var string = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i += 1) {
        string += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return string;
};




function setIFrameVariables(iFrame, iFrameId) {
    iFrame.id = iFrameId;
    iFrame.name = iFrameId;
    iFrame.src = 'javascript:false'; // eslint-disable-line
    iFrame.title = '';
    iFrame.role = 'presentation';
    iFrame.frameBorder = '0';
    iFrame.scrolling = 'no';
    iFrame.width = '100%';
    iFrame.height = '137px';
    (iFrame.frameElement || iFrame).style.cssText = 'border:0;min-width:100%;';
}







function writeIFrameDocument(iFrame, url, config) {
    var doc = void 0;
    var dom = void 0;
    try {
        doc = iFrame.contentWindow.document;
    } catch (err) {
        dom = document.domain;
        iFrame.src = 'javascript:var d=document.open();d.domain="' + dom + '";void(0);';
        doc = iFrame.contentWindow.document;
    }

    doc.open()._l = function () {
        // eslint-disable-line
        var js = this.createElement('script');
        if (dom) this.domain = dom;
        js.id = 'widget-iframe-async';
        js.src = url;
        js.async = true;
        this.body.appendChild(js);
    };
    doc.write('<html><head>');
    doc.write('<base href="' + config.baseURL + '">');
    doc.write('<meta charset="utf-8">');
    // Inject React Developer Tools if not in production
    if (true) {
        doc.write('<script>__REACT_DEVTOOLS_GLOBAL_HOOK__ = parent.__REACT_DEVTOOLS_GLOBAL_HOOK__</script>');
    }
    doc.write('<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">');
    doc.write('<meta name="viewport" content="width=device-width, initial-scale=1">');
    doc.write('<meta name="apple-mobile-web-app-capable" content="yes">');
    doc.write('<meta http-equiv="Cache-Control" content="no-transform">');
    doc.write('</head>');
    doc.write('<body onload="document._l();">');
    doc.write('<script>window.settings = ' + JSON.stringify(config) + ';</script>');
    if (shouldLoadInstana(config)) {
        doc.write('<script>(function(i,s,o,g,r,a,m){i[\'InstanaEumObject\']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)}, i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a, m)})(window, document, \'script\', \'//eum.instana.io/eum.min.js\', \'ineum\');ineum(\'apiKey\', \'o4yYT7q7TUqPjfapqD3piw\');</script>');
    }
    if (shouldLoadAnalytics(config)) {
        doc.write('<script async src="https://www.googletagmanager.com/gtag/js?id=UA-58323625-12"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date());gtag(\'config\', \'UA-58323625-12\');</script>');
    }
    if (config.analytics && config.analytics !== '') {
        doc.write('<script async src="https://www.googletagmanager.com/gtag/js?id=' + config.analytics + '"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date());gtag(\'config\', \'' + config.analytics + '\');</script>');
    }
    doc.write('<div id="mainContent" class="container"><div id="widget"></div></div>');
    doc.write('</body><html>');
    doc.close();
}








function shouldLoadInstana(config) {
    if (!config || !config.featureSwitches || !config.featureSwitches.all || !config.featureSwitches.all.instana || !config.featureSwitches.all.instana.active) {
        return false;
    }

    var magicNumber = Math.random() * 100;

    return config.featureSwitches.all.instana.percentage_active >= magicNumber;
};







function shouldLoadAnalytics(config) {
    if (!config || !config.featureSwitches || !config.featureSwitches.all || !config.featureSwitches.all.analytics || !config.featureSwitches.all.analytics.active) {
        return false;
    }

    var magicNumber = Math.random() * 100;

    return config.featureSwitches.all.analytics.percentage_active >= magicNumber;
};
