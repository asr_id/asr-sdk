/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 30);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var bind = __webpack_require__(5);

module.exports = bind.call(Function.call, Object.prototype.hasOwnProperty);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var fnToStr = Function.prototype.toString;

var constructorRegex = /^\s*class /;
var isES6ClassFn = function isES6ClassFn(value) {
	try {
		var fnStr = fnToStr.call(value);
		var singleStripped = fnStr.replace(/\/\/.*\n/g, '');
		var multiStripped = singleStripped.replace(/\/\*[.\s\S]*\*\//g, '');
		var spaceStripped = multiStripped.replace(/\n/mg, ' ').replace(/ {2}/g, ' ');
		return constructorRegex.test(spaceStripped);
	} catch (e) {
		return false; // not a function
	}
};

var tryFunctionObject = function tryFunctionObject(value) {
	try {
		if (isES6ClassFn(value)) { return false; }
		fnToStr.call(value);
		return true;
	} catch (e) {
		return false;
	}
};
var toStr = Object.prototype.toString;
var fnClass = '[object Function]';
var genClass = '[object GeneratorFunction]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isCallable(value) {
	if (!value) { return false; }
	if (typeof value !== 'function' && typeof value !== 'object') { return false; }
	if (hasToStringTag) { return tryFunctionObject(value); }
	if (isES6ClassFn(value)) { return false; }
	var strClass = toStr.call(value);
	return strClass === fnClass || strClass === genClass;
};


/***/ }),
/* 2 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keys = __webpack_require__(16);
var foreach = __webpack_require__(18);
var hasSymbols = typeof Symbol === 'function' && typeof Symbol() === 'symbol';

var toStr = Object.prototype.toString;

var isFunction = function (fn) {
	return typeof fn === 'function' && toStr.call(fn) === '[object Function]';
};

var arePropertyDescriptorsSupported = function () {
	var obj = {};
	try {
		Object.defineProperty(obj, 'x', { enumerable: false, value: obj });
        /* eslint-disable no-unused-vars, no-restricted-syntax */
        for (var _ in obj) { return false; }
        /* eslint-enable no-unused-vars, no-restricted-syntax */
		return obj.x === obj;
	} catch (e) { /* this is IE 8. */
		return false;
	}
};
var supportsDescriptors = Object.defineProperty && arePropertyDescriptorsSupported();

var defineProperty = function (object, name, value, predicate) {
	if (name in object && (!isFunction(predicate) || !predicate())) {
		return;
	}
	if (supportsDescriptors) {
		Object.defineProperty(object, name, {
			configurable: true,
			enumerable: false,
			value: value,
			writable: true
		});
	} else {
		object[name] = value;
	}
};

var defineProperties = function (object, map) {
	var predicates = arguments.length > 2 ? arguments[2] : {};
	var props = keys(map);
	if (hasSymbols) {
		props = props.concat(Object.getOwnPropertySymbols(map));
	}
	foreach(props, function (name) {
		defineProperty(object, name, map[name], predicates[name]);
	});
};

defineProperties.supportsDescriptors = !!supportsDescriptors;

module.exports = defineProperties;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(19);


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(20);

module.exports = Function.prototype.bind || implementation;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = function isPrimitive(value) {
	return value === null || (typeof value !== 'function' && typeof value !== 'object');
};


/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = Number.isNaN || function isNaN(a) {
	return a !== a;
};


/***/ }),
/* 8 */
/***/ (function(module, exports) {

var $isNaN = Number.isNaN || function (a) { return a !== a; };

module.exports = Number.isFinite || function (x) { return typeof x === 'number' && !$isNaN(x) && x !== Infinity && x !== -Infinity; };


/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = function sign(number) {
	return number >= 0 ? 1 : -1;
};


/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = function mod(number, modulo) {
	var remain = number % modulo;
	return Math.floor(remain >= 0 ? remain : remain + modulo);
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var ES = __webpack_require__(4);

module.exports = function find(predicate) {
	var list = ES.ToObject(this);
	var length = ES.ToInteger(ES.ToLength(list.length));
	if (!ES.IsCallable(predicate)) {
		throw new TypeError('Array#find: predicate must be a function');
	}
	if (length === 0) {
		return undefined;
	}
	var thisArg = arguments[1];
	for (var i = 0, value; i < length; i++) {
		value = list[i];
		if (ES.Call(predicate, thisArg, [value, i, list])) {
			return value;
		}
	}
	return undefined;
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function getPolyfill() {
	// Detect if an implementation exists
	// Detect early implementations which skipped holes in sparse arrays
  // eslint-disable-next-line no-sparse-arrays
	var implemented = Array.prototype.find && [, 1].find(function () {
		return true;
	}) !== 1;

  // eslint-disable-next-line global-require
	return implemented ? Array.prototype.find : __webpack_require__(11);
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.loadFeatureSwitches = exports.loadVersions = exports.messageEventCallback = exports.generateId = exports.loadIframeChecker = exports.initializeWidget = exports.loadWidgetConfig = exports.getScriptTagDataAttributes = exports.shouldLoadAnalytics = exports.shouldLoadInstana = exports.loadIFrame = undefined;
exports.extractHostname = extractHostname;
exports.extractActualVersion = extractActualVersion;

var _gateway = __webpack_require__(14);

var _gateway2 = _interopRequireDefault(_gateway);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

__webpack_require__(15).shim();

function mergeObject(target) {
    // eslint-disable-next-line no-plusplus
    for (var i = 1; i < arguments.length; i++) {
        // eslint-disable-next-line prefer-rest-params
        var source = arguments[i];
        // eslint-disable-next-line no-restricted-syntax
        for (var key in source) {
            // eslint-disable-next-line no-prototype-builtins
            if (source.hasOwnProperty(key)) {
                target[key] = source[key];
            }
        }
    }
    return target;
}

function setIFrameVariables(iFrame, iFrameId) {
    iFrame.id = iFrameId;
    iFrame.name = iFrameId;
    iFrame.src = 'javascript:false'; // eslint-disable-line
    iFrame.title = '';
    iFrame.role = 'presentation';
    iFrame.frameBorder = '0';
    iFrame.scrolling = 'no';
    iFrame.width = '100%';
    iFrame.height = '137px';
    (iFrame.frameElement || iFrame).style.cssText = 'border:0;min-width:100%;';
}

function writeIFrameDocument(iFrame, url, config) {
    var doc = void 0;
    var dom = void 0;
    try {
        doc = iFrame.contentWindow.document;
    } catch (err) {
        dom = document.domain;
        iFrame.src = 'javascript:var d=document.open();d.domain="' + dom + '";void(0);';
        doc = iFrame.contentWindow.document;
    }

    doc.open()._l = function () {
        // eslint-disable-line
        var js = this.createElement('script');
        if (dom) this.domain = dom;
        js.id = 'widget-iframe-async';
        js.src = url;
        js.async = true;
        this.body.appendChild(js);
    };
    doc.write('<html><head>');
    doc.write('<base href="' + config.baseURL + '">');
    doc.write('<meta charset="utf-8">');
    // Inject React Developer Tools if not in production
    if (true) {
        doc.write('<script>__REACT_DEVTOOLS_GLOBAL_HOOK__ = parent.__REACT_DEVTOOLS_GLOBAL_HOOK__</script>');
    }
    doc.write('<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">');
    doc.write('<meta name="viewport" content="width=device-width, initial-scale=1">');
    doc.write('<meta name="apple-mobile-web-app-capable" content="yes">');
    doc.write('<meta http-equiv="Cache-Control" content="no-transform">');
    doc.write('</head>');
    doc.write('<body onload="document._l();">');
    doc.write('<script>window.settings = ' + JSON.stringify(config) + ';</script>');
    if (shouldLoadInstana(config)) {
        doc.write('<script>(function(i,s,o,g,r,a,m){i[\'InstanaEumObject\']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)}, i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a, m)})(window, document, \'script\', \'//eum.instana.io/eum.min.js\', \'ineum\');ineum(\'apiKey\', \'o4yYT7q7TUqPjfapqD3piw\');</script>');
    }
    if (shouldLoadAnalytics(config)) {
        doc.write('<script async src="https://www.googletagmanager.com/gtag/js?id=UA-58323625-12"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date());gtag(\'config\', \'UA-58323625-12\');</script>');
    }
    if (config.analytics && config.analytics !== '') {
        doc.write('<script async src="https://www.googletagmanager.com/gtag/js?id=' + config.analytics + '"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date());gtag(\'config\', \'' + config.analytics + '\');</script>');
    }
    doc.write('<div id="mainContent" class="container"><div id="widget"></div></div>');
    doc.write('</body><html>');
    doc.close();
}

function switchIFrameContent(url, config) {
    var selectedIFrame = document.getElementById(config.iFrameId);
    var placeHolder = document.createElement('div');
    placeHolder.id = generateId(12);
    selectedIFrame.parentNode.insertBefore(placeHolder, selectedIFrame);
    selectedIFrame.parentNode.removeChild(selectedIFrame);

    var newFrame = document.createElement('iframe');

    setIFrameVariables(newFrame, config.iFrameId);

    placeHolder.parentNode.insertBefore(newFrame, placeHolder);
    placeHolder.parentNode.removeChild(placeHolder);

    writeIFrameDocument(newFrame, url, config);
}

/**
 * loadIFrame
 *
 * Generates iFrame and loads the given url as script-tag
 *
 * @param element
 * @param url
 * @param config
 */

var loadIFrame = exports.loadIFrame = function loadIFrame(element, url, config) {
    var iFrame = document.createElement('iframe');
    config.iFrameId = generateId(6);

    setIFrameVariables(iFrame, config.iFrameId);

    var scripts = document.getElementsByTagName('script');
    [].forEach.call(scripts, function (script) {
        if (script === element) {
            script.parentNode.insertBefore(iFrame, script);
            writeIFrameDocument(iFrame, url, config);
        }
    });
};

var shouldLoadInstana = exports.shouldLoadInstana = function shouldLoadInstana(config) {
    if (!config || !config.featureSwitches || !config.featureSwitches.all || !config.featureSwitches.all.instana || !config.featureSwitches.all.instana.active) {
        return false;
    }

    var magicNumber = Math.random() * 100;

    return config.featureSwitches.all.instana.percentage_active >= magicNumber;
};

var shouldLoadAnalytics = exports.shouldLoadAnalytics = function shouldLoadAnalytics(config) {
    if (!config || !config.featureSwitches || !config.featureSwitches.all || !config.featureSwitches.all.analytics || !config.featureSwitches.all.analytics.active) {
        return false;
    }

    var magicNumber = Math.random() * 100;

    return config.featureSwitches.all.analytics.percentage_active >= magicNumber;
};

/**
 * getScriptTagDataAttributes
 *
 * Parse script tag attributes and return all data-* attributes as object.
 * @param attributes
 * @returns {{}}
 */
var getScriptTagDataAttributes = exports.getScriptTagDataAttributes = function getScriptTagDataAttributes(attributes) {
    var dataAttributes = {};

    [].forEach.call(attributes, function (attribute) {
        if (/^data-/.test(attribute.name)) {
            var parameter = attribute.name.substr(5);
            dataAttributes[parameter] = attribute.value;
        }
    });

    return dataAttributes;
};

function dataHandler(data, config, callback) {
    config.customer_id = data.customer_id;

    if (data.allow_tags === 'yes') {
        if (data.allow_data_tags !== undefined) {
            var tags = data.allow_data_tags.map(function (item) {
                return item.trim();
            });

            // Loop through all given config attributes
            Object.keys(config).forEach(function (configTag) {
                if (tags.find(function (tag) {
                    return tag === configTag;
                })) {
                    // attribute exists in allow_data_tags and default config.
                    // so we may override the data attribute with given config attribute
                    data[configTag] = config[configTag];
                }

                if (data[configTag] === undefined) {
                    // here we add it if it doesn't exists in the default config
                    data[configTag] = config[configTag];
                }
            });
        } else {
            data = mergeObject({}, data, config);
        }
    } else {
        // Loop through all given config attributes
        Object.keys(config).forEach(function (configTag) {
            if (data[configTag] === undefined) {
                // here we add it if it doesn't exists in the default config
                data[configTag] = config[configTag];
            }
        });
    }

    // Add custom_css to the data
    if (config.custom_css !== undefined && config.custom_css !== '') {
        data.custom_css = config.custom_css;
    }

    // TODO: In the near future we're going to add a real domain check...
    // if (!checkDomain(data, document.referrer.toString())) {
    //     callback('Error: domain is not allowed.', null);
    // }

    if (data.type === 'tagging') {
        data.version = 'v2.0';
        data.language = 'en_GB';
        data.language_code = '2';
        data.theme = 'default';
    }

    callback(null, data);
}

/**
 * loadWidgetConfig
 *
 * @param loaderDomain
 * @param config
 * @param callback
 */
var loadWidgetConfig = exports.loadWidgetConfig = function loadWidgetConfig(loaderDomain, config, callback) {
    if (config === undefined || config.widget_id === undefined) {
        callback('Error: could not load widget config', null);
    } else {
        var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
        global.__GNS_ANTI_CACHE = antiCache;


        var configURL = _gateway2.default.default.configURL;
        if (loaderDomain && _gateway2.default[loaderDomain]) {
            configURL = _gateway2.default[loaderDomain].configURL;
        }
        var configFile = '' + configURL + config.widget_id + '.json?v=' + antiCache;

        var data = {};

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function xhrResponse() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    try {
                        data = JSON.parse(xhr.responseText);
                    } catch (error) {
                        callback('Error: could not load configuration file for widget id: ' + config.widget_id, null);
                        return;
                    }
                } else {
                    callback('Error: could not load configuration file for widget id: ' + config.widget_id, null);
                    return;
                }
                dataHandler(data, config, callback);
            }
        };
        xhr.open('get', configFile);
        xhr.responseType = 'text';
        xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
        xhr.send();
    }
};

// TODO: In the near future we're going to add a real domain check...
/**
 * checkDomain
 *
 * @param settings
 * @param referrer
 * @returns {boolean}
 */
// export const checkDomain = (settings, referrer) => {
//     const referrerArray = referrer.substr(7).replace('/', '.').split('.');
//     let loadAllowed = true; // TODO: Make this default to false
//
//     [].forEach.call(referrerArray, (ref) => {
//         // Logic to ban or allow the widget
//         if (settings.domain) {
//             if (settings.domain === ref) {
//                 loadAllowed = true;
//             }
//         } else {
//             loadAllowed = true;
//         }
//     });
//
//     return loadAllowed;
// };

/**
 * initializeWidget
 *
 * @param element
 * @param attributes
 */
var initializeWidget = exports.initializeWidget = function initializeWidget(element, attributes) {
    // Get script tag data-* attributes
    var dataAttributes = getScriptTagDataAttributes(attributes);
    var loaderDomain = extractHostname(element.src);

    var finalConfig = {};

    // Load widget config json and merge with script tag data-* attributes
    loadWidgetConfig(loaderDomain, dataAttributes, function (error, config) {
        if (error !== null) {
            element.parentNode.insertBefore(document.createTextNode(error), element);
            return;
        }

        finalConfig = mergeObject({}, finalConfig, config);

        // Load the iFrame with returned config
        if (config.sport !== undefined && config.type !== undefined) {
            if (config.theme === undefined) {
                finalConfig.theme = 'default';
            }

            finalConfig.loaderDomain = extractHostname(element.src);
            finalConfig.actualVersion = extractActualVersion(element.src);
            finalConfig.baseURL = element.src.replace('gns.widget.initializer.js', '');

            loadIframeChecker(element, finalConfig);
        }
    });

    loadFeatureSwitches(extractHostname(element.src, true), function (err, featureSwitcherConfig) {
        if (err !== null) {
            element.parentNode.insertBefore(document.createTextNode(err), element);
        }

        finalConfig.featureSwitches = featureSwitcherConfig;
        loadIframeChecker(element, finalConfig);
    });
};

var loadIframeChecker = exports.loadIframeChecker = function loadIframeChecker(element, config) {
    if (config.featureSwitches && config.widget_id) {
        var widgetUrl = 'widgets/' + config.theme + '/' + config.sport + '/' + config.type + '.js?viewCount=1';

        if (config.client_id !== undefined) {
            widgetUrl = widgetUrl + '&c=' + config.client_id;
        }
        loadIFrame(element, widgetUrl, config);
    }
};

/**
 * generateId
 *
 * @param length
 * @returns {string}
 */
var generateId = exports.generateId = function generateId(length) {
    var string = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i += 1) {
        string += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return string;
};

/**
 * messageEventCallback
 *
 * @param event
 */
var messageEventCallback = exports.messageEventCallback = function messageEventCallback(event) {
    var message = void 0;
    try {
        message = JSON.parse(event.data);
    } catch (error) {
        message = null;
    }

    if (message) {
        if (message.action) {
            switch (message.action) {
                case 'gns.resizeIFrame':
                    if (message.iFrameId !== undefined) {
                        var targetIFrame = document.getElementById(message.iFrameId);
                        if (targetIFrame) {
                            var parentZoomLevel = window.getComputedStyle(document.getElementsByTagName('html')[0]).zoom;
                            if (parentZoomLevel < 1) {
                                targetIFrame.height = Math.ceil(message.bounds.height / parentZoomLevel);
                            } else {
                                targetIFrame.height = message.bounds.height;
                            }
                        }
                    }
                    break;
                case 'gns.switchToWidget':
                    if (message.settings.iFrameId !== undefined) {
                        var _targetIFrame = document.getElementById(message.settings.iFrameId);
                        if (_targetIFrame) {
                            var widgetUrl = 'widgets/' + message.settings.theme + '/' + message.settings.sport + '/' + message.settings.type + '.js?viewCount=1';

                            if (message.settings.client_id !== undefined) {
                                widgetUrl = widgetUrl + '&c=' + message.settings.client_id;
                            }

                            switchIFrameContent(widgetUrl, message.settings);
                        }
                    }
                    break;
                default:
            }
        }
    }
};

/**
 * loadVersions
 *
 * @param loaderURL
 * @param callback
 */
var loadVersions = exports.loadVersions = function loadVersions(loaderURL, callback) {
    var versions = {};
    var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
    global.__GNS_ANTI_CACHE = antiCache;
    var configFile = loaderURL + '/versions.json?v=' + antiCache;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function xhrResponse() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                try {
                    versions = JSON.parse(xhr.responseText);
                } catch (error) {
                    callback('Error: could not parse versions.json', null);
                    return;
                }
            } else {
                callback('Error: could not load versions.json', null);
                return;
            }
            callback(null, versions);
        }
    };
    xhr.open('get', configFile);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
    xhr.send();
};

var loadFeatureSwitches = exports.loadFeatureSwitches = function loadFeatureSwitches(loaderURL, callback) {
	var featureSwitches = {};
	var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
    global.__GNS_ANTI_CACHE = antiCache;
    var configFileURL = loaderURL + '/feature_switches.json?v=' + antiCache;

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function xhrResponse() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                try {
					featureSwitches = JSON.parse(xhr.responseText)
                } catch (error) {
                    callback('Error: could not parse feature switches config file', null);
                }
            } else {
                callback('Error: could not load feature switches config file', null);
			}
			callback(null, featureSwitches);
        }
    };

    xhr.open('get', configFileURL);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
    xhr.send();
};

/**
 * extractHostname
 *
 * @param url
 * @param keepProtocol
 * @returns {*}
 */
function extractHostname(url) {
    var keepProtocol = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var hostname = void 0;

    // find & remove protocol (http, ftp, etc.) and get hostname
    if (url.indexOf('://') > -1) {
        hostname = url.split('/')[2];
    } else {
        hostname = url.split('/')[0];
    }

    // find & remove port number
    var port = hostname.split(':')[1];
    hostname = hostname.split(':')[0];

    // find & remove "?"
    hostname = hostname.split('?')[0];

    if (url.indexOf('://') > -1 && keepProtocol) {
        hostname = url.split('/')[0] + '//' + hostname;
        if (port) {
            hostname = hostname + ':' + port;
        }
    }

	return hostname;

}

function extractActualVersion(url) {
    var actualVersion = void 0;

    // Remove initializer part
    var cleanUrl = url.replace('/gns.widget.initializer.js', '');

    // find & remove protocol (http, ftp, etc.) and get hostname

    if (cleanUrl.indexOf('://') > -1) {
        actualVersion = cleanUrl.split('/')[3];
    } else {
        actualVersion = cleanUrl.split('/')[1];
    }

    if (!actualVersion) {
        return null;
    }

    // find & remove port number
    actualVersion = actualVersion.split(':')[0];

    // find & remove "?"
    actualVersion = actualVersion.split('?')[0];

    return actualVersion;
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    default: {
        baseURL: 'https://og2018-datatest-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: 'http://widgets.sports.labs.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'widget-builder.dev': {
        baseURL: 'https://og2018-datatest-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//widget-builder.dev:3000/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'test.widgets.sports.labs.gracenote.com': {
        baseURL: 'https://og2018-datatest-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//test.widgets.sports.labs.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'staging.widgets.sports.gracenote.com': {
        baseURL: 'https://og2018-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//staging.widgets.sports.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'widgets.sports.gracenote.com': {
        baseURL: 'https://og2018-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//staging.widgets.sports.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'staging-widgets.nbcolympics.com': {
        baseURL: 'https://stgapi-gracenote.nbcolympics.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//staging-widgets.nbcolympics.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.nbcolympics.com/configs/'
    },
    'widgets.nbcolympics.com': {
        baseURL: 'https://api-gracenote.nbcolympics.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//widgets.nbcolympics.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.nbcolympics.com/configs/'
    }
};

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(3);
var ES = __webpack_require__(4);

var implementation = __webpack_require__(11);
var getPolyfill = __webpack_require__(12);
var shim = __webpack_require__(29);

var slice = Array.prototype.slice;

var polyfill = getPolyfill();

var boundFindShim = function find(array, predicate) { // eslint-disable-line no-unused-vars
	ES.RequireObjectCoercible(array);
	var args = slice.call(arguments, 1);
	return polyfill.apply(array, args);
};

define(boundFindShim, {
	getPolyfill: getPolyfill,
	implementation: implementation,
	shim: shim
});

module.exports = boundFindShim;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// modified from https://github.com/es-shims/es5-shim
var has = Object.prototype.hasOwnProperty;
var toStr = Object.prototype.toString;
var slice = Array.prototype.slice;
var isArgs = __webpack_require__(17);
var isEnumerable = Object.prototype.propertyIsEnumerable;
var hasDontEnumBug = !isEnumerable.call({ toString: null }, 'toString');
var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
var dontEnums = [
	'toString',
	'toLocaleString',
	'valueOf',
	'hasOwnProperty',
	'isPrototypeOf',
	'propertyIsEnumerable',
	'constructor'
];
var equalsConstructorPrototype = function (o) {
	var ctor = o.constructor;
	return ctor && ctor.prototype === o;
};
var excludedKeys = {
	$console: true,
	$external: true,
	$frame: true,
	$frameElement: true,
	$frames: true,
	$innerHeight: true,
	$innerWidth: true,
	$outerHeight: true,
	$outerWidth: true,
	$pageXOffset: true,
	$pageYOffset: true,
	$parent: true,
	$scrollLeft: true,
	$scrollTop: true,
	$scrollX: true,
	$scrollY: true,
	$self: true,
	$webkitIndexedDB: true,
	$webkitStorageInfo: true,
	$window: true
};
var hasAutomationEqualityBug = (function () {
	/* global window */
	if (typeof window === 'undefined') { return false; }
	for (var k in window) {
		try {
			if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && typeof window[k] === 'object') {
				try {
					equalsConstructorPrototype(window[k]);
				} catch (e) {
					return true;
				}
			}
		} catch (e) {
			return true;
		}
	}
	return false;
}());
var equalsConstructorPrototypeIfNotBuggy = function (o) {
	/* global window */
	if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
		return equalsConstructorPrototype(o);
	}
	try {
		return equalsConstructorPrototype(o);
	} catch (e) {
		return false;
	}
};

var keysShim = function keys(object) {
	var isObject = object !== null && typeof object === 'object';
	var isFunction = toStr.call(object) === '[object Function]';
	var isArguments = isArgs(object);
	var isString = isObject && toStr.call(object) === '[object String]';
	var theKeys = [];

	if (!isObject && !isFunction && !isArguments) {
		throw new TypeError('Object.keys called on a non-object');
	}

	var skipProto = hasProtoEnumBug && isFunction;
	if (isString && object.length > 0 && !has.call(object, 0)) {
		for (var i = 0; i < object.length; ++i) {
			theKeys.push(String(i));
		}
	}

	if (isArguments && object.length > 0) {
		for (var j = 0; j < object.length; ++j) {
			theKeys.push(String(j));
		}
	} else {
		for (var name in object) {
			if (!(skipProto && name === 'prototype') && has.call(object, name)) {
				theKeys.push(String(name));
			}
		}
	}

	if (hasDontEnumBug) {
		var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);

		for (var k = 0; k < dontEnums.length; ++k) {
			if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
				theKeys.push(dontEnums[k]);
			}
		}
	}
	return theKeys;
};

keysShim.shim = function shimObjectKeys() {
	if (Object.keys) {
		var keysWorksWithArguments = (function () {
			// Safari 5.0 bug
			return (Object.keys(arguments) || '').length === 2;
		}(1, 2));
		if (!keysWorksWithArguments) {
			var originalKeys = Object.keys;
			Object.keys = function keys(object) {
				if (isArgs(object)) {
					return originalKeys(slice.call(object));
				} else {
					return originalKeys(object);
				}
			};
		}
	} else {
		Object.keys = keysShim;
	}
	return Object.keys || keysShim;
};

module.exports = keysShim;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;

module.exports = function isArguments(value) {
	var str = toStr.call(value);
	var isArgs = str === '[object Arguments]';
	if (!isArgs) {
		isArgs = str !== '[object Array]' &&
			value !== null &&
			typeof value === 'object' &&
			typeof value.length === 'number' &&
			value.length >= 0 &&
			toStr.call(value.callee) === '[object Function]';
	}
	return isArgs;
};


/***/ }),
/* 18 */
/***/ (function(module, exports) {


var hasOwn = Object.prototype.hasOwnProperty;
var toString = Object.prototype.toString;

module.exports = function forEach (obj, fn, ctx) {
    if (toString.call(fn) !== '[object Function]') {
        throw new TypeError('iterator must be a function');
    }
    var l = obj.length;
    if (l === +l) {
        for (var i = 0; i < l; i++) {
            fn.call(ctx, obj[i], i, obj);
        }
    } else {
        for (var k in obj) {
            if (hasOwn.call(obj, k)) {
                fn.call(ctx, obj[k], k, obj);
            }
        }
    }
};



/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var has = __webpack_require__(0);
var toPrimitive = __webpack_require__(21);

var toStr = Object.prototype.toString;
var hasSymbols = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol';

var $isNaN = __webpack_require__(7);
var $isFinite = __webpack_require__(8);
var MAX_SAFE_INTEGER = Number.MAX_SAFE_INTEGER || Math.pow(2, 53) - 1;

var assign = __webpack_require__(24);
var sign = __webpack_require__(9);
var mod = __webpack_require__(10);
var isPrimitive = __webpack_require__(25);
var parseInteger = parseInt;
var bind = __webpack_require__(5);
var arraySlice = bind.call(Function.call, Array.prototype.slice);
var strSlice = bind.call(Function.call, String.prototype.slice);
var isBinary = bind.call(Function.call, RegExp.prototype.test, /^0b[01]+$/i);
var isOctal = bind.call(Function.call, RegExp.prototype.test, /^0o[0-7]+$/i);
var regexExec = bind.call(Function.call, RegExp.prototype.exec);
var nonWS = ['\u0085', '\u200b', '\ufffe'].join('');
var nonWSregex = new RegExp('[' + nonWS + ']', 'g');
var hasNonWS = bind.call(Function.call, RegExp.prototype.test, nonWSregex);
var invalidHexLiteral = /^[-+]0x[0-9a-f]+$/i;
var isInvalidHexLiteral = bind.call(Function.call, RegExp.prototype.test, invalidHexLiteral);

// whitespace from: http://es5.github.io/#x15.5.4.20
// implementation from https://github.com/es-shims/es5-shim/blob/v3.4.0/es5-shim.js#L1304-L1324
var ws = [
	'\x09\x0A\x0B\x0C\x0D\x20\xA0\u1680\u180E\u2000\u2001\u2002\u2003',
	'\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028',
	'\u2029\uFEFF'
].join('');
var trimRegex = new RegExp('(^[' + ws + ']+)|([' + ws + ']+$)', 'g');
var replace = bind.call(Function.call, String.prototype.replace);
var trim = function (value) {
	return replace(value, trimRegex, '');
};

var ES5 = __webpack_require__(26);

var hasRegExpMatcher = __webpack_require__(28);

// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-abstract-operations
var ES6 = assign(assign({}, ES5), {

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-call-f-v-args
	Call: function Call(F, V) {
		var args = arguments.length > 2 ? arguments[2] : [];
		if (!this.IsCallable(F)) {
			throw new TypeError(F + ' is not a function');
		}
		return F.apply(V, args);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toprimitive
	ToPrimitive: toPrimitive,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toboolean
	// ToBoolean: ES5.ToBoolean,

	// http://www.ecma-international.org/ecma-262/6.0/#sec-tonumber
	ToNumber: function ToNumber(argument) {
		var value = isPrimitive(argument) ? argument : toPrimitive(argument, Number);
		if (typeof value === 'symbol') {
			throw new TypeError('Cannot convert a Symbol value to a number');
		}
		if (typeof value === 'string') {
			if (isBinary(value)) {
				return this.ToNumber(parseInteger(strSlice(value, 2), 2));
			} else if (isOctal(value)) {
				return this.ToNumber(parseInteger(strSlice(value, 2), 8));
			} else if (hasNonWS(value) || isInvalidHexLiteral(value)) {
				return NaN;
			} else {
				var trimmed = trim(value);
				if (trimmed !== value) {
					return this.ToNumber(trimmed);
				}
			}
		}
		return Number(value);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-tointeger
	// ToInteger: ES5.ToNumber,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toint32
	// ToInt32: ES5.ToInt32,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint32
	// ToUint32: ES5.ToUint32,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toint16
	ToInt16: function ToInt16(argument) {
		var int16bit = this.ToUint16(argument);
		return int16bit >= 0x8000 ? int16bit - 0x10000 : int16bit;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint16
	// ToUint16: ES5.ToUint16,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toint8
	ToInt8: function ToInt8(argument) {
		var int8bit = this.ToUint8(argument);
		return int8bit >= 0x80 ? int8bit - 0x100 : int8bit;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint8
	ToUint8: function ToUint8(argument) {
		var number = this.ToNumber(argument);
		if ($isNaN(number) || number === 0 || !$isFinite(number)) { return 0; }
		var posInt = sign(number) * Math.floor(Math.abs(number));
		return mod(posInt, 0x100);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint8clamp
	ToUint8Clamp: function ToUint8Clamp(argument) {
		var number = this.ToNumber(argument);
		if ($isNaN(number) || number <= 0) { return 0; }
		if (number >= 0xFF) { return 0xFF; }
		var f = Math.floor(argument);
		if (f + 0.5 < number) { return f + 1; }
		if (number < f + 0.5) { return f; }
		if (f % 2 !== 0) { return f + 1; }
		return f;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-tostring
	ToString: function ToString(argument) {
		if (typeof argument === 'symbol') {
			throw new TypeError('Cannot convert a Symbol value to a string');
		}
		return String(argument);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toobject
	ToObject: function ToObject(value) {
		this.RequireObjectCoercible(value);
		return Object(value);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-topropertykey
	ToPropertyKey: function ToPropertyKey(argument) {
		var key = this.ToPrimitive(argument, String);
		return typeof key === 'symbol' ? key : this.ToString(key);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
	ToLength: function ToLength(argument) {
		var len = this.ToInteger(argument);
		if (len <= 0) { return 0; } // includes converting -0 to +0
		if (len > MAX_SAFE_INTEGER) { return MAX_SAFE_INTEGER; }
		return len;
	},

	// http://www.ecma-international.org/ecma-262/6.0/#sec-canonicalnumericindexstring
	CanonicalNumericIndexString: function CanonicalNumericIndexString(argument) {
		if (toStr.call(argument) !== '[object String]') {
			throw new TypeError('must be a string');
		}
		if (argument === '-0') { return -0; }
		var n = this.ToNumber(argument);
		if (this.SameValue(this.ToString(n), argument)) { return n; }
		return void 0;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-requireobjectcoercible
	RequireObjectCoercible: ES5.CheckObjectCoercible,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isarray
	IsArray: Array.isArray || function IsArray(argument) {
		return toStr.call(argument) === '[object Array]';
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-iscallable
	// IsCallable: ES5.IsCallable,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isconstructor
	IsConstructor: function IsConstructor(argument) {
		return typeof argument === 'function' && !!argument.prototype; // unfortunately there's no way to truly check this without try/catch `new argument`
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isextensible-o
	IsExtensible: function IsExtensible(obj) {
		if (!Object.preventExtensions) { return true; }
		if (isPrimitive(obj)) {
			return false;
		}
		return Object.isExtensible(obj);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isinteger
	IsInteger: function IsInteger(argument) {
		if (typeof argument !== 'number' || $isNaN(argument) || !$isFinite(argument)) {
			return false;
		}
		var abs = Math.abs(argument);
		return Math.floor(abs) === abs;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-ispropertykey
	IsPropertyKey: function IsPropertyKey(argument) {
		return typeof argument === 'string' || typeof argument === 'symbol';
	},

	// http://www.ecma-international.org/ecma-262/6.0/#sec-isregexp
	IsRegExp: function IsRegExp(argument) {
		if (!argument || typeof argument !== 'object') {
			return false;
		}
		if (hasSymbols) {
			var isRegExp = argument[Symbol.match];
			if (typeof isRegExp !== 'undefined') {
				return ES5.ToBoolean(isRegExp);
			}
		}
		return hasRegExpMatcher(argument);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-samevalue
	// SameValue: ES5.SameValue,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-samevaluezero
	SameValueZero: function SameValueZero(x, y) {
		return (x === y) || ($isNaN(x) && $isNaN(y));
	},

	/**
	 * 7.3.2 GetV (V, P)
	 * 1. Assert: IsPropertyKey(P) is true.
	 * 2. Let O be ToObject(V).
	 * 3. ReturnIfAbrupt(O).
	 * 4. Return O.[[Get]](P, V).
	 */
	GetV: function GetV(V, P) {
		// 7.3.2.1
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}

		// 7.3.2.2-3
		var O = this.ToObject(V);

		// 7.3.2.4
		return O[P];
	},

	/**
	 * 7.3.9 - http://www.ecma-international.org/ecma-262/6.0/#sec-getmethod
	 * 1. Assert: IsPropertyKey(P) is true.
	 * 2. Let func be GetV(O, P).
	 * 3. ReturnIfAbrupt(func).
	 * 4. If func is either undefined or null, return undefined.
	 * 5. If IsCallable(func) is false, throw a TypeError exception.
	 * 6. Return func.
	 */
	GetMethod: function GetMethod(O, P) {
		// 7.3.9.1
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}

		// 7.3.9.2
		var func = this.GetV(O, P);

		// 7.3.9.4
		if (func == null) {
			return void 0;
		}

		// 7.3.9.5
		if (!this.IsCallable(func)) {
			throw new TypeError(P + 'is not a function');
		}

		// 7.3.9.6
		return func;
	},

	/**
	 * 7.3.1 Get (O, P) - http://www.ecma-international.org/ecma-262/6.0/#sec-get-o-p
	 * 1. Assert: Type(O) is Object.
	 * 2. Assert: IsPropertyKey(P) is true.
	 * 3. Return O.[[Get]](P, O).
	 */
	Get: function Get(O, P) {
		// 7.3.1.1
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		// 7.3.1.2
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}
		// 7.3.1.3
		return O[P];
	},

	Type: function Type(x) {
		if (typeof x === 'symbol') {
			return 'Symbol';
		}
		return ES5.Type(x);
	},

	// http://www.ecma-international.org/ecma-262/6.0/#sec-speciesconstructor
	SpeciesConstructor: function SpeciesConstructor(O, defaultConstructor) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		var C = O.constructor;
		if (typeof C === 'undefined') {
			return defaultConstructor;
		}
		if (this.Type(C) !== 'Object') {
			throw new TypeError('O.constructor is not an Object');
		}
		var S = hasSymbols && Symbol.species ? C[Symbol.species] : void 0;
		if (S == null) {
			return defaultConstructor;
		}
		if (this.IsConstructor(S)) {
			return S;
		}
		throw new TypeError('no constructor found');
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-completepropertydescriptor
	CompletePropertyDescriptor: function CompletePropertyDescriptor(Desc) {
		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (this.IsGenericDescriptor(Desc) || this.IsDataDescriptor(Desc)) {
			if (!has(Desc, '[[Value]]')) {
				Desc['[[Value]]'] = void 0;
			}
			if (!has(Desc, '[[Writable]]')) {
				Desc['[[Writable]]'] = false;
			}
		} else {
			if (!has(Desc, '[[Get]]')) {
				Desc['[[Get]]'] = void 0;
			}
			if (!has(Desc, '[[Set]]')) {
				Desc['[[Set]]'] = void 0;
			}
		}
		if (!has(Desc, '[[Enumerable]]')) {
			Desc['[[Enumerable]]'] = false;
		}
		if (!has(Desc, '[[Configurable]]')) {
			Desc['[[Configurable]]'] = false;
		}
		return Desc;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-set-o-p-v-throw
	Set: function Set(O, P, V, Throw) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('O must be an Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		if (this.Type(Throw) !== 'Boolean') {
			throw new TypeError('Throw must be a Boolean');
		}
		if (Throw) {
			O[P] = V;
			return true;
		} else {
			try {
				O[P] = V;
			} catch (e) {
				return false;
			}
		}
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-hasownproperty
	HasOwnProperty: function HasOwnProperty(O, P) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('O must be an Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		return has(O, P);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-hasproperty
	HasProperty: function HasProperty(O, P) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('O must be an Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		return P in O;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-isconcatspreadable
	IsConcatSpreadable: function IsConcatSpreadable(O) {
		if (this.Type(O) !== 'Object') {
			return false;
		}
		if (hasSymbols && typeof Symbol.isConcatSpreadable === 'symbol') {
			var spreadable = this.Get(O, Symbol.isConcatSpreadable);
			if (typeof spreadable !== 'undefined') {
				return this.ToBoolean(spreadable);
			}
		}
		return this.IsArray(O);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-invoke
	Invoke: function Invoke(O, P) {
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		var argumentsList = arraySlice(arguments, 2);
		var func = this.GetV(O, P);
		return this.Call(func, O, argumentsList);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-createiterresultobject
	CreateIterResultObject: function CreateIterResultObject(value, done) {
		if (this.Type(done) !== 'Boolean') {
			throw new TypeError('Assertion failed: Type(done) is not Boolean');
		}
		return {
			value: value,
			done: done
		};
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-regexpexec
	RegExpExec: function RegExpExec(R, S) {
		if (this.Type(R) !== 'Object') {
			throw new TypeError('R must be an Object');
		}
		if (this.Type(S) !== 'String') {
			throw new TypeError('S must be a String');
		}
		var exec = this.Get(R, 'exec');
		if (this.IsCallable(exec)) {
			var result = this.Call(exec, R, [S]);
			if (result === null || this.Type(result) === 'Object') {
				return result;
			}
			throw new TypeError('"exec" method must return `null` or an Object');
		}
		return regexExec(R, S);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-arrayspeciescreate
	ArraySpeciesCreate: function ArraySpeciesCreate(originalArray, length) {
		if (!this.IsInteger(length) || length < 0) {
			throw new TypeError('Assertion failed: length must be an integer >= 0');
		}
		var len = length === 0 ? 0 : length;
		var C;
		var isArray = this.IsArray(originalArray);
		if (isArray) {
			C = this.Get(originalArray, 'constructor');
			// TODO: figure out how to make a cross-realm normal Array, a same-realm Array
			// if (this.IsConstructor(C)) {
			// 	if C is another realm's Array, C = undefined
			// 	Object.getPrototypeOf(Object.getPrototypeOf(Object.getPrototypeOf(Array))) === null ?
			// }
			if (this.Type(C) === 'Object' && hasSymbols && Symbol.species) {
				C = this.Get(C, Symbol.species);
				if (C === null) {
					C = void 0;
				}
			}
		}
		if (typeof C === 'undefined') {
			return Array(len);
		}
		if (!this.IsConstructor(C)) {
			throw new TypeError('C must be a constructor');
		}
		return new C(len); // this.Construct(C, len);
	},

	CreateDataProperty: function CreateDataProperty(O, P, V) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}
		var oldDesc = Object.getOwnPropertyDescriptor(O, P);
		var extensible = oldDesc || (typeof Object.isExtensible !== 'function' || Object.isExtensible(O));
		var immutable = oldDesc && (!oldDesc.writable || !oldDesc.configurable);
		if (immutable || !extensible) {
			return false;
		}
		var newDesc = {
			configurable: true,
			enumerable: true,
			value: V,
			writable: true
		};
		Object.defineProperty(O, P, newDesc);
		return true;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-createdatapropertyorthrow
	CreateDataPropertyOrThrow: function CreateDataPropertyOrThrow(O, P, V) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}
		var success = this.CreateDataProperty(O, P, V);
		if (!success) {
			throw new TypeError('unable to create data property');
		}
		return success;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-advancestringindex
	AdvanceStringIndex: function AdvanceStringIndex(S, index, unicode) {
		if (this.Type(S) !== 'String') {
			throw new TypeError('Assertion failed: Type(S) is not String');
		}
		if (!this.IsInteger(index)) {
			throw new TypeError('Assertion failed: length must be an integer >= 0 and <= (2**53 - 1)');
		}
		if (index < 0 || index > MAX_SAFE_INTEGER) {
			throw new RangeError('Assertion failed: length must be an integer >= 0 and <= (2**53 - 1)');
		}
		if (this.Type(unicode) !== 'Boolean') {
			throw new TypeError('Assertion failed: Type(unicode) is not Boolean');
		}
		if (!unicode) {
			return index + 1;
		}
		var length = S.length;
		if ((index + 1) >= length) {
			return index + 1;
		}
		var first = S.charCodeAt(index);
		if (first < 0xD800 || first > 0xDBFF) {
			return index + 1;
		}
		var second = S.charCodeAt(index + 1);
		if (second < 0xDC00 || second > 0xDFFF) {
			return index + 1;
		}
		return index + 2;
	}
});

delete ES6.CheckObjectCoercible; // renamed in ES6 to RequireObjectCoercible

module.exports = ES6;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint no-invalid-this: 1 */

var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
var slice = Array.prototype.slice;
var toStr = Object.prototype.toString;
var funcType = '[object Function]';

module.exports = function bind(that) {
    var target = this;
    if (typeof target !== 'function' || toStr.call(target) !== funcType) {
        throw new TypeError(ERROR_MESSAGE + target);
    }
    var args = slice.call(arguments, 1);

    var bound;
    var binder = function () {
        if (this instanceof bound) {
            var result = target.apply(
                this,
                args.concat(slice.call(arguments))
            );
            if (Object(result) === result) {
                return result;
            }
            return this;
        } else {
            return target.apply(
                that,
                args.concat(slice.call(arguments))
            );
        }
    };

    var boundLength = Math.max(0, target.length - args.length);
    var boundArgs = [];
    for (var i = 0; i < boundLength; i++) {
        boundArgs.push('$' + i);
    }

    bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);

    if (target.prototype) {
        var Empty = function Empty() {};
        Empty.prototype = target.prototype;
        bound.prototype = new Empty();
        Empty.prototype = null;
    }

    return bound;
};


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var hasSymbols = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol';

var isPrimitive = __webpack_require__(6);
var isCallable = __webpack_require__(1);
var isDate = __webpack_require__(22);
var isSymbol = __webpack_require__(23);

var ordinaryToPrimitive = function OrdinaryToPrimitive(O, hint) {
	if (typeof O === 'undefined' || O === null) {
		throw new TypeError('Cannot call method on ' + O);
	}
	if (typeof hint !== 'string' || (hint !== 'number' && hint !== 'string')) {
		throw new TypeError('hint must be "string" or "number"');
	}
	var methodNames = hint === 'string' ? ['toString', 'valueOf'] : ['valueOf', 'toString'];
	var method, result, i;
	for (i = 0; i < methodNames.length; ++i) {
		method = O[methodNames[i]];
		if (isCallable(method)) {
			result = method.call(O);
			if (isPrimitive(result)) {
				return result;
			}
		}
	}
	throw new TypeError('No default value');
};

var GetMethod = function GetMethod(O, P) {
	var func = O[P];
	if (func !== null && typeof func !== 'undefined') {
		if (!isCallable(func)) {
			throw new TypeError(func + ' returned for property ' + P + ' of object ' + O + ' is not a function');
		}
		return func;
	}
};

// http://www.ecma-international.org/ecma-262/6.0/#sec-toprimitive
module.exports = function ToPrimitive(input, PreferredType) {
	if (isPrimitive(input)) {
		return input;
	}
	var hint = 'default';
	if (arguments.length > 1) {
		if (PreferredType === String) {
			hint = 'string';
		} else if (PreferredType === Number) {
			hint = 'number';
		}
	}

	var exoticToPrim;
	if (hasSymbols) {
		if (Symbol.toPrimitive) {
			exoticToPrim = GetMethod(input, Symbol.toPrimitive);
		} else if (isSymbol(input)) {
			exoticToPrim = Symbol.prototype.valueOf;
		}
	}
	if (typeof exoticToPrim !== 'undefined') {
		var result = exoticToPrim.call(input, hint);
		if (isPrimitive(result)) {
			return result;
		}
		throw new TypeError('unable to convert exotic object to primitive');
	}
	if (hint === 'default' && (isDate(input) || isSymbol(input))) {
		hint = 'string';
	}
	return ordinaryToPrimitive(input, hint === 'default' ? 'number' : hint);
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var getDay = Date.prototype.getDay;
var tryDateObject = function tryDateObject(value) {
	try {
		getDay.call(value);
		return true;
	} catch (e) {
		return false;
	}
};

var toStr = Object.prototype.toString;
var dateClass = '[object Date]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isDateObject(value) {
	if (typeof value !== 'object' || value === null) { return false; }
	return hasToStringTag ? tryDateObject(value) : toStr.call(value) === dateClass;
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;
var hasSymbols = typeof Symbol === 'function' && typeof Symbol() === 'symbol';

if (hasSymbols) {
	var symToStr = Symbol.prototype.toString;
	var symStringRegex = /^Symbol\(.*\)$/;
	var isSymbolObject = function isSymbolObject(value) {
		if (typeof value.valueOf() !== 'symbol') { return false; }
		return symStringRegex.test(symToStr.call(value));
	};
	module.exports = function isSymbol(value) {
		if (typeof value === 'symbol') { return true; }
		if (toStr.call(value) !== '[object Symbol]') { return false; }
		try {
			return isSymbolObject(value);
		} catch (e) {
			return false;
		}
	};
} else {
	module.exports = function isSymbol(value) {
		// this environment does not support Symbols.
		return false;
	};
}


/***/ }),
/* 24 */
/***/ (function(module, exports) {

var has = Object.prototype.hasOwnProperty;
module.exports = function assign(target, source) {
	if (Object.assign) {
		return Object.assign(target, source);
	}
	for (var key in source) {
		if (has.call(source, key)) {
			target[key] = source[key];
		}
	}
	return target;
};


/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = function isPrimitive(value) {
	return value === null || (typeof value !== 'function' && typeof value !== 'object');
};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $isNaN = __webpack_require__(7);
var $isFinite = __webpack_require__(8);

var sign = __webpack_require__(9);
var mod = __webpack_require__(10);

var IsCallable = __webpack_require__(1);
var toPrimitive = __webpack_require__(27);

var has = __webpack_require__(0);

// https://es5.github.io/#x9
var ES5 = {
	ToPrimitive: toPrimitive,

	ToBoolean: function ToBoolean(value) {
		return !!value;
	},
	ToNumber: function ToNumber(value) {
		return Number(value);
	},
	ToInteger: function ToInteger(value) {
		var number = this.ToNumber(value);
		if ($isNaN(number)) { return 0; }
		if (number === 0 || !$isFinite(number)) { return number; }
		return sign(number) * Math.floor(Math.abs(number));
	},
	ToInt32: function ToInt32(x) {
		return this.ToNumber(x) >> 0;
	},
	ToUint32: function ToUint32(x) {
		return this.ToNumber(x) >>> 0;
	},
	ToUint16: function ToUint16(value) {
		var number = this.ToNumber(value);
		if ($isNaN(number) || number === 0 || !$isFinite(number)) { return 0; }
		var posInt = sign(number) * Math.floor(Math.abs(number));
		return mod(posInt, 0x10000);
	},
	ToString: function ToString(value) {
		return String(value);
	},
	ToObject: function ToObject(value) {
		this.CheckObjectCoercible(value);
		return Object(value);
	},
	CheckObjectCoercible: function CheckObjectCoercible(value, optMessage) {
		/* jshint eqnull:true */
		if (value == null) {
			throw new TypeError(optMessage || 'Cannot call method on ' + value);
		}
		return value;
	},
	IsCallable: IsCallable,
	SameValue: function SameValue(x, y) {
		if (x === y) { // 0 === -0, but they are not identical.
			if (x === 0) { return 1 / x === 1 / y; }
			return true;
		}
		return $isNaN(x) && $isNaN(y);
	},

	// http://www.ecma-international.org/ecma-262/5.1/#sec-8
	Type: function Type(x) {
		if (x === null) {
			return 'Null';
		}
		if (typeof x === 'undefined') {
			return 'Undefined';
		}
		if (typeof x === 'function' || typeof x === 'object') {
			return 'Object';
		}
		if (typeof x === 'number') {
			return 'Number';
		}
		if (typeof x === 'boolean') {
			return 'Boolean';
		}
		if (typeof x === 'string') {
			return 'String';
		}
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-property-descriptor-specification-type
	IsPropertyDescriptor: function IsPropertyDescriptor(Desc) {
		if (this.Type(Desc) !== 'Object') {
			return false;
		}
		var allowed = {
			'[[Configurable]]': true,
			'[[Enumerable]]': true,
			'[[Get]]': true,
			'[[Set]]': true,
			'[[Value]]': true,
			'[[Writable]]': true
		};
		// jscs:disable
		for (var key in Desc) { // eslint-disable-line
			if (has(Desc, key) && !allowed[key]) {
				return false;
			}
		}
		// jscs:enable
		var isData = has(Desc, '[[Value]]');
		var IsAccessor = has(Desc, '[[Get]]') || has(Desc, '[[Set]]');
		if (isData && IsAccessor) {
			throw new TypeError('Property Descriptors may not be both accessor and data descriptors');
		}
		return true;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.1
	IsAccessorDescriptor: function IsAccessorDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return false;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (!has(Desc, '[[Get]]') && !has(Desc, '[[Set]]')) {
			return false;
		}

		return true;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.2
	IsDataDescriptor: function IsDataDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return false;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (!has(Desc, '[[Value]]') && !has(Desc, '[[Writable]]')) {
			return false;
		}

		return true;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.3
	IsGenericDescriptor: function IsGenericDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return false;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (!this.IsAccessorDescriptor(Desc) && !this.IsDataDescriptor(Desc)) {
			return true;
		}

		return false;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.4
	FromPropertyDescriptor: function FromPropertyDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return Desc;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (this.IsDataDescriptor(Desc)) {
			return {
				value: Desc['[[Value]]'],
				writable: !!Desc['[[Writable]]'],
				enumerable: !!Desc['[[Enumerable]]'],
				configurable: !!Desc['[[Configurable]]']
			};
		} else if (this.IsAccessorDescriptor(Desc)) {
			return {
				get: Desc['[[Get]]'],
				set: Desc['[[Set]]'],
				enumerable: !!Desc['[[Enumerable]]'],
				configurable: !!Desc['[[Configurable]]']
			};
		} else {
			throw new TypeError('FromPropertyDescriptor must be called with a fully populated Property Descriptor');
		}
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.5
	ToPropertyDescriptor: function ToPropertyDescriptor(Obj) {
		if (this.Type(Obj) !== 'Object') {
			throw new TypeError('ToPropertyDescriptor requires an object');
		}

		var desc = {};
		if (has(Obj, 'enumerable')) {
			desc['[[Enumerable]]'] = this.ToBoolean(Obj.enumerable);
		}
		if (has(Obj, 'configurable')) {
			desc['[[Configurable]]'] = this.ToBoolean(Obj.configurable);
		}
		if (has(Obj, 'value')) {
			desc['[[Value]]'] = Obj.value;
		}
		if (has(Obj, 'writable')) {
			desc['[[Writable]]'] = this.ToBoolean(Obj.writable);
		}
		if (has(Obj, 'get')) {
			var getter = Obj.get;
			if (typeof getter !== 'undefined' && !this.IsCallable(getter)) {
				throw new TypeError('getter must be a function');
			}
			desc['[[Get]]'] = getter;
		}
		if (has(Obj, 'set')) {
			var setter = Obj.set;
			if (typeof setter !== 'undefined' && !this.IsCallable(setter)) {
				throw new TypeError('setter must be a function');
			}
			desc['[[Set]]'] = setter;
		}

		if ((has(desc, '[[Get]]') || has(desc, '[[Set]]')) && (has(desc, '[[Value]]') || has(desc, '[[Writable]]'))) {
			throw new TypeError('Invalid property descriptor. Cannot both specify accessors and a value or writable attribute');
		}
		return desc;
	}
};

module.exports = ES5;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;

var isPrimitive = __webpack_require__(6);

var isCallable = __webpack_require__(1);

// https://es5.github.io/#x8.12
var ES5internalSlots = {
	'[[DefaultValue]]': function (O, hint) {
		var actualHint = hint || (toStr.call(O) === '[object Date]' ? String : Number);

		if (actualHint === String || actualHint === Number) {
			var methods = actualHint === String ? ['toString', 'valueOf'] : ['valueOf', 'toString'];
			var value, i;
			for (i = 0; i < methods.length; ++i) {
				if (isCallable(O[methods[i]])) {
					value = O[methods[i]]();
					if (isPrimitive(value)) {
						return value;
					}
				}
			}
			throw new TypeError('No default value');
		}
		throw new TypeError('invalid [[DefaultValue]] hint supplied');
	}
};

// https://es5.github.io/#x9
module.exports = function ToPrimitive(input, PreferredType) {
	if (isPrimitive(input)) {
		return input;
	}
	return ES5internalSlots['[[DefaultValue]]'](input, PreferredType);
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var has = __webpack_require__(0);
var regexExec = RegExp.prototype.exec;
var gOPD = Object.getOwnPropertyDescriptor;

var tryRegexExecCall = function tryRegexExec(value) {
	try {
		var lastIndex = value.lastIndex;
		value.lastIndex = 0;

		regexExec.call(value);
		return true;
	} catch (e) {
		return false;
	} finally {
		value.lastIndex = lastIndex;
	}
};
var toStr = Object.prototype.toString;
var regexClass = '[object RegExp]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isRegex(value) {
	if (!value || typeof value !== 'object') {
		return false;
	}
	if (!hasToStringTag) {
		return toStr.call(value) === regexClass;
	}

	var descriptor = gOPD(value, 'lastIndex');
	var hasLastIndexDataProperty = descriptor && has(descriptor, 'value');
	if (!hasLastIndexDataProperty) {
		return false;
	}

	return tryRegexExecCall(value);
};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(3);
var getPolyfill = __webpack_require__(12);

module.exports = function shimArrayPrototypeFind() {
	var polyfill = getPolyfill();

	define(Array.prototype, { find: polyfill }, {
		find: function () {
			return Array.prototype.find !== polyfill;
		}
	});

	return polyfill;
};


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _loaderUtils = __webpack_require__(13);

(function (global) {

    var ems = global.document.getElementsByTagName('script');
    var regexp = /.*gns\.widget\.loader\.([^/]+\.)?js/;
	var nEms = ems.length;

    var cloneAttributes = function cloneAttributes(element, attributes) {
        Object.keys(attributes).forEach(function (key) {
            element.setAttribute('data-' + key, attributes[key]);
        });
    };

    if (global.document.addEventListener) {
        var elements = [];
        var loaderDomain = '';
        var loaderURL = '';

        for (var index = 0; index < nEms; index += 1) {
            var element = ems[index];
            if (element.src.match(regexp) && elements.indexOf(element) < 0) {
                // Get loader url so we have one to load the versions from.
                loaderURL = (0, _loaderUtils.extractHostname)(element.src, true);

                elements.push(element);
            }
        }

        (0, _loaderUtils.loadVersions)(loaderURL, function (error, versions) {
            if (error !== null) {
                return;
			}
			

            var initializer = void 0;
            var versionPath = '';
            var arrayLength = elements.length;

            var _loop = function _loop(i) {

                var element = elements[i];
				var dataAttributes = (0, _loaderUtils.getScriptTagDataAttributes)(element.attributes);


                // Get loader domain and url per widget,
                // just in case they implement widgets loaded from different domains on one page
                loaderDomain = (0, _loaderUtils.extractHostname)(element.src);
                loaderURL = (0, _loaderUtils.extractHostname)(element.src, true);

                // eslint-disable-next-line no-loop-func
                (0, _loaderUtils.loadWidgetConfig)(loaderDomain, dataAttributes, function (err, config) {
                    if (err !== null) {
                        return;
                    }

                    if (versions && versions[config.version] !== undefined && versions[config.version] !== '') {
                        versionPath = '/' + versions[config.version];
                    }

                    initializer = document.createElement('script');
                    cloneAttributes(initializer, dataAttributes);
                    initializer.src = '' + loaderURL + versionPath + '/gns.widget.initializer.js';
                    if (element.parentNode) {
                        element.parentNode.insertBefore(initializer, element);
                        element.outerHTML = '';
                        delete elements[i];
                    }
                });
            };

            for (var i = arrayLength - 1; i >= 0; i -= 1) {
                _loop(i);
            }
		});
		
    } else {
        for (var _index = 0; _index < nEms; _index += 1) {
            var _element = ems[_index];

            if (_element.src.match(regexp)) {
                var paragraph = global.document.createElement('p');
                paragraph.id = 'browser-not-supported';
                paragraph.innerHTML = 'Unfortunately the browser you are currently using is not supported by our widgets. Please update your browser.';
                _element.parentNode.appendChild(paragraph);
            }
        }
    }
})(global);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYmRmYWNhZGYzYWNjZTZjNzdhOTMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9oYXMvc3JjL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvaXMtY2FsbGFibGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy93ZWJwYWNrL2J1aWxkaW4vZ2xvYmFsLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZGVmaW5lLXByb3BlcnRpZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9lczYuanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9mdW5jdGlvbi1iaW5kL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2hlbHBlcnMvaXNQcmltaXRpdmUuanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9oZWxwZXJzL2lzTmFOLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9pc0Zpbml0ZS5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvc2lnbi5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvbW9kLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvYXJyYXkucHJvdG90eXBlLmZpbmQvaW1wbGVtZW50YXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9hcnJheS5wcm90b3R5cGUuZmluZC9wb2x5ZmlsbC5qcyIsIndlYnBhY2s6Ly8vLi91dGlscy9sb2FkZXIvbG9hZGVyLXV0aWxzLmpzIiwid2VicGFjazovLy8uL2NvbmZpZ3MvZ2F0ZXdheS5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2FycmF5LnByb3RvdHlwZS5maW5kL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvb2JqZWN0LWtleXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9vYmplY3Qta2V5cy9pc0FyZ3VtZW50cy5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2ZvcmVhY2gvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9lczIwMTUuanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9mdW5jdGlvbi1iaW5kL2ltcGxlbWVudGF0aW9uLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2VzNi5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2lzLWRhdGUtb2JqZWN0L2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvaXMtc3ltYm9sL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9hc3NpZ24uanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9oZWxwZXJzL2lzUHJpbWl0aXZlLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvZXM1LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2VzNS5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2lzLXJlZ2V4L2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvYXJyYXkucHJvdG90eXBlLmZpbmQvc2hpbS5qcyIsIndlYnBhY2s6Ly8vLi9nbnMud2lkZ2V0LmxvYWRlci5qcyJdLCJuYW1lcyI6WyJleHRyYWN0SG9zdG5hbWUiLCJleHRyYWN0QWN0dWFsVmVyc2lvbiIsInJlcXVpcmUiLCJzaGltIiwibWVyZ2VPYmplY3QiLCJ0YXJnZXQiLCJpIiwiYXJndW1lbnRzIiwibGVuZ3RoIiwic291cmNlIiwia2V5IiwiaGFzT3duUHJvcGVydHkiLCJzZXRJRnJhbWVWYXJpYWJsZXMiLCJpRnJhbWUiLCJpRnJhbWVJZCIsImlkIiwibmFtZSIsInNyYyIsInRpdGxlIiwicm9sZSIsImZyYW1lQm9yZGVyIiwic2Nyb2xsaW5nIiwid2lkdGgiLCJoZWlnaHQiLCJmcmFtZUVsZW1lbnQiLCJzdHlsZSIsImNzc1RleHQiLCJ3cml0ZUlGcmFtZURvY3VtZW50IiwidXJsIiwiY29uZmlnIiwiZG9jIiwiZG9tIiwiY29udGVudFdpbmRvdyIsImRvY3VtZW50IiwiZXJyIiwiZG9tYWluIiwib3BlbiIsIl9sIiwianMiLCJjcmVhdGVFbGVtZW50IiwiYXN5bmMiLCJib2R5IiwiYXBwZW5kQ2hpbGQiLCJ3cml0ZSIsImJhc2VVUkwiLCJKU09OIiwic3RyaW5naWZ5Iiwic2hvdWxkTG9hZEluc3RhbmEiLCJzaG91bGRMb2FkQW5hbHl0aWNzIiwiYW5hbHl0aWNzIiwiY2xvc2UiLCJzd2l0Y2hJRnJhbWVDb250ZW50Iiwic2VsZWN0ZWRJRnJhbWUiLCJnZXRFbGVtZW50QnlJZCIsInBsYWNlSG9sZGVyIiwiZ2VuZXJhdGVJZCIsInBhcmVudE5vZGUiLCJpbnNlcnRCZWZvcmUiLCJyZW1vdmVDaGlsZCIsIm5ld0ZyYW1lIiwibG9hZElGcmFtZSIsImVsZW1lbnQiLCJzY3JpcHRzIiwiZ2V0RWxlbWVudHNCeVRhZ05hbWUiLCJmb3JFYWNoIiwiY2FsbCIsInNjcmlwdCIsImZlYXR1cmVTd2l0Y2hlcyIsImFsbCIsImluc3RhbmEiLCJhY3RpdmUiLCJtYWdpY051bWJlciIsIk1hdGgiLCJyYW5kb20iLCJwZXJjZW50YWdlX2FjdGl2ZSIsImdldFNjcmlwdFRhZ0RhdGFBdHRyaWJ1dGVzIiwiYXR0cmlidXRlcyIsImRhdGFBdHRyaWJ1dGVzIiwiYXR0cmlidXRlIiwidGVzdCIsInBhcmFtZXRlciIsInN1YnN0ciIsInZhbHVlIiwiZGF0YUhhbmRsZXIiLCJkYXRhIiwiY2FsbGJhY2siLCJjdXN0b21lcl9pZCIsImFsbG93X3RhZ3MiLCJhbGxvd19kYXRhX3RhZ3MiLCJ1bmRlZmluZWQiLCJ0YWdzIiwibWFwIiwiaXRlbSIsInRyaW0iLCJPYmplY3QiLCJrZXlzIiwiY29uZmlnVGFnIiwiZmluZCIsInRhZyIsImN1c3RvbV9jc3MiLCJ0eXBlIiwidmVyc2lvbiIsImxhbmd1YWdlIiwibGFuZ3VhZ2VfY29kZSIsInRoZW1lIiwibG9hZFdpZGdldENvbmZpZyIsImxvYWRlckRvbWFpbiIsIndpZGdldF9pZCIsImFudGlDYWNoZSIsImdsb2JhbCIsIl9fR05TX0FOVElfQ0FDSEUiLCJEYXRlIiwiZ2V0VGltZSIsImNvbmZpZ1VSTCIsImRlZmF1bHQiLCJjb25maWdGaWxlIiwieGhyIiwiWE1MSHR0cFJlcXVlc3QiLCJvbnJlYWR5c3RhdGVjaGFuZ2UiLCJ4aHJSZXNwb25zZSIsInJlYWR5U3RhdGUiLCJzdGF0dXMiLCJwYXJzZSIsInJlc3BvbnNlVGV4dCIsImVycm9yIiwicmVzcG9uc2VUeXBlIiwic2V0UmVxdWVzdEhlYWRlciIsInNlbmQiLCJpbml0aWFsaXplV2lkZ2V0IiwiZmluYWxDb25maWciLCJjcmVhdGVUZXh0Tm9kZSIsInNwb3J0IiwiYWN0dWFsVmVyc2lvbiIsInJlcGxhY2UiLCJsb2FkSWZyYW1lQ2hlY2tlciIsImxvYWRGZWF0dXJlU3dpdGNoZXMiLCJmZWF0dXJlU3dpdGNoZXJDb25maWciLCJ3aWRnZXRVcmwiLCJjbGllbnRfaWQiLCJzdHJpbmciLCJjaGFycyIsImNoYXJBdCIsImZsb29yIiwibWVzc2FnZUV2ZW50Q2FsbGJhY2siLCJldmVudCIsIm1lc3NhZ2UiLCJhY3Rpb24iLCJ0YXJnZXRJRnJhbWUiLCJwYXJlbnRab29tTGV2ZWwiLCJ3aW5kb3ciLCJnZXRDb21wdXRlZFN0eWxlIiwiem9vbSIsImNlaWwiLCJib3VuZHMiLCJzZXR0aW5ncyIsImxvYWRWZXJzaW9ucyIsImxvYWRlclVSTCIsInZlcnNpb25zIiwiY29uZmlnRmlsZVVSTCIsImtlZXBQcm90b2NvbCIsImhvc3RuYW1lIiwiaW5kZXhPZiIsInNwbGl0IiwicG9ydCIsImNsZWFuVXJsIiwiaGVhZGVycyIsIm1vY2tzIiwiZW1zIiwicmVnZXhwIiwibkVtcyIsImNsb25lQXR0cmlidXRlcyIsInNldEF0dHJpYnV0ZSIsImFkZEV2ZW50TGlzdGVuZXIiLCJlbGVtZW50cyIsImluZGV4IiwibWF0Y2giLCJwdXNoIiwiaW5pdGlhbGl6ZXIiLCJ2ZXJzaW9uUGF0aCIsImFycmF5TGVuZ3RoIiwib3V0ZXJIVE1MIiwicGFyYWdyYXBoIiwiaW5uZXJIVE1MIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7QUM3REE7O0FBRUE7Ozs7Ozs7O0FDRkE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0VBQW9FLEVBQUU7QUFDdEU7QUFDQSxFQUFFO0FBQ0YsZUFBZTtBQUNmO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDRCQUE0QixjQUFjO0FBQzFDO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxjQUFjLGNBQWM7QUFDNUIsZ0VBQWdFLGNBQWM7QUFDOUUsc0JBQXNCLGlDQUFpQztBQUN2RCwyQkFBMkIsY0FBYztBQUN6QztBQUNBO0FBQ0E7Ozs7Ozs7QUN0Q0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDRDQUE0Qzs7QUFFNUM7Ozs7Ozs7O0FDcEJBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLGdDQUFnQztBQUNuRTtBQUNBLDRCQUE0QixjQUFjO0FBQzFDO0FBQ0E7QUFDQSxFQUFFLFlBQVk7QUFDZDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7O0FBRUE7O0FBRUE7Ozs7Ozs7O0FDdkRBOztBQUVBOzs7Ozs7OztBQ0ZBOztBQUVBOztBQUVBOzs7Ozs7O0FDSkE7QUFDQTtBQUNBOzs7Ozs7O0FDRkE7QUFDQTtBQUNBOzs7Ozs7O0FDRkEsMkNBQTJDLGdCQUFnQjs7QUFFM0Qsa0RBQWtELGlGQUFpRjs7Ozs7OztBQ0ZuSTtBQUNBO0FBQ0E7Ozs7Ozs7QUNGQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNIQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixZQUFZO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3JCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7UUMrZGdCQSxlLEdBQUFBLGU7UUEyQkFDLG9CLEdBQUFBLG9COztBQXRnQmhCOzs7Ozs7QUFFQSxtQkFBQUMsQ0FBUSxFQUFSLEVBQWdDQyxJQUFoQzs7QUFFQSxTQUFTQyxXQUFULENBQXFCQyxNQUFyQixFQUE2QjtBQUN6QjtBQUNBLFNBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJQyxVQUFVQyxNQUE5QixFQUFzQ0YsR0FBdEMsRUFBMkM7QUFDdkM7QUFDQSxZQUFNRyxTQUFTRixVQUFVRCxDQUFWLENBQWY7QUFDQTtBQUNBLGFBQUssSUFBTUksR0FBWCxJQUFrQkQsTUFBbEIsRUFBMEI7QUFDdEI7QUFDQSxnQkFBSUEsT0FBT0UsY0FBUCxDQUFzQkQsR0FBdEIsQ0FBSixFQUFnQztBQUM1QkwsdUJBQU9LLEdBQVAsSUFBY0QsT0FBT0MsR0FBUCxDQUFkO0FBQ0g7QUFDSjtBQUNKO0FBQ0QsV0FBT0wsTUFBUDtBQUNIOztBQUVELFNBQVNPLGtCQUFULENBQTRCQyxNQUE1QixFQUFvQ0MsUUFBcEMsRUFBOEM7QUFDMUNELFdBQU9FLEVBQVAsR0FBWUQsUUFBWjtBQUNBRCxXQUFPRyxJQUFQLEdBQWNGLFFBQWQ7QUFDQUQsV0FBT0ksR0FBUCxHQUFhLGtCQUFiLENBSDBDLENBR1Q7QUFDakNKLFdBQU9LLEtBQVAsR0FBZSxFQUFmO0FBQ0FMLFdBQU9NLElBQVAsR0FBYyxjQUFkO0FBQ0FOLFdBQU9PLFdBQVAsR0FBcUIsR0FBckI7QUFDQVAsV0FBT1EsU0FBUCxHQUFtQixJQUFuQjtBQUNBUixXQUFPUyxLQUFQLEdBQWUsTUFBZjtBQUNBVCxXQUFPVSxNQUFQLEdBQWdCLE9BQWhCO0FBQ0EsS0FBQ1YsT0FBT1csWUFBUCxJQUF1QlgsTUFBeEIsRUFBZ0NZLEtBQWhDLENBQXNDQyxPQUF0QyxHQUFnRCwwQkFBaEQ7QUFDSDs7QUFFRCxTQUFTQyxtQkFBVCxDQUE2QmQsTUFBN0IsRUFBcUNlLEdBQXJDLEVBQTBDQyxNQUExQyxFQUFrRDtBQUM5QyxRQUFJQyxZQUFKO0FBQ0EsUUFBSUMsWUFBSjtBQUNBLFFBQUk7QUFDQUQsY0FBTWpCLE9BQU9tQixhQUFQLENBQXFCQyxRQUEzQjtBQUNILEtBRkQsQ0FFRSxPQUFPQyxHQUFQLEVBQVk7QUFDVkgsY0FBTUUsU0FBU0UsTUFBZjtBQUNBdEIsZUFBT0ksR0FBUCxtREFBMkRjLEdBQTNEO0FBQ0FELGNBQU1qQixPQUFPbUIsYUFBUCxDQUFxQkMsUUFBM0I7QUFDSDs7QUFFREgsUUFBSU0sSUFBSixHQUFXQyxFQUFYLEdBQWdCLFlBQVk7QUFBRTtBQUMxQixZQUFNQyxLQUFLLEtBQUtDLGFBQUwsQ0FBbUIsUUFBbkIsQ0FBWDtBQUNBLFlBQUlSLEdBQUosRUFBUyxLQUFLSSxNQUFMLEdBQWNKLEdBQWQ7QUFDVE8sV0FBR3ZCLEVBQUgsR0FBUSxxQkFBUjtBQUNBdUIsV0FBR3JCLEdBQUgsR0FBU1csR0FBVDtBQUNBVSxXQUFHRSxLQUFILEdBQVcsSUFBWDtBQUNBLGFBQUtDLElBQUwsQ0FBVUMsV0FBVixDQUFzQkosRUFBdEI7QUFDSCxLQVBEO0FBUUFSLFFBQUlhLEtBQUosQ0FBVSxjQUFWO0FBQ0FiLFFBQUlhLEtBQUosa0JBQXlCZCxPQUFPZSxPQUFoQztBQUNBZCxRQUFJYSxLQUFKLENBQVUsd0JBQVY7QUFDQTtBQUNBLFFBQUksSUFBSixFQUEyQztBQUN2Q2IsWUFBSWEsS0FBSixDQUFVLHlGQUFWO0FBQ0g7QUFDRGIsUUFBSWEsS0FBSixDQUFVLGdFQUFWO0FBQ0FiLFFBQUlhLEtBQUosQ0FBVSxzRUFBVjtBQUNBYixRQUFJYSxLQUFKLENBQVUsMERBQVY7QUFDQWIsUUFBSWEsS0FBSixDQUFVLDBEQUFWO0FBQ0FiLFFBQUlhLEtBQUosQ0FBVSxTQUFWO0FBQ0FiLFFBQUlhLEtBQUosQ0FBVSxnQ0FBVjtBQUNBYixRQUFJYSxLQUFKLGdDQUF1Q0UsS0FBS0MsU0FBTCxDQUFlakIsTUFBZixDQUF2QztBQUNBLFFBQUlrQixrQkFBa0JsQixNQUFsQixDQUFKLEVBQStCO0FBQzNCQyxZQUFJYSxLQUFKLENBQVUsdVhBQVY7QUFDSDtBQUNELFFBQUlLLG9CQUFvQm5CLE1BQXBCLENBQUosRUFBaUM7QUFDN0JDLFlBQUlhLEtBQUosQ0FBVSw4UEFBVjtBQUNIO0FBQ0QsUUFBSWQsT0FBT29CLFNBQVAsSUFBb0JwQixPQUFPb0IsU0FBUCxLQUFxQixFQUE3QyxFQUFpRDtBQUM3Q25CLFlBQUlhLEtBQUoscUVBQTRFZCxPQUFPb0IsU0FBbkYsNEpBQThPcEIsT0FBT29CLFNBQXJQO0FBQ0g7QUFDRG5CLFFBQUlhLEtBQUosQ0FBVSx1RUFBVjtBQUNBYixRQUFJYSxLQUFKLENBQVUsZUFBVjtBQUNBYixRQUFJb0IsS0FBSjtBQUNIOztBQUVELFNBQVNDLG1CQUFULENBQTZCdkIsR0FBN0IsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ3RDLFFBQU11QixpQkFBaUJuQixTQUFTb0IsY0FBVCxDQUF3QnhCLE9BQU9mLFFBQS9CLENBQXZCO0FBQ0EsUUFBTXdDLGNBQWNyQixTQUFTTSxhQUFULENBQXVCLEtBQXZCLENBQXBCO0FBQ0FlLGdCQUFZdkMsRUFBWixHQUFpQndDLFdBQVcsRUFBWCxDQUFqQjtBQUNBSCxtQkFBZUksVUFBZixDQUEwQkMsWUFBMUIsQ0FBdUNILFdBQXZDLEVBQW9ERixjQUFwRDtBQUNBQSxtQkFBZUksVUFBZixDQUEwQkUsV0FBMUIsQ0FBc0NOLGNBQXRDOztBQUVBLFFBQU1PLFdBQVcxQixTQUFTTSxhQUFULENBQXVCLFFBQXZCLENBQWpCOztBQUVBM0IsdUJBQW1CK0MsUUFBbkIsRUFBNkI5QixPQUFPZixRQUFwQzs7QUFFQXdDLGdCQUFZRSxVQUFaLENBQXVCQyxZQUF2QixDQUFvQ0UsUUFBcEMsRUFBOENMLFdBQTlDO0FBQ0FBLGdCQUFZRSxVQUFaLENBQXVCRSxXQUF2QixDQUFtQ0osV0FBbkM7O0FBRUEzQix3QkFBb0JnQyxRQUFwQixFQUE4Qi9CLEdBQTlCLEVBQW1DQyxNQUFuQztBQUNIOztBQUVEOzs7Ozs7Ozs7O0FBVU8sSUFBTStCLGtDQUFhLFNBQWJBLFVBQWEsQ0FBQ0MsT0FBRCxFQUFVakMsR0FBVixFQUFlQyxNQUFmLEVBQTBCO0FBQ2hELFFBQU1oQixTQUFTb0IsU0FBU00sYUFBVCxDQUF1QixRQUF2QixDQUFmO0FBQ0FWLFdBQU9mLFFBQVAsR0FBa0J5QyxXQUFXLENBQVgsQ0FBbEI7O0FBRUEzQyx1QkFBbUJDLE1BQW5CLEVBQTJCZ0IsT0FBT2YsUUFBbEM7O0FBRUEsUUFBTWdELFVBQVU3QixTQUFTOEIsb0JBQVQsQ0FBOEIsUUFBOUIsQ0FBaEI7QUFDQSxPQUFHQyxPQUFILENBQVdDLElBQVgsQ0FBZ0JILE9BQWhCLEVBQXlCLFVBQUNJLE1BQUQsRUFBWTtBQUNqQyxZQUFJQSxXQUFXTCxPQUFmLEVBQXdCO0FBQ3BCSyxtQkFBT1YsVUFBUCxDQUFrQkMsWUFBbEIsQ0FBK0I1QyxNQUEvQixFQUF1Q3FELE1BQXZDO0FBQ0F2QyxnQ0FBb0JkLE1BQXBCLEVBQTRCZSxHQUE1QixFQUFpQ0MsTUFBakM7QUFDSDtBQUNKLEtBTEQ7QUFNSCxDQWJNOztBQWVBLElBQU1rQixnREFBb0IsU0FBcEJBLGlCQUFvQixDQUFDbEIsTUFBRCxFQUFZO0FBQ3pDLFFBQ0UsQ0FBQ0EsTUFBRCxJQUNBLENBQUNBLE9BQU9zQyxlQURSLElBRUEsQ0FBQ3RDLE9BQU9zQyxlQUFQLENBQXVCQyxHQUZ4QixJQUdBLENBQUN2QyxPQUFPc0MsZUFBUCxDQUF1QkMsR0FBdkIsQ0FBMkJDLE9BSDVCLElBSUEsQ0FBQ3hDLE9BQU9zQyxlQUFQLENBQXVCQyxHQUF2QixDQUEyQkMsT0FBM0IsQ0FBbUNDLE1BTHRDLEVBTUU7QUFDRSxlQUFPLEtBQVA7QUFDSDs7QUFFRCxRQUFNQyxjQUFjQyxLQUFLQyxNQUFMLEtBQWdCLEdBQXBDOztBQUVBLFdBQVE1QyxPQUFPc0MsZUFBUCxDQUF1QkMsR0FBdkIsQ0FBMkJDLE9BQTNCLENBQW1DSyxpQkFBbkMsSUFBd0RILFdBQWhFO0FBQ0gsQ0FkTTs7QUFnQkEsSUFBTXZCLG9EQUFzQixTQUF0QkEsbUJBQXNCLENBQUNuQixNQUFELEVBQVk7QUFDM0MsUUFDRSxDQUFDQSxNQUFELElBQ0EsQ0FBQ0EsT0FBT3NDLGVBRFIsSUFFQSxDQUFDdEMsT0FBT3NDLGVBQVAsQ0FBdUJDLEdBRnhCLElBR0EsQ0FBQ3ZDLE9BQU9zQyxlQUFQLENBQXVCQyxHQUF2QixDQUEyQm5CLFNBSDVCLElBSUEsQ0FBQ3BCLE9BQU9zQyxlQUFQLENBQXVCQyxHQUF2QixDQUEyQm5CLFNBQTNCLENBQXFDcUIsTUFMeEMsRUFNRTtBQUNFLGVBQU8sS0FBUDtBQUNIOztBQUVELFFBQU1DLGNBQWNDLEtBQUtDLE1BQUwsS0FBZ0IsR0FBcEM7O0FBRUEsV0FBUTVDLE9BQU9zQyxlQUFQLENBQXVCQyxHQUF2QixDQUEyQm5CLFNBQTNCLENBQXFDeUIsaUJBQXJDLElBQTBESCxXQUFsRTtBQUNILENBZE07O0FBZ0JQOzs7Ozs7O0FBT08sSUFBTUksa0VBQTZCLFNBQTdCQSwwQkFBNkIsQ0FBQ0MsVUFBRCxFQUFnQjtBQUN0RCxRQUFNQyxpQkFBaUIsRUFBdkI7O0FBRUEsT0FBR2IsT0FBSCxDQUFXQyxJQUFYLENBQWdCVyxVQUFoQixFQUE0QixVQUFDRSxTQUFELEVBQWU7QUFDdkMsWUFBSSxTQUFTQyxJQUFULENBQWNELFVBQVU5RCxJQUF4QixDQUFKLEVBQW1DO0FBQy9CLGdCQUFNZ0UsWUFBWUYsVUFBVTlELElBQVYsQ0FBZWlFLE1BQWYsQ0FBc0IsQ0FBdEIsQ0FBbEI7QUFDQUosMkJBQWVHLFNBQWYsSUFBNEJGLFVBQVVJLEtBQXRDO0FBQ0g7QUFDSixLQUxEOztBQU9BLFdBQU9MLGNBQVA7QUFDSCxDQVhNOztBQWFQLFNBQVNNLFdBQVQsQ0FBcUJDLElBQXJCLEVBQTJCdkQsTUFBM0IsRUFBbUN3RCxRQUFuQyxFQUE2QztBQUN6Q3hELFdBQU95RCxXQUFQLEdBQXFCRixLQUFLRSxXQUExQjs7QUFFQSxRQUFJRixLQUFLRyxVQUFMLEtBQW9CLEtBQXhCLEVBQStCO0FBQzNCLFlBQUlILEtBQUtJLGVBQUwsS0FBeUJDLFNBQTdCLEVBQXdDO0FBQ3BDLGdCQUFNQyxPQUFPTixLQUFLSSxlQUFMLENBQXFCRyxHQUFyQixDQUF5QixVQUFDQyxJQUFEO0FBQUEsdUJBQVVBLEtBQUtDLElBQUwsRUFBVjtBQUFBLGFBQXpCLENBQWI7O0FBRUE7QUFDQUMsbUJBQU9DLElBQVAsQ0FBWWxFLE1BQVosRUFBb0JtQyxPQUFwQixDQUE0QixVQUFDZ0MsU0FBRCxFQUFlO0FBQ3ZDLG9CQUFJTixLQUFLTyxJQUFMLENBQVUsVUFBQ0MsR0FBRDtBQUFBLDJCQUFTQSxRQUFRRixTQUFqQjtBQUFBLGlCQUFWLENBQUosRUFBMkM7QUFDdkM7QUFDQTtBQUNBWix5QkFBS1ksU0FBTCxJQUFrQm5FLE9BQU9tRSxTQUFQLENBQWxCO0FBQ0g7O0FBRUQsb0JBQUlaLEtBQUtZLFNBQUwsTUFBb0JQLFNBQXhCLEVBQW1DO0FBQy9CO0FBQ0FMLHlCQUFLWSxTQUFMLElBQWtCbkUsT0FBT21FLFNBQVAsQ0FBbEI7QUFDSDtBQUNKLGFBWEQ7QUFZSCxTQWhCRCxNQWdCTztBQUNIWixtQkFBT2hGLFlBQVksRUFBWixFQUFnQmdGLElBQWhCLEVBQXNCdkQsTUFBdEIsQ0FBUDtBQUNIO0FBQ0osS0FwQkQsTUFvQk87QUFDSDtBQUNBaUUsZUFBT0MsSUFBUCxDQUFZbEUsTUFBWixFQUFvQm1DLE9BQXBCLENBQTRCLFVBQUNnQyxTQUFELEVBQWU7QUFDdkMsZ0JBQUlaLEtBQUtZLFNBQUwsTUFBb0JQLFNBQXhCLEVBQW1DO0FBQy9CO0FBQ0FMLHFCQUFLWSxTQUFMLElBQWtCbkUsT0FBT21FLFNBQVAsQ0FBbEI7QUFDSDtBQUNKLFNBTEQ7QUFNSDs7QUFFRDtBQUNBLFFBQUluRSxPQUFPc0UsVUFBUCxLQUFzQlYsU0FBdEIsSUFBbUM1RCxPQUFPc0UsVUFBUCxLQUFzQixFQUE3RCxFQUFpRTtBQUM3RGYsYUFBS2UsVUFBTCxHQUFrQnRFLE9BQU9zRSxVQUF6QjtBQUNIOztBQUVEO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFFBQUlmLEtBQUtnQixJQUFMLEtBQWMsU0FBbEIsRUFBNkI7QUFDekJoQixhQUFLaUIsT0FBTCxHQUFlLE1BQWY7QUFDQWpCLGFBQUtrQixRQUFMLEdBQWdCLE9BQWhCO0FBQ0FsQixhQUFLbUIsYUFBTCxHQUFxQixHQUFyQjtBQUNBbkIsYUFBS29CLEtBQUwsR0FBYSxTQUFiO0FBQ0g7O0FBRURuQixhQUFTLElBQVQsRUFBZUQsSUFBZjtBQUNIOztBQUVEOzs7Ozs7O0FBT08sSUFBTXFCLDhDQUFtQixTQUFuQkEsZ0JBQW1CLENBQUNDLFlBQUQsRUFBZTdFLE1BQWYsRUFBdUJ3RCxRQUF2QixFQUFvQztBQUNoRSxRQUFJeEQsV0FBVzRELFNBQVgsSUFBd0I1RCxPQUFPOEUsU0FBUCxLQUFxQmxCLFNBQWpELEVBQTREO0FBQ3hESixpQkFBUyxxQ0FBVCxFQUFnRCxJQUFoRDtBQUNILEtBRkQsTUFFTztBQUNILFlBQU11QixZQUFZQyxPQUFPQyxnQkFBUCxJQUEyQixJQUFJQyxJQUFKLEdBQVdDLE9BQVgsRUFBN0M7QUFDQUgsZUFBT0MsZ0JBQVAsR0FBMEJGLFNBQTFCOztBQUVBO0FBQ0EsWUFBSUssWUFBWSxrQkFBUUMsT0FBUixDQUFnQkQsU0FBaEM7QUFDQSxZQUFJUCxnQkFBZ0Isa0JBQVFBLFlBQVIsQ0FBcEIsRUFBMkM7QUFDdkNPLHdCQUFZLGtCQUFRUCxZQUFSLEVBQXNCTyxTQUFsQztBQUNIO0FBQ0QsWUFBTUUsa0JBQWdCRixTQUFoQixHQUE0QnBGLE9BQU84RSxTQUFuQyxnQkFBdURDLFNBQTdEOztBQUVBLFlBQUl4QixPQUFPLEVBQVg7O0FBRUEsWUFBTWdDLE1BQU0sSUFBSUMsY0FBSixFQUFaO0FBQ0FELFlBQUlFLGtCQUFKLEdBQXlCLFNBQVNDLFdBQVQsR0FBdUI7QUFDNUMsZ0JBQUlILElBQUlJLFVBQUosS0FBbUIsQ0FBdkIsRUFBMEI7QUFDdEIsb0JBQUlKLElBQUlLLE1BQUosS0FBZSxHQUFuQixFQUF3QjtBQUNwQix3QkFBSTtBQUNBckMsK0JBQU92QyxLQUFLNkUsS0FBTCxDQUFXTixJQUFJTyxZQUFmLENBQVA7QUFDSCxxQkFGRCxDQUVFLE9BQU9DLEtBQVAsRUFBYztBQUNadkMsOEZBQW9FeEQsT0FBTzhFLFNBQTNFLEVBQXdGLElBQXhGO0FBQ0E7QUFDSDtBQUNKLGlCQVBELE1BT087QUFDSHRCLDBGQUFvRXhELE9BQU84RSxTQUEzRSxFQUF3RixJQUF4RjtBQUNBO0FBQ0g7QUFDRHhCLDRCQUFZQyxJQUFaLEVBQWtCdkQsTUFBbEIsRUFBMEJ3RCxRQUExQjtBQUNIO0FBQ0osU0FmRDtBQWdCQStCLFlBQUloRixJQUFKLENBQVMsS0FBVCxFQUFnQitFLFVBQWhCO0FBQ0FDLFlBQUlTLFlBQUosR0FBbUIsTUFBbkI7QUFDQVQsWUFBSVUsZ0JBQUosQ0FBcUIsUUFBckIsRUFBK0IsaUNBQS9CO0FBQ0FWLFlBQUlXLElBQUo7QUFDSDtBQUNKLENBdENNOztBQXdDUDtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTU8sSUFBTUMsOENBQW1CLFNBQW5CQSxnQkFBbUIsQ0FBQ25FLE9BQUQsRUFBVWUsVUFBVixFQUF5QjtBQUNyRDtBQUNBLFFBQU1DLGlCQUFpQkYsMkJBQTJCQyxVQUEzQixDQUF2QjtBQUNBLFFBQU04QixlQUFlMUcsZ0JBQWdCNkQsUUFBUTVDLEdBQXhCLENBQXJCOztBQUVBLFFBQUlnSCxjQUFjLEVBQWxCOztBQUVBO0FBQ0F4QixxQkFBaUJDLFlBQWpCLEVBQStCN0IsY0FBL0IsRUFBK0MsVUFBQytDLEtBQUQsRUFBUS9GLE1BQVIsRUFBbUI7QUFDOUQsWUFBSStGLFVBQVUsSUFBZCxFQUFvQjtBQUNoQi9ELG9CQUFRTCxVQUFSLENBQW1CQyxZQUFuQixDQUFnQ3hCLFNBQVNpRyxjQUFULENBQXdCTixLQUF4QixDQUFoQyxFQUFnRS9ELE9BQWhFO0FBQ0E7QUFDSDs7QUFFRG9FLHNCQUFjN0gsWUFBWSxFQUFaLEVBQWdCNkgsV0FBaEIsRUFBNkJwRyxNQUE3QixDQUFkOztBQUVBO0FBQ0EsWUFBSUEsT0FBT3NHLEtBQVAsS0FBaUIxQyxTQUFqQixJQUE4QjVELE9BQU91RSxJQUFQLEtBQWdCWCxTQUFsRCxFQUE2RDtBQUN6RCxnQkFBSTVELE9BQU8yRSxLQUFQLEtBQWlCZixTQUFyQixFQUFnQztBQUM1QndDLDRCQUFZekIsS0FBWixHQUFvQixTQUFwQjtBQUNIOztBQUVEeUIsd0JBQVl2QixZQUFaLEdBQTJCMUcsZ0JBQWdCNkQsUUFBUTVDLEdBQXhCLENBQTNCO0FBQ0FnSCx3QkFBWUcsYUFBWixHQUE0Qm5JLHFCQUFxQjRELFFBQVE1QyxHQUE3QixDQUE1QjtBQUNBZ0gsd0JBQVlyRixPQUFaLEdBQXNCaUIsUUFBUTVDLEdBQVIsQ0FBWW9ILE9BQVosQ0FBb0IsMkJBQXBCLEVBQWlELEVBQWpELENBQXRCOztBQUVBQyw4QkFBa0J6RSxPQUFsQixFQUEyQm9FLFdBQTNCO0FBQ0g7QUFDSixLQXBCRDs7QUFzQkFNLHdCQUFvQnZJLGdCQUFnQjZELFFBQVE1QyxHQUF4QixFQUE2QixJQUE3QixDQUFwQixFQUF3RCxVQUFDaUIsR0FBRCxFQUFNc0cscUJBQU4sRUFBZ0M7QUFDcEYsWUFBSXRHLFFBQVEsSUFBWixFQUFrQjtBQUNkMkIsb0JBQVFMLFVBQVIsQ0FBbUJDLFlBQW5CLENBQWdDeEIsU0FBU2lHLGNBQVQsQ0FBd0JoRyxHQUF4QixDQUFoQyxFQUE4RDJCLE9BQTlEO0FBQ0g7O0FBRURvRSxvQkFBWTlELGVBQVosR0FBOEJxRSxxQkFBOUI7QUFDQUYsMEJBQWtCekUsT0FBbEIsRUFBMkJvRSxXQUEzQjtBQUNILEtBUEQ7QUFRSCxDQXRDTTs7QUF3Q0EsSUFBTUssZ0RBQW9CLFNBQXBCQSxpQkFBb0IsQ0FBQ3pFLE9BQUQsRUFBVWhDLE1BQVYsRUFBcUI7QUFDbEQsUUFBSUEsT0FBT3NDLGVBQVAsSUFBMEJ0QyxPQUFPOEUsU0FBckMsRUFBZ0Q7QUFDNUMsWUFBSThCLHlCQUF1QjVHLE9BQU8yRSxLQUE5QixTQUF1QzNFLE9BQU9zRyxLQUE5QyxTQUF1RHRHLE9BQU91RSxJQUE5RCxvQkFBSjs7QUFFQSxZQUFJdkUsT0FBTzZHLFNBQVAsS0FBcUJqRCxTQUF6QixFQUFvQztBQUNoQ2dELHdCQUFlQSxTQUFmLFdBQThCNUcsT0FBTzZHLFNBQXJDO0FBQ0g7QUFDRDlFLG1CQUFXQyxPQUFYLEVBQW9CNEUsU0FBcEIsRUFBK0I1RyxNQUEvQjtBQUNIO0FBQ0osQ0FUTTs7QUFXUDs7Ozs7O0FBTU8sSUFBTTBCLGtDQUFhLFNBQWJBLFVBQWEsQ0FBQy9DLE1BQUQsRUFBWTtBQUNsQyxRQUFJbUksU0FBUyxFQUFiO0FBQ0EsUUFBTUMsUUFBUSxnRUFBZDs7QUFFQSxTQUFLLElBQUl0SSxJQUFJLENBQWIsRUFBZ0JBLElBQUlFLE1BQXBCLEVBQTRCRixLQUFLLENBQWpDLEVBQW9DO0FBQ2hDcUksa0JBQVVDLE1BQU1DLE1BQU4sQ0FBYXJFLEtBQUtzRSxLQUFMLENBQVd0RSxLQUFLQyxNQUFMLEtBQWdCbUUsTUFBTXBJLE1BQWpDLENBQWIsQ0FBVjtBQUNIO0FBQ0QsV0FBT21JLE1BQVA7QUFDSCxDQVJNOztBQVVQOzs7OztBQUtPLElBQU1JLHNEQUF1QixTQUF2QkEsb0JBQXVCLENBQUNDLEtBQUQsRUFBVztBQUMzQyxRQUFJQyxnQkFBSjtBQUNBLFFBQUk7QUFDQUEsa0JBQVVwRyxLQUFLNkUsS0FBTCxDQUFXc0IsTUFBTTVELElBQWpCLENBQVY7QUFDSCxLQUZELENBRUUsT0FBT3dDLEtBQVAsRUFBYztBQUNacUIsa0JBQVUsSUFBVjtBQUNIOztBQUVELFFBQUlBLE9BQUosRUFBYTtBQUNULFlBQUlBLFFBQVFDLE1BQVosRUFBb0I7QUFDaEIsb0JBQVFELFFBQVFDLE1BQWhCO0FBQ0kscUJBQUssa0JBQUw7QUFDSSx3QkFBSUQsUUFBUW5JLFFBQVIsS0FBcUIyRSxTQUF6QixFQUFvQztBQUNoQyw0QkFBTTBELGVBQWVsSCxTQUFTb0IsY0FBVCxDQUF3QjRGLFFBQVFuSSxRQUFoQyxDQUFyQjtBQUNBLDRCQUFJcUksWUFBSixFQUFrQjtBQUNkLGdDQUFNQyxrQkFBa0JDLE9BQU9DLGdCQUFQLENBQXdCckgsU0FBUzhCLG9CQUFULENBQThCLE1BQTlCLEVBQXNDLENBQXRDLENBQXhCLEVBQWtFd0YsSUFBMUY7QUFDQSxnQ0FBSUgsa0JBQWtCLENBQXRCLEVBQXlCO0FBQ3JCRCw2Q0FBYTVILE1BQWIsR0FBc0JpRCxLQUFLZ0YsSUFBTCxDQUFVUCxRQUFRUSxNQUFSLENBQWVsSSxNQUFmLEdBQXdCNkgsZUFBbEMsQ0FBdEI7QUFDSCw2QkFGRCxNQUVPO0FBQ0hELDZDQUFhNUgsTUFBYixHQUFzQjBILFFBQVFRLE1BQVIsQ0FBZWxJLE1BQXJDO0FBQ0g7QUFDSjtBQUNKO0FBQ0Q7QUFDSixxQkFBSyxvQkFBTDtBQUNJLHdCQUFJMEgsUUFBUVMsUUFBUixDQUFpQjVJLFFBQWpCLEtBQThCMkUsU0FBbEMsRUFBNkM7QUFDekMsNEJBQU0wRCxnQkFBZWxILFNBQVNvQixjQUFULENBQXdCNEYsUUFBUVMsUUFBUixDQUFpQjVJLFFBQXpDLENBQXJCO0FBQ0EsNEJBQUlxSSxhQUFKLEVBQWtCO0FBQ2QsZ0NBQUlWLHlCQUF1QlEsUUFBUVMsUUFBUixDQUFpQmxELEtBQXhDLFNBQWlEeUMsUUFBUVMsUUFBUixDQUFpQnZCLEtBQWxFLFNBQTJFYyxRQUFRUyxRQUFSLENBQWlCdEQsSUFBNUYsb0JBQUo7O0FBRUEsZ0NBQUk2QyxRQUFRUyxRQUFSLENBQWlCaEIsU0FBakIsS0FBK0JqRCxTQUFuQyxFQUE4QztBQUMxQ2dELDRDQUFlQSxTQUFmLFdBQThCUSxRQUFRUyxRQUFSLENBQWlCaEIsU0FBL0M7QUFDSDs7QUFFRHZGLGdEQUFvQnNGLFNBQXBCLEVBQStCUSxRQUFRUyxRQUF2QztBQUNIO0FBQ0o7QUFDRDtBQUNKO0FBNUJKO0FBOEJIO0FBQ0o7QUFDSixDQTFDTTs7QUE0Q1A7Ozs7OztBQU1PLElBQU1DLHNDQUFlLFNBQWZBLFlBQWUsQ0FBQ0MsU0FBRCxFQUFZdkUsUUFBWixFQUF5QjtBQUNqRCxRQUFJd0UsV0FBVyxFQUFmO0FBQ0EsUUFBTWpELFlBQVlDLE9BQU9DLGdCQUFQLElBQTJCLElBQUlDLElBQUosR0FBV0MsT0FBWCxFQUE3QztBQUNBSCxXQUFPQyxnQkFBUCxHQUEwQkYsU0FBMUI7QUFDQSxRQUFNTyxhQUFnQnlDLFNBQWhCLHlCQUE2Q2hELFNBQW5EOztBQUVBLFFBQU1RLE1BQU0sSUFBSUMsY0FBSixFQUFaO0FBQ0FELFFBQUlFLGtCQUFKLEdBQXlCLFNBQVNDLFdBQVQsR0FBdUI7QUFDNUMsWUFBSUgsSUFBSUksVUFBSixLQUFtQixDQUF2QixFQUEwQjtBQUN0QixnQkFBSUosSUFBSUssTUFBSixLQUFlLEdBQW5CLEVBQXdCO0FBQ3BCLG9CQUFJO0FBQ0FvQywrQkFBV2hILEtBQUs2RSxLQUFMLENBQVdOLElBQUlPLFlBQWYsQ0FBWDtBQUNILGlCQUZELENBRUUsT0FBT0MsS0FBUCxFQUFjO0FBQ1p2Qyw2QkFBUyxxQ0FBVCxFQUFnRCxJQUFoRDtBQUNBO0FBQ0g7QUFDSixhQVBELE1BT087QUFDSEEseUJBQVMscUNBQVQsRUFBZ0QsSUFBaEQ7QUFDQTtBQUNIO0FBQ0RBLHFCQUFTLElBQVQsRUFBZXdFLFFBQWY7QUFDSDtBQUNKLEtBZkQ7QUFnQkF6QyxRQUFJaEYsSUFBSixDQUFTLEtBQVQsRUFBZ0IrRSxVQUFoQjtBQUNBQyxRQUFJUyxZQUFKLEdBQW1CLE1BQW5CO0FBQ0FULFFBQUlVLGdCQUFKLENBQXFCLFFBQXJCLEVBQStCLGlDQUEvQjtBQUNBVixRQUFJVyxJQUFKO0FBQ0gsQ0EzQk07O0FBNkJBLElBQU1RLG9EQUFzQixTQUF0QkEsbUJBQXNCLENBQUNxQixTQUFELEVBQVl2RSxRQUFaLEVBQXlCO0FBQ3hELFFBQU11QixZQUFZQyxPQUFPQyxnQkFBUCxJQUEyQixJQUFJQyxJQUFKLEdBQVdDLE9BQVgsRUFBN0M7QUFDQUgsV0FBT0MsZ0JBQVAsR0FBMEJGLFNBQTFCO0FBQ0EsUUFBTWtELGdCQUFtQkYsU0FBbkIsaUNBQXdEaEQsU0FBOUQ7O0FBRUEsUUFBTVEsTUFBTSxJQUFJQyxjQUFKLEVBQVo7O0FBRUFELFFBQUlFLGtCQUFKLEdBQXlCLFNBQVNDLFdBQVQsR0FBdUI7QUFDNUMsWUFBSUgsSUFBSUksVUFBSixLQUFtQixDQUF2QixFQUEwQjtBQUN0QixnQkFBSUosSUFBSUssTUFBSixLQUFlLEdBQW5CLEVBQXdCO0FBQ3BCLG9CQUFJO0FBQ0FwQyw2QkFBUyxJQUFULEVBQWV4QyxLQUFLNkUsS0FBTCxDQUFXTixJQUFJTyxZQUFmLENBQWY7QUFDSCxpQkFGRCxDQUVFLE9BQU9DLEtBQVAsRUFBYztBQUNadkMsNkJBQVMsb0RBQVQsRUFBK0QsSUFBL0Q7QUFDSDtBQUNKLGFBTkQsTUFNTztBQUNIQSx5QkFBUyxvREFBVCxFQUErRCxJQUEvRDtBQUNIO0FBQ0o7QUFDSixLQVpEOztBQWNBK0IsUUFBSWhGLElBQUosQ0FBUyxLQUFULEVBQWdCMEgsYUFBaEI7QUFDQTFDLFFBQUlTLFlBQUosR0FBbUIsTUFBbkI7QUFDQVQsUUFBSVUsZ0JBQUosQ0FBcUIsUUFBckIsRUFBK0IsaUNBQS9CO0FBQ0FWLFFBQUlXLElBQUo7QUFDSCxDQXpCTTs7QUEyQlA7Ozs7Ozs7QUFPTyxTQUFTL0gsZUFBVCxDQUF5QjRCLEdBQXpCLEVBQW9EO0FBQUEsUUFBdEJtSSxZQUFzQix1RUFBUCxLQUFPOztBQUN2RCxRQUFJQyxpQkFBSjs7QUFFQTtBQUNBLFFBQUlwSSxJQUFJcUksT0FBSixDQUFZLEtBQVosSUFBcUIsQ0FBQyxDQUExQixFQUE2QjtBQUN6QkQsbUJBQVdwSSxJQUFJc0ksS0FBSixDQUFVLEdBQVYsRUFBZSxDQUFmLENBQVg7QUFDSCxLQUZELE1BRU87QUFDSEYsbUJBQVdwSSxJQUFJc0ksS0FBSixDQUFVLEdBQVYsRUFBZSxDQUFmLENBQVg7QUFDSDs7QUFFRDtBQUNBLFFBQU1DLE9BQU9ILFNBQVNFLEtBQVQsQ0FBZSxHQUFmLEVBQW9CLENBQXBCLENBQWI7QUFDQUYsZUFBV0EsU0FBU0UsS0FBVCxDQUFlLEdBQWYsRUFBb0IsQ0FBcEIsQ0FBWDs7QUFFQTtBQUNBRixlQUFXQSxTQUFTRSxLQUFULENBQWUsR0FBZixFQUFvQixDQUFwQixDQUFYOztBQUVBLFFBQUl0SSxJQUFJcUksT0FBSixDQUFZLEtBQVosSUFBcUIsQ0FBQyxDQUF0QixJQUEyQkYsWUFBL0IsRUFBNkM7QUFDekNDLG1CQUFjcEksSUFBSXNJLEtBQUosQ0FBVSxHQUFWLEVBQWUsQ0FBZixDQUFkLFVBQW9DRixRQUFwQztBQUNBLFlBQUlHLElBQUosRUFBVTtBQUNOSCx1QkFBY0EsUUFBZCxTQUEwQkcsSUFBMUI7QUFDSDtBQUNKOztBQUVELFdBQU9ILFFBQVA7QUFDSDs7QUFFTSxTQUFTL0osb0JBQVQsQ0FBOEIyQixHQUE5QixFQUFtQztBQUN0QyxRQUFJd0csc0JBQUo7O0FBRUE7QUFDQSxRQUFNZ0MsV0FBV3hJLElBQUl5RyxPQUFKLENBQVksNEJBQVosRUFBMEMsRUFBMUMsQ0FBakI7O0FBRUE7O0FBRUEsUUFBSStCLFNBQVNILE9BQVQsQ0FBaUIsS0FBakIsSUFBMEIsQ0FBQyxDQUEvQixFQUFrQztBQUM5QjdCLHdCQUFnQmdDLFNBQVNGLEtBQVQsQ0FBZSxHQUFmLEVBQW9CLENBQXBCLENBQWhCO0FBQ0gsS0FGRCxNQUVPO0FBQ0g5Qix3QkFBZ0JnQyxTQUFTRixLQUFULENBQWUsR0FBZixFQUFvQixDQUFwQixDQUFoQjtBQUNIOztBQUVELFFBQUksQ0FBQzlCLGFBQUwsRUFBb0I7QUFDaEIsZUFBTyxJQUFQO0FBQ0g7O0FBRUQ7QUFDQUEsb0JBQWdCQSxjQUFjOEIsS0FBZCxDQUFvQixHQUFwQixFQUF5QixDQUF6QixDQUFoQjs7QUFFQTtBQUNBOUIsb0JBQWdCQSxjQUFjOEIsS0FBZCxDQUFvQixHQUFwQixFQUF5QixDQUF6QixDQUFoQjs7QUFHQSxXQUFPOUIsYUFBUDtBQUNILEM7Ozs7Ozs7Ozs7Ozs7a0JDaGlCYztBQUNYbEIsYUFBUztBQUNMdEUsaUJBQVMsdURBREo7QUFFTHlILGlCQUFTLEVBRko7QUFHTEMsZUFBTztBQUNIMUgscUJBQVMsaURBRE47QUFFSHlILHFCQUFTO0FBRk4sU0FIRjtBQU9McEQsbUJBQVc7QUFQTixLQURFO0FBVVgsMEJBQXNCO0FBQ2xCckUsaUJBQVMsdURBRFM7QUFFbEJ5SCxpQkFBUyxFQUZTO0FBR2xCQyxlQUFPO0FBQ0gxSCxxQkFBUyxrQ0FETjtBQUVIeUgscUJBQVM7QUFGTixTQUhXO0FBT2xCcEQsbUJBQVc7QUFQTyxLQVZYO0FBbUJYLDhDQUEwQztBQUN0Q3JFLGlCQUFTLHVEQUQ2QjtBQUV0Q3lILGlCQUFTLEVBRjZCO0FBR3RDQyxlQUFPO0FBQ0gxSCxxQkFBUyxpREFETjtBQUVIeUgscUJBQVM7QUFGTixTQUgrQjtBQU90Q3BELG1CQUFXO0FBUDJCLEtBbkIvQjtBQTRCWCw0Q0FBd0M7QUFDcENyRSxpQkFBUyw4Q0FEMkI7QUFFcEN5SCxpQkFBUyxFQUYyQjtBQUdwQ0MsZUFBTztBQUNIMUgscUJBQVMsK0NBRE47QUFFSHlILHFCQUFTO0FBRk4sU0FINkI7QUFPcENwRCxtQkFBVztBQVB5QixLQTVCN0I7QUFxQ1gsb0NBQWdDO0FBQzVCckUsaUJBQVMsOENBRG1CO0FBRTVCeUgsaUJBQVMsRUFGbUI7QUFHNUJDLGVBQU87QUFDSDFILHFCQUFTLCtDQUROO0FBRUh5SCxxQkFBUztBQUZOLFNBSHFCO0FBTzVCcEQsbUJBQVc7QUFQaUIsS0FyQ3JCO0FBOENYLHVDQUFtQztBQUMvQnJFLGlCQUFTLCtDQURzQjtBQUUvQnlILGlCQUFTLEVBRnNCO0FBRy9CQyxlQUFPO0FBQ0gxSCxxQkFBUywwQ0FETjtBQUVIeUgscUJBQVM7QUFGTixTQUh3QjtBQU8vQnBELG1CQUFXO0FBUG9CLEtBOUN4QjtBQXVEWCwrQkFBMkI7QUFDdkJyRSxpQkFBUyw0Q0FEYztBQUV2QnlILGlCQUFTLEVBRmM7QUFHdkJDLGVBQU87QUFDSDFILHFCQUFTLGtDQUROO0FBRUh5SCxxQkFBUztBQUZOLFNBSGdCO0FBT3ZCcEQsbUJBQVc7QUFQWTtBQXZEaEIsQzs7Ozs7OztBQ0FmOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLHFEQUFxRDtBQUNyRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDekJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QyxpQkFBaUI7QUFDMUQsc0RBQXNEO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDLGNBQWM7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlCQUFpQixzQkFBc0I7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQzNJQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNmQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixPQUFPO0FBQzlCO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBLDBCQUEwQjs7QUFFMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQSxJQUFJO0FBQ0o7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQSw2REFBNkQsVUFBVTtBQUN2RTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0MsVUFBVTtBQUNoRCx1QkFBdUIsYUFBYTtBQUNwQztBQUNBLHlCQUF5QixjQUFjO0FBQ3ZDLHlCQUF5QixVQUFVO0FBQ25DLG9CQUFvQixjQUFjO0FBQ2xDO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFVBQVUsRUFBRTtBQUM3QiwrQkFBK0IseUJBQXlCO0FBQ3hEO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLFdBQVc7QUFDckM7QUFDQSxtREFBbUQsVUFBVTtBQUM3RDtBQUNBLEVBQUU7O0FBRUY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGdFQUFnRTtBQUNoRSxFQUFFOztBQUVGO0FBQ0E7QUFDQSxrQ0FBa0MsYUFBYTtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQixFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0M7O0FBRWhDOzs7Ozs7OztBQ3BqQkE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQkFBbUIsaUJBQWlCO0FBQ3BDO0FBQ0E7O0FBRUEsOEVBQThFLHFDQUFxQyxFQUFFOztBQUVySDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNuREE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSx3QkFBd0I7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3pFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxtREFBbUQsY0FBYztBQUNqRTtBQUNBOzs7Ozs7OztBQ25CQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLGNBQWM7QUFDMUQ7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLGFBQWE7QUFDL0MsZ0RBQWdELGNBQWM7QUFDOUQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNYQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDRkE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsdUJBQXVCLFVBQVU7QUFDakMsMkNBQTJDLGVBQWU7QUFDMUQ7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSw2REFBNkQsVUFBVTtBQUN2RTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCLGlCQUFpQix3QkFBd0I7QUFDekM7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FDM09BOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsb0JBQW9CO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNwQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUN0Q0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDBCQUEwQixpQkFBaUI7QUFDM0M7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBOzs7Ozs7Ozs7O0FDZkE7O0FBRUEsQ0FBQyxVQUFDSixNQUFELEVBQVk7QUFDVCxRQUFNMEQsTUFBTTFELE9BQU81RSxRQUFQLENBQWdCOEIsb0JBQWhCLENBQXFDLFFBQXJDLENBQVo7QUFDQSxRQUFNeUcsU0FBUyxxQ0FBZjtBQUNBLFFBQU1DLE9BQU9GLElBQUkvSixNQUFqQjtBQUNBLFFBQU1rSyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQUM3RyxPQUFELEVBQVVlLFVBQVYsRUFBeUI7QUFDN0NrQixlQUFPQyxJQUFQLENBQVluQixVQUFaLEVBQXdCWixPQUF4QixDQUFnQyxVQUFDdEQsR0FBRCxFQUFTO0FBQ3JDbUQsb0JBQVE4RyxZQUFSLFdBQTZCakssR0FBN0IsRUFBb0NrRSxXQUFXbEUsR0FBWCxDQUFwQztBQUNILFNBRkQ7QUFHSCxLQUpEOztBQU1BLFFBQUltRyxPQUFPNUUsUUFBUCxDQUFnQjJJLGdCQUFwQixFQUFzQztBQUNsQyxZQUFNQyxXQUFXLEVBQWpCO0FBQ0EsWUFBSW5FLGVBQWUsRUFBbkI7QUFDQSxZQUFJa0QsWUFBWSxFQUFoQjs7QUFFQSxhQUFLLElBQUlrQixRQUFRLENBQWpCLEVBQW9CQSxRQUFRTCxJQUE1QixFQUFrQ0ssU0FBUyxDQUEzQyxFQUE4QztBQUMxQyxnQkFBTWpILFVBQVUwRyxJQUFJTyxLQUFKLENBQWhCO0FBQ0EsZ0JBQUlqSCxRQUFRNUMsR0FBUixDQUFZOEosS0FBWixDQUFrQlAsTUFBbEIsS0FBNkJLLFNBQVNaLE9BQVQsQ0FBaUJwRyxPQUFqQixJQUE0QixDQUE3RCxFQUFnRTtBQUM1RDtBQUNBK0YsNEJBQVksa0NBQWdCL0YsUUFBUTVDLEdBQXhCLEVBQTZCLElBQTdCLENBQVo7O0FBRUE0Six5QkFBU0csSUFBVCxDQUFjbkgsT0FBZDtBQUNIO0FBQ0o7O0FBRUQsdUNBQWErRixTQUFiLEVBQXdCLFVBQUNoQyxLQUFELEVBQVFpQyxRQUFSLEVBQXFCO0FBQ3pDLGdCQUFJakMsVUFBVSxJQUFkLEVBQW9CO0FBQ2hCO0FBQ0g7QUFDRCxnQkFBSXFELG9CQUFKO0FBQ0EsZ0JBQUlDLGNBQWMsRUFBbEI7QUFDQSxnQkFBTUMsY0FBY04sU0FBU3JLLE1BQTdCOztBQU55Qyx1Q0FPaENGLENBUGdDO0FBUXJDLG9CQUFNdUQsVUFBVWdILFNBQVN2SyxDQUFULENBQWhCO0FBQ0Esb0JBQU11RSxpQkFBaUIsNkNBQTJCaEIsUUFBUWUsVUFBbkMsQ0FBdkI7QUFDQTtBQUNBO0FBQ0E4QiwrQkFBZSxrQ0FBZ0I3QyxRQUFRNUMsR0FBeEIsQ0FBZjtBQUNBMkksNEJBQVksa0NBQWdCL0YsUUFBUTVDLEdBQXhCLEVBQTZCLElBQTdCLENBQVo7O0FBRUE7QUFDQSxtREFBaUJ5RixZQUFqQixFQUErQjdCLGNBQS9CLEVBQStDLFVBQUMzQyxHQUFELEVBQU1MLE1BQU4sRUFBaUI7QUFDNUQsd0JBQUlLLFFBQVEsSUFBWixFQUFrQjtBQUNkO0FBQ0g7O0FBRUQsd0JBQUkySCxZQUFZQSxTQUFTaEksT0FBT3dFLE9BQWhCLE1BQTZCWixTQUF6QyxJQUFzRG9FLFNBQVNoSSxPQUFPd0UsT0FBaEIsTUFBNkIsRUFBdkYsRUFBMkY7QUFDdkY2RSw0Q0FBa0JyQixTQUFTaEksT0FBT3dFLE9BQWhCLENBQWxCO0FBQ0g7O0FBRUQ0RSxrQ0FBY2hKLFNBQVNNLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBZDtBQUNBbUksb0NBQWdCTyxXQUFoQixFQUE2QnBHLGNBQTdCO0FBQ0FvRyxnQ0FBWWhLLEdBQVosUUFBcUIySSxTQUFyQixHQUFpQ3NCLFdBQWpDO0FBQ0Esd0JBQUlySCxRQUFRTCxVQUFaLEVBQXdCO0FBQ3BCSyxnQ0FBUUwsVUFBUixDQUFtQkMsWUFBbkIsQ0FBZ0N3SCxXQUFoQyxFQUE2Q3BILE9BQTdDO0FBQ0FBLGdDQUFRdUgsU0FBUixHQUFvQixFQUFwQjtBQUNBLCtCQUFPUCxTQUFTdkssQ0FBVCxDQUFQO0FBQ0g7QUFDSixpQkFqQkQ7QUFoQnFDOztBQU96QyxpQkFBSyxJQUFJQSxJQUFJNkssY0FBYyxDQUEzQixFQUE4QjdLLEtBQUssQ0FBbkMsRUFBc0NBLEtBQUssQ0FBM0MsRUFBOEM7QUFBQSxzQkFBckNBLENBQXFDO0FBMkI3QztBQUNKLFNBbkNEO0FBb0NILEtBbkRELE1BbURPO0FBQ0gsYUFBSyxJQUFJd0ssU0FBUSxDQUFqQixFQUFvQkEsU0FBUUwsSUFBNUIsRUFBa0NLLFVBQVMsQ0FBM0MsRUFBOEM7QUFDMUMsZ0JBQU1qSCxXQUFVMEcsSUFBSU8sTUFBSixDQUFoQjs7QUFFQSxnQkFBSWpILFNBQVE1QyxHQUFSLENBQVk4SixLQUFaLENBQWtCUCxNQUFsQixDQUFKLEVBQStCO0FBQzNCLG9CQUFNYSxZQUFZeEUsT0FBTzVFLFFBQVAsQ0FBZ0JNLGFBQWhCLENBQThCLEdBQTlCLENBQWxCO0FBQ0E4SSwwQkFBVXRLLEVBQVYsR0FBZSx1QkFBZjtBQUNBc0ssMEJBQVVDLFNBQVYsR0FBc0IsZ0hBQXRCO0FBQ0F6SCx5QkFBUUwsVUFBUixDQUFtQmQsV0FBbkIsQ0FBK0IySSxTQUEvQjtBQUNIO0FBQ0o7QUFDSjtBQUNKLENBekVELEVBeUVHeEUsTUF6RUgsRSIsImZpbGUiOiJnbnMud2lkZ2V0LmxvYWRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDMwKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBiZGZhY2FkZjNhY2NlNmM3N2E5MyIsInZhciBiaW5kID0gcmVxdWlyZSgnZnVuY3Rpb24tYmluZCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGJpbmQuY2FsbChGdW5jdGlvbi5jYWxsLCBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5KTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9oYXMvc3JjL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgZm5Ub1N0ciA9IEZ1bmN0aW9uLnByb3RvdHlwZS50b1N0cmluZztcblxudmFyIGNvbnN0cnVjdG9yUmVnZXggPSAvXlxccypjbGFzcyAvO1xudmFyIGlzRVM2Q2xhc3NGbiA9IGZ1bmN0aW9uIGlzRVM2Q2xhc3NGbih2YWx1ZSkge1xuXHR0cnkge1xuXHRcdHZhciBmblN0ciA9IGZuVG9TdHIuY2FsbCh2YWx1ZSk7XG5cdFx0dmFyIHNpbmdsZVN0cmlwcGVkID0gZm5TdHIucmVwbGFjZSgvXFwvXFwvLipcXG4vZywgJycpO1xuXHRcdHZhciBtdWx0aVN0cmlwcGVkID0gc2luZ2xlU3RyaXBwZWQucmVwbGFjZSgvXFwvXFwqWy5cXHNcXFNdKlxcKlxcLy9nLCAnJyk7XG5cdFx0dmFyIHNwYWNlU3RyaXBwZWQgPSBtdWx0aVN0cmlwcGVkLnJlcGxhY2UoL1xcbi9tZywgJyAnKS5yZXBsYWNlKC8gezJ9L2csICcgJyk7XG5cdFx0cmV0dXJuIGNvbnN0cnVjdG9yUmVnZXgudGVzdChzcGFjZVN0cmlwcGVkKTtcblx0fSBjYXRjaCAoZSkge1xuXHRcdHJldHVybiBmYWxzZTsgLy8gbm90IGEgZnVuY3Rpb25cblx0fVxufTtcblxudmFyIHRyeUZ1bmN0aW9uT2JqZWN0ID0gZnVuY3Rpb24gdHJ5RnVuY3Rpb25PYmplY3QodmFsdWUpIHtcblx0dHJ5IHtcblx0XHRpZiAoaXNFUzZDbGFzc0ZuKHZhbHVlKSkgeyByZXR1cm4gZmFsc2U7IH1cblx0XHRmblRvU3RyLmNhbGwodmFsdWUpO1xuXHRcdHJldHVybiB0cnVlO1xuXHR9IGNhdGNoIChlKSB7XG5cdFx0cmV0dXJuIGZhbHNlO1xuXHR9XG59O1xudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcbnZhciBmbkNsYXNzID0gJ1tvYmplY3QgRnVuY3Rpb25dJztcbnZhciBnZW5DbGFzcyA9ICdbb2JqZWN0IEdlbmVyYXRvckZ1bmN0aW9uXSc7XG52YXIgaGFzVG9TdHJpbmdUYWcgPSB0eXBlb2YgU3ltYm9sID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBTeW1ib2wudG9TdHJpbmdUYWcgPT09ICdzeW1ib2wnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGlzQ2FsbGFibGUodmFsdWUpIHtcblx0aWYgKCF2YWx1ZSkgeyByZXR1cm4gZmFsc2U7IH1cblx0aWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgdmFsdWUgIT09ICdvYmplY3QnKSB7IHJldHVybiBmYWxzZTsgfVxuXHRpZiAoaGFzVG9TdHJpbmdUYWcpIHsgcmV0dXJuIHRyeUZ1bmN0aW9uT2JqZWN0KHZhbHVlKTsgfVxuXHRpZiAoaXNFUzZDbGFzc0ZuKHZhbHVlKSkgeyByZXR1cm4gZmFsc2U7IH1cblx0dmFyIHN0ckNsYXNzID0gdG9TdHIuY2FsbCh2YWx1ZSk7XG5cdHJldHVybiBzdHJDbGFzcyA9PT0gZm5DbGFzcyB8fCBzdHJDbGFzcyA9PT0gZ2VuQ2xhc3M7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2lzLWNhbGxhYmxlL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwidmFyIGc7XHJcblxyXG4vLyBUaGlzIHdvcmtzIGluIG5vbi1zdHJpY3QgbW9kZVxyXG5nID0gKGZ1bmN0aW9uKCkge1xyXG5cdHJldHVybiB0aGlzO1xyXG59KSgpO1xyXG5cclxudHJ5IHtcclxuXHQvLyBUaGlzIHdvcmtzIGlmIGV2YWwgaXMgYWxsb3dlZCAoc2VlIENTUClcclxuXHRnID0gZyB8fCBGdW5jdGlvbihcInJldHVybiB0aGlzXCIpKCkgfHwgKDEsZXZhbCkoXCJ0aGlzXCIpO1xyXG59IGNhdGNoKGUpIHtcclxuXHQvLyBUaGlzIHdvcmtzIGlmIHRoZSB3aW5kb3cgcmVmZXJlbmNlIGlzIGF2YWlsYWJsZVxyXG5cdGlmKHR5cGVvZiB3aW5kb3cgPT09IFwib2JqZWN0XCIpXHJcblx0XHRnID0gd2luZG93O1xyXG59XHJcblxyXG4vLyBnIGNhbiBzdGlsbCBiZSB1bmRlZmluZWQsIGJ1dCBub3RoaW5nIHRvIGRvIGFib3V0IGl0Li4uXHJcbi8vIFdlIHJldHVybiB1bmRlZmluZWQsIGluc3RlYWQgb2Ygbm90aGluZyBoZXJlLCBzbyBpdCdzXHJcbi8vIGVhc2llciB0byBoYW5kbGUgdGhpcyBjYXNlLiBpZighZ2xvYmFsKSB7IC4uLn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZztcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL3dlYnBhY2svYnVpbGRpbi9nbG9iYWwuanNcbi8vIG1vZHVsZSBpZCA9IDJcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciBrZXlzID0gcmVxdWlyZSgnb2JqZWN0LWtleXMnKTtcbnZhciBmb3JlYWNoID0gcmVxdWlyZSgnZm9yZWFjaCcpO1xudmFyIGhhc1N5bWJvbHMgPSB0eXBlb2YgU3ltYm9sID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBTeW1ib2woKSA9PT0gJ3N5bWJvbCc7XG5cbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG5cbnZhciBpc0Z1bmN0aW9uID0gZnVuY3Rpb24gKGZuKSB7XG5cdHJldHVybiB0eXBlb2YgZm4gPT09ICdmdW5jdGlvbicgJiYgdG9TdHIuY2FsbChmbikgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG59O1xuXG52YXIgYXJlUHJvcGVydHlEZXNjcmlwdG9yc1N1cHBvcnRlZCA9IGZ1bmN0aW9uICgpIHtcblx0dmFyIG9iaiA9IHt9O1xuXHR0cnkge1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosICd4JywgeyBlbnVtZXJhYmxlOiBmYWxzZSwgdmFsdWU6IG9iaiB9KTtcbiAgICAgICAgLyogZXNsaW50LWRpc2FibGUgbm8tdW51c2VkLXZhcnMsIG5vLXJlc3RyaWN0ZWQtc3ludGF4ICovXG4gICAgICAgIGZvciAodmFyIF8gaW4gb2JqKSB7IHJldHVybiBmYWxzZTsgfVxuICAgICAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXVudXNlZC12YXJzLCBuby1yZXN0cmljdGVkLXN5bnRheCAqL1xuXHRcdHJldHVybiBvYmoueCA9PT0gb2JqO1xuXHR9IGNhdGNoIChlKSB7IC8qIHRoaXMgaXMgSUUgOC4gKi9cblx0XHRyZXR1cm4gZmFsc2U7XG5cdH1cbn07XG52YXIgc3VwcG9ydHNEZXNjcmlwdG9ycyA9IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSAmJiBhcmVQcm9wZXJ0eURlc2NyaXB0b3JzU3VwcG9ydGVkKCk7XG5cbnZhciBkZWZpbmVQcm9wZXJ0eSA9IGZ1bmN0aW9uIChvYmplY3QsIG5hbWUsIHZhbHVlLCBwcmVkaWNhdGUpIHtcblx0aWYgKG5hbWUgaW4gb2JqZWN0ICYmICghaXNGdW5jdGlvbihwcmVkaWNhdGUpIHx8ICFwcmVkaWNhdGUoKSkpIHtcblx0XHRyZXR1cm47XG5cdH1cblx0aWYgKHN1cHBvcnRzRGVzY3JpcHRvcnMpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqZWN0LCBuYW1lLCB7XG5cdFx0XHRjb25maWd1cmFibGU6IHRydWUsXG5cdFx0XHRlbnVtZXJhYmxlOiBmYWxzZSxcblx0XHRcdHZhbHVlOiB2YWx1ZSxcblx0XHRcdHdyaXRhYmxlOiB0cnVlXG5cdFx0fSk7XG5cdH0gZWxzZSB7XG5cdFx0b2JqZWN0W25hbWVdID0gdmFsdWU7XG5cdH1cbn07XG5cbnZhciBkZWZpbmVQcm9wZXJ0aWVzID0gZnVuY3Rpb24gKG9iamVjdCwgbWFwKSB7XG5cdHZhciBwcmVkaWNhdGVzID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgPyBhcmd1bWVudHNbMl0gOiB7fTtcblx0dmFyIHByb3BzID0ga2V5cyhtYXApO1xuXHRpZiAoaGFzU3ltYm9scykge1xuXHRcdHByb3BzID0gcHJvcHMuY29uY2F0KE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMobWFwKSk7XG5cdH1cblx0Zm9yZWFjaChwcm9wcywgZnVuY3Rpb24gKG5hbWUpIHtcblx0XHRkZWZpbmVQcm9wZXJ0eShvYmplY3QsIG5hbWUsIG1hcFtuYW1lXSwgcHJlZGljYXRlc1tuYW1lXSk7XG5cdH0pO1xufTtcblxuZGVmaW5lUHJvcGVydGllcy5zdXBwb3J0c0Rlc2NyaXB0b3JzID0gISFzdXBwb3J0c0Rlc2NyaXB0b3JzO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGRlZmluZVByb3BlcnRpZXM7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZGVmaW5lLXByb3BlcnRpZXMvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9lczIwMTUnKTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9lczYuanNcbi8vIG1vZHVsZSBpZCA9IDRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciBpbXBsZW1lbnRhdGlvbiA9IHJlcXVpcmUoJy4vaW1wbGVtZW50YXRpb24nKTtcblxubW9kdWxlLmV4cG9ydHMgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZCB8fCBpbXBsZW1lbnRhdGlvbjtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9mdW5jdGlvbi1iaW5kL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSA1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc1ByaW1pdGl2ZSh2YWx1ZSkge1xuXHRyZXR1cm4gdmFsdWUgPT09IG51bGwgfHwgKHR5cGVvZiB2YWx1ZSAhPT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgdmFsdWUgIT09ICdvYmplY3QnKTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2hlbHBlcnMvaXNQcmltaXRpdmUuanNcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCJtb2R1bGUuZXhwb3J0cyA9IE51bWJlci5pc05hTiB8fCBmdW5jdGlvbiBpc05hTihhKSB7XG5cdHJldHVybiBhICE9PSBhO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9oZWxwZXJzL2lzTmFOLmpzXG4vLyBtb2R1bGUgaWQgPSA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwidmFyICRpc05hTiA9IE51bWJlci5pc05hTiB8fCBmdW5jdGlvbiAoYSkgeyByZXR1cm4gYSAhPT0gYTsgfTtcblxubW9kdWxlLmV4cG9ydHMgPSBOdW1iZXIuaXNGaW5pdGUgfHwgZnVuY3Rpb24gKHgpIHsgcmV0dXJuIHR5cGVvZiB4ID09PSAnbnVtYmVyJyAmJiAhJGlzTmFOKHgpICYmIHggIT09IEluZmluaXR5ICYmIHggIT09IC1JbmZpbml0eTsgfTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9oZWxwZXJzL2lzRmluaXRlLmpzXG4vLyBtb2R1bGUgaWQgPSA4XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzaWduKG51bWJlcikge1xuXHRyZXR1cm4gbnVtYmVyID49IDAgPyAxIDogLTE7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvc2lnbi5qc1xuLy8gbW9kdWxlIGlkID0gOVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbW9kKG51bWJlciwgbW9kdWxvKSB7XG5cdHZhciByZW1haW4gPSBudW1iZXIgJSBtb2R1bG87XG5cdHJldHVybiBNYXRoLmZsb29yKHJlbWFpbiA+PSAwID8gcmVtYWluIDogcmVtYWluICsgbW9kdWxvKTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9tb2QuanNcbi8vIG1vZHVsZSBpZCA9IDEwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgRVMgPSByZXF1aXJlKCdlcy1hYnN0cmFjdC9lczYnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBmaW5kKHByZWRpY2F0ZSkge1xuXHR2YXIgbGlzdCA9IEVTLlRvT2JqZWN0KHRoaXMpO1xuXHR2YXIgbGVuZ3RoID0gRVMuVG9JbnRlZ2VyKEVTLlRvTGVuZ3RoKGxpc3QubGVuZ3RoKSk7XG5cdGlmICghRVMuSXNDYWxsYWJsZShwcmVkaWNhdGUpKSB7XG5cdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXJyYXkjZmluZDogcHJlZGljYXRlIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuXHR9XG5cdGlmIChsZW5ndGggPT09IDApIHtcblx0XHRyZXR1cm4gdW5kZWZpbmVkO1xuXHR9XG5cdHZhciB0aGlzQXJnID0gYXJndW1lbnRzWzFdO1xuXHRmb3IgKHZhciBpID0gMCwgdmFsdWU7IGkgPCBsZW5ndGg7IGkrKykge1xuXHRcdHZhbHVlID0gbGlzdFtpXTtcblx0XHRpZiAoRVMuQ2FsbChwcmVkaWNhdGUsIHRoaXNBcmcsIFt2YWx1ZSwgaSwgbGlzdF0pKSB7XG5cdFx0XHRyZXR1cm4gdmFsdWU7XG5cdFx0fVxuXHR9XG5cdHJldHVybiB1bmRlZmluZWQ7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2FycmF5LnByb3RvdHlwZS5maW5kL2ltcGxlbWVudGF0aW9uLmpzXG4vLyBtb2R1bGUgaWQgPSAxMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBnZXRQb2x5ZmlsbCgpIHtcblx0Ly8gRGV0ZWN0IGlmIGFuIGltcGxlbWVudGF0aW9uIGV4aXN0c1xuXHQvLyBEZXRlY3QgZWFybHkgaW1wbGVtZW50YXRpb25zIHdoaWNoIHNraXBwZWQgaG9sZXMgaW4gc3BhcnNlIGFycmF5c1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tc3BhcnNlLWFycmF5c1xuXHR2YXIgaW1wbGVtZW50ZWQgPSBBcnJheS5wcm90b3R5cGUuZmluZCAmJiBbLCAxXS5maW5kKGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSkgIT09IDE7XG5cbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGdsb2JhbC1yZXF1aXJlXG5cdHJldHVybiBpbXBsZW1lbnRlZCA/IEFycmF5LnByb3RvdHlwZS5maW5kIDogcmVxdWlyZSgnLi9pbXBsZW1lbnRhdGlvbicpO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9hcnJheS5wcm90b3R5cGUuZmluZC9wb2x5ZmlsbC5qc1xuLy8gbW9kdWxlIGlkID0gMTJcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCJpbXBvcnQgY29uZmlncyBmcm9tICdjb25maWdzL2dhdGV3YXknO1xuXG5yZXF1aXJlKCdhcnJheS5wcm90b3R5cGUuZmluZCcpLnNoaW0oKTtcblxuZnVuY3Rpb24gbWVyZ2VPYmplY3QodGFyZ2V0KSB7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXBsdXNwbHVzXG4gICAgZm9yIChsZXQgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHByZWZlci1yZXN0LXBhcmFtc1xuICAgICAgICBjb25zdCBzb3VyY2UgPSBhcmd1bWVudHNbaV07XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1yZXN0cmljdGVkLXN5bnRheFxuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBzb3VyY2UpIHtcbiAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wcm90b3R5cGUtYnVpbHRpbnNcbiAgICAgICAgICAgIGlmIChzb3VyY2UuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgICAgIHRhcmdldFtrZXldID0gc291cmNlW2tleV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHRhcmdldDtcbn1cblxuZnVuY3Rpb24gc2V0SUZyYW1lVmFyaWFibGVzKGlGcmFtZSwgaUZyYW1lSWQpIHtcbiAgICBpRnJhbWUuaWQgPSBpRnJhbWVJZDtcbiAgICBpRnJhbWUubmFtZSA9IGlGcmFtZUlkO1xuICAgIGlGcmFtZS5zcmMgPSAnamF2YXNjcmlwdDpmYWxzZSc7IC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICBpRnJhbWUudGl0bGUgPSAnJztcbiAgICBpRnJhbWUucm9sZSA9ICdwcmVzZW50YXRpb24nO1xuICAgIGlGcmFtZS5mcmFtZUJvcmRlciA9ICcwJztcbiAgICBpRnJhbWUuc2Nyb2xsaW5nID0gJ25vJztcbiAgICBpRnJhbWUud2lkdGggPSAnMTAwJSc7XG4gICAgaUZyYW1lLmhlaWdodCA9ICcxMzdweCc7XG4gICAgKGlGcmFtZS5mcmFtZUVsZW1lbnQgfHwgaUZyYW1lKS5zdHlsZS5jc3NUZXh0ID0gJ2JvcmRlcjowO21pbi13aWR0aDoxMDAlOyc7XG59XG5cbmZ1bmN0aW9uIHdyaXRlSUZyYW1lRG9jdW1lbnQoaUZyYW1lLCB1cmwsIGNvbmZpZykge1xuICAgIGxldCBkb2M7XG4gICAgbGV0IGRvbTtcbiAgICB0cnkge1xuICAgICAgICBkb2MgPSBpRnJhbWUuY29udGVudFdpbmRvdy5kb2N1bWVudDtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgZG9tID0gZG9jdW1lbnQuZG9tYWluO1xuICAgICAgICBpRnJhbWUuc3JjID0gYGphdmFzY3JpcHQ6dmFyIGQ9ZG9jdW1lbnQub3BlbigpO2QuZG9tYWluPVwiJHtkb219XCI7dm9pZCgwKTtgO1xuICAgICAgICBkb2MgPSBpRnJhbWUuY29udGVudFdpbmRvdy5kb2N1bWVudDtcbiAgICB9XG5cbiAgICBkb2Mub3BlbigpLl9sID0gZnVuY3Rpb24gKCkgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgICAgIGNvbnN0IGpzID0gdGhpcy5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiAgICAgICAgaWYgKGRvbSkgdGhpcy5kb21haW4gPSBkb207XG4gICAgICAgIGpzLmlkID0gJ3dpZGdldC1pZnJhbWUtYXN5bmMnO1xuICAgICAgICBqcy5zcmMgPSB1cmw7XG4gICAgICAgIGpzLmFzeW5jID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5ib2R5LmFwcGVuZENoaWxkKGpzKTtcbiAgICB9O1xuICAgIGRvYy53cml0ZSgnPGh0bWw+PGhlYWQ+Jyk7XG4gICAgZG9jLndyaXRlKGA8YmFzZSBocmVmPVwiJHtjb25maWcuYmFzZVVSTH1cIj5gKTtcbiAgICBkb2Mud3JpdGUoJzxtZXRhIGNoYXJzZXQ9XCJ1dGYtOFwiPicpO1xuICAgIC8vIEluamVjdCBSZWFjdCBEZXZlbG9wZXIgVG9vbHMgaWYgbm90IGluIHByb2R1Y3Rpb25cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgICBkb2Mud3JpdGUoJzxzY3JpcHQ+X19SRUFDVF9ERVZUT09MU19HTE9CQUxfSE9PS19fID0gcGFyZW50Ll9fUkVBQ1RfREVWVE9PTFNfR0xPQkFMX0hPT0tfXzwvc2NyaXB0PicpO1xuICAgIH1cbiAgICBkb2Mud3JpdGUoJzxtZXRhIGh0dHAtZXF1aXY9XCJYLVVBLUNvbXBhdGlibGVcIiBjb250ZW50PVwiSUU9ZWRnZSxjaHJvbWU9MVwiPicpO1xuICAgIGRvYy53cml0ZSgnPG1ldGEgbmFtZT1cInZpZXdwb3J0XCIgY29udGVudD1cIndpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xXCI+Jyk7XG4gICAgZG9jLndyaXRlKCc8bWV0YSBuYW1lPVwiYXBwbGUtbW9iaWxlLXdlYi1hcHAtY2FwYWJsZVwiIGNvbnRlbnQ9XCJ5ZXNcIj4nKTtcbiAgICBkb2Mud3JpdGUoJzxtZXRhIGh0dHAtZXF1aXY9XCJDYWNoZS1Db250cm9sXCIgY29udGVudD1cIm5vLXRyYW5zZm9ybVwiPicpO1xuICAgIGRvYy53cml0ZSgnPC9oZWFkPicpO1xuICAgIGRvYy53cml0ZSgnPGJvZHkgb25sb2FkPVwiZG9jdW1lbnQuX2woKTtcIj4nKTtcbiAgICBkb2Mud3JpdGUoYDxzY3JpcHQ+d2luZG93LnNldHRpbmdzID0gJHtKU09OLnN0cmluZ2lmeShjb25maWcpfTs8L3NjcmlwdD5gKTtcbiAgICBpZiAoc2hvdWxkTG9hZEluc3RhbmEoY29uZmlnKSkge1xuICAgICAgICBkb2Mud3JpdGUoJzxzY3JpcHQ+KGZ1bmN0aW9uKGkscyxvLGcscixhLG0pe2lbXFwnSW5zdGFuYUV1bU9iamVjdFxcJ109cjtpW3JdPWlbcl18fGZ1bmN0aW9uKCl7KGlbcl0ucT1pW3JdLnF8fFtdKS5wdXNoKGFyZ3VtZW50cyl9LCBpW3JdLmw9MSpuZXcgRGF0ZSgpO2E9cy5jcmVhdGVFbGVtZW50KG8pLCBtPXMuZ2V0RWxlbWVudHNCeVRhZ05hbWUobylbMF07YS5hc3luYz0xO2Euc3JjPWc7bS5wYXJlbnROb2RlLmluc2VydEJlZm9yZShhLCBtKX0pKHdpbmRvdywgZG9jdW1lbnQsIFxcJ3NjcmlwdFxcJywgXFwnLy9ldW0uaW5zdGFuYS5pby9ldW0ubWluLmpzXFwnLCBcXCdpbmV1bVxcJyk7aW5ldW0oXFwnYXBpS2V5XFwnLCBcXCdvNHlZVDdxN1RVcVBqZmFwcUQzcGl3XFwnKTs8L3NjcmlwdD4nKTtcbiAgICB9XG4gICAgaWYgKHNob3VsZExvYWRBbmFseXRpY3MoY29uZmlnKSkge1xuICAgICAgICBkb2Mud3JpdGUoJzxzY3JpcHQgYXN5bmMgc3JjPVwiaHR0cHM6Ly93d3cuZ29vZ2xldGFnbWFuYWdlci5jb20vZ3RhZy9qcz9pZD1VQS01ODMyMzYyNS0xMlwiPjwvc2NyaXB0PjxzY3JpcHQ+d2luZG93LmRhdGFMYXllciA9IHdpbmRvdy5kYXRhTGF5ZXIgfHwgW107ZnVuY3Rpb24gZ3RhZygpe2RhdGFMYXllci5wdXNoKGFyZ3VtZW50cyk7fWd0YWcoXFwnanNcXCcsIG5ldyBEYXRlKCkpO2d0YWcoXFwnY29uZmlnXFwnLCBcXCdVQS01ODMyMzYyNS0xMlxcJyk7PC9zY3JpcHQ+Jyk7XG4gICAgfVxuICAgIGlmIChjb25maWcuYW5hbHl0aWNzICYmIGNvbmZpZy5hbmFseXRpY3MgIT09ICcnKSB7XG4gICAgICAgIGRvYy53cml0ZShgPHNjcmlwdCBhc3luYyBzcmM9XCJodHRwczovL3d3dy5nb29nbGV0YWdtYW5hZ2VyLmNvbS9ndGFnL2pzP2lkPSR7Y29uZmlnLmFuYWx5dGljc31cIj48L3NjcmlwdD48c2NyaXB0PndpbmRvdy5kYXRhTGF5ZXIgPSB3aW5kb3cuZGF0YUxheWVyIHx8IFtdO2Z1bmN0aW9uIGd0YWcoKXtkYXRhTGF5ZXIucHVzaChhcmd1bWVudHMpO31ndGFnKCdqcycsIG5ldyBEYXRlKCkpO2d0YWcoJ2NvbmZpZycsICcke2NvbmZpZy5hbmFseXRpY3N9Jyk7PC9zY3JpcHQ+YCk7XG4gICAgfVxuICAgIGRvYy53cml0ZSgnPGRpdiBpZD1cIm1haW5Db250ZW50XCIgY2xhc3M9XCJjb250YWluZXJcIj48ZGl2IGlkPVwid2lkZ2V0XCI+PC9kaXY+PC9kaXY+Jyk7XG4gICAgZG9jLndyaXRlKCc8L2JvZHk+PGh0bWw+Jyk7XG4gICAgZG9jLmNsb3NlKCk7XG59XG5cbmZ1bmN0aW9uIHN3aXRjaElGcmFtZUNvbnRlbnQodXJsLCBjb25maWcpIHtcbiAgICBjb25zdCBzZWxlY3RlZElGcmFtZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGNvbmZpZy5pRnJhbWVJZCk7XG4gICAgY29uc3QgcGxhY2VIb2xkZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICBwbGFjZUhvbGRlci5pZCA9IGdlbmVyYXRlSWQoMTIpO1xuICAgIHNlbGVjdGVkSUZyYW1lLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKHBsYWNlSG9sZGVyLCBzZWxlY3RlZElGcmFtZSk7XG4gICAgc2VsZWN0ZWRJRnJhbWUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzZWxlY3RlZElGcmFtZSk7XG5cbiAgICBjb25zdCBuZXdGcmFtZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xuXG4gICAgc2V0SUZyYW1lVmFyaWFibGVzKG5ld0ZyYW1lLCBjb25maWcuaUZyYW1lSWQpO1xuXG4gICAgcGxhY2VIb2xkZXIucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUobmV3RnJhbWUsIHBsYWNlSG9sZGVyKTtcbiAgICBwbGFjZUhvbGRlci5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHBsYWNlSG9sZGVyKTtcblxuICAgIHdyaXRlSUZyYW1lRG9jdW1lbnQobmV3RnJhbWUsIHVybCwgY29uZmlnKTtcbn1cblxuLyoqXG4gKiBsb2FkSUZyYW1lXG4gKlxuICogR2VuZXJhdGVzIGlGcmFtZSBhbmQgbG9hZHMgdGhlIGdpdmVuIHVybCBhcyBzY3JpcHQtdGFnXG4gKlxuICogQHBhcmFtIGVsZW1lbnRcbiAqIEBwYXJhbSB1cmxcbiAqIEBwYXJhbSBjb25maWdcbiAqL1xuXG5leHBvcnQgY29uc3QgbG9hZElGcmFtZSA9IChlbGVtZW50LCB1cmwsIGNvbmZpZykgPT4ge1xuICAgIGNvbnN0IGlGcmFtZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xuICAgIGNvbmZpZy5pRnJhbWVJZCA9IGdlbmVyYXRlSWQoNik7XG5cbiAgICBzZXRJRnJhbWVWYXJpYWJsZXMoaUZyYW1lLCBjb25maWcuaUZyYW1lSWQpO1xuXG4gICAgY29uc3Qgc2NyaXB0cyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdzY3JpcHQnKTtcbiAgICBbXS5mb3JFYWNoLmNhbGwoc2NyaXB0cywgKHNjcmlwdCkgPT4ge1xuICAgICAgICBpZiAoc2NyaXB0ID09PSBlbGVtZW50KSB7XG4gICAgICAgICAgICBzY3JpcHQucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoaUZyYW1lLCBzY3JpcHQpO1xuICAgICAgICAgICAgd3JpdGVJRnJhbWVEb2N1bWVudChpRnJhbWUsIHVybCwgY29uZmlnKTtcbiAgICAgICAgfVxuICAgIH0pO1xufTtcblxuZXhwb3J0IGNvbnN0IHNob3VsZExvYWRJbnN0YW5hID0gKGNvbmZpZykgPT4ge1xuICAgIGlmIChcbiAgICAgICFjb25maWcgfHxcbiAgICAgICFjb25maWcuZmVhdHVyZVN3aXRjaGVzIHx8XG4gICAgICAhY29uZmlnLmZlYXR1cmVTd2l0Y2hlcy5hbGwgfHxcbiAgICAgICFjb25maWcuZmVhdHVyZVN3aXRjaGVzLmFsbC5pbnN0YW5hIHx8XG4gICAgICAhY29uZmlnLmZlYXR1cmVTd2l0Y2hlcy5hbGwuaW5zdGFuYS5hY3RpdmVcbiAgICApIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGNvbnN0IG1hZ2ljTnVtYmVyID0gTWF0aC5yYW5kb20oKSAqIDEwMDtcblxuICAgIHJldHVybiAoY29uZmlnLmZlYXR1cmVTd2l0Y2hlcy5hbGwuaW5zdGFuYS5wZXJjZW50YWdlX2FjdGl2ZSA+PSBtYWdpY051bWJlcik7XG59O1xuXG5leHBvcnQgY29uc3Qgc2hvdWxkTG9hZEFuYWx5dGljcyA9IChjb25maWcpID0+IHtcbiAgICBpZiAoXG4gICAgICAhY29uZmlnIHx8XG4gICAgICAhY29uZmlnLmZlYXR1cmVTd2l0Y2hlcyB8fFxuICAgICAgIWNvbmZpZy5mZWF0dXJlU3dpdGNoZXMuYWxsIHx8XG4gICAgICAhY29uZmlnLmZlYXR1cmVTd2l0Y2hlcy5hbGwuYW5hbHl0aWNzIHx8XG4gICAgICAhY29uZmlnLmZlYXR1cmVTd2l0Y2hlcy5hbGwuYW5hbHl0aWNzLmFjdGl2ZVxuICAgICkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgY29uc3QgbWFnaWNOdW1iZXIgPSBNYXRoLnJhbmRvbSgpICogMTAwO1xuXG4gICAgcmV0dXJuIChjb25maWcuZmVhdHVyZVN3aXRjaGVzLmFsbC5hbmFseXRpY3MucGVyY2VudGFnZV9hY3RpdmUgPj0gbWFnaWNOdW1iZXIpO1xufTtcblxuLyoqXG4gKiBnZXRTY3JpcHRUYWdEYXRhQXR0cmlidXRlc1xuICpcbiAqIFBhcnNlIHNjcmlwdCB0YWcgYXR0cmlidXRlcyBhbmQgcmV0dXJuIGFsbCBkYXRhLSogYXR0cmlidXRlcyBhcyBvYmplY3QuXG4gKiBAcGFyYW0gYXR0cmlidXRlc1xuICogQHJldHVybnMge3t9fVxuICovXG5leHBvcnQgY29uc3QgZ2V0U2NyaXB0VGFnRGF0YUF0dHJpYnV0ZXMgPSAoYXR0cmlidXRlcykgPT4ge1xuICAgIGNvbnN0IGRhdGFBdHRyaWJ1dGVzID0ge307XG5cbiAgICBbXS5mb3JFYWNoLmNhbGwoYXR0cmlidXRlcywgKGF0dHJpYnV0ZSkgPT4ge1xuICAgICAgICBpZiAoL15kYXRhLS8udGVzdChhdHRyaWJ1dGUubmFtZSkpIHtcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IGF0dHJpYnV0ZS5uYW1lLnN1YnN0cig1KTtcbiAgICAgICAgICAgIGRhdGFBdHRyaWJ1dGVzW3BhcmFtZXRlcl0gPSBhdHRyaWJ1dGUudmFsdWU7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIHJldHVybiBkYXRhQXR0cmlidXRlcztcbn07XG5cbmZ1bmN0aW9uIGRhdGFIYW5kbGVyKGRhdGEsIGNvbmZpZywgY2FsbGJhY2spIHtcbiAgICBjb25maWcuY3VzdG9tZXJfaWQgPSBkYXRhLmN1c3RvbWVyX2lkO1xuXG4gICAgaWYgKGRhdGEuYWxsb3dfdGFncyA9PT0gJ3llcycpIHtcbiAgICAgICAgaWYgKGRhdGEuYWxsb3dfZGF0YV90YWdzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGNvbnN0IHRhZ3MgPSBkYXRhLmFsbG93X2RhdGFfdGFncy5tYXAoKGl0ZW0pID0+IGl0ZW0udHJpbSgpKTtcblxuICAgICAgICAgICAgLy8gTG9vcCB0aHJvdWdoIGFsbCBnaXZlbiBjb25maWcgYXR0cmlidXRlc1xuICAgICAgICAgICAgT2JqZWN0LmtleXMoY29uZmlnKS5mb3JFYWNoKChjb25maWdUYWcpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAodGFncy5maW5kKCh0YWcpID0+IHRhZyA9PT0gY29uZmlnVGFnKSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBhdHRyaWJ1dGUgZXhpc3RzIGluIGFsbG93X2RhdGFfdGFncyBhbmQgZGVmYXVsdCBjb25maWcuXG4gICAgICAgICAgICAgICAgICAgIC8vIHNvIHdlIG1heSBvdmVycmlkZSB0aGUgZGF0YSBhdHRyaWJ1dGUgd2l0aCBnaXZlbiBjb25maWcgYXR0cmlidXRlXG4gICAgICAgICAgICAgICAgICAgIGRhdGFbY29uZmlnVGFnXSA9IGNvbmZpZ1tjb25maWdUYWddO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChkYXRhW2NvbmZpZ1RhZ10gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAvLyBoZXJlIHdlIGFkZCBpdCBpZiBpdCBkb2Vzbid0IGV4aXN0cyBpbiB0aGUgZGVmYXVsdCBjb25maWdcbiAgICAgICAgICAgICAgICAgICAgZGF0YVtjb25maWdUYWddID0gY29uZmlnW2NvbmZpZ1RhZ107XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBkYXRhID0gbWVyZ2VPYmplY3Qoe30sIGRhdGEsIGNvbmZpZyk7XG4gICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICAvLyBMb29wIHRocm91Z2ggYWxsIGdpdmVuIGNvbmZpZyBhdHRyaWJ1dGVzXG4gICAgICAgIE9iamVjdC5rZXlzKGNvbmZpZykuZm9yRWFjaCgoY29uZmlnVGFnKSA9PiB7XG4gICAgICAgICAgICBpZiAoZGF0YVtjb25maWdUYWddID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAvLyBoZXJlIHdlIGFkZCBpdCBpZiBpdCBkb2Vzbid0IGV4aXN0cyBpbiB0aGUgZGVmYXVsdCBjb25maWdcbiAgICAgICAgICAgICAgICBkYXRhW2NvbmZpZ1RhZ10gPSBjb25maWdbY29uZmlnVGFnXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gQWRkIGN1c3RvbV9jc3MgdG8gdGhlIGRhdGFcbiAgICBpZiAoY29uZmlnLmN1c3RvbV9jc3MgIT09IHVuZGVmaW5lZCAmJiBjb25maWcuY3VzdG9tX2NzcyAhPT0gJycpIHtcbiAgICAgICAgZGF0YS5jdXN0b21fY3NzID0gY29uZmlnLmN1c3RvbV9jc3M7XG4gICAgfVxuXG4gICAgLy8gVE9ETzogSW4gdGhlIG5lYXIgZnV0dXJlIHdlJ3JlIGdvaW5nIHRvIGFkZCBhIHJlYWwgZG9tYWluIGNoZWNrLi4uXG4gICAgLy8gaWYgKCFjaGVja0RvbWFpbihkYXRhLCBkb2N1bWVudC5yZWZlcnJlci50b1N0cmluZygpKSkge1xuICAgIC8vICAgICBjYWxsYmFjaygnRXJyb3I6IGRvbWFpbiBpcyBub3QgYWxsb3dlZC4nLCBudWxsKTtcbiAgICAvLyB9XG5cbiAgICBpZiAoZGF0YS50eXBlID09PSAndGFnZ2luZycpIHtcbiAgICAgICAgZGF0YS52ZXJzaW9uID0gJ3YyLjAnO1xuICAgICAgICBkYXRhLmxhbmd1YWdlID0gJ2VuX0dCJztcbiAgICAgICAgZGF0YS5sYW5ndWFnZV9jb2RlID0gJzInO1xuICAgICAgICBkYXRhLnRoZW1lID0gJ2RlZmF1bHQnO1xuICAgIH1cblxuICAgIGNhbGxiYWNrKG51bGwsIGRhdGEpO1xufVxuXG4vKipcbiAqIGxvYWRXaWRnZXRDb25maWdcbiAqXG4gKiBAcGFyYW0gbG9hZGVyRG9tYWluXG4gKiBAcGFyYW0gY29uZmlnXG4gKiBAcGFyYW0gY2FsbGJhY2tcbiAqL1xuZXhwb3J0IGNvbnN0IGxvYWRXaWRnZXRDb25maWcgPSAobG9hZGVyRG9tYWluLCBjb25maWcsIGNhbGxiYWNrKSA9PiB7XG4gICAgaWYgKGNvbmZpZyA9PT0gdW5kZWZpbmVkIHx8IGNvbmZpZy53aWRnZXRfaWQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBjYWxsYmFjaygnRXJyb3I6IGNvdWxkIG5vdCBsb2FkIHdpZGdldCBjb25maWcnLCBudWxsKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBhbnRpQ2FjaGUgPSBnbG9iYWwuX19HTlNfQU5USV9DQUNIRSB8fCBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgZ2xvYmFsLl9fR05TX0FOVElfQ0FDSEUgPSBhbnRpQ2FjaGU7XG5cbiAgICAgICAgLy8gR2VuZXJhdGUgd2lkZ2V0IGNvbmZpZ3VyYXRpb24gdXJsXG4gICAgICAgIGxldCBjb25maWdVUkwgPSBjb25maWdzLmRlZmF1bHQuY29uZmlnVVJMO1xuICAgICAgICBpZiAobG9hZGVyRG9tYWluICYmIGNvbmZpZ3NbbG9hZGVyRG9tYWluXSkge1xuICAgICAgICAgICAgY29uZmlnVVJMID0gY29uZmlnc1tsb2FkZXJEb21haW5dLmNvbmZpZ1VSTDtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBjb25maWdGaWxlID0gYCR7Y29uZmlnVVJMfSR7Y29uZmlnLndpZGdldF9pZH0uanNvbj92PSR7YW50aUNhY2hlfWA7XG5cbiAgICAgICAgbGV0IGRhdGEgPSB7fTtcblxuICAgICAgICBjb25zdCB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcbiAgICAgICAgeGhyLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uIHhoclJlc3BvbnNlKCkge1xuICAgICAgICAgICAgaWYgKHhoci5yZWFkeVN0YXRlID09PSA0KSB7XG4gICAgICAgICAgICAgICAgaWYgKHhoci5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UoeGhyLnJlc3BvbnNlVGV4dCk7XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhgRXJyb3I6IGNvdWxkIG5vdCBsb2FkIGNvbmZpZ3VyYXRpb24gZmlsZSBmb3Igd2lkZ2V0IGlkOiAke2NvbmZpZy53aWRnZXRfaWR9YCwgbnVsbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhgRXJyb3I6IGNvdWxkIG5vdCBsb2FkIGNvbmZpZ3VyYXRpb24gZmlsZSBmb3Igd2lkZ2V0IGlkOiAke2NvbmZpZy53aWRnZXRfaWR9YCwgbnVsbCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZGF0YUhhbmRsZXIoZGF0YSwgY29uZmlnLCBjYWxsYmFjayk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHhoci5vcGVuKCdnZXQnLCBjb25maWdGaWxlKTtcbiAgICAgICAgeGhyLnJlc3BvbnNlVHlwZSA9ICd0ZXh0JztcbiAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoJ0FjY2VwdCcsICdhcHBsaWNhdGlvbi9qc29uOyBjaGFyc2V0PXV0Zi04Jyk7XG4gICAgICAgIHhoci5zZW5kKCk7XG4gICAgfVxufTtcblxuLy8gVE9ETzogSW4gdGhlIG5lYXIgZnV0dXJlIHdlJ3JlIGdvaW5nIHRvIGFkZCBhIHJlYWwgZG9tYWluIGNoZWNrLi4uXG4vKipcbiAqIGNoZWNrRG9tYWluXG4gKlxuICogQHBhcmFtIHNldHRpbmdzXG4gKiBAcGFyYW0gcmVmZXJyZXJcbiAqIEByZXR1cm5zIHtib29sZWFufVxuICovXG4vLyBleHBvcnQgY29uc3QgY2hlY2tEb21haW4gPSAoc2V0dGluZ3MsIHJlZmVycmVyKSA9PiB7XG4vLyAgICAgY29uc3QgcmVmZXJyZXJBcnJheSA9IHJlZmVycmVyLnN1YnN0cig3KS5yZXBsYWNlKCcvJywgJy4nKS5zcGxpdCgnLicpO1xuLy8gICAgIGxldCBsb2FkQWxsb3dlZCA9IHRydWU7IC8vIFRPRE86IE1ha2UgdGhpcyBkZWZhdWx0IHRvIGZhbHNlXG4vL1xuLy8gICAgIFtdLmZvckVhY2guY2FsbChyZWZlcnJlckFycmF5LCAocmVmKSA9PiB7XG4vLyAgICAgICAgIC8vIExvZ2ljIHRvIGJhbiBvciBhbGxvdyB0aGUgd2lkZ2V0XG4vLyAgICAgICAgIGlmIChzZXR0aW5ncy5kb21haW4pIHtcbi8vICAgICAgICAgICAgIGlmIChzZXR0aW5ncy5kb21haW4gPT09IHJlZikge1xuLy8gICAgICAgICAgICAgICAgIGxvYWRBbGxvd2VkID0gdHJ1ZTtcbi8vICAgICAgICAgICAgIH1cbi8vICAgICAgICAgfSBlbHNlIHtcbi8vICAgICAgICAgICAgIGxvYWRBbGxvd2VkID0gdHJ1ZTtcbi8vICAgICAgICAgfVxuLy8gICAgIH0pO1xuLy9cbi8vICAgICByZXR1cm4gbG9hZEFsbG93ZWQ7XG4vLyB9O1xuXG4vKipcbiAqIGluaXRpYWxpemVXaWRnZXRcbiAqXG4gKiBAcGFyYW0gZWxlbWVudFxuICogQHBhcmFtIGF0dHJpYnV0ZXNcbiAqL1xuZXhwb3J0IGNvbnN0IGluaXRpYWxpemVXaWRnZXQgPSAoZWxlbWVudCwgYXR0cmlidXRlcykgPT4ge1xuICAgIC8vIEdldCBzY3JpcHQgdGFnIGRhdGEtKiBhdHRyaWJ1dGVzXG4gICAgY29uc3QgZGF0YUF0dHJpYnV0ZXMgPSBnZXRTY3JpcHRUYWdEYXRhQXR0cmlidXRlcyhhdHRyaWJ1dGVzKTtcbiAgICBjb25zdCBsb2FkZXJEb21haW4gPSBleHRyYWN0SG9zdG5hbWUoZWxlbWVudC5zcmMpO1xuXG4gICAgbGV0IGZpbmFsQ29uZmlnID0ge307XG5cbiAgICAvLyBMb2FkIHdpZGdldCBjb25maWcganNvbiBhbmQgbWVyZ2Ugd2l0aCBzY3JpcHQgdGFnIGRhdGEtKiBhdHRyaWJ1dGVzXG4gICAgbG9hZFdpZGdldENvbmZpZyhsb2FkZXJEb21haW4sIGRhdGFBdHRyaWJ1dGVzLCAoZXJyb3IsIGNvbmZpZykgPT4ge1xuICAgICAgICBpZiAoZXJyb3IgIT09IG51bGwpIHtcbiAgICAgICAgICAgIGVsZW1lbnQucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoZXJyb3IpLCBlbGVtZW50KTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGZpbmFsQ29uZmlnID0gbWVyZ2VPYmplY3Qoe30sIGZpbmFsQ29uZmlnLCBjb25maWcpO1xuXG4gICAgICAgIC8vIExvYWQgdGhlIGlGcmFtZSB3aXRoIHJldHVybmVkIGNvbmZpZ1xuICAgICAgICBpZiAoY29uZmlnLnNwb3J0ICE9PSB1bmRlZmluZWQgJiYgY29uZmlnLnR5cGUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaWYgKGNvbmZpZy50aGVtZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgZmluYWxDb25maWcudGhlbWUgPSAnZGVmYXVsdCc7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZpbmFsQ29uZmlnLmxvYWRlckRvbWFpbiA9IGV4dHJhY3RIb3N0bmFtZShlbGVtZW50LnNyYyk7XG4gICAgICAgICAgICBmaW5hbENvbmZpZy5hY3R1YWxWZXJzaW9uID0gZXh0cmFjdEFjdHVhbFZlcnNpb24oZWxlbWVudC5zcmMpO1xuICAgICAgICAgICAgZmluYWxDb25maWcuYmFzZVVSTCA9IGVsZW1lbnQuc3JjLnJlcGxhY2UoJ2ducy53aWRnZXQuaW5pdGlhbGl6ZXIuanMnLCAnJyk7XG5cbiAgICAgICAgICAgIGxvYWRJZnJhbWVDaGVja2VyKGVsZW1lbnQsIGZpbmFsQ29uZmlnKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgbG9hZEZlYXR1cmVTd2l0Y2hlcyhleHRyYWN0SG9zdG5hbWUoZWxlbWVudC5zcmMsIHRydWUpLCAoZXJyLCBmZWF0dXJlU3dpdGNoZXJDb25maWcpID0+IHtcbiAgICAgICAgaWYgKGVyciAhPT0gbnVsbCkge1xuICAgICAgICAgICAgZWxlbWVudC5wYXJlbnROb2RlLmluc2VydEJlZm9yZShkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShlcnIpLCBlbGVtZW50KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZpbmFsQ29uZmlnLmZlYXR1cmVTd2l0Y2hlcyA9IGZlYXR1cmVTd2l0Y2hlckNvbmZpZztcbiAgICAgICAgbG9hZElmcmFtZUNoZWNrZXIoZWxlbWVudCwgZmluYWxDb25maWcpO1xuICAgIH0pO1xufTtcblxuZXhwb3J0IGNvbnN0IGxvYWRJZnJhbWVDaGVja2VyID0gKGVsZW1lbnQsIGNvbmZpZykgPT4ge1xuICAgIGlmIChjb25maWcuZmVhdHVyZVN3aXRjaGVzICYmIGNvbmZpZy53aWRnZXRfaWQpIHtcbiAgICAgICAgbGV0IHdpZGdldFVybCA9IGB3aWRnZXRzLyR7Y29uZmlnLnRoZW1lfS8ke2NvbmZpZy5zcG9ydH0vJHtjb25maWcudHlwZX0uanM/dmlld0NvdW50PTFgO1xuXG4gICAgICAgIGlmIChjb25maWcuY2xpZW50X2lkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHdpZGdldFVybCA9IGAke3dpZGdldFVybH0mYz0ke2NvbmZpZy5jbGllbnRfaWR9YDtcbiAgICAgICAgfVxuICAgICAgICBsb2FkSUZyYW1lKGVsZW1lbnQsIHdpZGdldFVybCwgY29uZmlnKTtcbiAgICB9XG59O1xuXG4vKipcbiAqIGdlbmVyYXRlSWRcbiAqXG4gKiBAcGFyYW0gbGVuZ3RoXG4gKiBAcmV0dXJucyB7c3RyaW5nfVxuICovXG5leHBvcnQgY29uc3QgZ2VuZXJhdGVJZCA9IChsZW5ndGgpID0+IHtcbiAgICBsZXQgc3RyaW5nID0gJyc7XG4gICAgY29uc3QgY2hhcnMgPSAnQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVphYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ejAxMjM0NTY3ODknO1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBsZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICBzdHJpbmcgKz0gY2hhcnMuY2hhckF0KE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIGNoYXJzLmxlbmd0aCkpO1xuICAgIH1cbiAgICByZXR1cm4gc3RyaW5nO1xufTtcblxuLyoqXG4gKiBtZXNzYWdlRXZlbnRDYWxsYmFja1xuICpcbiAqIEBwYXJhbSBldmVudFxuICovXG5leHBvcnQgY29uc3QgbWVzc2FnZUV2ZW50Q2FsbGJhY2sgPSAoZXZlbnQpID0+IHtcbiAgICBsZXQgbWVzc2FnZTtcbiAgICB0cnkge1xuICAgICAgICBtZXNzYWdlID0gSlNPTi5wYXJzZShldmVudC5kYXRhKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICBtZXNzYWdlID0gbnVsbDtcbiAgICB9XG5cbiAgICBpZiAobWVzc2FnZSkge1xuICAgICAgICBpZiAobWVzc2FnZS5hY3Rpb24pIHtcbiAgICAgICAgICAgIHN3aXRjaCAobWVzc2FnZS5hY3Rpb24pIHtcbiAgICAgICAgICAgICAgICBjYXNlICdnbnMucmVzaXplSUZyYW1lJzpcbiAgICAgICAgICAgICAgICAgICAgaWYgKG1lc3NhZ2UuaUZyYW1lSWQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGFyZ2V0SUZyYW1lID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQobWVzc2FnZS5pRnJhbWVJZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0SUZyYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcGFyZW50Wm9vbUxldmVsID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2h0bWwnKVswXSkuem9vbTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyZW50Wm9vbUxldmVsIDwgMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRJRnJhbWUuaGVpZ2h0ID0gTWF0aC5jZWlsKG1lc3NhZ2UuYm91bmRzLmhlaWdodCAvIHBhcmVudFpvb21MZXZlbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0SUZyYW1lLmhlaWdodCA9IG1lc3NhZ2UuYm91bmRzLmhlaWdodDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnZ25zLnN3aXRjaFRvV2lkZ2V0JzpcbiAgICAgICAgICAgICAgICAgICAgaWYgKG1lc3NhZ2Uuc2V0dGluZ3MuaUZyYW1lSWQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGFyZ2V0SUZyYW1lID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQobWVzc2FnZS5zZXR0aW5ncy5pRnJhbWVJZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0SUZyYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHdpZGdldFVybCA9IGB3aWRnZXRzLyR7bWVzc2FnZS5zZXR0aW5ncy50aGVtZX0vJHttZXNzYWdlLnNldHRpbmdzLnNwb3J0fS8ke21lc3NhZ2Uuc2V0dGluZ3MudHlwZX0uanM/dmlld0NvdW50PTFgO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1lc3NhZ2Uuc2V0dGluZ3MuY2xpZW50X2lkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkZ2V0VXJsID0gYCR7d2lkZ2V0VXJsfSZjPSR7bWVzc2FnZS5zZXR0aW5ncy5jbGllbnRfaWR9YDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzd2l0Y2hJRnJhbWVDb250ZW50KHdpZGdldFVybCwgbWVzc2FnZS5zZXR0aW5ncyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn07XG5cbi8qKlxuICogbG9hZFZlcnNpb25zXG4gKlxuICogQHBhcmFtIGxvYWRlclVSTFxuICogQHBhcmFtIGNhbGxiYWNrXG4gKi9cbmV4cG9ydCBjb25zdCBsb2FkVmVyc2lvbnMgPSAobG9hZGVyVVJMLCBjYWxsYmFjaykgPT4ge1xuICAgIGxldCB2ZXJzaW9ucyA9IHt9O1xuICAgIGNvbnN0IGFudGlDYWNoZSA9IGdsb2JhbC5fX0dOU19BTlRJX0NBQ0hFIHx8IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgIGdsb2JhbC5fX0dOU19BTlRJX0NBQ0hFID0gYW50aUNhY2hlO1xuICAgIGNvbnN0IGNvbmZpZ0ZpbGUgPSBgJHtsb2FkZXJVUkx9L3ZlcnNpb25zLmpzb24/dj0ke2FudGlDYWNoZX1gO1xuXG4gICAgY29uc3QgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgeGhyLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uIHhoclJlc3BvbnNlKCkge1xuICAgICAgICBpZiAoeGhyLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgIGlmICh4aHIuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICB2ZXJzaW9ucyA9IEpTT04ucGFyc2UoeGhyLnJlc3BvbnNlVGV4dCk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soJ0Vycm9yOiBjb3VsZCBub3QgbG9hZCB2ZXJzaW9ucy5qc29uJywgbnVsbCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKCdFcnJvcjogY291bGQgbm90IGxvYWQgdmVyc2lvbnMuanNvbicsIG51bGwpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhbGxiYWNrKG51bGwsIHZlcnNpb25zKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgeGhyLm9wZW4oJ2dldCcsIGNvbmZpZ0ZpbGUpO1xuICAgIHhoci5yZXNwb25zZVR5cGUgPSAndGV4dCc7XG4gICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoJ0FjY2VwdCcsICdhcHBsaWNhdGlvbi9qc29uOyBjaGFyc2V0PXV0Zi04Jyk7XG4gICAgeGhyLnNlbmQoKTtcbn07XG5cbmV4cG9ydCBjb25zdCBsb2FkRmVhdHVyZVN3aXRjaGVzID0gKGxvYWRlclVSTCwgY2FsbGJhY2spID0+IHtcbiAgICBjb25zdCBhbnRpQ2FjaGUgPSBnbG9iYWwuX19HTlNfQU5USV9DQUNIRSB8fCBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICBnbG9iYWwuX19HTlNfQU5USV9DQUNIRSA9IGFudGlDYWNoZTtcbiAgICBjb25zdCBjb25maWdGaWxlVVJMID0gYCR7bG9hZGVyVVJMfS9mZWF0dXJlX3N3aXRjaGVzLmpzb24/dj0ke2FudGlDYWNoZX1gO1xuXG4gICAgY29uc3QgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cbiAgICB4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24geGhyUmVzcG9uc2UoKSB7XG4gICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gNCkge1xuICAgICAgICAgICAgaWYgKHhoci5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKG51bGwsIEpTT04ucGFyc2UoeGhyLnJlc3BvbnNlVGV4dCkpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCdFcnJvcjogY291bGQgbm90IGxvYWQgZmVhdHVyZSBzd2l0Y2hlcyBjb25maWcgZmlsZScsIG51bGwpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2soJ0Vycm9yOiBjb3VsZCBub3QgbG9hZCBmZWF0dXJlIHN3aXRjaGVzIGNvbmZpZyBmaWxlJywgbnVsbCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgeGhyLm9wZW4oJ2dldCcsIGNvbmZpZ0ZpbGVVUkwpO1xuICAgIHhoci5yZXNwb25zZVR5cGUgPSAndGV4dCc7XG4gICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoJ0FjY2VwdCcsICdhcHBsaWNhdGlvbi9qc29uOyBjaGFyc2V0PXV0Zi04Jyk7XG4gICAgeGhyLnNlbmQoKTtcbn07XG5cbi8qKlxuICogZXh0cmFjdEhvc3RuYW1lXG4gKlxuICogQHBhcmFtIHVybFxuICogQHBhcmFtIGtlZXBQcm90b2NvbFxuICogQHJldHVybnMgeyp9XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBleHRyYWN0SG9zdG5hbWUodXJsLCBrZWVwUHJvdG9jb2wgPSBmYWxzZSkge1xuICAgIGxldCBob3N0bmFtZTtcblxuICAgIC8vIGZpbmQgJiByZW1vdmUgcHJvdG9jb2wgKGh0dHAsIGZ0cCwgZXRjLikgYW5kIGdldCBob3N0bmFtZVxuICAgIGlmICh1cmwuaW5kZXhPZignOi8vJykgPiAtMSkge1xuICAgICAgICBob3N0bmFtZSA9IHVybC5zcGxpdCgnLycpWzJdO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGhvc3RuYW1lID0gdXJsLnNwbGl0KCcvJylbMF07XG4gICAgfVxuXG4gICAgLy8gZmluZCAmIHJlbW92ZSBwb3J0IG51bWJlclxuICAgIGNvbnN0IHBvcnQgPSBob3N0bmFtZS5zcGxpdCgnOicpWzFdO1xuICAgIGhvc3RuYW1lID0gaG9zdG5hbWUuc3BsaXQoJzonKVswXTtcblxuICAgIC8vIGZpbmQgJiByZW1vdmUgXCI/XCJcbiAgICBob3N0bmFtZSA9IGhvc3RuYW1lLnNwbGl0KCc/JylbMF07XG5cbiAgICBpZiAodXJsLmluZGV4T2YoJzovLycpID4gLTEgJiYga2VlcFByb3RvY29sKSB7XG4gICAgICAgIGhvc3RuYW1lID0gYCR7dXJsLnNwbGl0KCcvJylbMF19Ly8ke2hvc3RuYW1lfWA7XG4gICAgICAgIGlmIChwb3J0KSB7XG4gICAgICAgICAgICBob3N0bmFtZSA9IGAke2hvc3RuYW1lfToke3BvcnR9YDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBob3N0bmFtZTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGV4dHJhY3RBY3R1YWxWZXJzaW9uKHVybCkge1xuICAgIGxldCBhY3R1YWxWZXJzaW9uO1xuXG4gICAgLy8gUmVtb3ZlIGluaXRpYWxpemVyIHBhcnRcbiAgICBjb25zdCBjbGVhblVybCA9IHVybC5yZXBsYWNlKCcvZ25zLndpZGdldC5pbml0aWFsaXplci5qcycsICcnKTtcblxuICAgIC8vIGZpbmQgJiByZW1vdmUgcHJvdG9jb2wgKGh0dHAsIGZ0cCwgZXRjLikgYW5kIGdldCBob3N0bmFtZVxuXG4gICAgaWYgKGNsZWFuVXJsLmluZGV4T2YoJzovLycpID4gLTEpIHtcbiAgICAgICAgYWN0dWFsVmVyc2lvbiA9IGNsZWFuVXJsLnNwbGl0KCcvJylbM107XG4gICAgfSBlbHNlIHtcbiAgICAgICAgYWN0dWFsVmVyc2lvbiA9IGNsZWFuVXJsLnNwbGl0KCcvJylbMV07XG4gICAgfVxuXG4gICAgaWYgKCFhY3R1YWxWZXJzaW9uKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIC8vIGZpbmQgJiByZW1vdmUgcG9ydCBudW1iZXJcbiAgICBhY3R1YWxWZXJzaW9uID0gYWN0dWFsVmVyc2lvbi5zcGxpdCgnOicpWzBdO1xuXG4gICAgLy8gZmluZCAmIHJlbW92ZSBcIj9cIlxuICAgIGFjdHVhbFZlcnNpb24gPSBhY3R1YWxWZXJzaW9uLnNwbGl0KCc/JylbMF07XG5cblxuICAgIHJldHVybiBhY3R1YWxWZXJzaW9uO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdXRpbHMvbG9hZGVyL2xvYWRlci11dGlscy5qcyIsImV4cG9ydCBkZWZhdWx0IHtcbiAgICBkZWZhdWx0OiB7XG4gICAgICAgIGJhc2VVUkw6ICdodHRwczovL29nMjAxOC1kYXRhdGVzdC1hcGkuc3BvcnRzLmdyYWNlbm90ZS5jb20vc3ZjLycsXG4gICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICBtb2Nrczoge1xuICAgICAgICAgICAgYmFzZVVSTDogJ2h0dHA6Ly93aWRnZXRzLnNwb3J0cy5sYWJzLmdyYWNlbm90ZS5jb20vbW9ja3MvJyxcbiAgICAgICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICB9LFxuICAgICAgICBjb25maWdVUkw6ICdodHRwczovL3dpZGdldHMuc3BvcnRzLmdyYWNlbm90ZS5jb20vY29uZmlncy8nLFxuICAgIH0sXG4gICAgJ3dpZGdldC1idWlsZGVyLmRldic6IHtcbiAgICAgICAgYmFzZVVSTDogJ2h0dHBzOi8vb2cyMDE4LWRhdGF0ZXN0LWFwaS5zcG9ydHMuZ3JhY2Vub3RlLmNvbS9zdmMvJyxcbiAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIG1vY2tzOiB7XG4gICAgICAgICAgICBiYXNlVVJMOiAnLy93aWRnZXQtYnVpbGRlci5kZXY6MzAwMC9tb2Nrcy8nLFxuICAgICAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIH0sXG4gICAgICAgIGNvbmZpZ1VSTDogJ2h0dHBzOi8vd2lkZ2V0cy5zcG9ydHMuZ3JhY2Vub3RlLmNvbS9jb25maWdzLycsXG4gICAgfSxcbiAgICAndGVzdC53aWRnZXRzLnNwb3J0cy5sYWJzLmdyYWNlbm90ZS5jb20nOiB7XG4gICAgICAgIGJhc2VVUkw6ICdodHRwczovL29nMjAxOC1kYXRhdGVzdC1hcGkuc3BvcnRzLmdyYWNlbm90ZS5jb20vc3ZjLycsXG4gICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICBtb2Nrczoge1xuICAgICAgICAgICAgYmFzZVVSTDogJy8vdGVzdC53aWRnZXRzLnNwb3J0cy5sYWJzLmdyYWNlbm90ZS5jb20vbW9ja3MvJyxcbiAgICAgICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICB9LFxuICAgICAgICBjb25maWdVUkw6ICdodHRwczovL3dpZGdldHMuc3BvcnRzLmdyYWNlbm90ZS5jb20vY29uZmlncy8nLFxuICAgIH0sXG4gICAgJ3N0YWdpbmcud2lkZ2V0cy5zcG9ydHMuZ3JhY2Vub3RlLmNvbSc6IHtcbiAgICAgICAgYmFzZVVSTDogJ2h0dHBzOi8vb2cyMDE4LWFwaS5zcG9ydHMuZ3JhY2Vub3RlLmNvbS9zdmMvJyxcbiAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIG1vY2tzOiB7XG4gICAgICAgICAgICBiYXNlVVJMOiAnLy9zdGFnaW5nLndpZGdldHMuc3BvcnRzLmdyYWNlbm90ZS5jb20vbW9ja3MvJyxcbiAgICAgICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICB9LFxuICAgICAgICBjb25maWdVUkw6ICdodHRwczovL3dpZGdldHMuc3BvcnRzLmdyYWNlbm90ZS5jb20vY29uZmlncy8nLFxuICAgIH0sXG4gICAgJ3dpZGdldHMuc3BvcnRzLmdyYWNlbm90ZS5jb20nOiB7XG4gICAgICAgIGJhc2VVUkw6ICdodHRwczovL29nMjAxOC1hcGkuc3BvcnRzLmdyYWNlbm90ZS5jb20vc3ZjLycsXG4gICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICBtb2Nrczoge1xuICAgICAgICAgICAgYmFzZVVSTDogJy8vc3RhZ2luZy53aWRnZXRzLnNwb3J0cy5ncmFjZW5vdGUuY29tL21vY2tzLycsXG4gICAgICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgfSxcbiAgICAgICAgY29uZmlnVVJMOiAnaHR0cHM6Ly93aWRnZXRzLnNwb3J0cy5ncmFjZW5vdGUuY29tL2NvbmZpZ3MvJyxcbiAgICB9LFxuICAgICdzdGFnaW5nLXdpZGdldHMubmJjb2x5bXBpY3MuY29tJzoge1xuICAgICAgICBiYXNlVVJMOiAnaHR0cHM6Ly9zdGdhcGktZ3JhY2Vub3RlLm5iY29seW1waWNzLmNvbS9zdmMvJyxcbiAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIG1vY2tzOiB7XG4gICAgICAgICAgICBiYXNlVVJMOiAnLy9zdGFnaW5nLXdpZGdldHMubmJjb2x5bXBpY3MuY29tL21vY2tzLycsXG4gICAgICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgfSxcbiAgICAgICAgY29uZmlnVVJMOiAnaHR0cHM6Ly93aWRnZXRzLm5iY29seW1waWNzLmNvbS9jb25maWdzLycsXG4gICAgfSxcbiAgICAnd2lkZ2V0cy5uYmNvbHltcGljcy5jb20nOiB7XG4gICAgICAgIGJhc2VVUkw6ICdodHRwczovL2FwaS1ncmFjZW5vdGUubmJjb2x5bXBpY3MuY29tL3N2Yy8nLFxuICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgbW9ja3M6IHtcbiAgICAgICAgICAgIGJhc2VVUkw6ICcvL3dpZGdldHMubmJjb2x5bXBpY3MuY29tL21vY2tzLycsXG4gICAgICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgfSxcbiAgICAgICAgY29uZmlnVVJMOiAnaHR0cHM6Ly93aWRnZXRzLm5iY29seW1waWNzLmNvbS9jb25maWdzLycsXG4gICAgfSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9jb25maWdzL2dhdGV3YXkuanMiLCIndXNlIHN0cmljdCc7XG5cbnZhciBkZWZpbmUgPSByZXF1aXJlKCdkZWZpbmUtcHJvcGVydGllcycpO1xudmFyIEVTID0gcmVxdWlyZSgnZXMtYWJzdHJhY3QvZXM2Jyk7XG5cbnZhciBpbXBsZW1lbnRhdGlvbiA9IHJlcXVpcmUoJy4vaW1wbGVtZW50YXRpb24nKTtcbnZhciBnZXRQb2x5ZmlsbCA9IHJlcXVpcmUoJy4vcG9seWZpbGwnKTtcbnZhciBzaGltID0gcmVxdWlyZSgnLi9zaGltJyk7XG5cbnZhciBzbGljZSA9IEFycmF5LnByb3RvdHlwZS5zbGljZTtcblxudmFyIHBvbHlmaWxsID0gZ2V0UG9seWZpbGwoKTtcblxudmFyIGJvdW5kRmluZFNoaW0gPSBmdW5jdGlvbiBmaW5kKGFycmF5LCBwcmVkaWNhdGUpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXHRFUy5SZXF1aXJlT2JqZWN0Q29lcmNpYmxlKGFycmF5KTtcblx0dmFyIGFyZ3MgPSBzbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG5cdHJldHVybiBwb2x5ZmlsbC5hcHBseShhcnJheSwgYXJncyk7XG59O1xuXG5kZWZpbmUoYm91bmRGaW5kU2hpbSwge1xuXHRnZXRQb2x5ZmlsbDogZ2V0UG9seWZpbGwsXG5cdGltcGxlbWVudGF0aW9uOiBpbXBsZW1lbnRhdGlvbixcblx0c2hpbTogc2hpbVxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gYm91bmRGaW5kU2hpbTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9hcnJheS5wcm90b3R5cGUuZmluZC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMTVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbi8vIG1vZGlmaWVkIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL2VzLXNoaW1zL2VzNS1zaGltXG52YXIgaGFzID0gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eTtcbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG52YXIgc2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG52YXIgaXNBcmdzID0gcmVxdWlyZSgnLi9pc0FyZ3VtZW50cycpO1xudmFyIGlzRW51bWVyYWJsZSA9IE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGU7XG52YXIgaGFzRG9udEVudW1CdWcgPSAhaXNFbnVtZXJhYmxlLmNhbGwoeyB0b1N0cmluZzogbnVsbCB9LCAndG9TdHJpbmcnKTtcbnZhciBoYXNQcm90b0VudW1CdWcgPSBpc0VudW1lcmFibGUuY2FsbChmdW5jdGlvbiAoKSB7fSwgJ3Byb3RvdHlwZScpO1xudmFyIGRvbnRFbnVtcyA9IFtcblx0J3RvU3RyaW5nJyxcblx0J3RvTG9jYWxlU3RyaW5nJyxcblx0J3ZhbHVlT2YnLFxuXHQnaGFzT3duUHJvcGVydHknLFxuXHQnaXNQcm90b3R5cGVPZicsXG5cdCdwcm9wZXJ0eUlzRW51bWVyYWJsZScsXG5cdCdjb25zdHJ1Y3Rvcidcbl07XG52YXIgZXF1YWxzQ29uc3RydWN0b3JQcm90b3R5cGUgPSBmdW5jdGlvbiAobykge1xuXHR2YXIgY3RvciA9IG8uY29uc3RydWN0b3I7XG5cdHJldHVybiBjdG9yICYmIGN0b3IucHJvdG90eXBlID09PSBvO1xufTtcbnZhciBleGNsdWRlZEtleXMgPSB7XG5cdCRjb25zb2xlOiB0cnVlLFxuXHQkZXh0ZXJuYWw6IHRydWUsXG5cdCRmcmFtZTogdHJ1ZSxcblx0JGZyYW1lRWxlbWVudDogdHJ1ZSxcblx0JGZyYW1lczogdHJ1ZSxcblx0JGlubmVySGVpZ2h0OiB0cnVlLFxuXHQkaW5uZXJXaWR0aDogdHJ1ZSxcblx0JG91dGVySGVpZ2h0OiB0cnVlLFxuXHQkb3V0ZXJXaWR0aDogdHJ1ZSxcblx0JHBhZ2VYT2Zmc2V0OiB0cnVlLFxuXHQkcGFnZVlPZmZzZXQ6IHRydWUsXG5cdCRwYXJlbnQ6IHRydWUsXG5cdCRzY3JvbGxMZWZ0OiB0cnVlLFxuXHQkc2Nyb2xsVG9wOiB0cnVlLFxuXHQkc2Nyb2xsWDogdHJ1ZSxcblx0JHNjcm9sbFk6IHRydWUsXG5cdCRzZWxmOiB0cnVlLFxuXHQkd2Via2l0SW5kZXhlZERCOiB0cnVlLFxuXHQkd2Via2l0U3RvcmFnZUluZm86IHRydWUsXG5cdCR3aW5kb3c6IHRydWVcbn07XG52YXIgaGFzQXV0b21hdGlvbkVxdWFsaXR5QnVnID0gKGZ1bmN0aW9uICgpIHtcblx0LyogZ2xvYmFsIHdpbmRvdyAqL1xuXHRpZiAodHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcpIHsgcmV0dXJuIGZhbHNlOyB9XG5cdGZvciAodmFyIGsgaW4gd2luZG93KSB7XG5cdFx0dHJ5IHtcblx0XHRcdGlmICghZXhjbHVkZWRLZXlzWyckJyArIGtdICYmIGhhcy5jYWxsKHdpbmRvdywgaykgJiYgd2luZG93W2tdICE9PSBudWxsICYmIHR5cGVvZiB3aW5kb3dba10gPT09ICdvYmplY3QnKSB7XG5cdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0ZXF1YWxzQ29uc3RydWN0b3JQcm90b3R5cGUod2luZG93W2tdKTtcblx0XHRcdFx0fSBjYXRjaCAoZSkge1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fSBjYXRjaCAoZSkge1xuXHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0fVxuXHR9XG5cdHJldHVybiBmYWxzZTtcbn0oKSk7XG52YXIgZXF1YWxzQ29uc3RydWN0b3JQcm90b3R5cGVJZk5vdEJ1Z2d5ID0gZnVuY3Rpb24gKG8pIHtcblx0LyogZ2xvYmFsIHdpbmRvdyAqL1xuXHRpZiAodHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcgfHwgIWhhc0F1dG9tYXRpb25FcXVhbGl0eUJ1Zykge1xuXHRcdHJldHVybiBlcXVhbHNDb25zdHJ1Y3RvclByb3RvdHlwZShvKTtcblx0fVxuXHR0cnkge1xuXHRcdHJldHVybiBlcXVhbHNDb25zdHJ1Y3RvclByb3RvdHlwZShvKTtcblx0fSBjYXRjaCAoZSkge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fVxufTtcblxudmFyIGtleXNTaGltID0gZnVuY3Rpb24ga2V5cyhvYmplY3QpIHtcblx0dmFyIGlzT2JqZWN0ID0gb2JqZWN0ICE9PSBudWxsICYmIHR5cGVvZiBvYmplY3QgPT09ICdvYmplY3QnO1xuXHR2YXIgaXNGdW5jdGlvbiA9IHRvU3RyLmNhbGwob2JqZWN0KSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcblx0dmFyIGlzQXJndW1lbnRzID0gaXNBcmdzKG9iamVjdCk7XG5cdHZhciBpc1N0cmluZyA9IGlzT2JqZWN0ICYmIHRvU3RyLmNhbGwob2JqZWN0KSA9PT0gJ1tvYmplY3QgU3RyaW5nXSc7XG5cdHZhciB0aGVLZXlzID0gW107XG5cblx0aWYgKCFpc09iamVjdCAmJiAhaXNGdW5jdGlvbiAmJiAhaXNBcmd1bWVudHMpIHtcblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdPYmplY3Qua2V5cyBjYWxsZWQgb24gYSBub24tb2JqZWN0Jyk7XG5cdH1cblxuXHR2YXIgc2tpcFByb3RvID0gaGFzUHJvdG9FbnVtQnVnICYmIGlzRnVuY3Rpb247XG5cdGlmIChpc1N0cmluZyAmJiBvYmplY3QubGVuZ3RoID4gMCAmJiAhaGFzLmNhbGwob2JqZWN0LCAwKSkge1xuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgb2JqZWN0Lmxlbmd0aDsgKytpKSB7XG5cdFx0XHR0aGVLZXlzLnB1c2goU3RyaW5nKGkpKTtcblx0XHR9XG5cdH1cblxuXHRpZiAoaXNBcmd1bWVudHMgJiYgb2JqZWN0Lmxlbmd0aCA+IDApIHtcblx0XHRmb3IgKHZhciBqID0gMDsgaiA8IG9iamVjdC5sZW5ndGg7ICsraikge1xuXHRcdFx0dGhlS2V5cy5wdXNoKFN0cmluZyhqKSk7XG5cdFx0fVxuXHR9IGVsc2Uge1xuXHRcdGZvciAodmFyIG5hbWUgaW4gb2JqZWN0KSB7XG5cdFx0XHRpZiAoIShza2lwUHJvdG8gJiYgbmFtZSA9PT0gJ3Byb3RvdHlwZScpICYmIGhhcy5jYWxsKG9iamVjdCwgbmFtZSkpIHtcblx0XHRcdFx0dGhlS2V5cy5wdXNoKFN0cmluZyhuYW1lKSk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0aWYgKGhhc0RvbnRFbnVtQnVnKSB7XG5cdFx0dmFyIHNraXBDb25zdHJ1Y3RvciA9IGVxdWFsc0NvbnN0cnVjdG9yUHJvdG90eXBlSWZOb3RCdWdneShvYmplY3QpO1xuXG5cdFx0Zm9yICh2YXIgayA9IDA7IGsgPCBkb250RW51bXMubGVuZ3RoOyArK2spIHtcblx0XHRcdGlmICghKHNraXBDb25zdHJ1Y3RvciAmJiBkb250RW51bXNba10gPT09ICdjb25zdHJ1Y3RvcicpICYmIGhhcy5jYWxsKG9iamVjdCwgZG9udEVudW1zW2tdKSkge1xuXHRcdFx0XHR0aGVLZXlzLnB1c2goZG9udEVudW1zW2tdKTtcblx0XHRcdH1cblx0XHR9XG5cdH1cblx0cmV0dXJuIHRoZUtleXM7XG59O1xuXG5rZXlzU2hpbS5zaGltID0gZnVuY3Rpb24gc2hpbU9iamVjdEtleXMoKSB7XG5cdGlmIChPYmplY3Qua2V5cykge1xuXHRcdHZhciBrZXlzV29ya3NXaXRoQXJndW1lbnRzID0gKGZ1bmN0aW9uICgpIHtcblx0XHRcdC8vIFNhZmFyaSA1LjAgYnVnXG5cdFx0XHRyZXR1cm4gKE9iamVjdC5rZXlzKGFyZ3VtZW50cykgfHwgJycpLmxlbmd0aCA9PT0gMjtcblx0XHR9KDEsIDIpKTtcblx0XHRpZiAoIWtleXNXb3Jrc1dpdGhBcmd1bWVudHMpIHtcblx0XHRcdHZhciBvcmlnaW5hbEtleXMgPSBPYmplY3Qua2V5cztcblx0XHRcdE9iamVjdC5rZXlzID0gZnVuY3Rpb24ga2V5cyhvYmplY3QpIHtcblx0XHRcdFx0aWYgKGlzQXJncyhvYmplY3QpKSB7XG5cdFx0XHRcdFx0cmV0dXJuIG9yaWdpbmFsS2V5cyhzbGljZS5jYWxsKG9iamVjdCkpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHJldHVybiBvcmlnaW5hbEtleXMob2JqZWN0KTtcblx0XHRcdFx0fVxuXHRcdFx0fTtcblx0XHR9XG5cdH0gZWxzZSB7XG5cdFx0T2JqZWN0LmtleXMgPSBrZXlzU2hpbTtcblx0fVxuXHRyZXR1cm4gT2JqZWN0LmtleXMgfHwga2V5c1NoaW07XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGtleXNTaGltO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL29iamVjdC1rZXlzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAxNlxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc0FyZ3VtZW50cyh2YWx1ZSkge1xuXHR2YXIgc3RyID0gdG9TdHIuY2FsbCh2YWx1ZSk7XG5cdHZhciBpc0FyZ3MgPSBzdHIgPT09ICdbb2JqZWN0IEFyZ3VtZW50c10nO1xuXHRpZiAoIWlzQXJncykge1xuXHRcdGlzQXJncyA9IHN0ciAhPT0gJ1tvYmplY3QgQXJyYXldJyAmJlxuXHRcdFx0dmFsdWUgIT09IG51bGwgJiZcblx0XHRcdHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiZcblx0XHRcdHR5cGVvZiB2YWx1ZS5sZW5ndGggPT09ICdudW1iZXInICYmXG5cdFx0XHR2YWx1ZS5sZW5ndGggPj0gMCAmJlxuXHRcdFx0dG9TdHIuY2FsbCh2YWx1ZS5jYWxsZWUpID09PSAnW29iamVjdCBGdW5jdGlvbl0nO1xuXHR9XG5cdHJldHVybiBpc0FyZ3M7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL29iamVjdC1rZXlzL2lzQXJndW1lbnRzLmpzXG4vLyBtb2R1bGUgaWQgPSAxN1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIlxudmFyIGhhc093biA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHk7XG52YXIgdG9TdHJpbmcgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGZvckVhY2ggKG9iaiwgZm4sIGN0eCkge1xuICAgIGlmICh0b1N0cmluZy5jYWxsKGZuKSAhPT0gJ1tvYmplY3QgRnVuY3Rpb25dJykge1xuICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdpdGVyYXRvciBtdXN0IGJlIGEgZnVuY3Rpb24nKTtcbiAgICB9XG4gICAgdmFyIGwgPSBvYmoubGVuZ3RoO1xuICAgIGlmIChsID09PSArbCkge1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGw7IGkrKykge1xuICAgICAgICAgICAgZm4uY2FsbChjdHgsIG9ialtpXSwgaSwgb2JqKTtcbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIGZvciAodmFyIGsgaW4gb2JqKSB7XG4gICAgICAgICAgICBpZiAoaGFzT3duLmNhbGwob2JqLCBrKSkge1xuICAgICAgICAgICAgICAgIGZuLmNhbGwoY3R4LCBvYmpba10sIGssIG9iaik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59O1xuXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZm9yZWFjaC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMThcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciBoYXMgPSByZXF1aXJlKCdoYXMnKTtcbnZhciB0b1ByaW1pdGl2ZSA9IHJlcXVpcmUoJ2VzLXRvLXByaW1pdGl2ZS9lczYnKTtcblxudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcbnZhciBoYXNTeW1ib2xzID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSAnc3ltYm9sJztcblxudmFyICRpc05hTiA9IHJlcXVpcmUoJy4vaGVscGVycy9pc05hTicpO1xudmFyICRpc0Zpbml0ZSA9IHJlcXVpcmUoJy4vaGVscGVycy9pc0Zpbml0ZScpO1xudmFyIE1BWF9TQUZFX0lOVEVHRVIgPSBOdW1iZXIuTUFYX1NBRkVfSU5URUdFUiB8fCBNYXRoLnBvdygyLCA1MykgLSAxO1xuXG52YXIgYXNzaWduID0gcmVxdWlyZSgnLi9oZWxwZXJzL2Fzc2lnbicpO1xudmFyIHNpZ24gPSByZXF1aXJlKCcuL2hlbHBlcnMvc2lnbicpO1xudmFyIG1vZCA9IHJlcXVpcmUoJy4vaGVscGVycy9tb2QnKTtcbnZhciBpc1ByaW1pdGl2ZSA9IHJlcXVpcmUoJy4vaGVscGVycy9pc1ByaW1pdGl2ZScpO1xudmFyIHBhcnNlSW50ZWdlciA9IHBhcnNlSW50O1xudmFyIGJpbmQgPSByZXF1aXJlKCdmdW5jdGlvbi1iaW5kJyk7XG52YXIgYXJyYXlTbGljZSA9IGJpbmQuY2FsbChGdW5jdGlvbi5jYWxsLCBBcnJheS5wcm90b3R5cGUuc2xpY2UpO1xudmFyIHN0clNsaWNlID0gYmluZC5jYWxsKEZ1bmN0aW9uLmNhbGwsIFN0cmluZy5wcm90b3R5cGUuc2xpY2UpO1xudmFyIGlzQmluYXJ5ID0gYmluZC5jYWxsKEZ1bmN0aW9uLmNhbGwsIFJlZ0V4cC5wcm90b3R5cGUudGVzdCwgL14wYlswMV0rJC9pKTtcbnZhciBpc09jdGFsID0gYmluZC5jYWxsKEZ1bmN0aW9uLmNhbGwsIFJlZ0V4cC5wcm90b3R5cGUudGVzdCwgL14wb1swLTddKyQvaSk7XG52YXIgcmVnZXhFeGVjID0gYmluZC5jYWxsKEZ1bmN0aW9uLmNhbGwsIFJlZ0V4cC5wcm90b3R5cGUuZXhlYyk7XG52YXIgbm9uV1MgPSBbJ1xcdTAwODUnLCAnXFx1MjAwYicsICdcXHVmZmZlJ10uam9pbignJyk7XG52YXIgbm9uV1NyZWdleCA9IG5ldyBSZWdFeHAoJ1snICsgbm9uV1MgKyAnXScsICdnJyk7XG52YXIgaGFzTm9uV1MgPSBiaW5kLmNhbGwoRnVuY3Rpb24uY2FsbCwgUmVnRXhwLnByb3RvdHlwZS50ZXN0LCBub25XU3JlZ2V4KTtcbnZhciBpbnZhbGlkSGV4TGl0ZXJhbCA9IC9eWy0rXTB4WzAtOWEtZl0rJC9pO1xudmFyIGlzSW52YWxpZEhleExpdGVyYWwgPSBiaW5kLmNhbGwoRnVuY3Rpb24uY2FsbCwgUmVnRXhwLnByb3RvdHlwZS50ZXN0LCBpbnZhbGlkSGV4TGl0ZXJhbCk7XG5cbi8vIHdoaXRlc3BhY2UgZnJvbTogaHR0cDovL2VzNS5naXRodWIuaW8vI3gxNS41LjQuMjBcbi8vIGltcGxlbWVudGF0aW9uIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL2VzLXNoaW1zL2VzNS1zaGltL2Jsb2IvdjMuNC4wL2VzNS1zaGltLmpzI0wxMzA0LUwxMzI0XG52YXIgd3MgPSBbXG5cdCdcXHgwOVxceDBBXFx4MEJcXHgwQ1xceDBEXFx4MjBcXHhBMFxcdTE2ODBcXHUxODBFXFx1MjAwMFxcdTIwMDFcXHUyMDAyXFx1MjAwMycsXG5cdCdcXHUyMDA0XFx1MjAwNVxcdTIwMDZcXHUyMDA3XFx1MjAwOFxcdTIwMDlcXHUyMDBBXFx1MjAyRlxcdTIwNUZcXHUzMDAwXFx1MjAyOCcsXG5cdCdcXHUyMDI5XFx1RkVGRidcbl0uam9pbignJyk7XG52YXIgdHJpbVJlZ2V4ID0gbmV3IFJlZ0V4cCgnKF5bJyArIHdzICsgJ10rKXwoWycgKyB3cyArICddKyQpJywgJ2cnKTtcbnZhciByZXBsYWNlID0gYmluZC5jYWxsKEZ1bmN0aW9uLmNhbGwsIFN0cmluZy5wcm90b3R5cGUucmVwbGFjZSk7XG52YXIgdHJpbSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuXHRyZXR1cm4gcmVwbGFjZSh2YWx1ZSwgdHJpbVJlZ2V4LCAnJyk7XG59O1xuXG52YXIgRVM1ID0gcmVxdWlyZSgnLi9lczUnKTtcblxudmFyIGhhc1JlZ0V4cE1hdGNoZXIgPSByZXF1aXJlKCdpcy1yZWdleCcpO1xuXG4vLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtYWJzdHJhY3Qtb3BlcmF0aW9uc1xudmFyIEVTNiA9IGFzc2lnbihhc3NpZ24oe30sIEVTNSksIHtcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtY2FsbC1mLXYtYXJnc1xuXHRDYWxsOiBmdW5jdGlvbiBDYWxsKEYsIFYpIHtcblx0XHR2YXIgYXJncyA9IGFyZ3VtZW50cy5sZW5ndGggPiAyID8gYXJndW1lbnRzWzJdIDogW107XG5cdFx0aWYgKCF0aGlzLklzQ2FsbGFibGUoRikpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoRiArICcgaXMgbm90IGEgZnVuY3Rpb24nKTtcblx0XHR9XG5cdFx0cmV0dXJuIEYuYXBwbHkoViwgYXJncyk7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvcHJpbWl0aXZlXG5cdFRvUHJpbWl0aXZlOiB0b1ByaW1pdGl2ZSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG9ib29sZWFuXG5cdC8vIFRvQm9vbGVhbjogRVM1LlRvQm9vbGVhbixcblxuXHQvLyBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtdG9udW1iZXJcblx0VG9OdW1iZXI6IGZ1bmN0aW9uIFRvTnVtYmVyKGFyZ3VtZW50KSB7XG5cdFx0dmFyIHZhbHVlID0gaXNQcmltaXRpdmUoYXJndW1lbnQpID8gYXJndW1lbnQgOiB0b1ByaW1pdGl2ZShhcmd1bWVudCwgTnVtYmVyKTtcblx0XHRpZiAodHlwZW9mIHZhbHVlID09PSAnc3ltYm9sJykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQ2Fubm90IGNvbnZlcnQgYSBTeW1ib2wgdmFsdWUgdG8gYSBudW1iZXInKTtcblx0XHR9XG5cdFx0aWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycpIHtcblx0XHRcdGlmIChpc0JpbmFyeSh2YWx1ZSkpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuVG9OdW1iZXIocGFyc2VJbnRlZ2VyKHN0clNsaWNlKHZhbHVlLCAyKSwgMikpO1xuXHRcdFx0fSBlbHNlIGlmIChpc09jdGFsKHZhbHVlKSkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5Ub051bWJlcihwYXJzZUludGVnZXIoc3RyU2xpY2UodmFsdWUsIDIpLCA4KSk7XG5cdFx0XHR9IGVsc2UgaWYgKGhhc05vbldTKHZhbHVlKSB8fCBpc0ludmFsaWRIZXhMaXRlcmFsKHZhbHVlKSkge1xuXHRcdFx0XHRyZXR1cm4gTmFOO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0dmFyIHRyaW1tZWQgPSB0cmltKHZhbHVlKTtcblx0XHRcdFx0aWYgKHRyaW1tZWQgIT09IHZhbHVlKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHRoaXMuVG9OdW1iZXIodHJpbW1lZCk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdFx0cmV0dXJuIE51bWJlcih2YWx1ZSk7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvaW50ZWdlclxuXHQvLyBUb0ludGVnZXI6IEVTNS5Ub051bWJlcixcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG9pbnQzMlxuXHQvLyBUb0ludDMyOiBFUzUuVG9JbnQzMixcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG91aW50MzJcblx0Ly8gVG9VaW50MzI6IEVTNS5Ub1VpbnQzMixcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG9pbnQxNlxuXHRUb0ludDE2OiBmdW5jdGlvbiBUb0ludDE2KGFyZ3VtZW50KSB7XG5cdFx0dmFyIGludDE2Yml0ID0gdGhpcy5Ub1VpbnQxNihhcmd1bWVudCk7XG5cdFx0cmV0dXJuIGludDE2Yml0ID49IDB4ODAwMCA/IGludDE2Yml0IC0gMHgxMDAwMCA6IGludDE2Yml0O1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b3VpbnQxNlxuXHQvLyBUb1VpbnQxNjogRVM1LlRvVWludDE2LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b2ludDhcblx0VG9JbnQ4OiBmdW5jdGlvbiBUb0ludDgoYXJndW1lbnQpIHtcblx0XHR2YXIgaW50OGJpdCA9IHRoaXMuVG9VaW50OChhcmd1bWVudCk7XG5cdFx0cmV0dXJuIGludDhiaXQgPj0gMHg4MCA/IGludDhiaXQgLSAweDEwMCA6IGludDhiaXQ7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvdWludDhcblx0VG9VaW50ODogZnVuY3Rpb24gVG9VaW50OChhcmd1bWVudCkge1xuXHRcdHZhciBudW1iZXIgPSB0aGlzLlRvTnVtYmVyKGFyZ3VtZW50KTtcblx0XHRpZiAoJGlzTmFOKG51bWJlcikgfHwgbnVtYmVyID09PSAwIHx8ICEkaXNGaW5pdGUobnVtYmVyKSkgeyByZXR1cm4gMDsgfVxuXHRcdHZhciBwb3NJbnQgPSBzaWduKG51bWJlcikgKiBNYXRoLmZsb29yKE1hdGguYWJzKG51bWJlcikpO1xuXHRcdHJldHVybiBtb2QocG9zSW50LCAweDEwMCk7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvdWludDhjbGFtcFxuXHRUb1VpbnQ4Q2xhbXA6IGZ1bmN0aW9uIFRvVWludDhDbGFtcChhcmd1bWVudCkge1xuXHRcdHZhciBudW1iZXIgPSB0aGlzLlRvTnVtYmVyKGFyZ3VtZW50KTtcblx0XHRpZiAoJGlzTmFOKG51bWJlcikgfHwgbnVtYmVyIDw9IDApIHsgcmV0dXJuIDA7IH1cblx0XHRpZiAobnVtYmVyID49IDB4RkYpIHsgcmV0dXJuIDB4RkY7IH1cblx0XHR2YXIgZiA9IE1hdGguZmxvb3IoYXJndW1lbnQpO1xuXHRcdGlmIChmICsgMC41IDwgbnVtYmVyKSB7IHJldHVybiBmICsgMTsgfVxuXHRcdGlmIChudW1iZXIgPCBmICsgMC41KSB7IHJldHVybiBmOyB9XG5cdFx0aWYgKGYgJSAyICE9PSAwKSB7IHJldHVybiBmICsgMTsgfVxuXHRcdHJldHVybiBmO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b3N0cmluZ1xuXHRUb1N0cmluZzogZnVuY3Rpb24gVG9TdHJpbmcoYXJndW1lbnQpIHtcblx0XHRpZiAodHlwZW9mIGFyZ3VtZW50ID09PSAnc3ltYm9sJykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQ2Fubm90IGNvbnZlcnQgYSBTeW1ib2wgdmFsdWUgdG8gYSBzdHJpbmcnKTtcblx0XHR9XG5cdFx0cmV0dXJuIFN0cmluZyhhcmd1bWVudCk7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvb2JqZWN0XG5cdFRvT2JqZWN0OiBmdW5jdGlvbiBUb09iamVjdCh2YWx1ZSkge1xuXHRcdHRoaXMuUmVxdWlyZU9iamVjdENvZXJjaWJsZSh2YWx1ZSk7XG5cdFx0cmV0dXJuIE9iamVjdCh2YWx1ZSk7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvcHJvcGVydHlrZXlcblx0VG9Qcm9wZXJ0eUtleTogZnVuY3Rpb24gVG9Qcm9wZXJ0eUtleShhcmd1bWVudCkge1xuXHRcdHZhciBrZXkgPSB0aGlzLlRvUHJpbWl0aXZlKGFyZ3VtZW50LCBTdHJpbmcpO1xuXHRcdHJldHVybiB0eXBlb2Yga2V5ID09PSAnc3ltYm9sJyA/IGtleSA6IHRoaXMuVG9TdHJpbmcoa2V5KTtcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG9sZW5ndGhcblx0VG9MZW5ndGg6IGZ1bmN0aW9uIFRvTGVuZ3RoKGFyZ3VtZW50KSB7XG5cdFx0dmFyIGxlbiA9IHRoaXMuVG9JbnRlZ2VyKGFyZ3VtZW50KTtcblx0XHRpZiAobGVuIDw9IDApIHsgcmV0dXJuIDA7IH0gLy8gaW5jbHVkZXMgY29udmVydGluZyAtMCB0byArMFxuXHRcdGlmIChsZW4gPiBNQVhfU0FGRV9JTlRFR0VSKSB7IHJldHVybiBNQVhfU0FGRV9JTlRFR0VSOyB9XG5cdFx0cmV0dXJuIGxlbjtcblx0fSxcblxuXHQvLyBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtY2Fub25pY2FsbnVtZXJpY2luZGV4c3RyaW5nXG5cdENhbm9uaWNhbE51bWVyaWNJbmRleFN0cmluZzogZnVuY3Rpb24gQ2Fub25pY2FsTnVtZXJpY0luZGV4U3RyaW5nKGFyZ3VtZW50KSB7XG5cdFx0aWYgKHRvU3RyLmNhbGwoYXJndW1lbnQpICE9PSAnW29iamVjdCBTdHJpbmddJykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignbXVzdCBiZSBhIHN0cmluZycpO1xuXHRcdH1cblx0XHRpZiAoYXJndW1lbnQgPT09ICctMCcpIHsgcmV0dXJuIC0wOyB9XG5cdFx0dmFyIG4gPSB0aGlzLlRvTnVtYmVyKGFyZ3VtZW50KTtcblx0XHRpZiAodGhpcy5TYW1lVmFsdWUodGhpcy5Ub1N0cmluZyhuKSwgYXJndW1lbnQpKSB7IHJldHVybiBuOyB9XG5cdFx0cmV0dXJuIHZvaWQgMDtcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtcmVxdWlyZW9iamVjdGNvZXJjaWJsZVxuXHRSZXF1aXJlT2JqZWN0Q29lcmNpYmxlOiBFUzUuQ2hlY2tPYmplY3RDb2VyY2libGUsXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLWlzYXJyYXlcblx0SXNBcnJheTogQXJyYXkuaXNBcnJheSB8fCBmdW5jdGlvbiBJc0FycmF5KGFyZ3VtZW50KSB7XG5cdFx0cmV0dXJuIHRvU3RyLmNhbGwoYXJndW1lbnQpID09PSAnW29iamVjdCBBcnJheV0nO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy1pc2NhbGxhYmxlXG5cdC8vIElzQ2FsbGFibGU6IEVTNS5Jc0NhbGxhYmxlLFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy1pc2NvbnN0cnVjdG9yXG5cdElzQ29uc3RydWN0b3I6IGZ1bmN0aW9uIElzQ29uc3RydWN0b3IoYXJndW1lbnQpIHtcblx0XHRyZXR1cm4gdHlwZW9mIGFyZ3VtZW50ID09PSAnZnVuY3Rpb24nICYmICEhYXJndW1lbnQucHJvdG90eXBlOyAvLyB1bmZvcnR1bmF0ZWx5IHRoZXJlJ3Mgbm8gd2F5IHRvIHRydWx5IGNoZWNrIHRoaXMgd2l0aG91dCB0cnkvY2F0Y2ggYG5ldyBhcmd1bWVudGBcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtaXNleHRlbnNpYmxlLW9cblx0SXNFeHRlbnNpYmxlOiBmdW5jdGlvbiBJc0V4dGVuc2libGUob2JqKSB7XG5cdFx0aWYgKCFPYmplY3QucHJldmVudEV4dGVuc2lvbnMpIHsgcmV0dXJuIHRydWU7IH1cblx0XHRpZiAoaXNQcmltaXRpdmUob2JqKSkge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0XHRyZXR1cm4gT2JqZWN0LmlzRXh0ZW5zaWJsZShvYmopO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy1pc2ludGVnZXJcblx0SXNJbnRlZ2VyOiBmdW5jdGlvbiBJc0ludGVnZXIoYXJndW1lbnQpIHtcblx0XHRpZiAodHlwZW9mIGFyZ3VtZW50ICE9PSAnbnVtYmVyJyB8fCAkaXNOYU4oYXJndW1lbnQpIHx8ICEkaXNGaW5pdGUoYXJndW1lbnQpKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXHRcdHZhciBhYnMgPSBNYXRoLmFicyhhcmd1bWVudCk7XG5cdFx0cmV0dXJuIE1hdGguZmxvb3IoYWJzKSA9PT0gYWJzO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy1pc3Byb3BlcnR5a2V5XG5cdElzUHJvcGVydHlLZXk6IGZ1bmN0aW9uIElzUHJvcGVydHlLZXkoYXJndW1lbnQpIHtcblx0XHRyZXR1cm4gdHlwZW9mIGFyZ3VtZW50ID09PSAnc3RyaW5nJyB8fCB0eXBlb2YgYXJndW1lbnQgPT09ICdzeW1ib2wnO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly93d3cuZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1pc3JlZ2V4cFxuXHRJc1JlZ0V4cDogZnVuY3Rpb24gSXNSZWdFeHAoYXJndW1lbnQpIHtcblx0XHRpZiAoIWFyZ3VtZW50IHx8IHR5cGVvZiBhcmd1bWVudCAhPT0gJ29iamVjdCcpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cdFx0aWYgKGhhc1N5bWJvbHMpIHtcblx0XHRcdHZhciBpc1JlZ0V4cCA9IGFyZ3VtZW50W1N5bWJvbC5tYXRjaF07XG5cdFx0XHRpZiAodHlwZW9mIGlzUmVnRXhwICE9PSAndW5kZWZpbmVkJykge1xuXHRcdFx0XHRyZXR1cm4gRVM1LlRvQm9vbGVhbihpc1JlZ0V4cCk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdHJldHVybiBoYXNSZWdFeHBNYXRjaGVyKGFyZ3VtZW50KTtcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtc2FtZXZhbHVlXG5cdC8vIFNhbWVWYWx1ZTogRVM1LlNhbWVWYWx1ZSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtc2FtZXZhbHVlemVyb1xuXHRTYW1lVmFsdWVaZXJvOiBmdW5jdGlvbiBTYW1lVmFsdWVaZXJvKHgsIHkpIHtcblx0XHRyZXR1cm4gKHggPT09IHkpIHx8ICgkaXNOYU4oeCkgJiYgJGlzTmFOKHkpKTtcblx0fSxcblxuXHQvKipcblx0ICogNy4zLjIgR2V0ViAoViwgUClcblx0ICogMS4gQXNzZXJ0OiBJc1Byb3BlcnR5S2V5KFApIGlzIHRydWUuXG5cdCAqIDIuIExldCBPIGJlIFRvT2JqZWN0KFYpLlxuXHQgKiAzLiBSZXR1cm5JZkFicnVwdChPKS5cblx0ICogNC4gUmV0dXJuIE8uW1tHZXRdXShQLCBWKS5cblx0ICovXG5cdEdldFY6IGZ1bmN0aW9uIEdldFYoViwgUCkge1xuXHRcdC8vIDcuMy4yLjFcblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eUtleShQKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogSXNQcm9wZXJ0eUtleShQKSBpcyBub3QgdHJ1ZScpO1xuXHRcdH1cblxuXHRcdC8vIDcuMy4yLjItM1xuXHRcdHZhciBPID0gdGhpcy5Ub09iamVjdChWKTtcblxuXHRcdC8vIDcuMy4yLjRcblx0XHRyZXR1cm4gT1tQXTtcblx0fSxcblxuXHQvKipcblx0ICogNy4zLjkgLSBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtZ2V0bWV0aG9kXG5cdCAqIDEuIEFzc2VydDogSXNQcm9wZXJ0eUtleShQKSBpcyB0cnVlLlxuXHQgKiAyLiBMZXQgZnVuYyBiZSBHZXRWKE8sIFApLlxuXHQgKiAzLiBSZXR1cm5JZkFicnVwdChmdW5jKS5cblx0ICogNC4gSWYgZnVuYyBpcyBlaXRoZXIgdW5kZWZpbmVkIG9yIG51bGwsIHJldHVybiB1bmRlZmluZWQuXG5cdCAqIDUuIElmIElzQ2FsbGFibGUoZnVuYykgaXMgZmFsc2UsIHRocm93IGEgVHlwZUVycm9yIGV4Y2VwdGlvbi5cblx0ICogNi4gUmV0dXJuIGZ1bmMuXG5cdCAqL1xuXHRHZXRNZXRob2Q6IGZ1bmN0aW9uIEdldE1ldGhvZChPLCBQKSB7XG5cdFx0Ly8gNy4zLjkuMVxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBJc1Byb3BlcnR5S2V5KFApIGlzIG5vdCB0cnVlJyk7XG5cdFx0fVxuXG5cdFx0Ly8gNy4zLjkuMlxuXHRcdHZhciBmdW5jID0gdGhpcy5HZXRWKE8sIFApO1xuXG5cdFx0Ly8gNy4zLjkuNFxuXHRcdGlmIChmdW5jID09IG51bGwpIHtcblx0XHRcdHJldHVybiB2b2lkIDA7XG5cdFx0fVxuXG5cdFx0Ly8gNy4zLjkuNVxuXHRcdGlmICghdGhpcy5Jc0NhbGxhYmxlKGZ1bmMpKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKFAgKyAnaXMgbm90IGEgZnVuY3Rpb24nKTtcblx0XHR9XG5cblx0XHQvLyA3LjMuOS42XG5cdFx0cmV0dXJuIGZ1bmM7XG5cdH0sXG5cblx0LyoqXG5cdCAqIDcuMy4xIEdldCAoTywgUCkgLSBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtZ2V0LW8tcFxuXHQgKiAxLiBBc3NlcnQ6IFR5cGUoTykgaXMgT2JqZWN0LlxuXHQgKiAyLiBBc3NlcnQ6IElzUHJvcGVydHlLZXkoUCkgaXMgdHJ1ZS5cblx0ICogMy4gUmV0dXJuIE8uW1tHZXRdXShQLCBPKS5cblx0ICovXG5cdEdldDogZnVuY3Rpb24gR2V0KE8sIFApIHtcblx0XHQvLyA3LjMuMS4xXG5cdFx0aWYgKHRoaXMuVHlwZShPKSAhPT0gJ09iamVjdCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IFR5cGUoTykgaXMgbm90IE9iamVjdCcpO1xuXHRcdH1cblx0XHQvLyA3LjMuMS4yXG5cdFx0aWYgKCF0aGlzLklzUHJvcGVydHlLZXkoUCkpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IElzUHJvcGVydHlLZXkoUCkgaXMgbm90IHRydWUnKTtcblx0XHR9XG5cdFx0Ly8gNy4zLjEuM1xuXHRcdHJldHVybiBPW1BdO1xuXHR9LFxuXG5cdFR5cGU6IGZ1bmN0aW9uIFR5cGUoeCkge1xuXHRcdGlmICh0eXBlb2YgeCA9PT0gJ3N5bWJvbCcpIHtcblx0XHRcdHJldHVybiAnU3ltYm9sJztcblx0XHR9XG5cdFx0cmV0dXJuIEVTNS5UeXBlKHgpO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly93d3cuZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1zcGVjaWVzY29uc3RydWN0b3Jcblx0U3BlY2llc0NvbnN0cnVjdG9yOiBmdW5jdGlvbiBTcGVjaWVzQ29uc3RydWN0b3IoTywgZGVmYXVsdENvbnN0cnVjdG9yKSB7XG5cdFx0aWYgKHRoaXMuVHlwZShPKSAhPT0gJ09iamVjdCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IFR5cGUoTykgaXMgbm90IE9iamVjdCcpO1xuXHRcdH1cblx0XHR2YXIgQyA9IE8uY29uc3RydWN0b3I7XG5cdFx0aWYgKHR5cGVvZiBDID09PSAndW5kZWZpbmVkJykge1xuXHRcdFx0cmV0dXJuIGRlZmF1bHRDb25zdHJ1Y3Rvcjtcblx0XHR9XG5cdFx0aWYgKHRoaXMuVHlwZShDKSAhPT0gJ09iamVjdCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ08uY29uc3RydWN0b3IgaXMgbm90IGFuIE9iamVjdCcpO1xuXHRcdH1cblx0XHR2YXIgUyA9IGhhc1N5bWJvbHMgJiYgU3ltYm9sLnNwZWNpZXMgPyBDW1N5bWJvbC5zcGVjaWVzXSA6IHZvaWQgMDtcblx0XHRpZiAoUyA9PSBudWxsKSB7XG5cdFx0XHRyZXR1cm4gZGVmYXVsdENvbnN0cnVjdG9yO1xuXHRcdH1cblx0XHRpZiAodGhpcy5Jc0NvbnN0cnVjdG9yKFMpKSB7XG5cdFx0XHRyZXR1cm4gUztcblx0XHR9XG5cdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignbm8gY29uc3RydWN0b3IgZm91bmQnKTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1jb21wbGV0ZXByb3BlcnR5ZGVzY3JpcHRvclxuXHRDb21wbGV0ZVByb3BlcnR5RGVzY3JpcHRvcjogZnVuY3Rpb24gQ29tcGxldGVQcm9wZXJ0eURlc2NyaXB0b3IoRGVzYykge1xuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5RGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignRGVzYyBtdXN0IGJlIGEgUHJvcGVydHkgRGVzY3JpcHRvcicpO1xuXHRcdH1cblxuXHRcdGlmICh0aGlzLklzR2VuZXJpY0Rlc2NyaXB0b3IoRGVzYykgfHwgdGhpcy5Jc0RhdGFEZXNjcmlwdG9yKERlc2MpKSB7XG5cdFx0XHRpZiAoIWhhcyhEZXNjLCAnW1tWYWx1ZV1dJykpIHtcblx0XHRcdFx0RGVzY1snW1tWYWx1ZV1dJ10gPSB2b2lkIDA7XG5cdFx0XHR9XG5cdFx0XHRpZiAoIWhhcyhEZXNjLCAnW1tXcml0YWJsZV1dJykpIHtcblx0XHRcdFx0RGVzY1snW1tXcml0YWJsZV1dJ10gPSBmYWxzZTtcblx0XHRcdH1cblx0XHR9IGVsc2Uge1xuXHRcdFx0aWYgKCFoYXMoRGVzYywgJ1tbR2V0XV0nKSkge1xuXHRcdFx0XHREZXNjWydbW0dldF1dJ10gPSB2b2lkIDA7XG5cdFx0XHR9XG5cdFx0XHRpZiAoIWhhcyhEZXNjLCAnW1tTZXRdXScpKSB7XG5cdFx0XHRcdERlc2NbJ1tbU2V0XV0nXSA9IHZvaWQgMDtcblx0XHRcdH1cblx0XHR9XG5cdFx0aWYgKCFoYXMoRGVzYywgJ1tbRW51bWVyYWJsZV1dJykpIHtcblx0XHRcdERlc2NbJ1tbRW51bWVyYWJsZV1dJ10gPSBmYWxzZTtcblx0XHR9XG5cdFx0aWYgKCFoYXMoRGVzYywgJ1tbQ29uZmlndXJhYmxlXV0nKSkge1xuXHRcdFx0RGVzY1snW1tDb25maWd1cmFibGVdXSddID0gZmFsc2U7XG5cdFx0fVxuXHRcdHJldHVybiBEZXNjO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLXNldC1vLXAtdi10aHJvd1xuXHRTZXQ6IGZ1bmN0aW9uIFNldChPLCBQLCBWLCBUaHJvdykge1xuXHRcdGlmICh0aGlzLlR5cGUoTykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdPIG11c3QgYmUgYW4gT2JqZWN0Jyk7XG5cdFx0fVxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdQIG11c3QgYmUgYSBQcm9wZXJ0eSBLZXknKTtcblx0XHR9XG5cdFx0aWYgKHRoaXMuVHlwZShUaHJvdykgIT09ICdCb29sZWFuJykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignVGhyb3cgbXVzdCBiZSBhIEJvb2xlYW4nKTtcblx0XHR9XG5cdFx0aWYgKFRocm93KSB7XG5cdFx0XHRPW1BdID0gVjtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0cnkge1xuXHRcdFx0XHRPW1BdID0gVjtcblx0XHRcdH0gY2F0Y2ggKGUpIHtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXHRcdH1cblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1oYXNvd25wcm9wZXJ0eVxuXHRIYXNPd25Qcm9wZXJ0eTogZnVuY3Rpb24gSGFzT3duUHJvcGVydHkoTywgUCkge1xuXHRcdGlmICh0aGlzLlR5cGUoTykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdPIG11c3QgYmUgYW4gT2JqZWN0Jyk7XG5cdFx0fVxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdQIG11c3QgYmUgYSBQcm9wZXJ0eSBLZXknKTtcblx0XHR9XG5cdFx0cmV0dXJuIGhhcyhPLCBQKTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1oYXNwcm9wZXJ0eVxuXHRIYXNQcm9wZXJ0eTogZnVuY3Rpb24gSGFzUHJvcGVydHkoTywgUCkge1xuXHRcdGlmICh0aGlzLlR5cGUoTykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdPIG11c3QgYmUgYW4gT2JqZWN0Jyk7XG5cdFx0fVxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdQIG11c3QgYmUgYSBQcm9wZXJ0eSBLZXknKTtcblx0XHR9XG5cdFx0cmV0dXJuIFAgaW4gTztcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1pc2NvbmNhdHNwcmVhZGFibGVcblx0SXNDb25jYXRTcHJlYWRhYmxlOiBmdW5jdGlvbiBJc0NvbmNhdFNwcmVhZGFibGUoTykge1xuXHRcdGlmICh0aGlzLlR5cGUoTykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXHRcdGlmIChoYXNTeW1ib2xzICYmIHR5cGVvZiBTeW1ib2wuaXNDb25jYXRTcHJlYWRhYmxlID09PSAnc3ltYm9sJykge1xuXHRcdFx0dmFyIHNwcmVhZGFibGUgPSB0aGlzLkdldChPLCBTeW1ib2wuaXNDb25jYXRTcHJlYWRhYmxlKTtcblx0XHRcdGlmICh0eXBlb2Ygc3ByZWFkYWJsZSAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuVG9Cb29sZWFuKHNwcmVhZGFibGUpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRyZXR1cm4gdGhpcy5Jc0FycmF5KE8pO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLWludm9rZVxuXHRJbnZva2U6IGZ1bmN0aW9uIEludm9rZShPLCBQKSB7XG5cdFx0aWYgKCF0aGlzLklzUHJvcGVydHlLZXkoUCkpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ1AgbXVzdCBiZSBhIFByb3BlcnR5IEtleScpO1xuXHRcdH1cblx0XHR2YXIgYXJndW1lbnRzTGlzdCA9IGFycmF5U2xpY2UoYXJndW1lbnRzLCAyKTtcblx0XHR2YXIgZnVuYyA9IHRoaXMuR2V0VihPLCBQKTtcblx0XHRyZXR1cm4gdGhpcy5DYWxsKGZ1bmMsIE8sIGFyZ3VtZW50c0xpc3QpO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLWNyZWF0ZWl0ZXJyZXN1bHRvYmplY3Rcblx0Q3JlYXRlSXRlclJlc3VsdE9iamVjdDogZnVuY3Rpb24gQ3JlYXRlSXRlclJlc3VsdE9iamVjdCh2YWx1ZSwgZG9uZSkge1xuXHRcdGlmICh0aGlzLlR5cGUoZG9uZSkgIT09ICdCb29sZWFuJykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogVHlwZShkb25lKSBpcyBub3QgQm9vbGVhbicpO1xuXHRcdH1cblx0XHRyZXR1cm4ge1xuXHRcdFx0dmFsdWU6IHZhbHVlLFxuXHRcdFx0ZG9uZTogZG9uZVxuXHRcdH07XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtcmVnZXhwZXhlY1xuXHRSZWdFeHBFeGVjOiBmdW5jdGlvbiBSZWdFeHBFeGVjKFIsIFMpIHtcblx0XHRpZiAodGhpcy5UeXBlKFIpICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignUiBtdXN0IGJlIGFuIE9iamVjdCcpO1xuXHRcdH1cblx0XHRpZiAodGhpcy5UeXBlKFMpICE9PSAnU3RyaW5nJykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignUyBtdXN0IGJlIGEgU3RyaW5nJyk7XG5cdFx0fVxuXHRcdHZhciBleGVjID0gdGhpcy5HZXQoUiwgJ2V4ZWMnKTtcblx0XHRpZiAodGhpcy5Jc0NhbGxhYmxlKGV4ZWMpKSB7XG5cdFx0XHR2YXIgcmVzdWx0ID0gdGhpcy5DYWxsKGV4ZWMsIFIsIFtTXSk7XG5cdFx0XHRpZiAocmVzdWx0ID09PSBudWxsIHx8IHRoaXMuVHlwZShyZXN1bHQpID09PSAnT2JqZWN0Jykge1xuXHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xuXHRcdFx0fVxuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignXCJleGVjXCIgbWV0aG9kIG11c3QgcmV0dXJuIGBudWxsYCBvciBhbiBPYmplY3QnKTtcblx0XHR9XG5cdFx0cmV0dXJuIHJlZ2V4RXhlYyhSLCBTKTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1hcnJheXNwZWNpZXNjcmVhdGVcblx0QXJyYXlTcGVjaWVzQ3JlYXRlOiBmdW5jdGlvbiBBcnJheVNwZWNpZXNDcmVhdGUob3JpZ2luYWxBcnJheSwgbGVuZ3RoKSB7XG5cdFx0aWYgKCF0aGlzLklzSW50ZWdlcihsZW5ndGgpIHx8IGxlbmd0aCA8IDApIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IGxlbmd0aCBtdXN0IGJlIGFuIGludGVnZXIgPj0gMCcpO1xuXHRcdH1cblx0XHR2YXIgbGVuID0gbGVuZ3RoID09PSAwID8gMCA6IGxlbmd0aDtcblx0XHR2YXIgQztcblx0XHR2YXIgaXNBcnJheSA9IHRoaXMuSXNBcnJheShvcmlnaW5hbEFycmF5KTtcblx0XHRpZiAoaXNBcnJheSkge1xuXHRcdFx0QyA9IHRoaXMuR2V0KG9yaWdpbmFsQXJyYXksICdjb25zdHJ1Y3RvcicpO1xuXHRcdFx0Ly8gVE9ETzogZmlndXJlIG91dCBob3cgdG8gbWFrZSBhIGNyb3NzLXJlYWxtIG5vcm1hbCBBcnJheSwgYSBzYW1lLXJlYWxtIEFycmF5XG5cdFx0XHQvLyBpZiAodGhpcy5Jc0NvbnN0cnVjdG9yKEMpKSB7XG5cdFx0XHQvLyBcdGlmIEMgaXMgYW5vdGhlciByZWFsbSdzIEFycmF5LCBDID0gdW5kZWZpbmVkXG5cdFx0XHQvLyBcdE9iamVjdC5nZXRQcm90b3R5cGVPZihPYmplY3QuZ2V0UHJvdG90eXBlT2YoT2JqZWN0LmdldFByb3RvdHlwZU9mKEFycmF5KSkpID09PSBudWxsID9cblx0XHRcdC8vIH1cblx0XHRcdGlmICh0aGlzLlR5cGUoQykgPT09ICdPYmplY3QnICYmIGhhc1N5bWJvbHMgJiYgU3ltYm9sLnNwZWNpZXMpIHtcblx0XHRcdFx0QyA9IHRoaXMuR2V0KEMsIFN5bWJvbC5zcGVjaWVzKTtcblx0XHRcdFx0aWYgKEMgPT09IG51bGwpIHtcblx0XHRcdFx0XHRDID0gdm9pZCAwO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHRcdGlmICh0eXBlb2YgQyA9PT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdHJldHVybiBBcnJheShsZW4pO1xuXHRcdH1cblx0XHRpZiAoIXRoaXMuSXNDb25zdHJ1Y3RvcihDKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQyBtdXN0IGJlIGEgY29uc3RydWN0b3InKTtcblx0XHR9XG5cdFx0cmV0dXJuIG5ldyBDKGxlbik7IC8vIHRoaXMuQ29uc3RydWN0KEMsIGxlbik7XG5cdH0sXG5cblx0Q3JlYXRlRGF0YVByb3BlcnR5OiBmdW5jdGlvbiBDcmVhdGVEYXRhUHJvcGVydHkoTywgUCwgVikge1xuXHRcdGlmICh0aGlzLlR5cGUoTykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBUeXBlKE8pIGlzIG5vdCBPYmplY3QnKTtcblx0XHR9XG5cdFx0aWYgKCF0aGlzLklzUHJvcGVydHlLZXkoUCkpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IElzUHJvcGVydHlLZXkoUCkgaXMgbm90IHRydWUnKTtcblx0XHR9XG5cdFx0dmFyIG9sZERlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKE8sIFApO1xuXHRcdHZhciBleHRlbnNpYmxlID0gb2xkRGVzYyB8fCAodHlwZW9mIE9iamVjdC5pc0V4dGVuc2libGUgIT09ICdmdW5jdGlvbicgfHwgT2JqZWN0LmlzRXh0ZW5zaWJsZShPKSk7XG5cdFx0dmFyIGltbXV0YWJsZSA9IG9sZERlc2MgJiYgKCFvbGREZXNjLndyaXRhYmxlIHx8ICFvbGREZXNjLmNvbmZpZ3VyYWJsZSk7XG5cdFx0aWYgKGltbXV0YWJsZSB8fCAhZXh0ZW5zaWJsZSkge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0XHR2YXIgbmV3RGVzYyA9IHtcblx0XHRcdGNvbmZpZ3VyYWJsZTogdHJ1ZSxcblx0XHRcdGVudW1lcmFibGU6IHRydWUsXG5cdFx0XHR2YWx1ZTogVixcblx0XHRcdHdyaXRhYmxlOiB0cnVlXG5cdFx0fTtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoTywgUCwgbmV3RGVzYyk7XG5cdFx0cmV0dXJuIHRydWU7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtY3JlYXRlZGF0YXByb3BlcnR5b3J0aHJvd1xuXHRDcmVhdGVEYXRhUHJvcGVydHlPclRocm93OiBmdW5jdGlvbiBDcmVhdGVEYXRhUHJvcGVydHlPclRocm93KE8sIFAsIFYpIHtcblx0XHRpZiAodGhpcy5UeXBlKE8pICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogVHlwZShPKSBpcyBub3QgT2JqZWN0Jyk7XG5cdFx0fVxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBJc1Byb3BlcnR5S2V5KFApIGlzIG5vdCB0cnVlJyk7XG5cdFx0fVxuXHRcdHZhciBzdWNjZXNzID0gdGhpcy5DcmVhdGVEYXRhUHJvcGVydHkoTywgUCwgVik7XG5cdFx0aWYgKCFzdWNjZXNzKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCd1bmFibGUgdG8gY3JlYXRlIGRhdGEgcHJvcGVydHknKTtcblx0XHR9XG5cdFx0cmV0dXJuIHN1Y2Nlc3M7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtYWR2YW5jZXN0cmluZ2luZGV4XG5cdEFkdmFuY2VTdHJpbmdJbmRleDogZnVuY3Rpb24gQWR2YW5jZVN0cmluZ0luZGV4KFMsIGluZGV4LCB1bmljb2RlKSB7XG5cdFx0aWYgKHRoaXMuVHlwZShTKSAhPT0gJ1N0cmluZycpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IFR5cGUoUykgaXMgbm90IFN0cmluZycpO1xuXHRcdH1cblx0XHRpZiAoIXRoaXMuSXNJbnRlZ2VyKGluZGV4KSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogbGVuZ3RoIG11c3QgYmUgYW4gaW50ZWdlciA+PSAwIGFuZCA8PSAoMioqNTMgLSAxKScpO1xuXHRcdH1cblx0XHRpZiAoaW5kZXggPCAwIHx8IGluZGV4ID4gTUFYX1NBRkVfSU5URUdFUikge1xuXHRcdFx0dGhyb3cgbmV3IFJhbmdlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IGxlbmd0aCBtdXN0IGJlIGFuIGludGVnZXIgPj0gMCBhbmQgPD0gKDIqKjUzIC0gMSknKTtcblx0XHR9XG5cdFx0aWYgKHRoaXMuVHlwZSh1bmljb2RlKSAhPT0gJ0Jvb2xlYW4nKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBUeXBlKHVuaWNvZGUpIGlzIG5vdCBCb29sZWFuJyk7XG5cdFx0fVxuXHRcdGlmICghdW5pY29kZSkge1xuXHRcdFx0cmV0dXJuIGluZGV4ICsgMTtcblx0XHR9XG5cdFx0dmFyIGxlbmd0aCA9IFMubGVuZ3RoO1xuXHRcdGlmICgoaW5kZXggKyAxKSA+PSBsZW5ndGgpIHtcblx0XHRcdHJldHVybiBpbmRleCArIDE7XG5cdFx0fVxuXHRcdHZhciBmaXJzdCA9IFMuY2hhckNvZGVBdChpbmRleCk7XG5cdFx0aWYgKGZpcnN0IDwgMHhEODAwIHx8IGZpcnN0ID4gMHhEQkZGKSB7XG5cdFx0XHRyZXR1cm4gaW5kZXggKyAxO1xuXHRcdH1cblx0XHR2YXIgc2Vjb25kID0gUy5jaGFyQ29kZUF0KGluZGV4ICsgMSk7XG5cdFx0aWYgKHNlY29uZCA8IDB4REMwMCB8fCBzZWNvbmQgPiAweERGRkYpIHtcblx0XHRcdHJldHVybiBpbmRleCArIDE7XG5cdFx0fVxuXHRcdHJldHVybiBpbmRleCArIDI7XG5cdH1cbn0pO1xuXG5kZWxldGUgRVM2LkNoZWNrT2JqZWN0Q29lcmNpYmxlOyAvLyByZW5hbWVkIGluIEVTNiB0byBSZXF1aXJlT2JqZWN0Q29lcmNpYmxlXG5cbm1vZHVsZS5leHBvcnRzID0gRVM2O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2VzMjAxNS5qc1xuLy8gbW9kdWxlIGlkID0gMTlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbi8qIGVzbGludCBuby1pbnZhbGlkLXRoaXM6IDEgKi9cblxudmFyIEVSUk9SX01FU1NBR0UgPSAnRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQgY2FsbGVkIG9uIGluY29tcGF0aWJsZSAnO1xudmFyIHNsaWNlID0gQXJyYXkucHJvdG90eXBlLnNsaWNlO1xudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcbnZhciBmdW5jVHlwZSA9ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYmluZCh0aGF0KSB7XG4gICAgdmFyIHRhcmdldCA9IHRoaXM7XG4gICAgaWYgKHR5cGVvZiB0YXJnZXQgIT09ICdmdW5jdGlvbicgfHwgdG9TdHIuY2FsbCh0YXJnZXQpICE9PSBmdW5jVHlwZSkge1xuICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKEVSUk9SX01FU1NBR0UgKyB0YXJnZXQpO1xuICAgIH1cbiAgICB2YXIgYXJncyA9IHNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcblxuICAgIHZhciBib3VuZDtcbiAgICB2YXIgYmluZGVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcyBpbnN0YW5jZW9mIGJvdW5kKSB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gdGFyZ2V0LmFwcGx5KFxuICAgICAgICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgICAgICAgYXJncy5jb25jYXQoc2xpY2UuY2FsbChhcmd1bWVudHMpKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIGlmIChPYmplY3QocmVzdWx0KSA9PT0gcmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRhcmdldC5hcHBseShcbiAgICAgICAgICAgICAgICB0aGF0LFxuICAgICAgICAgICAgICAgIGFyZ3MuY29uY2F0KHNsaWNlLmNhbGwoYXJndW1lbnRzKSlcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIGJvdW5kTGVuZ3RoID0gTWF0aC5tYXgoMCwgdGFyZ2V0Lmxlbmd0aCAtIGFyZ3MubGVuZ3RoKTtcbiAgICB2YXIgYm91bmRBcmdzID0gW107XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBib3VuZExlbmd0aDsgaSsrKSB7XG4gICAgICAgIGJvdW5kQXJncy5wdXNoKCckJyArIGkpO1xuICAgIH1cblxuICAgIGJvdW5kID0gRnVuY3Rpb24oJ2JpbmRlcicsICdyZXR1cm4gZnVuY3Rpb24gKCcgKyBib3VuZEFyZ3Muam9pbignLCcpICsgJyl7IHJldHVybiBiaW5kZXIuYXBwbHkodGhpcyxhcmd1bWVudHMpOyB9JykoYmluZGVyKTtcblxuICAgIGlmICh0YXJnZXQucHJvdG90eXBlKSB7XG4gICAgICAgIHZhciBFbXB0eSA9IGZ1bmN0aW9uIEVtcHR5KCkge307XG4gICAgICAgIEVtcHR5LnByb3RvdHlwZSA9IHRhcmdldC5wcm90b3R5cGU7XG4gICAgICAgIGJvdW5kLnByb3RvdHlwZSA9IG5ldyBFbXB0eSgpO1xuICAgICAgICBFbXB0eS5wcm90b3R5cGUgPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBib3VuZDtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZnVuY3Rpb24tYmluZC9pbXBsZW1lbnRhdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gMjBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciBoYXNTeW1ib2xzID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSAnc3ltYm9sJztcblxudmFyIGlzUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9oZWxwZXJzL2lzUHJpbWl0aXZlJyk7XG52YXIgaXNDYWxsYWJsZSA9IHJlcXVpcmUoJ2lzLWNhbGxhYmxlJyk7XG52YXIgaXNEYXRlID0gcmVxdWlyZSgnaXMtZGF0ZS1vYmplY3QnKTtcbnZhciBpc1N5bWJvbCA9IHJlcXVpcmUoJ2lzLXN5bWJvbCcpO1xuXG52YXIgb3JkaW5hcnlUb1ByaW1pdGl2ZSA9IGZ1bmN0aW9uIE9yZGluYXJ5VG9QcmltaXRpdmUoTywgaGludCkge1xuXHRpZiAodHlwZW9mIE8gPT09ICd1bmRlZmluZWQnIHx8IE8gPT09IG51bGwpIHtcblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdDYW5ub3QgY2FsbCBtZXRob2Qgb24gJyArIE8pO1xuXHR9XG5cdGlmICh0eXBlb2YgaGludCAhPT0gJ3N0cmluZycgfHwgKGhpbnQgIT09ICdudW1iZXInICYmIGhpbnQgIT09ICdzdHJpbmcnKSkge1xuXHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ2hpbnQgbXVzdCBiZSBcInN0cmluZ1wiIG9yIFwibnVtYmVyXCInKTtcblx0fVxuXHR2YXIgbWV0aG9kTmFtZXMgPSBoaW50ID09PSAnc3RyaW5nJyA/IFsndG9TdHJpbmcnLCAndmFsdWVPZiddIDogWyd2YWx1ZU9mJywgJ3RvU3RyaW5nJ107XG5cdHZhciBtZXRob2QsIHJlc3VsdCwgaTtcblx0Zm9yIChpID0gMDsgaSA8IG1ldGhvZE5hbWVzLmxlbmd0aDsgKytpKSB7XG5cdFx0bWV0aG9kID0gT1ttZXRob2ROYW1lc1tpXV07XG5cdFx0aWYgKGlzQ2FsbGFibGUobWV0aG9kKSkge1xuXHRcdFx0cmVzdWx0ID0gbWV0aG9kLmNhbGwoTyk7XG5cdFx0XHRpZiAoaXNQcmltaXRpdmUocmVzdWx0KSkge1xuXHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXHR0aHJvdyBuZXcgVHlwZUVycm9yKCdObyBkZWZhdWx0IHZhbHVlJyk7XG59O1xuXG52YXIgR2V0TWV0aG9kID0gZnVuY3Rpb24gR2V0TWV0aG9kKE8sIFApIHtcblx0dmFyIGZ1bmMgPSBPW1BdO1xuXHRpZiAoZnVuYyAhPT0gbnVsbCAmJiB0eXBlb2YgZnVuYyAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRpZiAoIWlzQ2FsbGFibGUoZnVuYykpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoZnVuYyArICcgcmV0dXJuZWQgZm9yIHByb3BlcnR5ICcgKyBQICsgJyBvZiBvYmplY3QgJyArIE8gKyAnIGlzIG5vdCBhIGZ1bmN0aW9uJyk7XG5cdFx0fVxuXHRcdHJldHVybiBmdW5jO1xuXHR9XG59O1xuXG4vLyBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtdG9wcmltaXRpdmVcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gVG9QcmltaXRpdmUoaW5wdXQsIFByZWZlcnJlZFR5cGUpIHtcblx0aWYgKGlzUHJpbWl0aXZlKGlucHV0KSkge1xuXHRcdHJldHVybiBpbnB1dDtcblx0fVxuXHR2YXIgaGludCA9ICdkZWZhdWx0Jztcblx0aWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG5cdFx0aWYgKFByZWZlcnJlZFR5cGUgPT09IFN0cmluZykge1xuXHRcdFx0aGludCA9ICdzdHJpbmcnO1xuXHRcdH0gZWxzZSBpZiAoUHJlZmVycmVkVHlwZSA9PT0gTnVtYmVyKSB7XG5cdFx0XHRoaW50ID0gJ251bWJlcic7XG5cdFx0fVxuXHR9XG5cblx0dmFyIGV4b3RpY1RvUHJpbTtcblx0aWYgKGhhc1N5bWJvbHMpIHtcblx0XHRpZiAoU3ltYm9sLnRvUHJpbWl0aXZlKSB7XG5cdFx0XHRleG90aWNUb1ByaW0gPSBHZXRNZXRob2QoaW5wdXQsIFN5bWJvbC50b1ByaW1pdGl2ZSk7XG5cdFx0fSBlbHNlIGlmIChpc1N5bWJvbChpbnB1dCkpIHtcblx0XHRcdGV4b3RpY1RvUHJpbSA9IFN5bWJvbC5wcm90b3R5cGUudmFsdWVPZjtcblx0XHR9XG5cdH1cblx0aWYgKHR5cGVvZiBleG90aWNUb1ByaW0gIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0dmFyIHJlc3VsdCA9IGV4b3RpY1RvUHJpbS5jYWxsKGlucHV0LCBoaW50KTtcblx0XHRpZiAoaXNQcmltaXRpdmUocmVzdWx0KSkge1xuXHRcdFx0cmV0dXJuIHJlc3VsdDtcblx0XHR9XG5cdFx0dGhyb3cgbmV3IFR5cGVFcnJvcigndW5hYmxlIHRvIGNvbnZlcnQgZXhvdGljIG9iamVjdCB0byBwcmltaXRpdmUnKTtcblx0fVxuXHRpZiAoaGludCA9PT0gJ2RlZmF1bHQnICYmIChpc0RhdGUoaW5wdXQpIHx8IGlzU3ltYm9sKGlucHV0KSkpIHtcblx0XHRoaW50ID0gJ3N0cmluZyc7XG5cdH1cblx0cmV0dXJuIG9yZGluYXJ5VG9QcmltaXRpdmUoaW5wdXQsIGhpbnQgPT09ICdkZWZhdWx0JyA/ICdudW1iZXInIDogaGludCk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLXRvLXByaW1pdGl2ZS9lczYuanNcbi8vIG1vZHVsZSBpZCA9IDIxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgZ2V0RGF5ID0gRGF0ZS5wcm90b3R5cGUuZ2V0RGF5O1xudmFyIHRyeURhdGVPYmplY3QgPSBmdW5jdGlvbiB0cnlEYXRlT2JqZWN0KHZhbHVlKSB7XG5cdHRyeSB7XG5cdFx0Z2V0RGF5LmNhbGwodmFsdWUpO1xuXHRcdHJldHVybiB0cnVlO1xuXHR9IGNhdGNoIChlKSB7XG5cdFx0cmV0dXJuIGZhbHNlO1xuXHR9XG59O1xuXG52YXIgdG9TdHIgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xudmFyIGRhdGVDbGFzcyA9ICdbb2JqZWN0IERhdGVdJztcbnZhciBoYXNUb1N0cmluZ1RhZyA9IHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIFN5bWJvbC50b1N0cmluZ1RhZyA9PT0gJ3N5bWJvbCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNEYXRlT2JqZWN0KHZhbHVlKSB7XG5cdGlmICh0eXBlb2YgdmFsdWUgIT09ICdvYmplY3QnIHx8IHZhbHVlID09PSBudWxsKSB7IHJldHVybiBmYWxzZTsgfVxuXHRyZXR1cm4gaGFzVG9TdHJpbmdUYWcgPyB0cnlEYXRlT2JqZWN0KHZhbHVlKSA6IHRvU3RyLmNhbGwodmFsdWUpID09PSBkYXRlQ2xhc3M7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2lzLWRhdGUtb2JqZWN0L2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAyMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcbnZhciBoYXNTeW1ib2xzID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgU3ltYm9sKCkgPT09ICdzeW1ib2wnO1xuXG5pZiAoaGFzU3ltYm9scykge1xuXHR2YXIgc3ltVG9TdHIgPSBTeW1ib2wucHJvdG90eXBlLnRvU3RyaW5nO1xuXHR2YXIgc3ltU3RyaW5nUmVnZXggPSAvXlN5bWJvbFxcKC4qXFwpJC87XG5cdHZhciBpc1N5bWJvbE9iamVjdCA9IGZ1bmN0aW9uIGlzU3ltYm9sT2JqZWN0KHZhbHVlKSB7XG5cdFx0aWYgKHR5cGVvZiB2YWx1ZS52YWx1ZU9mKCkgIT09ICdzeW1ib2wnKSB7IHJldHVybiBmYWxzZTsgfVxuXHRcdHJldHVybiBzeW1TdHJpbmdSZWdleC50ZXN0KHN5bVRvU3RyLmNhbGwodmFsdWUpKTtcblx0fTtcblx0bW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc1N5bWJvbCh2YWx1ZSkge1xuXHRcdGlmICh0eXBlb2YgdmFsdWUgPT09ICdzeW1ib2wnKSB7IHJldHVybiB0cnVlOyB9XG5cdFx0aWYgKHRvU3RyLmNhbGwodmFsdWUpICE9PSAnW29iamVjdCBTeW1ib2xdJykgeyByZXR1cm4gZmFsc2U7IH1cblx0XHR0cnkge1xuXHRcdFx0cmV0dXJuIGlzU3ltYm9sT2JqZWN0KHZhbHVlKTtcblx0XHR9IGNhdGNoIChlKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXHR9O1xufSBlbHNlIHtcblx0bW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc1N5bWJvbCh2YWx1ZSkge1xuXHRcdC8vIHRoaXMgZW52aXJvbm1lbnQgZG9lcyBub3Qgc3VwcG9ydCBTeW1ib2xzLlxuXHRcdHJldHVybiBmYWxzZTtcblx0fTtcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9pcy1zeW1ib2wvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDIzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwidmFyIGhhcyA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGFzc2lnbih0YXJnZXQsIHNvdXJjZSkge1xuXHRpZiAoT2JqZWN0LmFzc2lnbikge1xuXHRcdHJldHVybiBPYmplY3QuYXNzaWduKHRhcmdldCwgc291cmNlKTtcblx0fVxuXHRmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7XG5cdFx0aWYgKGhhcy5jYWxsKHNvdXJjZSwga2V5KSkge1xuXHRcdFx0dGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTtcblx0XHR9XG5cdH1cblx0cmV0dXJuIHRhcmdldDtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9hc3NpZ24uanNcbi8vIG1vZHVsZSBpZCA9IDI0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc1ByaW1pdGl2ZSh2YWx1ZSkge1xuXHRyZXR1cm4gdmFsdWUgPT09IG51bGwgfHwgKHR5cGVvZiB2YWx1ZSAhPT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgdmFsdWUgIT09ICdvYmplY3QnKTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9pc1ByaW1pdGl2ZS5qc1xuLy8gbW9kdWxlIGlkID0gMjVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciAkaXNOYU4gPSByZXF1aXJlKCcuL2hlbHBlcnMvaXNOYU4nKTtcbnZhciAkaXNGaW5pdGUgPSByZXF1aXJlKCcuL2hlbHBlcnMvaXNGaW5pdGUnKTtcblxudmFyIHNpZ24gPSByZXF1aXJlKCcuL2hlbHBlcnMvc2lnbicpO1xudmFyIG1vZCA9IHJlcXVpcmUoJy4vaGVscGVycy9tb2QnKTtcblxudmFyIElzQ2FsbGFibGUgPSByZXF1aXJlKCdpcy1jYWxsYWJsZScpO1xudmFyIHRvUHJpbWl0aXZlID0gcmVxdWlyZSgnZXMtdG8tcHJpbWl0aXZlL2VzNScpO1xuXG52YXIgaGFzID0gcmVxdWlyZSgnaGFzJyk7XG5cbi8vIGh0dHBzOi8vZXM1LmdpdGh1Yi5pby8jeDlcbnZhciBFUzUgPSB7XG5cdFRvUHJpbWl0aXZlOiB0b1ByaW1pdGl2ZSxcblxuXHRUb0Jvb2xlYW46IGZ1bmN0aW9uIFRvQm9vbGVhbih2YWx1ZSkge1xuXHRcdHJldHVybiAhIXZhbHVlO1xuXHR9LFxuXHRUb051bWJlcjogZnVuY3Rpb24gVG9OdW1iZXIodmFsdWUpIHtcblx0XHRyZXR1cm4gTnVtYmVyKHZhbHVlKTtcblx0fSxcblx0VG9JbnRlZ2VyOiBmdW5jdGlvbiBUb0ludGVnZXIodmFsdWUpIHtcblx0XHR2YXIgbnVtYmVyID0gdGhpcy5Ub051bWJlcih2YWx1ZSk7XG5cdFx0aWYgKCRpc05hTihudW1iZXIpKSB7IHJldHVybiAwOyB9XG5cdFx0aWYgKG51bWJlciA9PT0gMCB8fCAhJGlzRmluaXRlKG51bWJlcikpIHsgcmV0dXJuIG51bWJlcjsgfVxuXHRcdHJldHVybiBzaWduKG51bWJlcikgKiBNYXRoLmZsb29yKE1hdGguYWJzKG51bWJlcikpO1xuXHR9LFxuXHRUb0ludDMyOiBmdW5jdGlvbiBUb0ludDMyKHgpIHtcblx0XHRyZXR1cm4gdGhpcy5Ub051bWJlcih4KSA+PiAwO1xuXHR9LFxuXHRUb1VpbnQzMjogZnVuY3Rpb24gVG9VaW50MzIoeCkge1xuXHRcdHJldHVybiB0aGlzLlRvTnVtYmVyKHgpID4+PiAwO1xuXHR9LFxuXHRUb1VpbnQxNjogZnVuY3Rpb24gVG9VaW50MTYodmFsdWUpIHtcblx0XHR2YXIgbnVtYmVyID0gdGhpcy5Ub051bWJlcih2YWx1ZSk7XG5cdFx0aWYgKCRpc05hTihudW1iZXIpIHx8IG51bWJlciA9PT0gMCB8fCAhJGlzRmluaXRlKG51bWJlcikpIHsgcmV0dXJuIDA7IH1cblx0XHR2YXIgcG9zSW50ID0gc2lnbihudW1iZXIpICogTWF0aC5mbG9vcihNYXRoLmFicyhudW1iZXIpKTtcblx0XHRyZXR1cm4gbW9kKHBvc0ludCwgMHgxMDAwMCk7XG5cdH0sXG5cdFRvU3RyaW5nOiBmdW5jdGlvbiBUb1N0cmluZyh2YWx1ZSkge1xuXHRcdHJldHVybiBTdHJpbmcodmFsdWUpO1xuXHR9LFxuXHRUb09iamVjdDogZnVuY3Rpb24gVG9PYmplY3QodmFsdWUpIHtcblx0XHR0aGlzLkNoZWNrT2JqZWN0Q29lcmNpYmxlKHZhbHVlKTtcblx0XHRyZXR1cm4gT2JqZWN0KHZhbHVlKTtcblx0fSxcblx0Q2hlY2tPYmplY3RDb2VyY2libGU6IGZ1bmN0aW9uIENoZWNrT2JqZWN0Q29lcmNpYmxlKHZhbHVlLCBvcHRNZXNzYWdlKSB7XG5cdFx0LyoganNoaW50IGVxbnVsbDp0cnVlICovXG5cdFx0aWYgKHZhbHVlID09IG51bGwpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3Iob3B0TWVzc2FnZSB8fCAnQ2Fubm90IGNhbGwgbWV0aG9kIG9uICcgKyB2YWx1ZSk7XG5cdFx0fVxuXHRcdHJldHVybiB2YWx1ZTtcblx0fSxcblx0SXNDYWxsYWJsZTogSXNDYWxsYWJsZSxcblx0U2FtZVZhbHVlOiBmdW5jdGlvbiBTYW1lVmFsdWUoeCwgeSkge1xuXHRcdGlmICh4ID09PSB5KSB7IC8vIDAgPT09IC0wLCBidXQgdGhleSBhcmUgbm90IGlkZW50aWNhbC5cblx0XHRcdGlmICh4ID09PSAwKSB7IHJldHVybiAxIC8geCA9PT0gMSAvIHk7IH1cblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0XHRyZXR1cm4gJGlzTmFOKHgpICYmICRpc05hTih5KTtcblx0fSxcblxuXHQvLyBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNS4xLyNzZWMtOFxuXHRUeXBlOiBmdW5jdGlvbiBUeXBlKHgpIHtcblx0XHRpZiAoeCA9PT0gbnVsbCkge1xuXHRcdFx0cmV0dXJuICdOdWxsJztcblx0XHR9XG5cdFx0aWYgKHR5cGVvZiB4ID09PSAndW5kZWZpbmVkJykge1xuXHRcdFx0cmV0dXJuICdVbmRlZmluZWQnO1xuXHRcdH1cblx0XHRpZiAodHlwZW9mIHggPT09ICdmdW5jdGlvbicgfHwgdHlwZW9mIHggPT09ICdvYmplY3QnKSB7XG5cdFx0XHRyZXR1cm4gJ09iamVjdCc7XG5cdFx0fVxuXHRcdGlmICh0eXBlb2YgeCA9PT0gJ251bWJlcicpIHtcblx0XHRcdHJldHVybiAnTnVtYmVyJztcblx0XHR9XG5cdFx0aWYgKHR5cGVvZiB4ID09PSAnYm9vbGVhbicpIHtcblx0XHRcdHJldHVybiAnQm9vbGVhbic7XG5cdFx0fVxuXHRcdGlmICh0eXBlb2YgeCA9PT0gJ3N0cmluZycpIHtcblx0XHRcdHJldHVybiAnU3RyaW5nJztcblx0XHR9XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtcHJvcGVydHktZGVzY3JpcHRvci1zcGVjaWZpY2F0aW9uLXR5cGVcblx0SXNQcm9wZXJ0eURlc2NyaXB0b3I6IGZ1bmN0aW9uIElzUHJvcGVydHlEZXNjcmlwdG9yKERlc2MpIHtcblx0XHRpZiAodGhpcy5UeXBlKERlc2MpICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0XHR2YXIgYWxsb3dlZCA9IHtcblx0XHRcdCdbW0NvbmZpZ3VyYWJsZV1dJzogdHJ1ZSxcblx0XHRcdCdbW0VudW1lcmFibGVdXSc6IHRydWUsXG5cdFx0XHQnW1tHZXRdXSc6IHRydWUsXG5cdFx0XHQnW1tTZXRdXSc6IHRydWUsXG5cdFx0XHQnW1tWYWx1ZV1dJzogdHJ1ZSxcblx0XHRcdCdbW1dyaXRhYmxlXV0nOiB0cnVlXG5cdFx0fTtcblx0XHQvLyBqc2NzOmRpc2FibGVcblx0XHRmb3IgKHZhciBrZXkgaW4gRGVzYykgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG5cdFx0XHRpZiAoaGFzKERlc2MsIGtleSkgJiYgIWFsbG93ZWRba2V5XSkge1xuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdC8vIGpzY3M6ZW5hYmxlXG5cdFx0dmFyIGlzRGF0YSA9IGhhcyhEZXNjLCAnW1tWYWx1ZV1dJyk7XG5cdFx0dmFyIElzQWNjZXNzb3IgPSBoYXMoRGVzYywgJ1tbR2V0XV0nKSB8fCBoYXMoRGVzYywgJ1tbU2V0XV0nKTtcblx0XHRpZiAoaXNEYXRhICYmIElzQWNjZXNzb3IpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ1Byb3BlcnR5IERlc2NyaXB0b3JzIG1heSBub3QgYmUgYm90aCBhY2Nlc3NvciBhbmQgZGF0YSBkZXNjcmlwdG9ycycpO1xuXHRcdH1cblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi81LjEvI3NlYy04LjEwLjFcblx0SXNBY2Nlc3NvckRlc2NyaXB0b3I6IGZ1bmN0aW9uIElzQWNjZXNzb3JEZXNjcmlwdG9yKERlc2MpIHtcblx0XHRpZiAodHlwZW9mIERlc2MgPT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdFx0aWYgKCF0aGlzLklzUHJvcGVydHlEZXNjcmlwdG9yKERlc2MpKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdEZXNjIG11c3QgYmUgYSBQcm9wZXJ0eSBEZXNjcmlwdG9yJyk7XG5cdFx0fVxuXG5cdFx0aWYgKCFoYXMoRGVzYywgJ1tbR2V0XV0nKSAmJiAhaGFzKERlc2MsICdbW1NldF1dJykpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi81LjEvI3NlYy04LjEwLjJcblx0SXNEYXRhRGVzY3JpcHRvcjogZnVuY3Rpb24gSXNEYXRhRGVzY3JpcHRvcihEZXNjKSB7XG5cdFx0aWYgKHR5cGVvZiBEZXNjID09PSAndW5kZWZpbmVkJykge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5RGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignRGVzYyBtdXN0IGJlIGEgUHJvcGVydHkgRGVzY3JpcHRvcicpO1xuXHRcdH1cblxuXHRcdGlmICghaGFzKERlc2MsICdbW1ZhbHVlXV0nKSAmJiAhaGFzKERlc2MsICdbW1dyaXRhYmxlXV0nKSkge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblxuXHRcdHJldHVybiB0cnVlO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzUuMS8jc2VjLTguMTAuM1xuXHRJc0dlbmVyaWNEZXNjcmlwdG9yOiBmdW5jdGlvbiBJc0dlbmVyaWNEZXNjcmlwdG9yKERlc2MpIHtcblx0XHRpZiAodHlwZW9mIERlc2MgPT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdFx0aWYgKCF0aGlzLklzUHJvcGVydHlEZXNjcmlwdG9yKERlc2MpKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdEZXNjIG11c3QgYmUgYSBQcm9wZXJ0eSBEZXNjcmlwdG9yJyk7XG5cdFx0fVxuXG5cdFx0aWYgKCF0aGlzLklzQWNjZXNzb3JEZXNjcmlwdG9yKERlc2MpICYmICF0aGlzLklzRGF0YURlc2NyaXB0b3IoRGVzYykpIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblxuXHRcdHJldHVybiBmYWxzZTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi81LjEvI3NlYy04LjEwLjRcblx0RnJvbVByb3BlcnR5RGVzY3JpcHRvcjogZnVuY3Rpb24gRnJvbVByb3BlcnR5RGVzY3JpcHRvcihEZXNjKSB7XG5cdFx0aWYgKHR5cGVvZiBEZXNjID09PSAndW5kZWZpbmVkJykge1xuXHRcdFx0cmV0dXJuIERlc2M7XG5cdFx0fVxuXG5cdFx0aWYgKCF0aGlzLklzUHJvcGVydHlEZXNjcmlwdG9yKERlc2MpKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdEZXNjIG11c3QgYmUgYSBQcm9wZXJ0eSBEZXNjcmlwdG9yJyk7XG5cdFx0fVxuXG5cdFx0aWYgKHRoaXMuSXNEYXRhRGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0dmFsdWU6IERlc2NbJ1tbVmFsdWVdXSddLFxuXHRcdFx0XHR3cml0YWJsZTogISFEZXNjWydbW1dyaXRhYmxlXV0nXSxcblx0XHRcdFx0ZW51bWVyYWJsZTogISFEZXNjWydbW0VudW1lcmFibGVdXSddLFxuXHRcdFx0XHRjb25maWd1cmFibGU6ICEhRGVzY1snW1tDb25maWd1cmFibGVdXSddXG5cdFx0XHR9O1xuXHRcdH0gZWxzZSBpZiAodGhpcy5Jc0FjY2Vzc29yRGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0Z2V0OiBEZXNjWydbW0dldF1dJ10sXG5cdFx0XHRcdHNldDogRGVzY1snW1tTZXRdXSddLFxuXHRcdFx0XHRlbnVtZXJhYmxlOiAhIURlc2NbJ1tbRW51bWVyYWJsZV1dJ10sXG5cdFx0XHRcdGNvbmZpZ3VyYWJsZTogISFEZXNjWydbW0NvbmZpZ3VyYWJsZV1dJ11cblx0XHRcdH07XG5cdFx0fSBlbHNlIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Zyb21Qcm9wZXJ0eURlc2NyaXB0b3IgbXVzdCBiZSBjYWxsZWQgd2l0aCBhIGZ1bGx5IHBvcHVsYXRlZCBQcm9wZXJ0eSBEZXNjcmlwdG9yJyk7XG5cdFx0fVxuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzUuMS8jc2VjLTguMTAuNVxuXHRUb1Byb3BlcnR5RGVzY3JpcHRvcjogZnVuY3Rpb24gVG9Qcm9wZXJ0eURlc2NyaXB0b3IoT2JqKSB7XG5cdFx0aWYgKHRoaXMuVHlwZShPYmopICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignVG9Qcm9wZXJ0eURlc2NyaXB0b3IgcmVxdWlyZXMgYW4gb2JqZWN0Jyk7XG5cdFx0fVxuXG5cdFx0dmFyIGRlc2MgPSB7fTtcblx0XHRpZiAoaGFzKE9iaiwgJ2VudW1lcmFibGUnKSkge1xuXHRcdFx0ZGVzY1snW1tFbnVtZXJhYmxlXV0nXSA9IHRoaXMuVG9Cb29sZWFuKE9iai5lbnVtZXJhYmxlKTtcblx0XHR9XG5cdFx0aWYgKGhhcyhPYmosICdjb25maWd1cmFibGUnKSkge1xuXHRcdFx0ZGVzY1snW1tDb25maWd1cmFibGVdXSddID0gdGhpcy5Ub0Jvb2xlYW4oT2JqLmNvbmZpZ3VyYWJsZSk7XG5cdFx0fVxuXHRcdGlmIChoYXMoT2JqLCAndmFsdWUnKSkge1xuXHRcdFx0ZGVzY1snW1tWYWx1ZV1dJ10gPSBPYmoudmFsdWU7XG5cdFx0fVxuXHRcdGlmIChoYXMoT2JqLCAnd3JpdGFibGUnKSkge1xuXHRcdFx0ZGVzY1snW1tXcml0YWJsZV1dJ10gPSB0aGlzLlRvQm9vbGVhbihPYmoud3JpdGFibGUpO1xuXHRcdH1cblx0XHRpZiAoaGFzKE9iaiwgJ2dldCcpKSB7XG5cdFx0XHR2YXIgZ2V0dGVyID0gT2JqLmdldDtcblx0XHRcdGlmICh0eXBlb2YgZ2V0dGVyICE9PSAndW5kZWZpbmVkJyAmJiAhdGhpcy5Jc0NhbGxhYmxlKGdldHRlcikpIHtcblx0XHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignZ2V0dGVyIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuXHRcdFx0fVxuXHRcdFx0ZGVzY1snW1tHZXRdXSddID0gZ2V0dGVyO1xuXHRcdH1cblx0XHRpZiAoaGFzKE9iaiwgJ3NldCcpKSB7XG5cdFx0XHR2YXIgc2V0dGVyID0gT2JqLnNldDtcblx0XHRcdGlmICh0eXBlb2Ygc2V0dGVyICE9PSAndW5kZWZpbmVkJyAmJiAhdGhpcy5Jc0NhbGxhYmxlKHNldHRlcikpIHtcblx0XHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignc2V0dGVyIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuXHRcdFx0fVxuXHRcdFx0ZGVzY1snW1tTZXRdXSddID0gc2V0dGVyO1xuXHRcdH1cblxuXHRcdGlmICgoaGFzKGRlc2MsICdbW0dldF1dJykgfHwgaGFzKGRlc2MsICdbW1NldF1dJykpICYmIChoYXMoZGVzYywgJ1tbVmFsdWVdXScpIHx8IGhhcyhkZXNjLCAnW1tXcml0YWJsZV1dJykpKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdJbnZhbGlkIHByb3BlcnR5IGRlc2NyaXB0b3IuIENhbm5vdCBib3RoIHNwZWNpZnkgYWNjZXNzb3JzIGFuZCBhIHZhbHVlIG9yIHdyaXRhYmxlIGF0dHJpYnV0ZScpO1xuXHRcdH1cblx0XHRyZXR1cm4gZGVzYztcblx0fVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBFUzU7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvZXM1LmpzXG4vLyBtb2R1bGUgaWQgPSAyNlxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcblxudmFyIGlzUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9oZWxwZXJzL2lzUHJpbWl0aXZlJyk7XG5cbnZhciBpc0NhbGxhYmxlID0gcmVxdWlyZSgnaXMtY2FsbGFibGUnKTtcblxuLy8gaHR0cHM6Ly9lczUuZ2l0aHViLmlvLyN4OC4xMlxudmFyIEVTNWludGVybmFsU2xvdHMgPSB7XG5cdCdbW0RlZmF1bHRWYWx1ZV1dJzogZnVuY3Rpb24gKE8sIGhpbnQpIHtcblx0XHR2YXIgYWN0dWFsSGludCA9IGhpbnQgfHwgKHRvU3RyLmNhbGwoTykgPT09ICdbb2JqZWN0IERhdGVdJyA/IFN0cmluZyA6IE51bWJlcik7XG5cblx0XHRpZiAoYWN0dWFsSGludCA9PT0gU3RyaW5nIHx8IGFjdHVhbEhpbnQgPT09IE51bWJlcikge1xuXHRcdFx0dmFyIG1ldGhvZHMgPSBhY3R1YWxIaW50ID09PSBTdHJpbmcgPyBbJ3RvU3RyaW5nJywgJ3ZhbHVlT2YnXSA6IFsndmFsdWVPZicsICd0b1N0cmluZyddO1xuXHRcdFx0dmFyIHZhbHVlLCBpO1xuXHRcdFx0Zm9yIChpID0gMDsgaSA8IG1ldGhvZHMubGVuZ3RoOyArK2kpIHtcblx0XHRcdFx0aWYgKGlzQ2FsbGFibGUoT1ttZXRob2RzW2ldXSkpIHtcblx0XHRcdFx0XHR2YWx1ZSA9IE9bbWV0aG9kc1tpXV0oKTtcblx0XHRcdFx0XHRpZiAoaXNQcmltaXRpdmUodmFsdWUpKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm4gdmFsdWU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdObyBkZWZhdWx0IHZhbHVlJyk7XG5cdFx0fVxuXHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ2ludmFsaWQgW1tEZWZhdWx0VmFsdWVdXSBoaW50IHN1cHBsaWVkJyk7XG5cdH1cbn07XG5cbi8vIGh0dHBzOi8vZXM1LmdpdGh1Yi5pby8jeDlcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gVG9QcmltaXRpdmUoaW5wdXQsIFByZWZlcnJlZFR5cGUpIHtcblx0aWYgKGlzUHJpbWl0aXZlKGlucHV0KSkge1xuXHRcdHJldHVybiBpbnB1dDtcblx0fVxuXHRyZXR1cm4gRVM1aW50ZXJuYWxTbG90c1snW1tEZWZhdWx0VmFsdWVdXSddKGlucHV0LCBQcmVmZXJyZWRUeXBlKTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2VzNS5qc1xuLy8gbW9kdWxlIGlkID0gMjdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciBoYXMgPSByZXF1aXJlKCdoYXMnKTtcbnZhciByZWdleEV4ZWMgPSBSZWdFeHAucHJvdG90eXBlLmV4ZWM7XG52YXIgZ09QRCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3I7XG5cbnZhciB0cnlSZWdleEV4ZWNDYWxsID0gZnVuY3Rpb24gdHJ5UmVnZXhFeGVjKHZhbHVlKSB7XG5cdHRyeSB7XG5cdFx0dmFyIGxhc3RJbmRleCA9IHZhbHVlLmxhc3RJbmRleDtcblx0XHR2YWx1ZS5sYXN0SW5kZXggPSAwO1xuXG5cdFx0cmVnZXhFeGVjLmNhbGwodmFsdWUpO1xuXHRcdHJldHVybiB0cnVlO1xuXHR9IGNhdGNoIChlKSB7XG5cdFx0cmV0dXJuIGZhbHNlO1xuXHR9IGZpbmFsbHkge1xuXHRcdHZhbHVlLmxhc3RJbmRleCA9IGxhc3RJbmRleDtcblx0fVxufTtcbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG52YXIgcmVnZXhDbGFzcyA9ICdbb2JqZWN0IFJlZ0V4cF0nO1xudmFyIGhhc1RvU3RyaW5nVGFnID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgU3ltYm9sLnRvU3RyaW5nVGFnID09PSAnc3ltYm9sJztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc1JlZ2V4KHZhbHVlKSB7XG5cdGlmICghdmFsdWUgfHwgdHlwZW9mIHZhbHVlICE9PSAnb2JqZWN0Jykge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fVxuXHRpZiAoIWhhc1RvU3RyaW5nVGFnKSB7XG5cdFx0cmV0dXJuIHRvU3RyLmNhbGwodmFsdWUpID09PSByZWdleENsYXNzO1xuXHR9XG5cblx0dmFyIGRlc2NyaXB0b3IgPSBnT1BEKHZhbHVlLCAnbGFzdEluZGV4Jyk7XG5cdHZhciBoYXNMYXN0SW5kZXhEYXRhUHJvcGVydHkgPSBkZXNjcmlwdG9yICYmIGhhcyhkZXNjcmlwdG9yLCAndmFsdWUnKTtcblx0aWYgKCFoYXNMYXN0SW5kZXhEYXRhUHJvcGVydHkpIHtcblx0XHRyZXR1cm4gZmFsc2U7XG5cdH1cblxuXHRyZXR1cm4gdHJ5UmVnZXhFeGVjQ2FsbCh2YWx1ZSk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2lzLXJlZ2V4L2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAyOFxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxudmFyIGRlZmluZSA9IHJlcXVpcmUoJ2RlZmluZS1wcm9wZXJ0aWVzJyk7XG52YXIgZ2V0UG9seWZpbGwgPSByZXF1aXJlKCcuL3BvbHlmaWxsJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gc2hpbUFycmF5UHJvdG90eXBlRmluZCgpIHtcblx0dmFyIHBvbHlmaWxsID0gZ2V0UG9seWZpbGwoKTtcblxuXHRkZWZpbmUoQXJyYXkucHJvdG90eXBlLCB7IGZpbmQ6IHBvbHlmaWxsIH0sIHtcblx0XHRmaW5kOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRyZXR1cm4gQXJyYXkucHJvdG90eXBlLmZpbmQgIT09IHBvbHlmaWxsO1xuXHRcdH1cblx0fSk7XG5cblx0cmV0dXJuIHBvbHlmaWxsO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9hcnJheS5wcm90b3R5cGUuZmluZC9zaGltLmpzXG4vLyBtb2R1bGUgaWQgPSAyOVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsImltcG9ydCB7IGdldFNjcmlwdFRhZ0RhdGFBdHRyaWJ1dGVzLCBsb2FkVmVyc2lvbnMsIGxvYWRXaWRnZXRDb25maWcsIGV4dHJhY3RIb3N0bmFtZSB9IGZyb20gJ3V0aWxzL2xvYWRlci9sb2FkZXItdXRpbHMnO1xuXG4oKGdsb2JhbCkgPT4ge1xuICAgIGNvbnN0IGVtcyA9IGdsb2JhbC5kb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0Jyk7XG4gICAgY29uc3QgcmVnZXhwID0gLy4qZ25zXFwud2lkZ2V0XFwubG9hZGVyXFwuKFteL10rXFwuKT9qcy87XG4gICAgY29uc3QgbkVtcyA9IGVtcy5sZW5ndGg7XG4gICAgY29uc3QgY2xvbmVBdHRyaWJ1dGVzID0gKGVsZW1lbnQsIGF0dHJpYnV0ZXMpID0+IHtcbiAgICAgICAgT2JqZWN0LmtleXMoYXR0cmlidXRlcykuZm9yRWFjaCgoa2V5KSA9PiB7XG4gICAgICAgICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZShgZGF0YS0ke2tleX1gLCBhdHRyaWJ1dGVzW2tleV0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgaWYgKGdsb2JhbC5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKSB7XG4gICAgICAgIGNvbnN0IGVsZW1lbnRzID0gW107XG4gICAgICAgIGxldCBsb2FkZXJEb21haW4gPSAnJztcbiAgICAgICAgbGV0IGxvYWRlclVSTCA9ICcnO1xuXG4gICAgICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCBuRW1zOyBpbmRleCArPSAxKSB7XG4gICAgICAgICAgICBjb25zdCBlbGVtZW50ID0gZW1zW2luZGV4XTtcbiAgICAgICAgICAgIGlmIChlbGVtZW50LnNyYy5tYXRjaChyZWdleHApICYmIGVsZW1lbnRzLmluZGV4T2YoZWxlbWVudCkgPCAwKSB7XG4gICAgICAgICAgICAgICAgLy8gR2V0IGxvYWRlciB1cmwgc28gd2UgaGF2ZSBvbmUgdG8gbG9hZCB0aGUgdmVyc2lvbnMgZnJvbS5cbiAgICAgICAgICAgICAgICBsb2FkZXJVUkwgPSBleHRyYWN0SG9zdG5hbWUoZWxlbWVudC5zcmMsIHRydWUpO1xuXG4gICAgICAgICAgICAgICAgZWxlbWVudHMucHVzaChlbGVtZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGxvYWRWZXJzaW9ucyhsb2FkZXJVUkwsIChlcnJvciwgdmVyc2lvbnMpID0+IHtcbiAgICAgICAgICAgIGlmIChlcnJvciAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBpbml0aWFsaXplcjtcbiAgICAgICAgICAgIGxldCB2ZXJzaW9uUGF0aCA9ICcnO1xuICAgICAgICAgICAgY29uc3QgYXJyYXlMZW5ndGggPSBlbGVtZW50cy5sZW5ndGg7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gYXJyYXlMZW5ndGggLSAxOyBpID49IDA7IGkgLT0gMSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSBlbGVtZW50c1tpXTtcbiAgICAgICAgICAgICAgICBjb25zdCBkYXRhQXR0cmlidXRlcyA9IGdldFNjcmlwdFRhZ0RhdGFBdHRyaWJ1dGVzKGVsZW1lbnQuYXR0cmlidXRlcyk7XG4gICAgICAgICAgICAgICAgLy8gR2V0IGxvYWRlciBkb21haW4gYW5kIHVybCBwZXIgd2lkZ2V0LFxuICAgICAgICAgICAgICAgIC8vIGp1c3QgaW4gY2FzZSB0aGV5IGltcGxlbWVudCB3aWRnZXRzIGxvYWRlZCBmcm9tIGRpZmZlcmVudCBkb21haW5zIG9uIG9uZSBwYWdlXG4gICAgICAgICAgICAgICAgbG9hZGVyRG9tYWluID0gZXh0cmFjdEhvc3RuYW1lKGVsZW1lbnQuc3JjKTtcbiAgICAgICAgICAgICAgICBsb2FkZXJVUkwgPSBleHRyYWN0SG9zdG5hbWUoZWxlbWVudC5zcmMsIHRydWUpO1xuXG4gICAgICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLWxvb3AtZnVuY1xuICAgICAgICAgICAgICAgIGxvYWRXaWRnZXRDb25maWcobG9hZGVyRG9tYWluLCBkYXRhQXR0cmlidXRlcywgKGVyciwgY29uZmlnKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnIgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGlmICh2ZXJzaW9ucyAmJiB2ZXJzaW9uc1tjb25maWcudmVyc2lvbl0gIT09IHVuZGVmaW5lZCAmJiB2ZXJzaW9uc1tjb25maWcudmVyc2lvbl0gIT09ICcnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2ZXJzaW9uUGF0aCA9IGAvJHt2ZXJzaW9uc1tjb25maWcudmVyc2lvbl19YDtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGluaXRpYWxpemVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICAgICAgICAgICAgICAgIGNsb25lQXR0cmlidXRlcyhpbml0aWFsaXplciwgZGF0YUF0dHJpYnV0ZXMpO1xuICAgICAgICAgICAgICAgICAgICBpbml0aWFsaXplci5zcmMgPSBgJHtsb2FkZXJVUkx9JHt2ZXJzaW9uUGF0aH0vZ25zLndpZGdldC5pbml0aWFsaXplci5qc2A7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LnBhcmVudE5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoaW5pdGlhbGl6ZXIsIGVsZW1lbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5vdXRlckhUTUwgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBlbGVtZW50c1tpXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgbkVtczsgaW5kZXggKz0gMSkge1xuICAgICAgICAgICAgY29uc3QgZWxlbWVudCA9IGVtc1tpbmRleF07XG5cbiAgICAgICAgICAgIGlmIChlbGVtZW50LnNyYy5tYXRjaChyZWdleHApKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgcGFyYWdyYXBoID0gZ2xvYmFsLmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3AnKTtcbiAgICAgICAgICAgICAgICBwYXJhZ3JhcGguaWQgPSAnYnJvd3Nlci1ub3Qtc3VwcG9ydGVkJztcbiAgICAgICAgICAgICAgICBwYXJhZ3JhcGguaW5uZXJIVE1MID0gJ1VuZm9ydHVuYXRlbHkgdGhlIGJyb3dzZXIgeW91IGFyZSBjdXJyZW50bHkgdXNpbmcgaXMgbm90IHN1cHBvcnRlZCBieSBvdXIgd2lkZ2V0cy4gUGxlYXNlIHVwZGF0ZSB5b3VyIGJyb3dzZXIuJztcbiAgICAgICAgICAgICAgICBlbGVtZW50LnBhcmVudE5vZGUuYXBwZW5kQ2hpbGQocGFyYWdyYXBoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0pKGdsb2JhbCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9nbnMud2lkZ2V0LmxvYWRlci5qcyJdLCJzb3VyY2VSb290IjoiIn0=