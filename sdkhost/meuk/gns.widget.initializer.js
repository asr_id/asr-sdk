/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 31);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var bind = __webpack_require__(5);

module.exports = bind.call(Function.call, Object.prototype.hasOwnProperty);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var fnToStr = Function.prototype.toString;

var constructorRegex = /^\s*class /;
var isES6ClassFn = function isES6ClassFn(value) {
	try {
		var fnStr = fnToStr.call(value);
		var singleStripped = fnStr.replace(/\/\/.*\n/g, '');
		var multiStripped = singleStripped.replace(/\/\*[.\s\S]*\*\//g, '');
		var spaceStripped = multiStripped.replace(/\n/mg, ' ').replace(/ {2}/g, ' ');
		return constructorRegex.test(spaceStripped);
	} catch (e) {
		return false; // not a function
	}
};

var tryFunctionObject = function tryFunctionObject(value) {
	try {
		if (isES6ClassFn(value)) { return false; }
		fnToStr.call(value);
		return true;
	} catch (e) {
		return false;
	}
};
var toStr = Object.prototype.toString;
var fnClass = '[object Function]';
var genClass = '[object GeneratorFunction]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isCallable(value) {
	if (!value) { return false; }
	if (typeof value !== 'function' && typeof value !== 'object') { return false; }
	if (hasToStringTag) { return tryFunctionObject(value); }
	if (isES6ClassFn(value)) { return false; }
	var strClass = toStr.call(value);
	return strClass === fnClass || strClass === genClass;
};


/***/ }),
/* 2 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keys = __webpack_require__(16);
var foreach = __webpack_require__(18);
var hasSymbols = typeof Symbol === 'function' && typeof Symbol() === 'symbol';

var toStr = Object.prototype.toString;

var isFunction = function (fn) {
	return typeof fn === 'function' && toStr.call(fn) === '[object Function]';
};

var arePropertyDescriptorsSupported = function () {
	var obj = {};
	try {
		Object.defineProperty(obj, 'x', { enumerable: false, value: obj });
        /* eslint-disable no-unused-vars, no-restricted-syntax */
        for (var _ in obj) { return false; }
        /* eslint-enable no-unused-vars, no-restricted-syntax */
		return obj.x === obj;
	} catch (e) { /* this is IE 8. */
		return false;
	}
};
var supportsDescriptors = Object.defineProperty && arePropertyDescriptorsSupported();

var defineProperty = function (object, name, value, predicate) {
	if (name in object && (!isFunction(predicate) || !predicate())) {
		return;
	}
	if (supportsDescriptors) {
		Object.defineProperty(object, name, {
			configurable: true,
			enumerable: false,
			value: value,
			writable: true
		});
	} else {
		object[name] = value;
	}
};

var defineProperties = function (object, map) {
	var predicates = arguments.length > 2 ? arguments[2] : {};
	var props = keys(map);
	if (hasSymbols) {
		props = props.concat(Object.getOwnPropertySymbols(map));
	}
	foreach(props, function (name) {
		defineProperty(object, name, map[name], predicates[name]);
	});
};

defineProperties.supportsDescriptors = !!supportsDescriptors;

module.exports = defineProperties;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(19);


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(20);

module.exports = Function.prototype.bind || implementation;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = function isPrimitive(value) {
	return value === null || (typeof value !== 'function' && typeof value !== 'object');
};


/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = Number.isNaN || function isNaN(a) {
	return a !== a;
};


/***/ }),
/* 8 */
/***/ (function(module, exports) {

var $isNaN = Number.isNaN || function (a) { return a !== a; };

module.exports = Number.isFinite || function (x) { return typeof x === 'number' && !$isNaN(x) && x !== Infinity && x !== -Infinity; };


/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = function sign(number) {
	return number >= 0 ? 1 : -1;
};


/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = function mod(number, modulo) {
	var remain = number % modulo;
	return Math.floor(remain >= 0 ? remain : remain + modulo);
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var ES = __webpack_require__(4);

module.exports = function find(predicate) {
	var list = ES.ToObject(this);
	var length = ES.ToInteger(ES.ToLength(list.length));
	if (!ES.IsCallable(predicate)) {
		throw new TypeError('Array#find: predicate must be a function');
	}
	if (length === 0) {
		return undefined;
	}
	var thisArg = arguments[1];
	for (var i = 0, value; i < length; i++) {
		value = list[i];
		if (ES.Call(predicate, thisArg, [value, i, list])) {
			return value;
		}
	}
	return undefined;
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function getPolyfill() {
	// Detect if an implementation exists
	// Detect early implementations which skipped holes in sparse arrays
  // eslint-disable-next-line no-sparse-arrays
	var implemented = Array.prototype.find && [, 1].find(function () {
		return true;
	}) !== 1;

  // eslint-disable-next-line global-require
	return implemented ? Array.prototype.find : __webpack_require__(11);
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.loadFeatureSwitches = exports.loadVersions = exports.messageEventCallback = exports.generateId = exports.loadIframeChecker = exports.initializeWidget = exports.loadWidgetConfig = exports.getScriptTagDataAttributes = exports.shouldLoadAnalytics = exports.shouldLoadInstana = exports.loadIFrame = undefined;
exports.extractHostname = extractHostname;
exports.extractActualVersion = extractActualVersion;

var _gateway = __webpack_require__(14);

var _gateway2 = _interopRequireDefault(_gateway);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

__webpack_require__(15).shim();

function mergeObject(target) {
    // eslint-disable-next-line no-plusplus
    for (var i = 1; i < arguments.length; i++) {
        // eslint-disable-next-line prefer-rest-params
        var source = arguments[i];
        // eslint-disable-next-line no-restricted-syntax
        for (var key in source) {
            // eslint-disable-next-line no-prototype-builtins
            if (source.hasOwnProperty(key)) {
                target[key] = source[key];
            }
        }
    }
    return target;
}

function setIFrameVariables(iFrame, iFrameId) {
    iFrame.id = iFrameId;
    iFrame.name = iFrameId;
    iFrame.src = 'javascript:false'; // eslint-disable-line
    iFrame.title = '';
    iFrame.role = 'presentation';
    iFrame.frameBorder = '0';
    iFrame.scrolling = 'no';
    iFrame.width = '100%';
    iFrame.height = '137px';
    (iFrame.frameElement || iFrame).style.cssText = 'border:0;min-width:100%;';
}

function writeIFrameDocument(iFrame, url, config) {
    var doc = void 0;
    var dom = void 0;
    try {
        doc = iFrame.contentWindow.document;
    } catch (err) {
        dom = document.domain;
        iFrame.src = 'javascript:var d=document.open();d.domain="' + dom + '";void(0);';
        doc = iFrame.contentWindow.document;
    }

    doc.open()._l = function () {
        // eslint-disable-line
        var js = this.createElement('script');
        if (dom) this.domain = dom;
        js.id = 'widget-iframe-async';
        js.src = url;
        js.async = true;
        this.body.appendChild(js);
    };
    doc.write('<html><head>');
    doc.write('<base href="' + config.baseURL + '">');
    doc.write('<meta charset="utf-8">');
    // Inject React Developer Tools if not in production
    if (true) {
        doc.write('<script>__REACT_DEVTOOLS_GLOBAL_HOOK__ = parent.__REACT_DEVTOOLS_GLOBAL_HOOK__</script>');
    }
    doc.write('<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">');
    doc.write('<meta name="viewport" content="width=device-width, initial-scale=1">');
    doc.write('<meta name="apple-mobile-web-app-capable" content="yes">');
    doc.write('<meta http-equiv="Cache-Control" content="no-transform">');
    doc.write('</head>');
    doc.write('<body onload="document._l();">');
    doc.write('<script>window.settings = ' + JSON.stringify(config) + ';</script>');
    if (shouldLoadInstana(config)) {
        doc.write('<script>(function(i,s,o,g,r,a,m){i[\'InstanaEumObject\']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)}, i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a, m)})(window, document, \'script\', \'//eum.instana.io/eum.min.js\', \'ineum\');ineum(\'apiKey\', \'o4yYT7q7TUqPjfapqD3piw\');</script>');
    }
    if (shouldLoadAnalytics(config)) {
        doc.write('<script async src="https://www.googletagmanager.com/gtag/js?id=UA-58323625-12"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date());gtag(\'config\', \'UA-58323625-12\');</script>');
    }
    if (config.analytics && config.analytics !== '') {
        doc.write('<script async src="https://www.googletagmanager.com/gtag/js?id=' + config.analytics + '"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date());gtag(\'config\', \'' + config.analytics + '\');</script>');
    }
    doc.write('<div id="mainContent" class="container"><div id="widget"></div></div>');
    doc.write('</body><html>');
    doc.close();
}

function switchIFrameContent(url, config) {
    var selectedIFrame = document.getElementById(config.iFrameId);
    var placeHolder = document.createElement('div');
    placeHolder.id = generateId(12);
    selectedIFrame.parentNode.insertBefore(placeHolder, selectedIFrame);
    selectedIFrame.parentNode.removeChild(selectedIFrame);

    var newFrame = document.createElement('iframe');

    setIFrameVariables(newFrame, config.iFrameId);

    placeHolder.parentNode.insertBefore(newFrame, placeHolder);
    placeHolder.parentNode.removeChild(placeHolder);

    writeIFrameDocument(newFrame, url, config);
}

/**
 * loadIFrame
 *
 * Generates iFrame and loads the given url as script-tag
 *
 * @param element
 * @param url
 * @param config
 */

var loadIFrame = exports.loadIFrame = function loadIFrame(element, url, config) {
    var iFrame = document.createElement('iframe');
    config.iFrameId = generateId(6);

    setIFrameVariables(iFrame, config.iFrameId);

    var scripts = document.getElementsByTagName('script');
    [].forEach.call(scripts, function (script) {
        if (script === element) {
            script.parentNode.insertBefore(iFrame, script);
            writeIFrameDocument(iFrame, url, config);
        }
    });
};

var shouldLoadInstana = exports.shouldLoadInstana = function shouldLoadInstana(config) {
    if (!config || !config.featureSwitches || !config.featureSwitches.all || !config.featureSwitches.all.instana || !config.featureSwitches.all.instana.active) {
        return false;
    }

    var magicNumber = Math.random() * 100;

    return config.featureSwitches.all.instana.percentage_active >= magicNumber;
};

var shouldLoadAnalytics = exports.shouldLoadAnalytics = function shouldLoadAnalytics(config) {
    if (!config || !config.featureSwitches || !config.featureSwitches.all || !config.featureSwitches.all.analytics || !config.featureSwitches.all.analytics.active) {
        return false;
    }

    var magicNumber = Math.random() * 100;

    return config.featureSwitches.all.analytics.percentage_active >= magicNumber;
};

/**
 * getScriptTagDataAttributes
 *
 * Parse script tag attributes and return all data-* attributes as object.
 * @param attributes
 * @returns {{}}
 */
var getScriptTagDataAttributes = exports.getScriptTagDataAttributes = function getScriptTagDataAttributes(attributes) {
    var dataAttributes = {};

    [].forEach.call(attributes, function (attribute) {
        if (/^data-/.test(attribute.name)) {
            var parameter = attribute.name.substr(5);
            dataAttributes[parameter] = attribute.value;
        }
    });

    return dataAttributes;
};

function dataHandler(data, config, callback) {
    config.customer_id = data.customer_id;

    if (data.allow_tags === 'yes') {
        if (data.allow_data_tags !== undefined) {
            var tags = data.allow_data_tags.map(function (item) {
                return item.trim();
            });

            // Loop through all given config attributes
            Object.keys(config).forEach(function (configTag) {
                if (tags.find(function (tag) {
                    return tag === configTag;
                })) {
                    // attribute exists in allow_data_tags and default config.
                    // so we may override the data attribute with given config attribute
                    data[configTag] = config[configTag];
                }

                if (data[configTag] === undefined) {
                    // here we add it if it doesn't exists in the default config
                    data[configTag] = config[configTag];
                }
            });
        } else {
            data = mergeObject({}, data, config);
        }
    } else {
        // Loop through all given config attributes
        Object.keys(config).forEach(function (configTag) {
            if (data[configTag] === undefined) {
                // here we add it if it doesn't exists in the default config
                data[configTag] = config[configTag];
            }
        });
    }

    // Add custom_css to the data
    if (config.custom_css !== undefined && config.custom_css !== '') {
        data.custom_css = config.custom_css;
    }

    // TODO: In the near future we're going to add a real domain check...
    // if (!checkDomain(data, document.referrer.toString())) {
    //     callback('Error: domain is not allowed.', null);
    // }

    if (data.type === 'tagging') {
        data.version = 'v2.0';
        data.language = 'en_GB';
        data.language_code = '2';
        data.theme = 'default';
    }

    callback(null, data);
}

/**
 * loadWidgetConfig
 *
 * @param loaderDomain
 * @param config
 * @param callback
 */
var loadWidgetConfig = exports.loadWidgetConfig = function loadWidgetConfig(loaderDomain, config, callback) {
    if (config === undefined || config.widget_id === undefined) {
        callback('Error: could not load widget config', null);
    } else {
        var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
        global.__GNS_ANTI_CACHE = antiCache;

        // Generate widget configuration url
        var configURL = _gateway2.default.default.configURL;
        if (loaderDomain && _gateway2.default[loaderDomain]) {
            configURL = _gateway2.default[loaderDomain].configURL;
        }
        var configFile = '' + configURL + config.widget_id + '.json?v=' + antiCache;

        var data = {};

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function xhrResponse() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    try {
                        data = JSON.parse(xhr.responseText);
                    } catch (error) {
                        callback('Error: could not load configuration file for widget id: ' + config.widget_id, null);
                        return;
                    }
                } else {
                    callback('Error: could not load configuration file for widget id: ' + config.widget_id, null);
                    return;
                }
                dataHandler(data, config, callback);
            }
        };
        xhr.open('get', configFile);
        xhr.responseType = 'text';
        xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
        xhr.send();
    }
};

// TODO: In the near future we're going to add a real domain check...
/**
 * checkDomain
 *
 * @param settings
 * @param referrer
 * @returns {boolean}
 */
// export const checkDomain = (settings, referrer) => {
//     const referrerArray = referrer.substr(7).replace('/', '.').split('.');
//     let loadAllowed = true; // TODO: Make this default to false
//
//     [].forEach.call(referrerArray, (ref) => {
//         // Logic to ban or allow the widget
//         if (settings.domain) {
//             if (settings.domain === ref) {
//                 loadAllowed = true;
//             }
//         } else {
//             loadAllowed = true;
//         }
//     });
//
//     return loadAllowed;
// };

/**
 * initializeWidget
 *
 * @param element
 * @param attributes
 */
var initializeWidget = exports.initializeWidget = function initializeWidget(element, attributes) {
    // Get script tag data-* attributes
    var dataAttributes = getScriptTagDataAttributes(attributes);
    var loaderDomain = extractHostname(element.src);

    var finalConfig = {};

    // Load widget config json and merge with script tag data-* attributes
    loadWidgetConfig(loaderDomain, dataAttributes, function (error, config) {
        if (error !== null) {
            element.parentNode.insertBefore(document.createTextNode(error), element);
            return;
        }

        finalConfig = mergeObject({}, finalConfig, config);

        // Load the iFrame with returned config
        if (config.sport !== undefined && config.type !== undefined) {
            if (config.theme === undefined) {
                finalConfig.theme = 'default';
            }

            finalConfig.loaderDomain = extractHostname(element.src);
            finalConfig.actualVersion = extractActualVersion(element.src);
            finalConfig.baseURL = element.src.replace('gns.widget.initializer.js', '');

            loadIframeChecker(element, finalConfig);
        }
    });

    loadFeatureSwitches(extractHostname(element.src, true), function (err, featureSwitcherConfig) {
        if (err !== null) {
            element.parentNode.insertBefore(document.createTextNode(err), element);
        }

        finalConfig.featureSwitches = featureSwitcherConfig;
        loadIframeChecker(element, finalConfig);
    });
};

var loadIframeChecker = exports.loadIframeChecker = function loadIframeChecker(element, config) {
    if (config.featureSwitches && config.widget_id) {
        var widgetUrl = 'widgets/' + config.theme + '/' + config.sport + '/' + config.type + '.js?viewCount=1';

        if (config.client_id !== undefined) {
            widgetUrl = widgetUrl + '&c=' + config.client_id;
        }
        loadIFrame(element, widgetUrl, config);
    }
};

/**
 * generateId
 *
 * @param length
 * @returns {string}
 */
var generateId = exports.generateId = function generateId(length) {
    var string = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i += 1) {
        string += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return string;
};

/**
 * messageEventCallback
 *
 * @param event
 */
var messageEventCallback = exports.messageEventCallback = function messageEventCallback(event) {
    var message = void 0;
    try {
        message = JSON.parse(event.data);
    } catch (error) {
        message = null;
    }

    if (message) {
        if (message.action) {
            switch (message.action) {
                case 'gns.resizeIFrame':
                    if (message.iFrameId !== undefined) {
                        var targetIFrame = document.getElementById(message.iFrameId);
                        if (targetIFrame) {
                            var parentZoomLevel = window.getComputedStyle(document.getElementsByTagName('html')[0]).zoom;
                            if (parentZoomLevel < 1) {
                                targetIFrame.height = Math.ceil(message.bounds.height / parentZoomLevel);
                            } else {
                                targetIFrame.height = message.bounds.height;
                            }
                        }
                    }
                    break;
                case 'gns.switchToWidget':
                    if (message.settings.iFrameId !== undefined) {
                        var _targetIFrame = document.getElementById(message.settings.iFrameId);
                        if (_targetIFrame) {
                            var widgetUrl = 'widgets/' + message.settings.theme + '/' + message.settings.sport + '/' + message.settings.type + '.js?viewCount=1';

                            if (message.settings.client_id !== undefined) {
                                widgetUrl = widgetUrl + '&c=' + message.settings.client_id;
                            }

                            switchIFrameContent(widgetUrl, message.settings);
                        }
                    }
                    break;
                default:
            }
        }
    }
};

/**
 * loadVersions
 *
 * @param loaderURL
 * @param callback
 */
var loadVersions = exports.loadVersions = function loadVersions(loaderURL, callback) {
    var versions = {};
    var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
    global.__GNS_ANTI_CACHE = antiCache;
    var configFile = loaderURL + '/versions.json?v=' + antiCache;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function xhrResponse() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                try {
                    versions = JSON.parse(xhr.responseText);
                } catch (error) {
                    callback('Error: could not load versions.json', null);
                    return;
                }
            } else {
                callback('Error: could not load versions.json', null);
                return;
            }
            callback(null, versions);
        }
    };
    xhr.open('get', configFile);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
    xhr.send();
};

var loadFeatureSwitches = exports.loadFeatureSwitches = function loadFeatureSwitches(loaderURL, callback) {
    var antiCache = global.__GNS_ANTI_CACHE || new Date().getTime();
    global.__GNS_ANTI_CACHE = antiCache;
    var configFileURL = loaderURL + '/feature_switches.json?v=' + antiCache;

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function xhrResponse() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                try {
                    callback(null, JSON.parse(xhr.responseText));
                } catch (error) {
                    callback('Error: could not load feature switches config file', null);
                }
            } else {
                callback('Error: could not load feature switches config file', null);
            }
        }
    };

    xhr.open('get', configFileURL);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
    xhr.send();
};

/**
 * extractHostname
 *
 * @param url
 * @param keepProtocol
 * @returns {*}
 */
function extractHostname(url) {
    var keepProtocol = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var hostname = void 0;

    // find & remove protocol (http, ftp, etc.) and get hostname
    if (url.indexOf('://') > -1) {
        hostname = url.split('/')[2];
    } else {
        hostname = url.split('/')[0];
    }

    // find & remove port number
    var port = hostname.split(':')[1];
    hostname = hostname.split(':')[0];

    // find & remove "?"
    hostname = hostname.split('?')[0];

    if (url.indexOf('://') > -1 && keepProtocol) {
        hostname = url.split('/')[0] + '//' + hostname;
        if (port) {
            hostname = hostname + ':' + port;
        }
    }

    return hostname;
}

function extractActualVersion(url) {
    var actualVersion = void 0;

    // Remove initializer part
    var cleanUrl = url.replace('/gns.widget.initializer.js', '');

    // find & remove protocol (http, ftp, etc.) and get hostname

    if (cleanUrl.indexOf('://') > -1) {
        actualVersion = cleanUrl.split('/')[3];
    } else {
        actualVersion = cleanUrl.split('/')[1];
    }

    if (!actualVersion) {
        return null;
    }

    // find & remove port number
    actualVersion = actualVersion.split(':')[0];

    // find & remove "?"
    actualVersion = actualVersion.split('?')[0];

    return actualVersion;
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    default: {
        baseURL: 'https://og2018-datatest-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: 'http://widgets.sports.labs.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'widget-builder.dev': {
        baseURL: 'https://og2018-datatest-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//widget-builder.dev:3000/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'test.widgets.sports.labs.gracenote.com': {
        baseURL: 'https://og2018-datatest-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//test.widgets.sports.labs.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'staging.widgets.sports.gracenote.com': {
        baseURL: 'https://og2018-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//staging.widgets.sports.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'widgets.sports.gracenote.com': {
        baseURL: 'https://og2018-api.sports.gracenote.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//staging.widgets.sports.gracenote.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.sports.gracenote.com/configs/'
    },
    'staging-widgets.nbcolympics.com': {
        baseURL: 'https://stgapi-gracenote.nbcolympics.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//staging-widgets.nbcolympics.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.nbcolympics.com/configs/'
    },
    'widgets.nbcolympics.com': {
        baseURL: 'https://api-gracenote.nbcolympics.com/svc/',
        headers: {},
        mocks: {
            baseURL: '//widgets.nbcolympics.com/mocks/',
            headers: {}
        },
        configURL: 'https://widgets.nbcolympics.com/configs/'
    }
};

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(3);
var ES = __webpack_require__(4);

var implementation = __webpack_require__(11);
var getPolyfill = __webpack_require__(12);
var shim = __webpack_require__(29);

var slice = Array.prototype.slice;

var polyfill = getPolyfill();

var boundFindShim = function find(array, predicate) { // eslint-disable-line no-unused-vars
	ES.RequireObjectCoercible(array);
	var args = slice.call(arguments, 1);
	return polyfill.apply(array, args);
};

define(boundFindShim, {
	getPolyfill: getPolyfill,
	implementation: implementation,
	shim: shim
});

module.exports = boundFindShim;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// modified from https://github.com/es-shims/es5-shim
var has = Object.prototype.hasOwnProperty;
var toStr = Object.prototype.toString;
var slice = Array.prototype.slice;
var isArgs = __webpack_require__(17);
var isEnumerable = Object.prototype.propertyIsEnumerable;
var hasDontEnumBug = !isEnumerable.call({ toString: null }, 'toString');
var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
var dontEnums = [
	'toString',
	'toLocaleString',
	'valueOf',
	'hasOwnProperty',
	'isPrototypeOf',
	'propertyIsEnumerable',
	'constructor'
];
var equalsConstructorPrototype = function (o) {
	var ctor = o.constructor;
	return ctor && ctor.prototype === o;
};
var excludedKeys = {
	$console: true,
	$external: true,
	$frame: true,
	$frameElement: true,
	$frames: true,
	$innerHeight: true,
	$innerWidth: true,
	$outerHeight: true,
	$outerWidth: true,
	$pageXOffset: true,
	$pageYOffset: true,
	$parent: true,
	$scrollLeft: true,
	$scrollTop: true,
	$scrollX: true,
	$scrollY: true,
	$self: true,
	$webkitIndexedDB: true,
	$webkitStorageInfo: true,
	$window: true
};
var hasAutomationEqualityBug = (function () {
	/* global window */
	if (typeof window === 'undefined') { return false; }
	for (var k in window) {
		try {
			if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && typeof window[k] === 'object') {
				try {
					equalsConstructorPrototype(window[k]);
				} catch (e) {
					return true;
				}
			}
		} catch (e) {
			return true;
		}
	}
	return false;
}());
var equalsConstructorPrototypeIfNotBuggy = function (o) {
	/* global window */
	if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
		return equalsConstructorPrototype(o);
	}
	try {
		return equalsConstructorPrototype(o);
	} catch (e) {
		return false;
	}
};

var keysShim = function keys(object) {
	var isObject = object !== null && typeof object === 'object';
	var isFunction = toStr.call(object) === '[object Function]';
	var isArguments = isArgs(object);
	var isString = isObject && toStr.call(object) === '[object String]';
	var theKeys = [];

	if (!isObject && !isFunction && !isArguments) {
		throw new TypeError('Object.keys called on a non-object');
	}

	var skipProto = hasProtoEnumBug && isFunction;
	if (isString && object.length > 0 && !has.call(object, 0)) {
		for (var i = 0; i < object.length; ++i) {
			theKeys.push(String(i));
		}
	}

	if (isArguments && object.length > 0) {
		for (var j = 0; j < object.length; ++j) {
			theKeys.push(String(j));
		}
	} else {
		for (var name in object) {
			if (!(skipProto && name === 'prototype') && has.call(object, name)) {
				theKeys.push(String(name));
			}
		}
	}

	if (hasDontEnumBug) {
		var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);

		for (var k = 0; k < dontEnums.length; ++k) {
			if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
				theKeys.push(dontEnums[k]);
			}
		}
	}
	return theKeys;
};

keysShim.shim = function shimObjectKeys() {
	if (Object.keys) {
		var keysWorksWithArguments = (function () {
			// Safari 5.0 bug
			return (Object.keys(arguments) || '').length === 2;
		}(1, 2));
		if (!keysWorksWithArguments) {
			var originalKeys = Object.keys;
			Object.keys = function keys(object) {
				if (isArgs(object)) {
					return originalKeys(slice.call(object));
				} else {
					return originalKeys(object);
				}
			};
		}
	} else {
		Object.keys = keysShim;
	}
	return Object.keys || keysShim;
};

module.exports = keysShim;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;

module.exports = function isArguments(value) {
	var str = toStr.call(value);
	var isArgs = str === '[object Arguments]';
	if (!isArgs) {
		isArgs = str !== '[object Array]' &&
			value !== null &&
			typeof value === 'object' &&
			typeof value.length === 'number' &&
			value.length >= 0 &&
			toStr.call(value.callee) === '[object Function]';
	}
	return isArgs;
};


/***/ }),
/* 18 */
/***/ (function(module, exports) {


var hasOwn = Object.prototype.hasOwnProperty;
var toString = Object.prototype.toString;

module.exports = function forEach (obj, fn, ctx) {
    if (toString.call(fn) !== '[object Function]') {
        throw new TypeError('iterator must be a function');
    }
    var l = obj.length;
    if (l === +l) {
        for (var i = 0; i < l; i++) {
            fn.call(ctx, obj[i], i, obj);
        }
    } else {
        for (var k in obj) {
            if (hasOwn.call(obj, k)) {
                fn.call(ctx, obj[k], k, obj);
            }
        }
    }
};



/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var has = __webpack_require__(0);
var toPrimitive = __webpack_require__(21);

var toStr = Object.prototype.toString;
var hasSymbols = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol';

var $isNaN = __webpack_require__(7);
var $isFinite = __webpack_require__(8);
var MAX_SAFE_INTEGER = Number.MAX_SAFE_INTEGER || Math.pow(2, 53) - 1;

var assign = __webpack_require__(24);
var sign = __webpack_require__(9);
var mod = __webpack_require__(10);
var isPrimitive = __webpack_require__(25);
var parseInteger = parseInt;
var bind = __webpack_require__(5);
var arraySlice = bind.call(Function.call, Array.prototype.slice);
var strSlice = bind.call(Function.call, String.prototype.slice);
var isBinary = bind.call(Function.call, RegExp.prototype.test, /^0b[01]+$/i);
var isOctal = bind.call(Function.call, RegExp.prototype.test, /^0o[0-7]+$/i);
var regexExec = bind.call(Function.call, RegExp.prototype.exec);
var nonWS = ['\u0085', '\u200b', '\ufffe'].join('');
var nonWSregex = new RegExp('[' + nonWS + ']', 'g');
var hasNonWS = bind.call(Function.call, RegExp.prototype.test, nonWSregex);
var invalidHexLiteral = /^[-+]0x[0-9a-f]+$/i;
var isInvalidHexLiteral = bind.call(Function.call, RegExp.prototype.test, invalidHexLiteral);

// whitespace from: http://es5.github.io/#x15.5.4.20
// implementation from https://github.com/es-shims/es5-shim/blob/v3.4.0/es5-shim.js#L1304-L1324
var ws = [
	'\x09\x0A\x0B\x0C\x0D\x20\xA0\u1680\u180E\u2000\u2001\u2002\u2003',
	'\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028',
	'\u2029\uFEFF'
].join('');
var trimRegex = new RegExp('(^[' + ws + ']+)|([' + ws + ']+$)', 'g');
var replace = bind.call(Function.call, String.prototype.replace);
var trim = function (value) {
	return replace(value, trimRegex, '');
};

var ES5 = __webpack_require__(26);

var hasRegExpMatcher = __webpack_require__(28);

// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-abstract-operations
var ES6 = assign(assign({}, ES5), {

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-call-f-v-args
	Call: function Call(F, V) {
		var args = arguments.length > 2 ? arguments[2] : [];
		if (!this.IsCallable(F)) {
			throw new TypeError(F + ' is not a function');
		}
		return F.apply(V, args);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toprimitive
	ToPrimitive: toPrimitive,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toboolean
	// ToBoolean: ES5.ToBoolean,

	// http://www.ecma-international.org/ecma-262/6.0/#sec-tonumber
	ToNumber: function ToNumber(argument) {
		var value = isPrimitive(argument) ? argument : toPrimitive(argument, Number);
		if (typeof value === 'symbol') {
			throw new TypeError('Cannot convert a Symbol value to a number');
		}
		if (typeof value === 'string') {
			if (isBinary(value)) {
				return this.ToNumber(parseInteger(strSlice(value, 2), 2));
			} else if (isOctal(value)) {
				return this.ToNumber(parseInteger(strSlice(value, 2), 8));
			} else if (hasNonWS(value) || isInvalidHexLiteral(value)) {
				return NaN;
			} else {
				var trimmed = trim(value);
				if (trimmed !== value) {
					return this.ToNumber(trimmed);
				}
			}
		}
		return Number(value);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-tointeger
	// ToInteger: ES5.ToNumber,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toint32
	// ToInt32: ES5.ToInt32,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint32
	// ToUint32: ES5.ToUint32,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toint16
	ToInt16: function ToInt16(argument) {
		var int16bit = this.ToUint16(argument);
		return int16bit >= 0x8000 ? int16bit - 0x10000 : int16bit;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint16
	// ToUint16: ES5.ToUint16,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toint8
	ToInt8: function ToInt8(argument) {
		var int8bit = this.ToUint8(argument);
		return int8bit >= 0x80 ? int8bit - 0x100 : int8bit;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint8
	ToUint8: function ToUint8(argument) {
		var number = this.ToNumber(argument);
		if ($isNaN(number) || number === 0 || !$isFinite(number)) { return 0; }
		var posInt = sign(number) * Math.floor(Math.abs(number));
		return mod(posInt, 0x100);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-touint8clamp
	ToUint8Clamp: function ToUint8Clamp(argument) {
		var number = this.ToNumber(argument);
		if ($isNaN(number) || number <= 0) { return 0; }
		if (number >= 0xFF) { return 0xFF; }
		var f = Math.floor(argument);
		if (f + 0.5 < number) { return f + 1; }
		if (number < f + 0.5) { return f; }
		if (f % 2 !== 0) { return f + 1; }
		return f;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-tostring
	ToString: function ToString(argument) {
		if (typeof argument === 'symbol') {
			throw new TypeError('Cannot convert a Symbol value to a string');
		}
		return String(argument);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-toobject
	ToObject: function ToObject(value) {
		this.RequireObjectCoercible(value);
		return Object(value);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-topropertykey
	ToPropertyKey: function ToPropertyKey(argument) {
		var key = this.ToPrimitive(argument, String);
		return typeof key === 'symbol' ? key : this.ToString(key);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
	ToLength: function ToLength(argument) {
		var len = this.ToInteger(argument);
		if (len <= 0) { return 0; } // includes converting -0 to +0
		if (len > MAX_SAFE_INTEGER) { return MAX_SAFE_INTEGER; }
		return len;
	},

	// http://www.ecma-international.org/ecma-262/6.0/#sec-canonicalnumericindexstring
	CanonicalNumericIndexString: function CanonicalNumericIndexString(argument) {
		if (toStr.call(argument) !== '[object String]') {
			throw new TypeError('must be a string');
		}
		if (argument === '-0') { return -0; }
		var n = this.ToNumber(argument);
		if (this.SameValue(this.ToString(n), argument)) { return n; }
		return void 0;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-requireobjectcoercible
	RequireObjectCoercible: ES5.CheckObjectCoercible,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isarray
	IsArray: Array.isArray || function IsArray(argument) {
		return toStr.call(argument) === '[object Array]';
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-iscallable
	// IsCallable: ES5.IsCallable,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isconstructor
	IsConstructor: function IsConstructor(argument) {
		return typeof argument === 'function' && !!argument.prototype; // unfortunately there's no way to truly check this without try/catch `new argument`
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isextensible-o
	IsExtensible: function IsExtensible(obj) {
		if (!Object.preventExtensions) { return true; }
		if (isPrimitive(obj)) {
			return false;
		}
		return Object.isExtensible(obj);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-isinteger
	IsInteger: function IsInteger(argument) {
		if (typeof argument !== 'number' || $isNaN(argument) || !$isFinite(argument)) {
			return false;
		}
		var abs = Math.abs(argument);
		return Math.floor(abs) === abs;
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-ispropertykey
	IsPropertyKey: function IsPropertyKey(argument) {
		return typeof argument === 'string' || typeof argument === 'symbol';
	},

	// http://www.ecma-international.org/ecma-262/6.0/#sec-isregexp
	IsRegExp: function IsRegExp(argument) {
		if (!argument || typeof argument !== 'object') {
			return false;
		}
		if (hasSymbols) {
			var isRegExp = argument[Symbol.match];
			if (typeof isRegExp !== 'undefined') {
				return ES5.ToBoolean(isRegExp);
			}
		}
		return hasRegExpMatcher(argument);
	},

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-samevalue
	// SameValue: ES5.SameValue,

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-samevaluezero
	SameValueZero: function SameValueZero(x, y) {
		return (x === y) || ($isNaN(x) && $isNaN(y));
	},

	/**
	 * 7.3.2 GetV (V, P)
	 * 1. Assert: IsPropertyKey(P) is true.
	 * 2. Let O be ToObject(V).
	 * 3. ReturnIfAbrupt(O).
	 * 4. Return O.[[Get]](P, V).
	 */
	GetV: function GetV(V, P) {
		// 7.3.2.1
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}

		// 7.3.2.2-3
		var O = this.ToObject(V);

		// 7.3.2.4
		return O[P];
	},

	/**
	 * 7.3.9 - http://www.ecma-international.org/ecma-262/6.0/#sec-getmethod
	 * 1. Assert: IsPropertyKey(P) is true.
	 * 2. Let func be GetV(O, P).
	 * 3. ReturnIfAbrupt(func).
	 * 4. If func is either undefined or null, return undefined.
	 * 5. If IsCallable(func) is false, throw a TypeError exception.
	 * 6. Return func.
	 */
	GetMethod: function GetMethod(O, P) {
		// 7.3.9.1
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}

		// 7.3.9.2
		var func = this.GetV(O, P);

		// 7.3.9.4
		if (func == null) {
			return void 0;
		}

		// 7.3.9.5
		if (!this.IsCallable(func)) {
			throw new TypeError(P + 'is not a function');
		}

		// 7.3.9.6
		return func;
	},

	/**
	 * 7.3.1 Get (O, P) - http://www.ecma-international.org/ecma-262/6.0/#sec-get-o-p
	 * 1. Assert: Type(O) is Object.
	 * 2. Assert: IsPropertyKey(P) is true.
	 * 3. Return O.[[Get]](P, O).
	 */
	Get: function Get(O, P) {
		// 7.3.1.1
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		// 7.3.1.2
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}
		// 7.3.1.3
		return O[P];
	},

	Type: function Type(x) {
		if (typeof x === 'symbol') {
			return 'Symbol';
		}
		return ES5.Type(x);
	},

	// http://www.ecma-international.org/ecma-262/6.0/#sec-speciesconstructor
	SpeciesConstructor: function SpeciesConstructor(O, defaultConstructor) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		var C = O.constructor;
		if (typeof C === 'undefined') {
			return defaultConstructor;
		}
		if (this.Type(C) !== 'Object') {
			throw new TypeError('O.constructor is not an Object');
		}
		var S = hasSymbols && Symbol.species ? C[Symbol.species] : void 0;
		if (S == null) {
			return defaultConstructor;
		}
		if (this.IsConstructor(S)) {
			return S;
		}
		throw new TypeError('no constructor found');
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-completepropertydescriptor
	CompletePropertyDescriptor: function CompletePropertyDescriptor(Desc) {
		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (this.IsGenericDescriptor(Desc) || this.IsDataDescriptor(Desc)) {
			if (!has(Desc, '[[Value]]')) {
				Desc['[[Value]]'] = void 0;
			}
			if (!has(Desc, '[[Writable]]')) {
				Desc['[[Writable]]'] = false;
			}
		} else {
			if (!has(Desc, '[[Get]]')) {
				Desc['[[Get]]'] = void 0;
			}
			if (!has(Desc, '[[Set]]')) {
				Desc['[[Set]]'] = void 0;
			}
		}
		if (!has(Desc, '[[Enumerable]]')) {
			Desc['[[Enumerable]]'] = false;
		}
		if (!has(Desc, '[[Configurable]]')) {
			Desc['[[Configurable]]'] = false;
		}
		return Desc;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-set-o-p-v-throw
	Set: function Set(O, P, V, Throw) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('O must be an Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		if (this.Type(Throw) !== 'Boolean') {
			throw new TypeError('Throw must be a Boolean');
		}
		if (Throw) {
			O[P] = V;
			return true;
		} else {
			try {
				O[P] = V;
			} catch (e) {
				return false;
			}
		}
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-hasownproperty
	HasOwnProperty: function HasOwnProperty(O, P) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('O must be an Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		return has(O, P);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-hasproperty
	HasProperty: function HasProperty(O, P) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('O must be an Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		return P in O;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-isconcatspreadable
	IsConcatSpreadable: function IsConcatSpreadable(O) {
		if (this.Type(O) !== 'Object') {
			return false;
		}
		if (hasSymbols && typeof Symbol.isConcatSpreadable === 'symbol') {
			var spreadable = this.Get(O, Symbol.isConcatSpreadable);
			if (typeof spreadable !== 'undefined') {
				return this.ToBoolean(spreadable);
			}
		}
		return this.IsArray(O);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-invoke
	Invoke: function Invoke(O, P) {
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('P must be a Property Key');
		}
		var argumentsList = arraySlice(arguments, 2);
		var func = this.GetV(O, P);
		return this.Call(func, O, argumentsList);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-createiterresultobject
	CreateIterResultObject: function CreateIterResultObject(value, done) {
		if (this.Type(done) !== 'Boolean') {
			throw new TypeError('Assertion failed: Type(done) is not Boolean');
		}
		return {
			value: value,
			done: done
		};
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-regexpexec
	RegExpExec: function RegExpExec(R, S) {
		if (this.Type(R) !== 'Object') {
			throw new TypeError('R must be an Object');
		}
		if (this.Type(S) !== 'String') {
			throw new TypeError('S must be a String');
		}
		var exec = this.Get(R, 'exec');
		if (this.IsCallable(exec)) {
			var result = this.Call(exec, R, [S]);
			if (result === null || this.Type(result) === 'Object') {
				return result;
			}
			throw new TypeError('"exec" method must return `null` or an Object');
		}
		return regexExec(R, S);
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-arrayspeciescreate
	ArraySpeciesCreate: function ArraySpeciesCreate(originalArray, length) {
		if (!this.IsInteger(length) || length < 0) {
			throw new TypeError('Assertion failed: length must be an integer >= 0');
		}
		var len = length === 0 ? 0 : length;
		var C;
		var isArray = this.IsArray(originalArray);
		if (isArray) {
			C = this.Get(originalArray, 'constructor');
			// TODO: figure out how to make a cross-realm normal Array, a same-realm Array
			// if (this.IsConstructor(C)) {
			// 	if C is another realm's Array, C = undefined
			// 	Object.getPrototypeOf(Object.getPrototypeOf(Object.getPrototypeOf(Array))) === null ?
			// }
			if (this.Type(C) === 'Object' && hasSymbols && Symbol.species) {
				C = this.Get(C, Symbol.species);
				if (C === null) {
					C = void 0;
				}
			}
		}
		if (typeof C === 'undefined') {
			return Array(len);
		}
		if (!this.IsConstructor(C)) {
			throw new TypeError('C must be a constructor');
		}
		return new C(len); // this.Construct(C, len);
	},

	CreateDataProperty: function CreateDataProperty(O, P, V) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}
		var oldDesc = Object.getOwnPropertyDescriptor(O, P);
		var extensible = oldDesc || (typeof Object.isExtensible !== 'function' || Object.isExtensible(O));
		var immutable = oldDesc && (!oldDesc.writable || !oldDesc.configurable);
		if (immutable || !extensible) {
			return false;
		}
		var newDesc = {
			configurable: true,
			enumerable: true,
			value: V,
			writable: true
		};
		Object.defineProperty(O, P, newDesc);
		return true;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-createdatapropertyorthrow
	CreateDataPropertyOrThrow: function CreateDataPropertyOrThrow(O, P, V) {
		if (this.Type(O) !== 'Object') {
			throw new TypeError('Assertion failed: Type(O) is not Object');
		}
		if (!this.IsPropertyKey(P)) {
			throw new TypeError('Assertion failed: IsPropertyKey(P) is not true');
		}
		var success = this.CreateDataProperty(O, P, V);
		if (!success) {
			throw new TypeError('unable to create data property');
		}
		return success;
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-advancestringindex
	AdvanceStringIndex: function AdvanceStringIndex(S, index, unicode) {
		if (this.Type(S) !== 'String') {
			throw new TypeError('Assertion failed: Type(S) is not String');
		}
		if (!this.IsInteger(index)) {
			throw new TypeError('Assertion failed: length must be an integer >= 0 and <= (2**53 - 1)');
		}
		if (index < 0 || index > MAX_SAFE_INTEGER) {
			throw new RangeError('Assertion failed: length must be an integer >= 0 and <= (2**53 - 1)');
		}
		if (this.Type(unicode) !== 'Boolean') {
			throw new TypeError('Assertion failed: Type(unicode) is not Boolean');
		}
		if (!unicode) {
			return index + 1;
		}
		var length = S.length;
		if ((index + 1) >= length) {
			return index + 1;
		}
		var first = S.charCodeAt(index);
		if (first < 0xD800 || first > 0xDBFF) {
			return index + 1;
		}
		var second = S.charCodeAt(index + 1);
		if (second < 0xDC00 || second > 0xDFFF) {
			return index + 1;
		}
		return index + 2;
	}
});

delete ES6.CheckObjectCoercible; // renamed in ES6 to RequireObjectCoercible

module.exports = ES6;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint no-invalid-this: 1 */

var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
var slice = Array.prototype.slice;
var toStr = Object.prototype.toString;
var funcType = '[object Function]';

module.exports = function bind(that) {
    var target = this;
    if (typeof target !== 'function' || toStr.call(target) !== funcType) {
        throw new TypeError(ERROR_MESSAGE + target);
    }
    var args = slice.call(arguments, 1);

    var bound;
    var binder = function () {
        if (this instanceof bound) {
            var result = target.apply(
                this,
                args.concat(slice.call(arguments))
            );
            if (Object(result) === result) {
                return result;
            }
            return this;
        } else {
            return target.apply(
                that,
                args.concat(slice.call(arguments))
            );
        }
    };

    var boundLength = Math.max(0, target.length - args.length);
    var boundArgs = [];
    for (var i = 0; i < boundLength; i++) {
        boundArgs.push('$' + i);
    }

    bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);

    if (target.prototype) {
        var Empty = function Empty() {};
        Empty.prototype = target.prototype;
        bound.prototype = new Empty();
        Empty.prototype = null;
    }

    return bound;
};


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var hasSymbols = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol';

var isPrimitive = __webpack_require__(6);
var isCallable = __webpack_require__(1);
var isDate = __webpack_require__(22);
var isSymbol = __webpack_require__(23);

var ordinaryToPrimitive = function OrdinaryToPrimitive(O, hint) {
	if (typeof O === 'undefined' || O === null) {
		throw new TypeError('Cannot call method on ' + O);
	}
	if (typeof hint !== 'string' || (hint !== 'number' && hint !== 'string')) {
		throw new TypeError('hint must be "string" or "number"');
	}
	var methodNames = hint === 'string' ? ['toString', 'valueOf'] : ['valueOf', 'toString'];
	var method, result, i;
	for (i = 0; i < methodNames.length; ++i) {
		method = O[methodNames[i]];
		if (isCallable(method)) {
			result = method.call(O);
			if (isPrimitive(result)) {
				return result;
			}
		}
	}
	throw new TypeError('No default value');
};

var GetMethod = function GetMethod(O, P) {
	var func = O[P];
	if (func !== null && typeof func !== 'undefined') {
		if (!isCallable(func)) {
			throw new TypeError(func + ' returned for property ' + P + ' of object ' + O + ' is not a function');
		}
		return func;
	}
};

// http://www.ecma-international.org/ecma-262/6.0/#sec-toprimitive
module.exports = function ToPrimitive(input, PreferredType) {
	if (isPrimitive(input)) {
		return input;
	}
	var hint = 'default';
	if (arguments.length > 1) {
		if (PreferredType === String) {
			hint = 'string';
		} else if (PreferredType === Number) {
			hint = 'number';
		}
	}

	var exoticToPrim;
	if (hasSymbols) {
		if (Symbol.toPrimitive) {
			exoticToPrim = GetMethod(input, Symbol.toPrimitive);
		} else if (isSymbol(input)) {
			exoticToPrim = Symbol.prototype.valueOf;
		}
	}
	if (typeof exoticToPrim !== 'undefined') {
		var result = exoticToPrim.call(input, hint);
		if (isPrimitive(result)) {
			return result;
		}
		throw new TypeError('unable to convert exotic object to primitive');
	}
	if (hint === 'default' && (isDate(input) || isSymbol(input))) {
		hint = 'string';
	}
	return ordinaryToPrimitive(input, hint === 'default' ? 'number' : hint);
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var getDay = Date.prototype.getDay;
var tryDateObject = function tryDateObject(value) {
	try {
		getDay.call(value);
		return true;
	} catch (e) {
		return false;
	}
};

var toStr = Object.prototype.toString;
var dateClass = '[object Date]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isDateObject(value) {
	if (typeof value !== 'object' || value === null) { return false; }
	return hasToStringTag ? tryDateObject(value) : toStr.call(value) === dateClass;
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;
var hasSymbols = typeof Symbol === 'function' && typeof Symbol() === 'symbol';

if (hasSymbols) {
	var symToStr = Symbol.prototype.toString;
	var symStringRegex = /^Symbol\(.*\)$/;
	var isSymbolObject = function isSymbolObject(value) {
		if (typeof value.valueOf() !== 'symbol') { return false; }
		return symStringRegex.test(symToStr.call(value));
	};
	module.exports = function isSymbol(value) {
		if (typeof value === 'symbol') { return true; }
		if (toStr.call(value) !== '[object Symbol]') { return false; }
		try {
			return isSymbolObject(value);
		} catch (e) {
			return false;
		}
	};
} else {
	module.exports = function isSymbol(value) {
		// this environment does not support Symbols.
		return false;
	};
}


/***/ }),
/* 24 */
/***/ (function(module, exports) {

var has = Object.prototype.hasOwnProperty;
module.exports = function assign(target, source) {
	if (Object.assign) {
		return Object.assign(target, source);
	}
	for (var key in source) {
		if (has.call(source, key)) {
			target[key] = source[key];
		}
	}
	return target;
};


/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = function isPrimitive(value) {
	return value === null || (typeof value !== 'function' && typeof value !== 'object');
};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $isNaN = __webpack_require__(7);
var $isFinite = __webpack_require__(8);

var sign = __webpack_require__(9);
var mod = __webpack_require__(10);

var IsCallable = __webpack_require__(1);
var toPrimitive = __webpack_require__(27);

var has = __webpack_require__(0);

// https://es5.github.io/#x9
var ES5 = {
	ToPrimitive: toPrimitive,

	ToBoolean: function ToBoolean(value) {
		return !!value;
	},
	ToNumber: function ToNumber(value) {
		return Number(value);
	},
	ToInteger: function ToInteger(value) {
		var number = this.ToNumber(value);
		if ($isNaN(number)) { return 0; }
		if (number === 0 || !$isFinite(number)) { return number; }
		return sign(number) * Math.floor(Math.abs(number));
	},
	ToInt32: function ToInt32(x) {
		return this.ToNumber(x) >> 0;
	},
	ToUint32: function ToUint32(x) {
		return this.ToNumber(x) >>> 0;
	},
	ToUint16: function ToUint16(value) {
		var number = this.ToNumber(value);
		if ($isNaN(number) || number === 0 || !$isFinite(number)) { return 0; }
		var posInt = sign(number) * Math.floor(Math.abs(number));
		return mod(posInt, 0x10000);
	},
	ToString: function ToString(value) {
		return String(value);
	},
	ToObject: function ToObject(value) {
		this.CheckObjectCoercible(value);
		return Object(value);
	},
	CheckObjectCoercible: function CheckObjectCoercible(value, optMessage) {
		/* jshint eqnull:true */
		if (value == null) {
			throw new TypeError(optMessage || 'Cannot call method on ' + value);
		}
		return value;
	},
	IsCallable: IsCallable,
	SameValue: function SameValue(x, y) {
		if (x === y) { // 0 === -0, but they are not identical.
			if (x === 0) { return 1 / x === 1 / y; }
			return true;
		}
		return $isNaN(x) && $isNaN(y);
	},

	// http://www.ecma-international.org/ecma-262/5.1/#sec-8
	Type: function Type(x) {
		if (x === null) {
			return 'Null';
		}
		if (typeof x === 'undefined') {
			return 'Undefined';
		}
		if (typeof x === 'function' || typeof x === 'object') {
			return 'Object';
		}
		if (typeof x === 'number') {
			return 'Number';
		}
		if (typeof x === 'boolean') {
			return 'Boolean';
		}
		if (typeof x === 'string') {
			return 'String';
		}
	},

	// http://ecma-international.org/ecma-262/6.0/#sec-property-descriptor-specification-type
	IsPropertyDescriptor: function IsPropertyDescriptor(Desc) {
		if (this.Type(Desc) !== 'Object') {
			return false;
		}
		var allowed = {
			'[[Configurable]]': true,
			'[[Enumerable]]': true,
			'[[Get]]': true,
			'[[Set]]': true,
			'[[Value]]': true,
			'[[Writable]]': true
		};
		// jscs:disable
		for (var key in Desc) { // eslint-disable-line
			if (has(Desc, key) && !allowed[key]) {
				return false;
			}
		}
		// jscs:enable
		var isData = has(Desc, '[[Value]]');
		var IsAccessor = has(Desc, '[[Get]]') || has(Desc, '[[Set]]');
		if (isData && IsAccessor) {
			throw new TypeError('Property Descriptors may not be both accessor and data descriptors');
		}
		return true;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.1
	IsAccessorDescriptor: function IsAccessorDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return false;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (!has(Desc, '[[Get]]') && !has(Desc, '[[Set]]')) {
			return false;
		}

		return true;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.2
	IsDataDescriptor: function IsDataDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return false;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (!has(Desc, '[[Value]]') && !has(Desc, '[[Writable]]')) {
			return false;
		}

		return true;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.3
	IsGenericDescriptor: function IsGenericDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return false;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (!this.IsAccessorDescriptor(Desc) && !this.IsDataDescriptor(Desc)) {
			return true;
		}

		return false;
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.4
	FromPropertyDescriptor: function FromPropertyDescriptor(Desc) {
		if (typeof Desc === 'undefined') {
			return Desc;
		}

		if (!this.IsPropertyDescriptor(Desc)) {
			throw new TypeError('Desc must be a Property Descriptor');
		}

		if (this.IsDataDescriptor(Desc)) {
			return {
				value: Desc['[[Value]]'],
				writable: !!Desc['[[Writable]]'],
				enumerable: !!Desc['[[Enumerable]]'],
				configurable: !!Desc['[[Configurable]]']
			};
		} else if (this.IsAccessorDescriptor(Desc)) {
			return {
				get: Desc['[[Get]]'],
				set: Desc['[[Set]]'],
				enumerable: !!Desc['[[Enumerable]]'],
				configurable: !!Desc['[[Configurable]]']
			};
		} else {
			throw new TypeError('FromPropertyDescriptor must be called with a fully populated Property Descriptor');
		}
	},

	// http://ecma-international.org/ecma-262/5.1/#sec-8.10.5
	ToPropertyDescriptor: function ToPropertyDescriptor(Obj) {
		if (this.Type(Obj) !== 'Object') {
			throw new TypeError('ToPropertyDescriptor requires an object');
		}

		var desc = {};
		if (has(Obj, 'enumerable')) {
			desc['[[Enumerable]]'] = this.ToBoolean(Obj.enumerable);
		}
		if (has(Obj, 'configurable')) {
			desc['[[Configurable]]'] = this.ToBoolean(Obj.configurable);
		}
		if (has(Obj, 'value')) {
			desc['[[Value]]'] = Obj.value;
		}
		if (has(Obj, 'writable')) {
			desc['[[Writable]]'] = this.ToBoolean(Obj.writable);
		}
		if (has(Obj, 'get')) {
			var getter = Obj.get;
			if (typeof getter !== 'undefined' && !this.IsCallable(getter)) {
				throw new TypeError('getter must be a function');
			}
			desc['[[Get]]'] = getter;
		}
		if (has(Obj, 'set')) {
			var setter = Obj.set;
			if (typeof setter !== 'undefined' && !this.IsCallable(setter)) {
				throw new TypeError('setter must be a function');
			}
			desc['[[Set]]'] = setter;
		}

		if ((has(desc, '[[Get]]') || has(desc, '[[Set]]')) && (has(desc, '[[Value]]') || has(desc, '[[Writable]]'))) {
			throw new TypeError('Invalid property descriptor. Cannot both specify accessors and a value or writable attribute');
		}
		return desc;
	}
};

module.exports = ES5;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;

var isPrimitive = __webpack_require__(6);

var isCallable = __webpack_require__(1);

// https://es5.github.io/#x8.12
var ES5internalSlots = {
	'[[DefaultValue]]': function (O, hint) {
		var actualHint = hint || (toStr.call(O) === '[object Date]' ? String : Number);

		if (actualHint === String || actualHint === Number) {
			var methods = actualHint === String ? ['toString', 'valueOf'] : ['valueOf', 'toString'];
			var value, i;
			for (i = 0; i < methods.length; ++i) {
				if (isCallable(O[methods[i]])) {
					value = O[methods[i]]();
					if (isPrimitive(value)) {
						return value;
					}
				}
			}
			throw new TypeError('No default value');
		}
		throw new TypeError('invalid [[DefaultValue]] hint supplied');
	}
};

// https://es5.github.io/#x9
module.exports = function ToPrimitive(input, PreferredType) {
	if (isPrimitive(input)) {
		return input;
	}
	return ES5internalSlots['[[DefaultValue]]'](input, PreferredType);
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var has = __webpack_require__(0);
var regexExec = RegExp.prototype.exec;
var gOPD = Object.getOwnPropertyDescriptor;

var tryRegexExecCall = function tryRegexExec(value) {
	try {
		var lastIndex = value.lastIndex;
		value.lastIndex = 0;

		regexExec.call(value);
		return true;
	} catch (e) {
		return false;
	} finally {
		value.lastIndex = lastIndex;
	}
};
var toStr = Object.prototype.toString;
var regexClass = '[object RegExp]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isRegex(value) {
	if (!value || typeof value !== 'object') {
		return false;
	}
	if (!hasToStringTag) {
		return toStr.call(value) === regexClass;
	}

	var descriptor = gOPD(value, 'lastIndex');
	var hasLastIndexDataProperty = descriptor && has(descriptor, 'value');
	if (!hasLastIndexDataProperty) {
		return false;
	}

	return tryRegexExecCall(value);
};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(3);
var getPolyfill = __webpack_require__(12);

module.exports = function shimArrayPrototypeFind() {
	var polyfill = getPolyfill();

	define(Array.prototype, { find: polyfill }, {
		find: function () {
			return Array.prototype.find !== polyfill;
		}
	});

	return polyfill;
};


/***/ }),
/* 30 */,
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _loaderUtils = __webpack_require__(13);

(function (global) {
    var ems = global.document.getElementsByTagName('script');
    var regexp = /.*gns\.widget\.initializer\.([^/]+\.)?js/;
    var nEms = ems.length;

    if (global.document.addEventListener) {
        // Global GNS object
        if (!global.GNS) {
            global.GNS = {};
        }
        var GNS = global.GNS;

        // Global GNS elements ( script tags )
        if (!GNS.elements) {
            GNS.elements = [];
        }
        var elements = GNS.elements;

        for (var index = 0; index < nEms; index += 1) {
            var element = ems[index];

            if (element.src.match(regexp) && elements.indexOf(element) < 0) {
                elements.push(element);
                (0, _loaderUtils.initializeWidget)(element, element.attributes);

                var eventMethod = global.addEventListener ? 'addEventListener' : 'attachEvent';
                var eventListener = global[eventMethod];
                var messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';

                eventListener(messageEvent, _loaderUtils.messageEventCallback, true);
            }
        }
    } else {
        for (var _index = 0; _index < nEms; _index += 1) {
            var _element = ems[_index];

            if (_element.src.match(regexp)) {
                var paragraph = global.document.createElement('p');
                paragraph.id = 'browser-not-supported';
                paragraph.innerHTML = 'Unfortunately the browser you are currently using is not supported by our widgets. Please update your browser.';
                _element.parentNode.appendChild(paragraph);
            }
        }
    }
})(global);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYmRmYWNhZGYzYWNjZTZjNzdhOTMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9oYXMvc3JjL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvaXMtY2FsbGFibGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy93ZWJwYWNrL2J1aWxkaW4vZ2xvYmFsLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZGVmaW5lLXByb3BlcnRpZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9lczYuanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9mdW5jdGlvbi1iaW5kL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2hlbHBlcnMvaXNQcmltaXRpdmUuanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9oZWxwZXJzL2lzTmFOLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9pc0Zpbml0ZS5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvc2lnbi5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvbW9kLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvYXJyYXkucHJvdG90eXBlLmZpbmQvaW1wbGVtZW50YXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9hcnJheS5wcm90b3R5cGUuZmluZC9wb2x5ZmlsbC5qcyIsIndlYnBhY2s6Ly8vLi91dGlscy9sb2FkZXIvbG9hZGVyLXV0aWxzLmpzIiwid2VicGFjazovLy8uL2NvbmZpZ3MvZ2F0ZXdheS5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2FycmF5LnByb3RvdHlwZS5maW5kL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvb2JqZWN0LWtleXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9vYmplY3Qta2V5cy9pc0FyZ3VtZW50cy5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2ZvcmVhY2gvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9lczIwMTUuanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9mdW5jdGlvbi1iaW5kL2ltcGxlbWVudGF0aW9uLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2VzNi5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2lzLWRhdGUtb2JqZWN0L2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvaXMtc3ltYm9sL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9hc3NpZ24uanMiLCJ3ZWJwYWNrOi8vLy4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9oZWxwZXJzL2lzUHJpbWl0aXZlLmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvZXM1LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvZXMtdG8tcHJpbWl0aXZlL2VzNS5qcyIsIndlYnBhY2s6Ly8vLi4vbm9kZV9tb2R1bGVzL2lzLXJlZ2V4L2luZGV4LmpzIiwid2VicGFjazovLy8uLi9ub2RlX21vZHVsZXMvYXJyYXkucHJvdG90eXBlLmZpbmQvc2hpbS5qcyIsIndlYnBhY2s6Ly8vLi9nbnMud2lkZ2V0LmluaXRpYWxpemVyLmpzIl0sIm5hbWVzIjpbImV4dHJhY3RIb3N0bmFtZSIsImV4dHJhY3RBY3R1YWxWZXJzaW9uIiwicmVxdWlyZSIsInNoaW0iLCJtZXJnZU9iamVjdCIsInRhcmdldCIsImkiLCJhcmd1bWVudHMiLCJsZW5ndGgiLCJzb3VyY2UiLCJrZXkiLCJoYXNPd25Qcm9wZXJ0eSIsInNldElGcmFtZVZhcmlhYmxlcyIsImlGcmFtZSIsImlGcmFtZUlkIiwiaWQiLCJuYW1lIiwic3JjIiwidGl0bGUiLCJyb2xlIiwiZnJhbWVCb3JkZXIiLCJzY3JvbGxpbmciLCJ3aWR0aCIsImhlaWdodCIsImZyYW1lRWxlbWVudCIsInN0eWxlIiwiY3NzVGV4dCIsIndyaXRlSUZyYW1lRG9jdW1lbnQiLCJ1cmwiLCJjb25maWciLCJkb2MiLCJkb20iLCJjb250ZW50V2luZG93IiwiZG9jdW1lbnQiLCJlcnIiLCJkb21haW4iLCJvcGVuIiwiX2wiLCJqcyIsImNyZWF0ZUVsZW1lbnQiLCJhc3luYyIsImJvZHkiLCJhcHBlbmRDaGlsZCIsIndyaXRlIiwiYmFzZVVSTCIsIkpTT04iLCJzdHJpbmdpZnkiLCJzaG91bGRMb2FkSW5zdGFuYSIsInNob3VsZExvYWRBbmFseXRpY3MiLCJhbmFseXRpY3MiLCJjbG9zZSIsInN3aXRjaElGcmFtZUNvbnRlbnQiLCJzZWxlY3RlZElGcmFtZSIsImdldEVsZW1lbnRCeUlkIiwicGxhY2VIb2xkZXIiLCJnZW5lcmF0ZUlkIiwicGFyZW50Tm9kZSIsImluc2VydEJlZm9yZSIsInJlbW92ZUNoaWxkIiwibmV3RnJhbWUiLCJsb2FkSUZyYW1lIiwiZWxlbWVudCIsInNjcmlwdHMiLCJnZXRFbGVtZW50c0J5VGFnTmFtZSIsImZvckVhY2giLCJjYWxsIiwic2NyaXB0IiwiZmVhdHVyZVN3aXRjaGVzIiwiYWxsIiwiaW5zdGFuYSIsImFjdGl2ZSIsIm1hZ2ljTnVtYmVyIiwiTWF0aCIsInJhbmRvbSIsInBlcmNlbnRhZ2VfYWN0aXZlIiwiZ2V0U2NyaXB0VGFnRGF0YUF0dHJpYnV0ZXMiLCJhdHRyaWJ1dGVzIiwiZGF0YUF0dHJpYnV0ZXMiLCJhdHRyaWJ1dGUiLCJ0ZXN0IiwicGFyYW1ldGVyIiwic3Vic3RyIiwidmFsdWUiLCJkYXRhSGFuZGxlciIsImRhdGEiLCJjYWxsYmFjayIsImN1c3RvbWVyX2lkIiwiYWxsb3dfdGFncyIsImFsbG93X2RhdGFfdGFncyIsInVuZGVmaW5lZCIsInRhZ3MiLCJtYXAiLCJpdGVtIiwidHJpbSIsIk9iamVjdCIsImtleXMiLCJjb25maWdUYWciLCJmaW5kIiwidGFnIiwiY3VzdG9tX2NzcyIsInR5cGUiLCJ2ZXJzaW9uIiwibGFuZ3VhZ2UiLCJsYW5ndWFnZV9jb2RlIiwidGhlbWUiLCJsb2FkV2lkZ2V0Q29uZmlnIiwibG9hZGVyRG9tYWluIiwid2lkZ2V0X2lkIiwiYW50aUNhY2hlIiwiZ2xvYmFsIiwiX19HTlNfQU5USV9DQUNIRSIsIkRhdGUiLCJnZXRUaW1lIiwiY29uZmlnVVJMIiwiZGVmYXVsdCIsImNvbmZpZ0ZpbGUiLCJ4aHIiLCJYTUxIdHRwUmVxdWVzdCIsIm9ucmVhZHlzdGF0ZWNoYW5nZSIsInhoclJlc3BvbnNlIiwicmVhZHlTdGF0ZSIsInN0YXR1cyIsInBhcnNlIiwicmVzcG9uc2VUZXh0IiwiZXJyb3IiLCJyZXNwb25zZVR5cGUiLCJzZXRSZXF1ZXN0SGVhZGVyIiwic2VuZCIsImluaXRpYWxpemVXaWRnZXQiLCJmaW5hbENvbmZpZyIsImNyZWF0ZVRleHROb2RlIiwic3BvcnQiLCJhY3R1YWxWZXJzaW9uIiwicmVwbGFjZSIsImxvYWRJZnJhbWVDaGVja2VyIiwibG9hZEZlYXR1cmVTd2l0Y2hlcyIsImZlYXR1cmVTd2l0Y2hlckNvbmZpZyIsIndpZGdldFVybCIsImNsaWVudF9pZCIsInN0cmluZyIsImNoYXJzIiwiY2hhckF0IiwiZmxvb3IiLCJtZXNzYWdlRXZlbnRDYWxsYmFjayIsImV2ZW50IiwibWVzc2FnZSIsImFjdGlvbiIsInRhcmdldElGcmFtZSIsInBhcmVudFpvb21MZXZlbCIsIndpbmRvdyIsImdldENvbXB1dGVkU3R5bGUiLCJ6b29tIiwiY2VpbCIsImJvdW5kcyIsInNldHRpbmdzIiwibG9hZFZlcnNpb25zIiwibG9hZGVyVVJMIiwidmVyc2lvbnMiLCJjb25maWdGaWxlVVJMIiwia2VlcFByb3RvY29sIiwiaG9zdG5hbWUiLCJpbmRleE9mIiwic3BsaXQiLCJwb3J0IiwiY2xlYW5VcmwiLCJoZWFkZXJzIiwibW9ja3MiLCJlbXMiLCJyZWdleHAiLCJuRW1zIiwiYWRkRXZlbnRMaXN0ZW5lciIsIkdOUyIsImVsZW1lbnRzIiwiaW5kZXgiLCJtYXRjaCIsInB1c2giLCJldmVudE1ldGhvZCIsImV2ZW50TGlzdGVuZXIiLCJtZXNzYWdlRXZlbnQiLCJwYXJhZ3JhcGgiLCJpbm5lckhUTUwiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7OztBQzdEQTs7QUFFQTs7Ozs7Ozs7QUNGQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvRUFBb0UsRUFBRTtBQUN0RTtBQUNBLEVBQUU7QUFDRixlQUFlO0FBQ2Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsNEJBQTRCLGNBQWM7QUFDMUM7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGNBQWMsY0FBYztBQUM1QixnRUFBZ0UsY0FBYztBQUM5RSxzQkFBc0IsaUNBQWlDO0FBQ3ZELDJCQUEyQixjQUFjO0FBQ3pDO0FBQ0E7QUFDQTs7Ozs7OztBQ3RDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsNENBQTRDOztBQUU1Qzs7Ozs7Ozs7QUNwQkE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsZ0NBQWdDO0FBQ25FO0FBQ0EsNEJBQTRCLGNBQWM7QUFDMUM7QUFDQTtBQUNBLEVBQUUsWUFBWTtBQUNkO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjs7QUFFQTs7QUFFQTs7Ozs7Ozs7QUN2REE7O0FBRUE7Ozs7Ozs7O0FDRkE7O0FBRUE7O0FBRUE7Ozs7Ozs7QUNKQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNGQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNGQSwyQ0FBMkMsZ0JBQWdCOztBQUUzRCxrREFBa0QsaUZBQWlGOzs7Ozs7O0FDRm5JO0FBQ0E7QUFDQTs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ0hBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLFlBQVk7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDckJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztRQytkZ0JBLGUsR0FBQUEsZTtRQTJCQUMsb0IsR0FBQUEsb0I7O0FBdGdCaEI7Ozs7OztBQUVBLG1CQUFBQyxDQUFRLEVBQVIsRUFBZ0NDLElBQWhDOztBQUVBLFNBQVNDLFdBQVQsQ0FBcUJDLE1BQXJCLEVBQTZCO0FBQ3pCO0FBQ0EsU0FBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUlDLFVBQVVDLE1BQTlCLEVBQXNDRixHQUF0QyxFQUEyQztBQUN2QztBQUNBLFlBQU1HLFNBQVNGLFVBQVVELENBQVYsQ0FBZjtBQUNBO0FBQ0EsYUFBSyxJQUFNSSxHQUFYLElBQWtCRCxNQUFsQixFQUEwQjtBQUN0QjtBQUNBLGdCQUFJQSxPQUFPRSxjQUFQLENBQXNCRCxHQUF0QixDQUFKLEVBQWdDO0FBQzVCTCx1QkFBT0ssR0FBUCxJQUFjRCxPQUFPQyxHQUFQLENBQWQ7QUFDSDtBQUNKO0FBQ0o7QUFDRCxXQUFPTCxNQUFQO0FBQ0g7O0FBRUQsU0FBU08sa0JBQVQsQ0FBNEJDLE1BQTVCLEVBQW9DQyxRQUFwQyxFQUE4QztBQUMxQ0QsV0FBT0UsRUFBUCxHQUFZRCxRQUFaO0FBQ0FELFdBQU9HLElBQVAsR0FBY0YsUUFBZDtBQUNBRCxXQUFPSSxHQUFQLEdBQWEsa0JBQWIsQ0FIMEMsQ0FHVDtBQUNqQ0osV0FBT0ssS0FBUCxHQUFlLEVBQWY7QUFDQUwsV0FBT00sSUFBUCxHQUFjLGNBQWQ7QUFDQU4sV0FBT08sV0FBUCxHQUFxQixHQUFyQjtBQUNBUCxXQUFPUSxTQUFQLEdBQW1CLElBQW5CO0FBQ0FSLFdBQU9TLEtBQVAsR0FBZSxNQUFmO0FBQ0FULFdBQU9VLE1BQVAsR0FBZ0IsT0FBaEI7QUFDQSxLQUFDVixPQUFPVyxZQUFQLElBQXVCWCxNQUF4QixFQUFnQ1ksS0FBaEMsQ0FBc0NDLE9BQXRDLEdBQWdELDBCQUFoRDtBQUNIOztBQUVELFNBQVNDLG1CQUFULENBQTZCZCxNQUE3QixFQUFxQ2UsR0FBckMsRUFBMENDLE1BQTFDLEVBQWtEO0FBQzlDLFFBQUlDLFlBQUo7QUFDQSxRQUFJQyxZQUFKO0FBQ0EsUUFBSTtBQUNBRCxjQUFNakIsT0FBT21CLGFBQVAsQ0FBcUJDLFFBQTNCO0FBQ0gsS0FGRCxDQUVFLE9BQU9DLEdBQVAsRUFBWTtBQUNWSCxjQUFNRSxTQUFTRSxNQUFmO0FBQ0F0QixlQUFPSSxHQUFQLG1EQUEyRGMsR0FBM0Q7QUFDQUQsY0FBTWpCLE9BQU9tQixhQUFQLENBQXFCQyxRQUEzQjtBQUNIOztBQUVESCxRQUFJTSxJQUFKLEdBQVdDLEVBQVgsR0FBZ0IsWUFBWTtBQUFFO0FBQzFCLFlBQU1DLEtBQUssS0FBS0MsYUFBTCxDQUFtQixRQUFuQixDQUFYO0FBQ0EsWUFBSVIsR0FBSixFQUFTLEtBQUtJLE1BQUwsR0FBY0osR0FBZDtBQUNUTyxXQUFHdkIsRUFBSCxHQUFRLHFCQUFSO0FBQ0F1QixXQUFHckIsR0FBSCxHQUFTVyxHQUFUO0FBQ0FVLFdBQUdFLEtBQUgsR0FBVyxJQUFYO0FBQ0EsYUFBS0MsSUFBTCxDQUFVQyxXQUFWLENBQXNCSixFQUF0QjtBQUNILEtBUEQ7QUFRQVIsUUFBSWEsS0FBSixDQUFVLGNBQVY7QUFDQWIsUUFBSWEsS0FBSixrQkFBeUJkLE9BQU9lLE9BQWhDO0FBQ0FkLFFBQUlhLEtBQUosQ0FBVSx3QkFBVjtBQUNBO0FBQ0EsUUFBSSxJQUFKLEVBQTJDO0FBQ3ZDYixZQUFJYSxLQUFKLENBQVUseUZBQVY7QUFDSDtBQUNEYixRQUFJYSxLQUFKLENBQVUsZ0VBQVY7QUFDQWIsUUFBSWEsS0FBSixDQUFVLHNFQUFWO0FBQ0FiLFFBQUlhLEtBQUosQ0FBVSwwREFBVjtBQUNBYixRQUFJYSxLQUFKLENBQVUsMERBQVY7QUFDQWIsUUFBSWEsS0FBSixDQUFVLFNBQVY7QUFDQWIsUUFBSWEsS0FBSixDQUFVLGdDQUFWO0FBQ0FiLFFBQUlhLEtBQUosZ0NBQXVDRSxLQUFLQyxTQUFMLENBQWVqQixNQUFmLENBQXZDO0FBQ0EsUUFBSWtCLGtCQUFrQmxCLE1BQWxCLENBQUosRUFBK0I7QUFDM0JDLFlBQUlhLEtBQUosQ0FBVSx1WEFBVjtBQUNIO0FBQ0QsUUFBSUssb0JBQW9CbkIsTUFBcEIsQ0FBSixFQUFpQztBQUM3QkMsWUFBSWEsS0FBSixDQUFVLDhQQUFWO0FBQ0g7QUFDRCxRQUFJZCxPQUFPb0IsU0FBUCxJQUFvQnBCLE9BQU9vQixTQUFQLEtBQXFCLEVBQTdDLEVBQWlEO0FBQzdDbkIsWUFBSWEsS0FBSixxRUFBNEVkLE9BQU9vQixTQUFuRiw0SkFBOE9wQixPQUFPb0IsU0FBclA7QUFDSDtBQUNEbkIsUUFBSWEsS0FBSixDQUFVLHVFQUFWO0FBQ0FiLFFBQUlhLEtBQUosQ0FBVSxlQUFWO0FBQ0FiLFFBQUlvQixLQUFKO0FBQ0g7O0FBRUQsU0FBU0MsbUJBQVQsQ0FBNkJ2QixHQUE3QixFQUFrQ0MsTUFBbEMsRUFBMEM7QUFDdEMsUUFBTXVCLGlCQUFpQm5CLFNBQVNvQixjQUFULENBQXdCeEIsT0FBT2YsUUFBL0IsQ0FBdkI7QUFDQSxRQUFNd0MsY0FBY3JCLFNBQVNNLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBcEI7QUFDQWUsZ0JBQVl2QyxFQUFaLEdBQWlCd0MsV0FBVyxFQUFYLENBQWpCO0FBQ0FILG1CQUFlSSxVQUFmLENBQTBCQyxZQUExQixDQUF1Q0gsV0FBdkMsRUFBb0RGLGNBQXBEO0FBQ0FBLG1CQUFlSSxVQUFmLENBQTBCRSxXQUExQixDQUFzQ04sY0FBdEM7O0FBRUEsUUFBTU8sV0FBVzFCLFNBQVNNLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBakI7O0FBRUEzQix1QkFBbUIrQyxRQUFuQixFQUE2QjlCLE9BQU9mLFFBQXBDOztBQUVBd0MsZ0JBQVlFLFVBQVosQ0FBdUJDLFlBQXZCLENBQW9DRSxRQUFwQyxFQUE4Q0wsV0FBOUM7QUFDQUEsZ0JBQVlFLFVBQVosQ0FBdUJFLFdBQXZCLENBQW1DSixXQUFuQzs7QUFFQTNCLHdCQUFvQmdDLFFBQXBCLEVBQThCL0IsR0FBOUIsRUFBbUNDLE1BQW5DO0FBQ0g7O0FBRUQ7Ozs7Ozs7Ozs7QUFVTyxJQUFNK0Isa0NBQWEsU0FBYkEsVUFBYSxDQUFDQyxPQUFELEVBQVVqQyxHQUFWLEVBQWVDLE1BQWYsRUFBMEI7QUFDaEQsUUFBTWhCLFNBQVNvQixTQUFTTSxhQUFULENBQXVCLFFBQXZCLENBQWY7QUFDQVYsV0FBT2YsUUFBUCxHQUFrQnlDLFdBQVcsQ0FBWCxDQUFsQjs7QUFFQTNDLHVCQUFtQkMsTUFBbkIsRUFBMkJnQixPQUFPZixRQUFsQzs7QUFFQSxRQUFNZ0QsVUFBVTdCLFNBQVM4QixvQkFBVCxDQUE4QixRQUE5QixDQUFoQjtBQUNBLE9BQUdDLE9BQUgsQ0FBV0MsSUFBWCxDQUFnQkgsT0FBaEIsRUFBeUIsVUFBQ0ksTUFBRCxFQUFZO0FBQ2pDLFlBQUlBLFdBQVdMLE9BQWYsRUFBd0I7QUFDcEJLLG1CQUFPVixVQUFQLENBQWtCQyxZQUFsQixDQUErQjVDLE1BQS9CLEVBQXVDcUQsTUFBdkM7QUFDQXZDLGdDQUFvQmQsTUFBcEIsRUFBNEJlLEdBQTVCLEVBQWlDQyxNQUFqQztBQUNIO0FBQ0osS0FMRDtBQU1ILENBYk07O0FBZUEsSUFBTWtCLGdEQUFvQixTQUFwQkEsaUJBQW9CLENBQUNsQixNQUFELEVBQVk7QUFDekMsUUFDRSxDQUFDQSxNQUFELElBQ0EsQ0FBQ0EsT0FBT3NDLGVBRFIsSUFFQSxDQUFDdEMsT0FBT3NDLGVBQVAsQ0FBdUJDLEdBRnhCLElBR0EsQ0FBQ3ZDLE9BQU9zQyxlQUFQLENBQXVCQyxHQUF2QixDQUEyQkMsT0FINUIsSUFJQSxDQUFDeEMsT0FBT3NDLGVBQVAsQ0FBdUJDLEdBQXZCLENBQTJCQyxPQUEzQixDQUFtQ0MsTUFMdEMsRUFNRTtBQUNFLGVBQU8sS0FBUDtBQUNIOztBQUVELFFBQU1DLGNBQWNDLEtBQUtDLE1BQUwsS0FBZ0IsR0FBcEM7O0FBRUEsV0FBUTVDLE9BQU9zQyxlQUFQLENBQXVCQyxHQUF2QixDQUEyQkMsT0FBM0IsQ0FBbUNLLGlCQUFuQyxJQUF3REgsV0FBaEU7QUFDSCxDQWRNOztBQWdCQSxJQUFNdkIsb0RBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBQ25CLE1BQUQsRUFBWTtBQUMzQyxRQUNFLENBQUNBLE1BQUQsSUFDQSxDQUFDQSxPQUFPc0MsZUFEUixJQUVBLENBQUN0QyxPQUFPc0MsZUFBUCxDQUF1QkMsR0FGeEIsSUFHQSxDQUFDdkMsT0FBT3NDLGVBQVAsQ0FBdUJDLEdBQXZCLENBQTJCbkIsU0FINUIsSUFJQSxDQUFDcEIsT0FBT3NDLGVBQVAsQ0FBdUJDLEdBQXZCLENBQTJCbkIsU0FBM0IsQ0FBcUNxQixNQUx4QyxFQU1FO0FBQ0UsZUFBTyxLQUFQO0FBQ0g7O0FBRUQsUUFBTUMsY0FBY0MsS0FBS0MsTUFBTCxLQUFnQixHQUFwQzs7QUFFQSxXQUFRNUMsT0FBT3NDLGVBQVAsQ0FBdUJDLEdBQXZCLENBQTJCbkIsU0FBM0IsQ0FBcUN5QixpQkFBckMsSUFBMERILFdBQWxFO0FBQ0gsQ0FkTTs7QUFnQlA7Ozs7Ozs7QUFPTyxJQUFNSSxrRUFBNkIsU0FBN0JBLDBCQUE2QixDQUFDQyxVQUFELEVBQWdCO0FBQ3RELFFBQU1DLGlCQUFpQixFQUF2Qjs7QUFFQSxPQUFHYixPQUFILENBQVdDLElBQVgsQ0FBZ0JXLFVBQWhCLEVBQTRCLFVBQUNFLFNBQUQsRUFBZTtBQUN2QyxZQUFJLFNBQVNDLElBQVQsQ0FBY0QsVUFBVTlELElBQXhCLENBQUosRUFBbUM7QUFDL0IsZ0JBQU1nRSxZQUFZRixVQUFVOUQsSUFBVixDQUFlaUUsTUFBZixDQUFzQixDQUF0QixDQUFsQjtBQUNBSiwyQkFBZUcsU0FBZixJQUE0QkYsVUFBVUksS0FBdEM7QUFDSDtBQUNKLEtBTEQ7O0FBT0EsV0FBT0wsY0FBUDtBQUNILENBWE07O0FBYVAsU0FBU00sV0FBVCxDQUFxQkMsSUFBckIsRUFBMkJ2RCxNQUEzQixFQUFtQ3dELFFBQW5DLEVBQTZDO0FBQ3pDeEQsV0FBT3lELFdBQVAsR0FBcUJGLEtBQUtFLFdBQTFCOztBQUVBLFFBQUlGLEtBQUtHLFVBQUwsS0FBb0IsS0FBeEIsRUFBK0I7QUFDM0IsWUFBSUgsS0FBS0ksZUFBTCxLQUF5QkMsU0FBN0IsRUFBd0M7QUFDcEMsZ0JBQU1DLE9BQU9OLEtBQUtJLGVBQUwsQ0FBcUJHLEdBQXJCLENBQXlCLFVBQUNDLElBQUQ7QUFBQSx1QkFBVUEsS0FBS0MsSUFBTCxFQUFWO0FBQUEsYUFBekIsQ0FBYjs7QUFFQTtBQUNBQyxtQkFBT0MsSUFBUCxDQUFZbEUsTUFBWixFQUFvQm1DLE9BQXBCLENBQTRCLFVBQUNnQyxTQUFELEVBQWU7QUFDdkMsb0JBQUlOLEtBQUtPLElBQUwsQ0FBVSxVQUFDQyxHQUFEO0FBQUEsMkJBQVNBLFFBQVFGLFNBQWpCO0FBQUEsaUJBQVYsQ0FBSixFQUEyQztBQUN2QztBQUNBO0FBQ0FaLHlCQUFLWSxTQUFMLElBQWtCbkUsT0FBT21FLFNBQVAsQ0FBbEI7QUFDSDs7QUFFRCxvQkFBSVosS0FBS1ksU0FBTCxNQUFvQlAsU0FBeEIsRUFBbUM7QUFDL0I7QUFDQUwseUJBQUtZLFNBQUwsSUFBa0JuRSxPQUFPbUUsU0FBUCxDQUFsQjtBQUNIO0FBQ0osYUFYRDtBQVlILFNBaEJELE1BZ0JPO0FBQ0haLG1CQUFPaEYsWUFBWSxFQUFaLEVBQWdCZ0YsSUFBaEIsRUFBc0J2RCxNQUF0QixDQUFQO0FBQ0g7QUFDSixLQXBCRCxNQW9CTztBQUNIO0FBQ0FpRSxlQUFPQyxJQUFQLENBQVlsRSxNQUFaLEVBQW9CbUMsT0FBcEIsQ0FBNEIsVUFBQ2dDLFNBQUQsRUFBZTtBQUN2QyxnQkFBSVosS0FBS1ksU0FBTCxNQUFvQlAsU0FBeEIsRUFBbUM7QUFDL0I7QUFDQUwscUJBQUtZLFNBQUwsSUFBa0JuRSxPQUFPbUUsU0FBUCxDQUFsQjtBQUNIO0FBQ0osU0FMRDtBQU1IOztBQUVEO0FBQ0EsUUFBSW5FLE9BQU9zRSxVQUFQLEtBQXNCVixTQUF0QixJQUFtQzVELE9BQU9zRSxVQUFQLEtBQXNCLEVBQTdELEVBQWlFO0FBQzdEZixhQUFLZSxVQUFMLEdBQWtCdEUsT0FBT3NFLFVBQXpCO0FBQ0g7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsUUFBSWYsS0FBS2dCLElBQUwsS0FBYyxTQUFsQixFQUE2QjtBQUN6QmhCLGFBQUtpQixPQUFMLEdBQWUsTUFBZjtBQUNBakIsYUFBS2tCLFFBQUwsR0FBZ0IsT0FBaEI7QUFDQWxCLGFBQUttQixhQUFMLEdBQXFCLEdBQXJCO0FBQ0FuQixhQUFLb0IsS0FBTCxHQUFhLFNBQWI7QUFDSDs7QUFFRG5CLGFBQVMsSUFBVCxFQUFlRCxJQUFmO0FBQ0g7O0FBRUQ7Ozs7Ozs7QUFPTyxJQUFNcUIsOENBQW1CLFNBQW5CQSxnQkFBbUIsQ0FBQ0MsWUFBRCxFQUFlN0UsTUFBZixFQUF1QndELFFBQXZCLEVBQW9DO0FBQ2hFLFFBQUl4RCxXQUFXNEQsU0FBWCxJQUF3QjVELE9BQU84RSxTQUFQLEtBQXFCbEIsU0FBakQsRUFBNEQ7QUFDeERKLGlCQUFTLHFDQUFULEVBQWdELElBQWhEO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsWUFBTXVCLFlBQVlDLE9BQU9DLGdCQUFQLElBQTJCLElBQUlDLElBQUosR0FBV0MsT0FBWCxFQUE3QztBQUNBSCxlQUFPQyxnQkFBUCxHQUEwQkYsU0FBMUI7O0FBRUE7QUFDQSxZQUFJSyxZQUFZLGtCQUFRQyxPQUFSLENBQWdCRCxTQUFoQztBQUNBLFlBQUlQLGdCQUFnQixrQkFBUUEsWUFBUixDQUFwQixFQUEyQztBQUN2Q08sd0JBQVksa0JBQVFQLFlBQVIsRUFBc0JPLFNBQWxDO0FBQ0g7QUFDRCxZQUFNRSxrQkFBZ0JGLFNBQWhCLEdBQTRCcEYsT0FBTzhFLFNBQW5DLGdCQUF1REMsU0FBN0Q7O0FBRUEsWUFBSXhCLE9BQU8sRUFBWDs7QUFFQSxZQUFNZ0MsTUFBTSxJQUFJQyxjQUFKLEVBQVo7QUFDQUQsWUFBSUUsa0JBQUosR0FBeUIsU0FBU0MsV0FBVCxHQUF1QjtBQUM1QyxnQkFBSUgsSUFBSUksVUFBSixLQUFtQixDQUF2QixFQUEwQjtBQUN0QixvQkFBSUosSUFBSUssTUFBSixLQUFlLEdBQW5CLEVBQXdCO0FBQ3BCLHdCQUFJO0FBQ0FyQywrQkFBT3ZDLEtBQUs2RSxLQUFMLENBQVdOLElBQUlPLFlBQWYsQ0FBUDtBQUNILHFCQUZELENBRUUsT0FBT0MsS0FBUCxFQUFjO0FBQ1p2Qyw4RkFBb0V4RCxPQUFPOEUsU0FBM0UsRUFBd0YsSUFBeEY7QUFDQTtBQUNIO0FBQ0osaUJBUEQsTUFPTztBQUNIdEIsMEZBQW9FeEQsT0FBTzhFLFNBQTNFLEVBQXdGLElBQXhGO0FBQ0E7QUFDSDtBQUNEeEIsNEJBQVlDLElBQVosRUFBa0J2RCxNQUFsQixFQUEwQndELFFBQTFCO0FBQ0g7QUFDSixTQWZEO0FBZ0JBK0IsWUFBSWhGLElBQUosQ0FBUyxLQUFULEVBQWdCK0UsVUFBaEI7QUFDQUMsWUFBSVMsWUFBSixHQUFtQixNQUFuQjtBQUNBVCxZQUFJVSxnQkFBSixDQUFxQixRQUFyQixFQUErQixpQ0FBL0I7QUFDQVYsWUFBSVcsSUFBSjtBQUNIO0FBQ0osQ0F0Q007O0FBd0NQO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNTyxJQUFNQyw4Q0FBbUIsU0FBbkJBLGdCQUFtQixDQUFDbkUsT0FBRCxFQUFVZSxVQUFWLEVBQXlCO0FBQ3JEO0FBQ0EsUUFBTUMsaUJBQWlCRiwyQkFBMkJDLFVBQTNCLENBQXZCO0FBQ0EsUUFBTThCLGVBQWUxRyxnQkFBZ0I2RCxRQUFRNUMsR0FBeEIsQ0FBckI7O0FBRUEsUUFBSWdILGNBQWMsRUFBbEI7O0FBRUE7QUFDQXhCLHFCQUFpQkMsWUFBakIsRUFBK0I3QixjQUEvQixFQUErQyxVQUFDK0MsS0FBRCxFQUFRL0YsTUFBUixFQUFtQjtBQUM5RCxZQUFJK0YsVUFBVSxJQUFkLEVBQW9CO0FBQ2hCL0Qsb0JBQVFMLFVBQVIsQ0FBbUJDLFlBQW5CLENBQWdDeEIsU0FBU2lHLGNBQVQsQ0FBd0JOLEtBQXhCLENBQWhDLEVBQWdFL0QsT0FBaEU7QUFDQTtBQUNIOztBQUVEb0Usc0JBQWM3SCxZQUFZLEVBQVosRUFBZ0I2SCxXQUFoQixFQUE2QnBHLE1BQTdCLENBQWQ7O0FBRUE7QUFDQSxZQUFJQSxPQUFPc0csS0FBUCxLQUFpQjFDLFNBQWpCLElBQThCNUQsT0FBT3VFLElBQVAsS0FBZ0JYLFNBQWxELEVBQTZEO0FBQ3pELGdCQUFJNUQsT0FBTzJFLEtBQVAsS0FBaUJmLFNBQXJCLEVBQWdDO0FBQzVCd0MsNEJBQVl6QixLQUFaLEdBQW9CLFNBQXBCO0FBQ0g7O0FBRUR5Qix3QkFBWXZCLFlBQVosR0FBMkIxRyxnQkFBZ0I2RCxRQUFRNUMsR0FBeEIsQ0FBM0I7QUFDQWdILHdCQUFZRyxhQUFaLEdBQTRCbkkscUJBQXFCNEQsUUFBUTVDLEdBQTdCLENBQTVCO0FBQ0FnSCx3QkFBWXJGLE9BQVosR0FBc0JpQixRQUFRNUMsR0FBUixDQUFZb0gsT0FBWixDQUFvQiwyQkFBcEIsRUFBaUQsRUFBakQsQ0FBdEI7O0FBRUFDLDhCQUFrQnpFLE9BQWxCLEVBQTJCb0UsV0FBM0I7QUFDSDtBQUNKLEtBcEJEOztBQXNCQU0sd0JBQW9CdkksZ0JBQWdCNkQsUUFBUTVDLEdBQXhCLEVBQTZCLElBQTdCLENBQXBCLEVBQXdELFVBQUNpQixHQUFELEVBQU1zRyxxQkFBTixFQUFnQztBQUNwRixZQUFJdEcsUUFBUSxJQUFaLEVBQWtCO0FBQ2QyQixvQkFBUUwsVUFBUixDQUFtQkMsWUFBbkIsQ0FBZ0N4QixTQUFTaUcsY0FBVCxDQUF3QmhHLEdBQXhCLENBQWhDLEVBQThEMkIsT0FBOUQ7QUFDSDs7QUFFRG9FLG9CQUFZOUQsZUFBWixHQUE4QnFFLHFCQUE5QjtBQUNBRiwwQkFBa0J6RSxPQUFsQixFQUEyQm9FLFdBQTNCO0FBQ0gsS0FQRDtBQVFILENBdENNOztBQXdDQSxJQUFNSyxnREFBb0IsU0FBcEJBLGlCQUFvQixDQUFDekUsT0FBRCxFQUFVaEMsTUFBVixFQUFxQjtBQUNsRCxRQUFJQSxPQUFPc0MsZUFBUCxJQUEwQnRDLE9BQU84RSxTQUFyQyxFQUFnRDtBQUM1QyxZQUFJOEIseUJBQXVCNUcsT0FBTzJFLEtBQTlCLFNBQXVDM0UsT0FBT3NHLEtBQTlDLFNBQXVEdEcsT0FBT3VFLElBQTlELG9CQUFKOztBQUVBLFlBQUl2RSxPQUFPNkcsU0FBUCxLQUFxQmpELFNBQXpCLEVBQW9DO0FBQ2hDZ0Qsd0JBQWVBLFNBQWYsV0FBOEI1RyxPQUFPNkcsU0FBckM7QUFDSDtBQUNEOUUsbUJBQVdDLE9BQVgsRUFBb0I0RSxTQUFwQixFQUErQjVHLE1BQS9CO0FBQ0g7QUFDSixDQVRNOztBQVdQOzs7Ozs7QUFNTyxJQUFNMEIsa0NBQWEsU0FBYkEsVUFBYSxDQUFDL0MsTUFBRCxFQUFZO0FBQ2xDLFFBQUltSSxTQUFTLEVBQWI7QUFDQSxRQUFNQyxRQUFRLGdFQUFkOztBQUVBLFNBQUssSUFBSXRJLElBQUksQ0FBYixFQUFnQkEsSUFBSUUsTUFBcEIsRUFBNEJGLEtBQUssQ0FBakMsRUFBb0M7QUFDaENxSSxrQkFBVUMsTUFBTUMsTUFBTixDQUFhckUsS0FBS3NFLEtBQUwsQ0FBV3RFLEtBQUtDLE1BQUwsS0FBZ0JtRSxNQUFNcEksTUFBakMsQ0FBYixDQUFWO0FBQ0g7QUFDRCxXQUFPbUksTUFBUDtBQUNILENBUk07O0FBVVA7Ozs7O0FBS08sSUFBTUksc0RBQXVCLFNBQXZCQSxvQkFBdUIsQ0FBQ0MsS0FBRCxFQUFXO0FBQzNDLFFBQUlDLGdCQUFKO0FBQ0EsUUFBSTtBQUNBQSxrQkFBVXBHLEtBQUs2RSxLQUFMLENBQVdzQixNQUFNNUQsSUFBakIsQ0FBVjtBQUNILEtBRkQsQ0FFRSxPQUFPd0MsS0FBUCxFQUFjO0FBQ1pxQixrQkFBVSxJQUFWO0FBQ0g7O0FBRUQsUUFBSUEsT0FBSixFQUFhO0FBQ1QsWUFBSUEsUUFBUUMsTUFBWixFQUFvQjtBQUNoQixvQkFBUUQsUUFBUUMsTUFBaEI7QUFDSSxxQkFBSyxrQkFBTDtBQUNJLHdCQUFJRCxRQUFRbkksUUFBUixLQUFxQjJFLFNBQXpCLEVBQW9DO0FBQ2hDLDRCQUFNMEQsZUFBZWxILFNBQVNvQixjQUFULENBQXdCNEYsUUFBUW5JLFFBQWhDLENBQXJCO0FBQ0EsNEJBQUlxSSxZQUFKLEVBQWtCO0FBQ2QsZ0NBQU1DLGtCQUFrQkMsT0FBT0MsZ0JBQVAsQ0FBd0JySCxTQUFTOEIsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBeEIsRUFBa0V3RixJQUExRjtBQUNBLGdDQUFJSCxrQkFBa0IsQ0FBdEIsRUFBeUI7QUFDckJELDZDQUFhNUgsTUFBYixHQUFzQmlELEtBQUtnRixJQUFMLENBQVVQLFFBQVFRLE1BQVIsQ0FBZWxJLE1BQWYsR0FBd0I2SCxlQUFsQyxDQUF0QjtBQUNILDZCQUZELE1BRU87QUFDSEQsNkNBQWE1SCxNQUFiLEdBQXNCMEgsUUFBUVEsTUFBUixDQUFlbEksTUFBckM7QUFDSDtBQUNKO0FBQ0o7QUFDRDtBQUNKLHFCQUFLLG9CQUFMO0FBQ0ksd0JBQUkwSCxRQUFRUyxRQUFSLENBQWlCNUksUUFBakIsS0FBOEIyRSxTQUFsQyxFQUE2QztBQUN6Qyw0QkFBTTBELGdCQUFlbEgsU0FBU29CLGNBQVQsQ0FBd0I0RixRQUFRUyxRQUFSLENBQWlCNUksUUFBekMsQ0FBckI7QUFDQSw0QkFBSXFJLGFBQUosRUFBa0I7QUFDZCxnQ0FBSVYseUJBQXVCUSxRQUFRUyxRQUFSLENBQWlCbEQsS0FBeEMsU0FBaUR5QyxRQUFRUyxRQUFSLENBQWlCdkIsS0FBbEUsU0FBMkVjLFFBQVFTLFFBQVIsQ0FBaUJ0RCxJQUE1RixvQkFBSjs7QUFFQSxnQ0FBSTZDLFFBQVFTLFFBQVIsQ0FBaUJoQixTQUFqQixLQUErQmpELFNBQW5DLEVBQThDO0FBQzFDZ0QsNENBQWVBLFNBQWYsV0FBOEJRLFFBQVFTLFFBQVIsQ0FBaUJoQixTQUEvQztBQUNIOztBQUVEdkYsZ0RBQW9Cc0YsU0FBcEIsRUFBK0JRLFFBQVFTLFFBQXZDO0FBQ0g7QUFDSjtBQUNEO0FBQ0o7QUE1Qko7QUE4Qkg7QUFDSjtBQUNKLENBMUNNOztBQTRDUDs7Ozs7O0FBTU8sSUFBTUMsc0NBQWUsU0FBZkEsWUFBZSxDQUFDQyxTQUFELEVBQVl2RSxRQUFaLEVBQXlCO0FBQ2pELFFBQUl3RSxXQUFXLEVBQWY7QUFDQSxRQUFNakQsWUFBWUMsT0FBT0MsZ0JBQVAsSUFBMkIsSUFBSUMsSUFBSixHQUFXQyxPQUFYLEVBQTdDO0FBQ0FILFdBQU9DLGdCQUFQLEdBQTBCRixTQUExQjtBQUNBLFFBQU1PLGFBQWdCeUMsU0FBaEIseUJBQTZDaEQsU0FBbkQ7O0FBRUEsUUFBTVEsTUFBTSxJQUFJQyxjQUFKLEVBQVo7QUFDQUQsUUFBSUUsa0JBQUosR0FBeUIsU0FBU0MsV0FBVCxHQUF1QjtBQUM1QyxZQUFJSCxJQUFJSSxVQUFKLEtBQW1CLENBQXZCLEVBQTBCO0FBQ3RCLGdCQUFJSixJQUFJSyxNQUFKLEtBQWUsR0FBbkIsRUFBd0I7QUFDcEIsb0JBQUk7QUFDQW9DLCtCQUFXaEgsS0FBSzZFLEtBQUwsQ0FBV04sSUFBSU8sWUFBZixDQUFYO0FBQ0gsaUJBRkQsQ0FFRSxPQUFPQyxLQUFQLEVBQWM7QUFDWnZDLDZCQUFTLHFDQUFULEVBQWdELElBQWhEO0FBQ0E7QUFDSDtBQUNKLGFBUEQsTUFPTztBQUNIQSx5QkFBUyxxQ0FBVCxFQUFnRCxJQUFoRDtBQUNBO0FBQ0g7QUFDREEscUJBQVMsSUFBVCxFQUFld0UsUUFBZjtBQUNIO0FBQ0osS0FmRDtBQWdCQXpDLFFBQUloRixJQUFKLENBQVMsS0FBVCxFQUFnQitFLFVBQWhCO0FBQ0FDLFFBQUlTLFlBQUosR0FBbUIsTUFBbkI7QUFDQVQsUUFBSVUsZ0JBQUosQ0FBcUIsUUFBckIsRUFBK0IsaUNBQS9CO0FBQ0FWLFFBQUlXLElBQUo7QUFDSCxDQTNCTTs7QUE2QkEsSUFBTVEsb0RBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBQ3FCLFNBQUQsRUFBWXZFLFFBQVosRUFBeUI7QUFDeEQsUUFBTXVCLFlBQVlDLE9BQU9DLGdCQUFQLElBQTJCLElBQUlDLElBQUosR0FBV0MsT0FBWCxFQUE3QztBQUNBSCxXQUFPQyxnQkFBUCxHQUEwQkYsU0FBMUI7QUFDQSxRQUFNa0QsZ0JBQW1CRixTQUFuQixpQ0FBd0RoRCxTQUE5RDs7QUFFQSxRQUFNUSxNQUFNLElBQUlDLGNBQUosRUFBWjs7QUFFQUQsUUFBSUUsa0JBQUosR0FBeUIsU0FBU0MsV0FBVCxHQUF1QjtBQUM1QyxZQUFJSCxJQUFJSSxVQUFKLEtBQW1CLENBQXZCLEVBQTBCO0FBQ3RCLGdCQUFJSixJQUFJSyxNQUFKLEtBQWUsR0FBbkIsRUFBd0I7QUFDcEIsb0JBQUk7QUFDQXBDLDZCQUFTLElBQVQsRUFBZXhDLEtBQUs2RSxLQUFMLENBQVdOLElBQUlPLFlBQWYsQ0FBZjtBQUNILGlCQUZELENBRUUsT0FBT0MsS0FBUCxFQUFjO0FBQ1p2Qyw2QkFBUyxvREFBVCxFQUErRCxJQUEvRDtBQUNIO0FBQ0osYUFORCxNQU1PO0FBQ0hBLHlCQUFTLG9EQUFULEVBQStELElBQS9EO0FBQ0g7QUFDSjtBQUNKLEtBWkQ7O0FBY0ErQixRQUFJaEYsSUFBSixDQUFTLEtBQVQsRUFBZ0IwSCxhQUFoQjtBQUNBMUMsUUFBSVMsWUFBSixHQUFtQixNQUFuQjtBQUNBVCxRQUFJVSxnQkFBSixDQUFxQixRQUFyQixFQUErQixpQ0FBL0I7QUFDQVYsUUFBSVcsSUFBSjtBQUNILENBekJNOztBQTJCUDs7Ozs7OztBQU9PLFNBQVMvSCxlQUFULENBQXlCNEIsR0FBekIsRUFBb0Q7QUFBQSxRQUF0Qm1JLFlBQXNCLHVFQUFQLEtBQU87O0FBQ3ZELFFBQUlDLGlCQUFKOztBQUVBO0FBQ0EsUUFBSXBJLElBQUlxSSxPQUFKLENBQVksS0FBWixJQUFxQixDQUFDLENBQTFCLEVBQTZCO0FBQ3pCRCxtQkFBV3BJLElBQUlzSSxLQUFKLENBQVUsR0FBVixFQUFlLENBQWYsQ0FBWDtBQUNILEtBRkQsTUFFTztBQUNIRixtQkFBV3BJLElBQUlzSSxLQUFKLENBQVUsR0FBVixFQUFlLENBQWYsQ0FBWDtBQUNIOztBQUVEO0FBQ0EsUUFBTUMsT0FBT0gsU0FBU0UsS0FBVCxDQUFlLEdBQWYsRUFBb0IsQ0FBcEIsQ0FBYjtBQUNBRixlQUFXQSxTQUFTRSxLQUFULENBQWUsR0FBZixFQUFvQixDQUFwQixDQUFYOztBQUVBO0FBQ0FGLGVBQVdBLFNBQVNFLEtBQVQsQ0FBZSxHQUFmLEVBQW9CLENBQXBCLENBQVg7O0FBRUEsUUFBSXRJLElBQUlxSSxPQUFKLENBQVksS0FBWixJQUFxQixDQUFDLENBQXRCLElBQTJCRixZQUEvQixFQUE2QztBQUN6Q0MsbUJBQWNwSSxJQUFJc0ksS0FBSixDQUFVLEdBQVYsRUFBZSxDQUFmLENBQWQsVUFBb0NGLFFBQXBDO0FBQ0EsWUFBSUcsSUFBSixFQUFVO0FBQ05ILHVCQUFjQSxRQUFkLFNBQTBCRyxJQUExQjtBQUNIO0FBQ0o7O0FBRUQsV0FBT0gsUUFBUDtBQUNIOztBQUVNLFNBQVMvSixvQkFBVCxDQUE4QjJCLEdBQTlCLEVBQW1DO0FBQ3RDLFFBQUl3RyxzQkFBSjs7QUFFQTtBQUNBLFFBQU1nQyxXQUFXeEksSUFBSXlHLE9BQUosQ0FBWSw0QkFBWixFQUEwQyxFQUExQyxDQUFqQjs7QUFFQTs7QUFFQSxRQUFJK0IsU0FBU0gsT0FBVCxDQUFpQixLQUFqQixJQUEwQixDQUFDLENBQS9CLEVBQWtDO0FBQzlCN0Isd0JBQWdCZ0MsU0FBU0YsS0FBVCxDQUFlLEdBQWYsRUFBb0IsQ0FBcEIsQ0FBaEI7QUFDSCxLQUZELE1BRU87QUFDSDlCLHdCQUFnQmdDLFNBQVNGLEtBQVQsQ0FBZSxHQUFmLEVBQW9CLENBQXBCLENBQWhCO0FBQ0g7O0FBRUQsUUFBSSxDQUFDOUIsYUFBTCxFQUFvQjtBQUNoQixlQUFPLElBQVA7QUFDSDs7QUFFRDtBQUNBQSxvQkFBZ0JBLGNBQWM4QixLQUFkLENBQW9CLEdBQXBCLEVBQXlCLENBQXpCLENBQWhCOztBQUVBO0FBQ0E5QixvQkFBZ0JBLGNBQWM4QixLQUFkLENBQW9CLEdBQXBCLEVBQXlCLENBQXpCLENBQWhCOztBQUdBLFdBQU85QixhQUFQO0FBQ0gsQzs7Ozs7Ozs7Ozs7OztrQkNoaUJjO0FBQ1hsQixhQUFTO0FBQ0x0RSxpQkFBUyx1REFESjtBQUVMeUgsaUJBQVMsRUFGSjtBQUdMQyxlQUFPO0FBQ0gxSCxxQkFBUyxpREFETjtBQUVIeUgscUJBQVM7QUFGTixTQUhGO0FBT0xwRCxtQkFBVztBQVBOLEtBREU7QUFVWCwwQkFBc0I7QUFDbEJyRSxpQkFBUyx1REFEUztBQUVsQnlILGlCQUFTLEVBRlM7QUFHbEJDLGVBQU87QUFDSDFILHFCQUFTLGtDQUROO0FBRUh5SCxxQkFBUztBQUZOLFNBSFc7QUFPbEJwRCxtQkFBVztBQVBPLEtBVlg7QUFtQlgsOENBQTBDO0FBQ3RDckUsaUJBQVMsdURBRDZCO0FBRXRDeUgsaUJBQVMsRUFGNkI7QUFHdENDLGVBQU87QUFDSDFILHFCQUFTLGlEQUROO0FBRUh5SCxxQkFBUztBQUZOLFNBSCtCO0FBT3RDcEQsbUJBQVc7QUFQMkIsS0FuQi9CO0FBNEJYLDRDQUF3QztBQUNwQ3JFLGlCQUFTLDhDQUQyQjtBQUVwQ3lILGlCQUFTLEVBRjJCO0FBR3BDQyxlQUFPO0FBQ0gxSCxxQkFBUywrQ0FETjtBQUVIeUgscUJBQVM7QUFGTixTQUg2QjtBQU9wQ3BELG1CQUFXO0FBUHlCLEtBNUI3QjtBQXFDWCxvQ0FBZ0M7QUFDNUJyRSxpQkFBUyw4Q0FEbUI7QUFFNUJ5SCxpQkFBUyxFQUZtQjtBQUc1QkMsZUFBTztBQUNIMUgscUJBQVMsK0NBRE47QUFFSHlILHFCQUFTO0FBRk4sU0FIcUI7QUFPNUJwRCxtQkFBVztBQVBpQixLQXJDckI7QUE4Q1gsdUNBQW1DO0FBQy9CckUsaUJBQVMsK0NBRHNCO0FBRS9CeUgsaUJBQVMsRUFGc0I7QUFHL0JDLGVBQU87QUFDSDFILHFCQUFTLDBDQUROO0FBRUh5SCxxQkFBUztBQUZOLFNBSHdCO0FBTy9CcEQsbUJBQVc7QUFQb0IsS0E5Q3hCO0FBdURYLCtCQUEyQjtBQUN2QnJFLGlCQUFTLDRDQURjO0FBRXZCeUgsaUJBQVMsRUFGYztBQUd2QkMsZUFBTztBQUNIMUgscUJBQVMsa0NBRE47QUFFSHlILHFCQUFTO0FBRk4sU0FIZ0I7QUFPdkJwRCxtQkFBVztBQVBZO0FBdkRoQixDOzs7Ozs7O0FDQWY7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEscURBQXFEO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUN6QkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDLGlCQUFpQjtBQUMxRCxzREFBc0Q7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUMsY0FBYztBQUNuRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsbUJBQW1CO0FBQ3BDO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUJBQWlCLHNCQUFzQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FDM0lBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2ZBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLE9BQU87QUFDOUI7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDcEJBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0EsMEJBQTBCOztBQUUxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBLElBQUk7QUFDSjtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBLDZEQUE2RCxVQUFVO0FBQ3ZFO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBLHNDQUFzQyxVQUFVO0FBQ2hELHVCQUF1QixhQUFhO0FBQ3BDO0FBQ0EseUJBQXlCLGNBQWM7QUFDdkMseUJBQXlCLFVBQVU7QUFDbkMsb0JBQW9CLGNBQWM7QUFDbEM7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsVUFBVSxFQUFFO0FBQzdCLCtCQUErQix5QkFBeUI7QUFDeEQ7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsV0FBVztBQUNyQztBQUNBLG1EQUFtRCxVQUFVO0FBQzdEO0FBQ0EsRUFBRTs7QUFFRjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZ0VBQWdFO0FBQ2hFLEVBQUU7O0FBRUY7QUFDQTtBQUNBLGtDQUFrQyxhQUFhO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CO0FBQ3BCLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQzs7QUFFaEM7Ozs7Ozs7O0FDcGpCQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQUFtQixpQkFBaUI7QUFDcEM7QUFDQTs7QUFFQSw4RUFBOEUscUNBQXFDLEVBQUU7O0FBRXJIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ25EQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLHdCQUF3QjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDekVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLG1EQUFtRCxjQUFjO0FBQ2pFO0FBQ0E7Ozs7Ozs7O0FDbkJBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEMsY0FBYztBQUMxRDtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsYUFBYTtBQUMvQyxnREFBZ0QsY0FBYztBQUM5RDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDMUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQ1hBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNGQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSx1QkFBdUIsVUFBVTtBQUNqQywyQ0FBMkMsZUFBZTtBQUMxRDtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLDZEQUE2RCxVQUFVO0FBQ3ZFO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEIsaUJBQWlCLHdCQUF3QjtBQUN6QztBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7QUMzT0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsY0FBYyxvQkFBb0I7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3BDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ3RDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMEJBQTBCLGlCQUFpQjtBQUMzQztBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7Ozs7Ozs7Ozs7O0FDZkE7O0FBRUEsQ0FBQyxVQUFDSixNQUFELEVBQVk7QUFDVCxRQUFNMEQsTUFBTTFELE9BQU81RSxRQUFQLENBQWdCOEIsb0JBQWhCLENBQXFDLFFBQXJDLENBQVo7QUFDQSxRQUFNeUcsU0FBUywwQ0FBZjtBQUNBLFFBQU1DLE9BQU9GLElBQUkvSixNQUFqQjs7QUFFQSxRQUFJcUcsT0FBTzVFLFFBQVAsQ0FBZ0J5SSxnQkFBcEIsRUFBc0M7QUFDbEM7QUFDQSxZQUFJLENBQUM3RCxPQUFPOEQsR0FBWixFQUFpQjtBQUNiOUQsbUJBQU84RCxHQUFQLEdBQWEsRUFBYjtBQUNIO0FBQ0QsWUFBTUEsTUFBTTlELE9BQU84RCxHQUFuQjs7QUFFQTtBQUNBLFlBQUksQ0FBQ0EsSUFBSUMsUUFBVCxFQUFtQjtBQUNmRCxnQkFBSUMsUUFBSixHQUFlLEVBQWY7QUFDSDtBQUNELFlBQU1BLFdBQVdELElBQUlDLFFBQXJCOztBQUVBLGFBQUssSUFBSUMsUUFBUSxDQUFqQixFQUFvQkEsUUFBUUosSUFBNUIsRUFBa0NJLFNBQVMsQ0FBM0MsRUFBOEM7QUFDMUMsZ0JBQU1oSCxVQUFVMEcsSUFBSU0sS0FBSixDQUFoQjs7QUFFQSxnQkFBSWhILFFBQVE1QyxHQUFSLENBQVk2SixLQUFaLENBQWtCTixNQUFsQixLQUE2QkksU0FBU1gsT0FBVCxDQUFpQnBHLE9BQWpCLElBQTRCLENBQTdELEVBQWdFO0FBQzVEK0cseUJBQVNHLElBQVQsQ0FBY2xILE9BQWQ7QUFDQSxtREFBaUJBLE9BQWpCLEVBQTBCQSxRQUFRZSxVQUFsQzs7QUFFQSxvQkFBTW9HLGNBQWNuRSxPQUFPNkQsZ0JBQVAsR0FBMEIsa0JBQTFCLEdBQStDLGFBQW5FO0FBQ0Esb0JBQU1PLGdCQUFnQnBFLE9BQU9tRSxXQUFQLENBQXRCO0FBQ0Esb0JBQU1FLGVBQWVGLGdCQUFnQixhQUFoQixHQUFnQyxXQUFoQyxHQUE4QyxTQUFuRTs7QUFFQUMsOEJBQWNDLFlBQWQscUNBQWtELElBQWxEO0FBQ0g7QUFDSjtBQUNKLEtBM0JELE1BMkJPO0FBQ0gsYUFBSyxJQUFJTCxTQUFRLENBQWpCLEVBQW9CQSxTQUFRSixJQUE1QixFQUFrQ0ksVUFBUyxDQUEzQyxFQUE4QztBQUMxQyxnQkFBTWhILFdBQVUwRyxJQUFJTSxNQUFKLENBQWhCOztBQUVBLGdCQUFJaEgsU0FBUTVDLEdBQVIsQ0FBWTZKLEtBQVosQ0FBa0JOLE1BQWxCLENBQUosRUFBK0I7QUFDM0Isb0JBQU1XLFlBQVl0RSxPQUFPNUUsUUFBUCxDQUFnQk0sYUFBaEIsQ0FBOEIsR0FBOUIsQ0FBbEI7QUFDQTRJLDBCQUFVcEssRUFBVixHQUFlLHVCQUFmO0FBQ0FvSywwQkFBVUMsU0FBVixHQUFzQixnSEFBdEI7QUFDQXZILHlCQUFRTCxVQUFSLENBQW1CZCxXQUFuQixDQUErQnlJLFNBQS9CO0FBQ0g7QUFDSjtBQUNKO0FBQ0osQ0E1Q0QsRUE0Q0d0RSxNQTVDSCxFIiwiZmlsZSI6Imducy53aWRnZXQuaW5pdGlhbGl6ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAzMSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYmRmYWNhZGYzYWNjZTZjNzdhOTMiLCJ2YXIgYmluZCA9IHJlcXVpcmUoJ2Z1bmN0aW9uLWJpbmQnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBiaW5kLmNhbGwoRnVuY3Rpb24uY2FsbCwgT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eSk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvaGFzL3NyYy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxudmFyIGZuVG9TdHIgPSBGdW5jdGlvbi5wcm90b3R5cGUudG9TdHJpbmc7XG5cbnZhciBjb25zdHJ1Y3RvclJlZ2V4ID0gL15cXHMqY2xhc3MgLztcbnZhciBpc0VTNkNsYXNzRm4gPSBmdW5jdGlvbiBpc0VTNkNsYXNzRm4odmFsdWUpIHtcblx0dHJ5IHtcblx0XHR2YXIgZm5TdHIgPSBmblRvU3RyLmNhbGwodmFsdWUpO1xuXHRcdHZhciBzaW5nbGVTdHJpcHBlZCA9IGZuU3RyLnJlcGxhY2UoL1xcL1xcLy4qXFxuL2csICcnKTtcblx0XHR2YXIgbXVsdGlTdHJpcHBlZCA9IHNpbmdsZVN0cmlwcGVkLnJlcGxhY2UoL1xcL1xcKlsuXFxzXFxTXSpcXCpcXC8vZywgJycpO1xuXHRcdHZhciBzcGFjZVN0cmlwcGVkID0gbXVsdGlTdHJpcHBlZC5yZXBsYWNlKC9cXG4vbWcsICcgJykucmVwbGFjZSgvIHsyfS9nLCAnICcpO1xuXHRcdHJldHVybiBjb25zdHJ1Y3RvclJlZ2V4LnRlc3Qoc3BhY2VTdHJpcHBlZCk7XG5cdH0gY2F0Y2ggKGUpIHtcblx0XHRyZXR1cm4gZmFsc2U7IC8vIG5vdCBhIGZ1bmN0aW9uXG5cdH1cbn07XG5cbnZhciB0cnlGdW5jdGlvbk9iamVjdCA9IGZ1bmN0aW9uIHRyeUZ1bmN0aW9uT2JqZWN0KHZhbHVlKSB7XG5cdHRyeSB7XG5cdFx0aWYgKGlzRVM2Q2xhc3NGbih2YWx1ZSkpIHsgcmV0dXJuIGZhbHNlOyB9XG5cdFx0Zm5Ub1N0ci5jYWxsKHZhbHVlKTtcblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSBjYXRjaCAoZSkge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fVxufTtcbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG52YXIgZm5DbGFzcyA9ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG52YXIgZ2VuQ2xhc3MgPSAnW29iamVjdCBHZW5lcmF0b3JGdW5jdGlvbl0nO1xudmFyIGhhc1RvU3RyaW5nVGFnID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgU3ltYm9sLnRvU3RyaW5nVGFnID09PSAnc3ltYm9sJztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc0NhbGxhYmxlKHZhbHVlKSB7XG5cdGlmICghdmFsdWUpIHsgcmV0dXJuIGZhbHNlOyB9XG5cdGlmICh0eXBlb2YgdmFsdWUgIT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIHZhbHVlICE9PSAnb2JqZWN0JykgeyByZXR1cm4gZmFsc2U7IH1cblx0aWYgKGhhc1RvU3RyaW5nVGFnKSB7IHJldHVybiB0cnlGdW5jdGlvbk9iamVjdCh2YWx1ZSk7IH1cblx0aWYgKGlzRVM2Q2xhc3NGbih2YWx1ZSkpIHsgcmV0dXJuIGZhbHNlOyB9XG5cdHZhciBzdHJDbGFzcyA9IHRvU3RyLmNhbGwodmFsdWUpO1xuXHRyZXR1cm4gc3RyQ2xhc3MgPT09IGZuQ2xhc3MgfHwgc3RyQ2xhc3MgPT09IGdlbkNsYXNzO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9pcy1jYWxsYWJsZS9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsInZhciBnO1xyXG5cclxuLy8gVGhpcyB3b3JrcyBpbiBub24tc3RyaWN0IG1vZGVcclxuZyA9IChmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcztcclxufSkoKTtcclxuXHJcbnRyeSB7XHJcblx0Ly8gVGhpcyB3b3JrcyBpZiBldmFsIGlzIGFsbG93ZWQgKHNlZSBDU1ApXHJcblx0ZyA9IGcgfHwgRnVuY3Rpb24oXCJyZXR1cm4gdGhpc1wiKSgpIHx8ICgxLGV2YWwpKFwidGhpc1wiKTtcclxufSBjYXRjaChlKSB7XHJcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcclxuXHRpZih0eXBlb2Ygd2luZG93ID09PSBcIm9iamVjdFwiKVxyXG5cdFx0ZyA9IHdpbmRvdztcclxufVxyXG5cclxuLy8gZyBjYW4gc3RpbGwgYmUgdW5kZWZpbmVkLCBidXQgbm90aGluZyB0byBkbyBhYm91dCBpdC4uLlxyXG4vLyBXZSByZXR1cm4gdW5kZWZpbmVkLCBpbnN0ZWFkIG9mIG5vdGhpbmcgaGVyZSwgc28gaXQnc1xyXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IGc7XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy93ZWJwYWNrL2J1aWxkaW4vZ2xvYmFsLmpzXG4vLyBtb2R1bGUgaWQgPSAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIga2V5cyA9IHJlcXVpcmUoJ29iamVjdC1rZXlzJyk7XG52YXIgZm9yZWFjaCA9IHJlcXVpcmUoJ2ZvcmVhY2gnKTtcbnZhciBoYXNTeW1ib2xzID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgU3ltYm9sKCkgPT09ICdzeW1ib2wnO1xuXG52YXIgdG9TdHIgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xuXG52YXIgaXNGdW5jdGlvbiA9IGZ1bmN0aW9uIChmbikge1xuXHRyZXR1cm4gdHlwZW9mIGZuID09PSAnZnVuY3Rpb24nICYmIHRvU3RyLmNhbGwoZm4pID09PSAnW29iamVjdCBGdW5jdGlvbl0nO1xufTtcblxudmFyIGFyZVByb3BlcnR5RGVzY3JpcHRvcnNTdXBwb3J0ZWQgPSBmdW5jdGlvbiAoKSB7XG5cdHZhciBvYmogPSB7fTtcblx0dHJ5IHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCAneCcsIHsgZW51bWVyYWJsZTogZmFsc2UsIHZhbHVlOiBvYmogfSk7XG4gICAgICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXVudXNlZC12YXJzLCBuby1yZXN0cmljdGVkLXN5bnRheCAqL1xuICAgICAgICBmb3IgKHZhciBfIGluIG9iaikgeyByZXR1cm4gZmFsc2U7IH1cbiAgICAgICAgLyogZXNsaW50LWVuYWJsZSBuby11bnVzZWQtdmFycywgbm8tcmVzdHJpY3RlZC1zeW50YXggKi9cblx0XHRyZXR1cm4gb2JqLnggPT09IG9iajtcblx0fSBjYXRjaCAoZSkgeyAvKiB0aGlzIGlzIElFIDguICovXG5cdFx0cmV0dXJuIGZhbHNlO1xuXHR9XG59O1xudmFyIHN1cHBvcnRzRGVzY3JpcHRvcnMgPSBPYmplY3QuZGVmaW5lUHJvcGVydHkgJiYgYXJlUHJvcGVydHlEZXNjcmlwdG9yc1N1cHBvcnRlZCgpO1xuXG52YXIgZGVmaW5lUHJvcGVydHkgPSBmdW5jdGlvbiAob2JqZWN0LCBuYW1lLCB2YWx1ZSwgcHJlZGljYXRlKSB7XG5cdGlmIChuYW1lIGluIG9iamVjdCAmJiAoIWlzRnVuY3Rpb24ocHJlZGljYXRlKSB8fCAhcHJlZGljYXRlKCkpKSB7XG5cdFx0cmV0dXJuO1xuXHR9XG5cdGlmIChzdXBwb3J0c0Rlc2NyaXB0b3JzKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG9iamVjdCwgbmFtZSwge1xuXHRcdFx0Y29uZmlndXJhYmxlOiB0cnVlLFxuXHRcdFx0ZW51bWVyYWJsZTogZmFsc2UsXG5cdFx0XHR2YWx1ZTogdmFsdWUsXG5cdFx0XHR3cml0YWJsZTogdHJ1ZVxuXHRcdH0pO1xuXHR9IGVsc2Uge1xuXHRcdG9iamVjdFtuYW1lXSA9IHZhbHVlO1xuXHR9XG59O1xuXG52YXIgZGVmaW5lUHJvcGVydGllcyA9IGZ1bmN0aW9uIChvYmplY3QsIG1hcCkge1xuXHR2YXIgcHJlZGljYXRlcyA9IGFyZ3VtZW50cy5sZW5ndGggPiAyID8gYXJndW1lbnRzWzJdIDoge307XG5cdHZhciBwcm9wcyA9IGtleXMobWFwKTtcblx0aWYgKGhhc1N5bWJvbHMpIHtcblx0XHRwcm9wcyA9IHByb3BzLmNvbmNhdChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG1hcCkpO1xuXHR9XG5cdGZvcmVhY2gocHJvcHMsIGZ1bmN0aW9uIChuYW1lKSB7XG5cdFx0ZGVmaW5lUHJvcGVydHkob2JqZWN0LCBuYW1lLCBtYXBbbmFtZV0sIHByZWRpY2F0ZXNbbmFtZV0pO1xuXHR9KTtcbn07XG5cbmRlZmluZVByb3BlcnRpZXMuc3VwcG9ydHNEZXNjcmlwdG9ycyA9ICEhc3VwcG9ydHNEZXNjcmlwdG9ycztcblxubW9kdWxlLmV4cG9ydHMgPSBkZWZpbmVQcm9wZXJ0aWVzO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2RlZmluZS1wcm9wZXJ0aWVzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vZXMyMDE1Jyk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvZXM2LmpzXG4vLyBtb2R1bGUgaWQgPSA0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgaW1wbGVtZW50YXRpb24gPSByZXF1aXJlKCcuL2ltcGxlbWVudGF0aW9uJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQgfHwgaW1wbGVtZW50YXRpb247XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZnVuY3Rpb24tYmluZC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gNVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNQcmltaXRpdmUodmFsdWUpIHtcblx0cmV0dXJuIHZhbHVlID09PSBudWxsIHx8ICh0eXBlb2YgdmFsdWUgIT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIHZhbHVlICE9PSAnb2JqZWN0Jyk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLXRvLXByaW1pdGl2ZS9oZWxwZXJzL2lzUHJpbWl0aXZlLmpzXG4vLyBtb2R1bGUgaWQgPSA2XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwibW9kdWxlLmV4cG9ydHMgPSBOdW1iZXIuaXNOYU4gfHwgZnVuY3Rpb24gaXNOYU4oYSkge1xuXHRyZXR1cm4gYSAhPT0gYTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9pc05hTi5qc1xuLy8gbW9kdWxlIGlkID0gN1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsInZhciAkaXNOYU4gPSBOdW1iZXIuaXNOYU4gfHwgZnVuY3Rpb24gKGEpIHsgcmV0dXJuIGEgIT09IGE7IH07XG5cbm1vZHVsZS5leHBvcnRzID0gTnVtYmVyLmlzRmluaXRlIHx8IGZ1bmN0aW9uICh4KSB7IHJldHVybiB0eXBlb2YgeCA9PT0gJ251bWJlcicgJiYgISRpc05hTih4KSAmJiB4ICE9PSBJbmZpbml0eSAmJiB4ICE9PSAtSW5maW5pdHk7IH07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvZXMtYWJzdHJhY3QvaGVscGVycy9pc0Zpbml0ZS5qc1xuLy8gbW9kdWxlIGlkID0gOFxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gc2lnbihudW1iZXIpIHtcblx0cmV0dXJuIG51bWJlciA+PSAwID8gMSA6IC0xO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9oZWxwZXJzL3NpZ24uanNcbi8vIG1vZHVsZSBpZCA9IDlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG1vZChudW1iZXIsIG1vZHVsbykge1xuXHR2YXIgcmVtYWluID0gbnVtYmVyICUgbW9kdWxvO1xuXHRyZXR1cm4gTWF0aC5mbG9vcihyZW1haW4gPj0gMCA/IHJlbWFpbiA6IHJlbWFpbiArIG1vZHVsbyk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvbW9kLmpzXG4vLyBtb2R1bGUgaWQgPSAxMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxudmFyIEVTID0gcmVxdWlyZSgnZXMtYWJzdHJhY3QvZXM2Jyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZmluZChwcmVkaWNhdGUpIHtcblx0dmFyIGxpc3QgPSBFUy5Ub09iamVjdCh0aGlzKTtcblx0dmFyIGxlbmd0aCA9IEVTLlRvSW50ZWdlcihFUy5Ub0xlbmd0aChsaXN0Lmxlbmd0aCkpO1xuXHRpZiAoIUVTLklzQ2FsbGFibGUocHJlZGljYXRlKSkge1xuXHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0FycmF5I2ZpbmQ6IHByZWRpY2F0ZSBtdXN0IGJlIGEgZnVuY3Rpb24nKTtcblx0fVxuXHRpZiAobGVuZ3RoID09PSAwKSB7XG5cdFx0cmV0dXJuIHVuZGVmaW5lZDtcblx0fVxuXHR2YXIgdGhpc0FyZyA9IGFyZ3VtZW50c1sxXTtcblx0Zm9yICh2YXIgaSA9IDAsIHZhbHVlOyBpIDwgbGVuZ3RoOyBpKyspIHtcblx0XHR2YWx1ZSA9IGxpc3RbaV07XG5cdFx0aWYgKEVTLkNhbGwocHJlZGljYXRlLCB0aGlzQXJnLCBbdmFsdWUsIGksIGxpc3RdKSkge1xuXHRcdFx0cmV0dXJuIHZhbHVlO1xuXHRcdH1cblx0fVxuXHRyZXR1cm4gdW5kZWZpbmVkO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9hcnJheS5wcm90b3R5cGUuZmluZC9pbXBsZW1lbnRhdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gMTFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZ2V0UG9seWZpbGwoKSB7XG5cdC8vIERldGVjdCBpZiBhbiBpbXBsZW1lbnRhdGlvbiBleGlzdHNcblx0Ly8gRGV0ZWN0IGVhcmx5IGltcGxlbWVudGF0aW9ucyB3aGljaCBza2lwcGVkIGhvbGVzIGluIHNwYXJzZSBhcnJheXNcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXNwYXJzZS1hcnJheXNcblx0dmFyIGltcGxlbWVudGVkID0gQXJyYXkucHJvdG90eXBlLmZpbmQgJiYgWywgMV0uZmluZChmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuIHRydWU7XG5cdH0pICE9PSAxO1xuXG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBnbG9iYWwtcmVxdWlyZVxuXHRyZXR1cm4gaW1wbGVtZW50ZWQgPyBBcnJheS5wcm90b3R5cGUuZmluZCA6IHJlcXVpcmUoJy4vaW1wbGVtZW50YXRpb24nKTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvYXJyYXkucHJvdG90eXBlLmZpbmQvcG9seWZpbGwuanNcbi8vIG1vZHVsZSBpZCA9IDEyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiaW1wb3J0IGNvbmZpZ3MgZnJvbSAnY29uZmlncy9nYXRld2F5JztcblxucmVxdWlyZSgnYXJyYXkucHJvdG90eXBlLmZpbmQnKS5zaGltKCk7XG5cbmZ1bmN0aW9uIG1lcmdlT2JqZWN0KHRhcmdldCkge1xuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wbHVzcGx1c1xuICAgIGZvciAobGV0IGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItcmVzdC1wYXJhbXNcbiAgICAgICAgY29uc3Qgc291cmNlID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcmVzdHJpY3RlZC1zeW50YXhcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gc291cmNlKSB7XG4gICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcHJvdG90eXBlLWJ1aWx0aW5zXG4gICAgICAgICAgICBpZiAoc291cmNlLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgICB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiB0YXJnZXQ7XG59XG5cbmZ1bmN0aW9uIHNldElGcmFtZVZhcmlhYmxlcyhpRnJhbWUsIGlGcmFtZUlkKSB7XG4gICAgaUZyYW1lLmlkID0gaUZyYW1lSWQ7XG4gICAgaUZyYW1lLm5hbWUgPSBpRnJhbWVJZDtcbiAgICBpRnJhbWUuc3JjID0gJ2phdmFzY3JpcHQ6ZmFsc2UnOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgaUZyYW1lLnRpdGxlID0gJyc7XG4gICAgaUZyYW1lLnJvbGUgPSAncHJlc2VudGF0aW9uJztcbiAgICBpRnJhbWUuZnJhbWVCb3JkZXIgPSAnMCc7XG4gICAgaUZyYW1lLnNjcm9sbGluZyA9ICdubyc7XG4gICAgaUZyYW1lLndpZHRoID0gJzEwMCUnO1xuICAgIGlGcmFtZS5oZWlnaHQgPSAnMTM3cHgnO1xuICAgIChpRnJhbWUuZnJhbWVFbGVtZW50IHx8IGlGcmFtZSkuc3R5bGUuY3NzVGV4dCA9ICdib3JkZXI6MDttaW4td2lkdGg6MTAwJTsnO1xufVxuXG5mdW5jdGlvbiB3cml0ZUlGcmFtZURvY3VtZW50KGlGcmFtZSwgdXJsLCBjb25maWcpIHtcbiAgICBsZXQgZG9jO1xuICAgIGxldCBkb207XG4gICAgdHJ5IHtcbiAgICAgICAgZG9jID0gaUZyYW1lLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQ7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIGRvbSA9IGRvY3VtZW50LmRvbWFpbjtcbiAgICAgICAgaUZyYW1lLnNyYyA9IGBqYXZhc2NyaXB0OnZhciBkPWRvY3VtZW50Lm9wZW4oKTtkLmRvbWFpbj1cIiR7ZG9tfVwiO3ZvaWQoMCk7YDtcbiAgICAgICAgZG9jID0gaUZyYW1lLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQ7XG4gICAgfVxuXG4gICAgZG9jLm9wZW4oKS5fbCA9IGZ1bmN0aW9uICgpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgICAgICBjb25zdCBqcyA9IHRoaXMuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICAgIGlmIChkb20pIHRoaXMuZG9tYWluID0gZG9tO1xuICAgICAgICBqcy5pZCA9ICd3aWRnZXQtaWZyYW1lLWFzeW5jJztcbiAgICAgICAganMuc3JjID0gdXJsO1xuICAgICAgICBqcy5hc3luYyA9IHRydWU7XG4gICAgICAgIHRoaXMuYm9keS5hcHBlbmRDaGlsZChqcyk7XG4gICAgfTtcbiAgICBkb2Mud3JpdGUoJzxodG1sPjxoZWFkPicpO1xuICAgIGRvYy53cml0ZShgPGJhc2UgaHJlZj1cIiR7Y29uZmlnLmJhc2VVUkx9XCI+YCk7XG4gICAgZG9jLndyaXRlKCc8bWV0YSBjaGFyc2V0PVwidXRmLThcIj4nKTtcbiAgICAvLyBJbmplY3QgUmVhY3QgRGV2ZWxvcGVyIFRvb2xzIGlmIG5vdCBpbiBwcm9kdWN0aW9uXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgZG9jLndyaXRlKCc8c2NyaXB0Pl9fUkVBQ1RfREVWVE9PTFNfR0xPQkFMX0hPT0tfXyA9IHBhcmVudC5fX1JFQUNUX0RFVlRPT0xTX0dMT0JBTF9IT09LX188L3NjcmlwdD4nKTtcbiAgICB9XG4gICAgZG9jLndyaXRlKCc8bWV0YSBodHRwLWVxdWl2PVwiWC1VQS1Db21wYXRpYmxlXCIgY29udGVudD1cIklFPWVkZ2UsY2hyb21lPTFcIj4nKTtcbiAgICBkb2Mud3JpdGUoJzxtZXRhIG5hbWU9XCJ2aWV3cG9ydFwiIGNvbnRlbnQ9XCJ3aWR0aD1kZXZpY2Utd2lkdGgsIGluaXRpYWwtc2NhbGU9MVwiPicpO1xuICAgIGRvYy53cml0ZSgnPG1ldGEgbmFtZT1cImFwcGxlLW1vYmlsZS13ZWItYXBwLWNhcGFibGVcIiBjb250ZW50PVwieWVzXCI+Jyk7XG4gICAgZG9jLndyaXRlKCc8bWV0YSBodHRwLWVxdWl2PVwiQ2FjaGUtQ29udHJvbFwiIGNvbnRlbnQ9XCJuby10cmFuc2Zvcm1cIj4nKTtcbiAgICBkb2Mud3JpdGUoJzwvaGVhZD4nKTtcbiAgICBkb2Mud3JpdGUoJzxib2R5IG9ubG9hZD1cImRvY3VtZW50Ll9sKCk7XCI+Jyk7XG4gICAgZG9jLndyaXRlKGA8c2NyaXB0PndpbmRvdy5zZXR0aW5ncyA9ICR7SlNPTi5zdHJpbmdpZnkoY29uZmlnKX07PC9zY3JpcHQ+YCk7XG4gICAgaWYgKHNob3VsZExvYWRJbnN0YW5hKGNvbmZpZykpIHtcbiAgICAgICAgZG9jLndyaXRlKCc8c2NyaXB0PihmdW5jdGlvbihpLHMsbyxnLHIsYSxtKXtpW1xcJ0luc3RhbmFFdW1PYmplY3RcXCddPXI7aVtyXT1pW3JdfHxmdW5jdGlvbigpeyhpW3JdLnE9aVtyXS5xfHxbXSkucHVzaChhcmd1bWVudHMpfSwgaVtyXS5sPTEqbmV3IERhdGUoKTthPXMuY3JlYXRlRWxlbWVudChvKSwgbT1zLmdldEVsZW1lbnRzQnlUYWdOYW1lKG8pWzBdO2EuYXN5bmM9MTthLnNyYz1nO20ucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoYSwgbSl9KSh3aW5kb3csIGRvY3VtZW50LCBcXCdzY3JpcHRcXCcsIFxcJy8vZXVtLmluc3RhbmEuaW8vZXVtLm1pbi5qc1xcJywgXFwnaW5ldW1cXCcpO2luZXVtKFxcJ2FwaUtleVxcJywgXFwnbzR5WVQ3cTdUVXFQamZhcHFEM3Bpd1xcJyk7PC9zY3JpcHQ+Jyk7XG4gICAgfVxuICAgIGlmIChzaG91bGRMb2FkQW5hbHl0aWNzKGNvbmZpZykpIHtcbiAgICAgICAgZG9jLndyaXRlKCc8c2NyaXB0IGFzeW5jIHNyYz1cImh0dHBzOi8vd3d3Lmdvb2dsZXRhZ21hbmFnZXIuY29tL2d0YWcvanM/aWQ9VUEtNTgzMjM2MjUtMTJcIj48L3NjcmlwdD48c2NyaXB0PndpbmRvdy5kYXRhTGF5ZXIgPSB3aW5kb3cuZGF0YUxheWVyIHx8IFtdO2Z1bmN0aW9uIGd0YWcoKXtkYXRhTGF5ZXIucHVzaChhcmd1bWVudHMpO31ndGFnKFxcJ2pzXFwnLCBuZXcgRGF0ZSgpKTtndGFnKFxcJ2NvbmZpZ1xcJywgXFwnVUEtNTgzMjM2MjUtMTJcXCcpOzwvc2NyaXB0PicpO1xuICAgIH1cbiAgICBpZiAoY29uZmlnLmFuYWx5dGljcyAmJiBjb25maWcuYW5hbHl0aWNzICE9PSAnJykge1xuICAgICAgICBkb2Mud3JpdGUoYDxzY3JpcHQgYXN5bmMgc3JjPVwiaHR0cHM6Ly93d3cuZ29vZ2xldGFnbWFuYWdlci5jb20vZ3RhZy9qcz9pZD0ke2NvbmZpZy5hbmFseXRpY3N9XCI+PC9zY3JpcHQ+PHNjcmlwdD53aW5kb3cuZGF0YUxheWVyID0gd2luZG93LmRhdGFMYXllciB8fCBbXTtmdW5jdGlvbiBndGFnKCl7ZGF0YUxheWVyLnB1c2goYXJndW1lbnRzKTt9Z3RhZygnanMnLCBuZXcgRGF0ZSgpKTtndGFnKCdjb25maWcnLCAnJHtjb25maWcuYW5hbHl0aWNzfScpOzwvc2NyaXB0PmApO1xuICAgIH1cbiAgICBkb2Mud3JpdGUoJzxkaXYgaWQ9XCJtYWluQ29udGVudFwiIGNsYXNzPVwiY29udGFpbmVyXCI+PGRpdiBpZD1cIndpZGdldFwiPjwvZGl2PjwvZGl2PicpO1xuICAgIGRvYy53cml0ZSgnPC9ib2R5PjxodG1sPicpO1xuICAgIGRvYy5jbG9zZSgpO1xufVxuXG5mdW5jdGlvbiBzd2l0Y2hJRnJhbWVDb250ZW50KHVybCwgY29uZmlnKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRJRnJhbWUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb25maWcuaUZyYW1lSWQpO1xuICAgIGNvbnN0IHBsYWNlSG9sZGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgcGxhY2VIb2xkZXIuaWQgPSBnZW5lcmF0ZUlkKDEyKTtcbiAgICBzZWxlY3RlZElGcmFtZS5wYXJlbnROb2RlLmluc2VydEJlZm9yZShwbGFjZUhvbGRlciwgc2VsZWN0ZWRJRnJhbWUpO1xuICAgIHNlbGVjdGVkSUZyYW1lLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc2VsZWN0ZWRJRnJhbWUpO1xuXG4gICAgY29uc3QgbmV3RnJhbWUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpZnJhbWUnKTtcblxuICAgIHNldElGcmFtZVZhcmlhYmxlcyhuZXdGcmFtZSwgY29uZmlnLmlGcmFtZUlkKTtcblxuICAgIHBsYWNlSG9sZGVyLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKG5ld0ZyYW1lLCBwbGFjZUhvbGRlcik7XG4gICAgcGxhY2VIb2xkZXIucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChwbGFjZUhvbGRlcik7XG5cbiAgICB3cml0ZUlGcmFtZURvY3VtZW50KG5ld0ZyYW1lLCB1cmwsIGNvbmZpZyk7XG59XG5cbi8qKlxuICogbG9hZElGcmFtZVxuICpcbiAqIEdlbmVyYXRlcyBpRnJhbWUgYW5kIGxvYWRzIHRoZSBnaXZlbiB1cmwgYXMgc2NyaXB0LXRhZ1xuICpcbiAqIEBwYXJhbSBlbGVtZW50XG4gKiBAcGFyYW0gdXJsXG4gKiBAcGFyYW0gY29uZmlnXG4gKi9cblxuZXhwb3J0IGNvbnN0IGxvYWRJRnJhbWUgPSAoZWxlbWVudCwgdXJsLCBjb25maWcpID0+IHtcbiAgICBjb25zdCBpRnJhbWUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpZnJhbWUnKTtcbiAgICBjb25maWcuaUZyYW1lSWQgPSBnZW5lcmF0ZUlkKDYpO1xuXG4gICAgc2V0SUZyYW1lVmFyaWFibGVzKGlGcmFtZSwgY29uZmlnLmlGcmFtZUlkKTtcblxuICAgIGNvbnN0IHNjcmlwdHMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0Jyk7XG4gICAgW10uZm9yRWFjaC5jYWxsKHNjcmlwdHMsIChzY3JpcHQpID0+IHtcbiAgICAgICAgaWYgKHNjcmlwdCA9PT0gZWxlbWVudCkge1xuICAgICAgICAgICAgc2NyaXB0LnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGlGcmFtZSwgc2NyaXB0KTtcbiAgICAgICAgICAgIHdyaXRlSUZyYW1lRG9jdW1lbnQoaUZyYW1lLCB1cmwsIGNvbmZpZyk7XG4gICAgICAgIH1cbiAgICB9KTtcbn07XG5cbmV4cG9ydCBjb25zdCBzaG91bGRMb2FkSW5zdGFuYSA9IChjb25maWcpID0+IHtcbiAgICBpZiAoXG4gICAgICAhY29uZmlnIHx8XG4gICAgICAhY29uZmlnLmZlYXR1cmVTd2l0Y2hlcyB8fFxuICAgICAgIWNvbmZpZy5mZWF0dXJlU3dpdGNoZXMuYWxsIHx8XG4gICAgICAhY29uZmlnLmZlYXR1cmVTd2l0Y2hlcy5hbGwuaW5zdGFuYSB8fFxuICAgICAgIWNvbmZpZy5mZWF0dXJlU3dpdGNoZXMuYWxsLmluc3RhbmEuYWN0aXZlXG4gICAgKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBjb25zdCBtYWdpY051bWJlciA9IE1hdGgucmFuZG9tKCkgKiAxMDA7XG5cbiAgICByZXR1cm4gKGNvbmZpZy5mZWF0dXJlU3dpdGNoZXMuYWxsLmluc3RhbmEucGVyY2VudGFnZV9hY3RpdmUgPj0gbWFnaWNOdW1iZXIpO1xufTtcblxuZXhwb3J0IGNvbnN0IHNob3VsZExvYWRBbmFseXRpY3MgPSAoY29uZmlnKSA9PiB7XG4gICAgaWYgKFxuICAgICAgIWNvbmZpZyB8fFxuICAgICAgIWNvbmZpZy5mZWF0dXJlU3dpdGNoZXMgfHxcbiAgICAgICFjb25maWcuZmVhdHVyZVN3aXRjaGVzLmFsbCB8fFxuICAgICAgIWNvbmZpZy5mZWF0dXJlU3dpdGNoZXMuYWxsLmFuYWx5dGljcyB8fFxuICAgICAgIWNvbmZpZy5mZWF0dXJlU3dpdGNoZXMuYWxsLmFuYWx5dGljcy5hY3RpdmVcbiAgICApIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGNvbnN0IG1hZ2ljTnVtYmVyID0gTWF0aC5yYW5kb20oKSAqIDEwMDtcblxuICAgIHJldHVybiAoY29uZmlnLmZlYXR1cmVTd2l0Y2hlcy5hbGwuYW5hbHl0aWNzLnBlcmNlbnRhZ2VfYWN0aXZlID49IG1hZ2ljTnVtYmVyKTtcbn07XG5cbi8qKlxuICogZ2V0U2NyaXB0VGFnRGF0YUF0dHJpYnV0ZXNcbiAqXG4gKiBQYXJzZSBzY3JpcHQgdGFnIGF0dHJpYnV0ZXMgYW5kIHJldHVybiBhbGwgZGF0YS0qIGF0dHJpYnV0ZXMgYXMgb2JqZWN0LlxuICogQHBhcmFtIGF0dHJpYnV0ZXNcbiAqIEByZXR1cm5zIHt7fX1cbiAqL1xuZXhwb3J0IGNvbnN0IGdldFNjcmlwdFRhZ0RhdGFBdHRyaWJ1dGVzID0gKGF0dHJpYnV0ZXMpID0+IHtcbiAgICBjb25zdCBkYXRhQXR0cmlidXRlcyA9IHt9O1xuXG4gICAgW10uZm9yRWFjaC5jYWxsKGF0dHJpYnV0ZXMsIChhdHRyaWJ1dGUpID0+IHtcbiAgICAgICAgaWYgKC9eZGF0YS0vLnRlc3QoYXR0cmlidXRlLm5hbWUpKSB7XG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSBhdHRyaWJ1dGUubmFtZS5zdWJzdHIoNSk7XG4gICAgICAgICAgICBkYXRhQXR0cmlidXRlc1twYXJhbWV0ZXJdID0gYXR0cmlidXRlLnZhbHVlO1xuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICByZXR1cm4gZGF0YUF0dHJpYnV0ZXM7XG59O1xuXG5mdW5jdGlvbiBkYXRhSGFuZGxlcihkYXRhLCBjb25maWcsIGNhbGxiYWNrKSB7XG4gICAgY29uZmlnLmN1c3RvbWVyX2lkID0gZGF0YS5jdXN0b21lcl9pZDtcblxuICAgIGlmIChkYXRhLmFsbG93X3RhZ3MgPT09ICd5ZXMnKSB7XG4gICAgICAgIGlmIChkYXRhLmFsbG93X2RhdGFfdGFncyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBjb25zdCB0YWdzID0gZGF0YS5hbGxvd19kYXRhX3RhZ3MubWFwKChpdGVtKSA9PiBpdGVtLnRyaW0oKSk7XG5cbiAgICAgICAgICAgIC8vIExvb3AgdGhyb3VnaCBhbGwgZ2l2ZW4gY29uZmlnIGF0dHJpYnV0ZXNcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKGNvbmZpZykuZm9yRWFjaCgoY29uZmlnVGFnKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHRhZ3MuZmluZCgodGFnKSA9PiB0YWcgPT09IGNvbmZpZ1RhZykpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gYXR0cmlidXRlIGV4aXN0cyBpbiBhbGxvd19kYXRhX3RhZ3MgYW5kIGRlZmF1bHQgY29uZmlnLlxuICAgICAgICAgICAgICAgICAgICAvLyBzbyB3ZSBtYXkgb3ZlcnJpZGUgdGhlIGRhdGEgYXR0cmlidXRlIHdpdGggZ2l2ZW4gY29uZmlnIGF0dHJpYnV0ZVxuICAgICAgICAgICAgICAgICAgICBkYXRhW2NvbmZpZ1RhZ10gPSBjb25maWdbY29uZmlnVGFnXTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoZGF0YVtjb25maWdUYWddID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSBhZGQgaXQgaWYgaXQgZG9lc24ndCBleGlzdHMgaW4gdGhlIGRlZmF1bHQgY29uZmlnXG4gICAgICAgICAgICAgICAgICAgIGRhdGFbY29uZmlnVGFnXSA9IGNvbmZpZ1tjb25maWdUYWddO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgZGF0YSA9IG1lcmdlT2JqZWN0KHt9LCBkYXRhLCBjb25maWcpO1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgLy8gTG9vcCB0aHJvdWdoIGFsbCBnaXZlbiBjb25maWcgYXR0cmlidXRlc1xuICAgICAgICBPYmplY3Qua2V5cyhjb25maWcpLmZvckVhY2goKGNvbmZpZ1RhZykgPT4ge1xuICAgICAgICAgICAgaWYgKGRhdGFbY29uZmlnVGFnXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSBhZGQgaXQgaWYgaXQgZG9lc24ndCBleGlzdHMgaW4gdGhlIGRlZmF1bHQgY29uZmlnXG4gICAgICAgICAgICAgICAgZGF0YVtjb25maWdUYWddID0gY29uZmlnW2NvbmZpZ1RhZ107XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIEFkZCBjdXN0b21fY3NzIHRvIHRoZSBkYXRhXG4gICAgaWYgKGNvbmZpZy5jdXN0b21fY3NzICE9PSB1bmRlZmluZWQgJiYgY29uZmlnLmN1c3RvbV9jc3MgIT09ICcnKSB7XG4gICAgICAgIGRhdGEuY3VzdG9tX2NzcyA9IGNvbmZpZy5jdXN0b21fY3NzO1xuICAgIH1cblxuICAgIC8vIFRPRE86IEluIHRoZSBuZWFyIGZ1dHVyZSB3ZSdyZSBnb2luZyB0byBhZGQgYSByZWFsIGRvbWFpbiBjaGVjay4uLlxuICAgIC8vIGlmICghY2hlY2tEb21haW4oZGF0YSwgZG9jdW1lbnQucmVmZXJyZXIudG9TdHJpbmcoKSkpIHtcbiAgICAvLyAgICAgY2FsbGJhY2soJ0Vycm9yOiBkb21haW4gaXMgbm90IGFsbG93ZWQuJywgbnVsbCk7XG4gICAgLy8gfVxuXG4gICAgaWYgKGRhdGEudHlwZSA9PT0gJ3RhZ2dpbmcnKSB7XG4gICAgICAgIGRhdGEudmVyc2lvbiA9ICd2Mi4wJztcbiAgICAgICAgZGF0YS5sYW5ndWFnZSA9ICdlbl9HQic7XG4gICAgICAgIGRhdGEubGFuZ3VhZ2VfY29kZSA9ICcyJztcbiAgICAgICAgZGF0YS50aGVtZSA9ICdkZWZhdWx0JztcbiAgICB9XG5cbiAgICBjYWxsYmFjayhudWxsLCBkYXRhKTtcbn1cblxuLyoqXG4gKiBsb2FkV2lkZ2V0Q29uZmlnXG4gKlxuICogQHBhcmFtIGxvYWRlckRvbWFpblxuICogQHBhcmFtIGNvbmZpZ1xuICogQHBhcmFtIGNhbGxiYWNrXG4gKi9cbmV4cG9ydCBjb25zdCBsb2FkV2lkZ2V0Q29uZmlnID0gKGxvYWRlckRvbWFpbiwgY29uZmlnLCBjYWxsYmFjaykgPT4ge1xuICAgIGlmIChjb25maWcgPT09IHVuZGVmaW5lZCB8fCBjb25maWcud2lkZ2V0X2lkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgY2FsbGJhY2soJ0Vycm9yOiBjb3VsZCBub3QgbG9hZCB3aWRnZXQgY29uZmlnJywgbnVsbCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgY29uc3QgYW50aUNhY2hlID0gZ2xvYmFsLl9fR05TX0FOVElfQ0FDSEUgfHwgbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgIGdsb2JhbC5fX0dOU19BTlRJX0NBQ0hFID0gYW50aUNhY2hlO1xuXG4gICAgICAgIC8vIEdlbmVyYXRlIHdpZGdldCBjb25maWd1cmF0aW9uIHVybFxuICAgICAgICBsZXQgY29uZmlnVVJMID0gY29uZmlncy5kZWZhdWx0LmNvbmZpZ1VSTDtcbiAgICAgICAgaWYgKGxvYWRlckRvbWFpbiAmJiBjb25maWdzW2xvYWRlckRvbWFpbl0pIHtcbiAgICAgICAgICAgIGNvbmZpZ1VSTCA9IGNvbmZpZ3NbbG9hZGVyRG9tYWluXS5jb25maWdVUkw7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgY29uZmlnRmlsZSA9IGAke2NvbmZpZ1VSTH0ke2NvbmZpZy53aWRnZXRfaWR9Lmpzb24/dj0ke2FudGlDYWNoZX1gO1xuXG4gICAgICAgIGxldCBkYXRhID0ge307XG5cbiAgICAgICAgY29uc3QgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgIHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbiB4aHJSZXNwb25zZSgpIHtcbiAgICAgICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gNCkge1xuICAgICAgICAgICAgICAgIGlmICh4aHIuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHhoci5yZXNwb25zZVRleHQpO1xuICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soYEVycm9yOiBjb3VsZCBub3QgbG9hZCBjb25maWd1cmF0aW9uIGZpbGUgZm9yIHdpZGdldCBpZDogJHtjb25maWcud2lkZ2V0X2lkfWAsIG51bGwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soYEVycm9yOiBjb3VsZCBub3QgbG9hZCBjb25maWd1cmF0aW9uIGZpbGUgZm9yIHdpZGdldCBpZDogJHtjb25maWcud2lkZ2V0X2lkfWAsIG51bGwpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGRhdGFIYW5kbGVyKGRhdGEsIGNvbmZpZywgY2FsbGJhY2spO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB4aHIub3BlbignZ2V0JywgY29uZmlnRmlsZSk7XG4gICAgICAgIHhoci5yZXNwb25zZVR5cGUgPSAndGV4dCc7XG4gICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdBY2NlcHQnLCAnYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOCcpO1xuICAgICAgICB4aHIuc2VuZCgpO1xuICAgIH1cbn07XG5cbi8vIFRPRE86IEluIHRoZSBuZWFyIGZ1dHVyZSB3ZSdyZSBnb2luZyB0byBhZGQgYSByZWFsIGRvbWFpbiBjaGVjay4uLlxuLyoqXG4gKiBjaGVja0RvbWFpblxuICpcbiAqIEBwYXJhbSBzZXR0aW5nc1xuICogQHBhcmFtIHJlZmVycmVyXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuLy8gZXhwb3J0IGNvbnN0IGNoZWNrRG9tYWluID0gKHNldHRpbmdzLCByZWZlcnJlcikgPT4ge1xuLy8gICAgIGNvbnN0IHJlZmVycmVyQXJyYXkgPSByZWZlcnJlci5zdWJzdHIoNykucmVwbGFjZSgnLycsICcuJykuc3BsaXQoJy4nKTtcbi8vICAgICBsZXQgbG9hZEFsbG93ZWQgPSB0cnVlOyAvLyBUT0RPOiBNYWtlIHRoaXMgZGVmYXVsdCB0byBmYWxzZVxuLy9cbi8vICAgICBbXS5mb3JFYWNoLmNhbGwocmVmZXJyZXJBcnJheSwgKHJlZikgPT4ge1xuLy8gICAgICAgICAvLyBMb2dpYyB0byBiYW4gb3IgYWxsb3cgdGhlIHdpZGdldFxuLy8gICAgICAgICBpZiAoc2V0dGluZ3MuZG9tYWluKSB7XG4vLyAgICAgICAgICAgICBpZiAoc2V0dGluZ3MuZG9tYWluID09PSByZWYpIHtcbi8vICAgICAgICAgICAgICAgICBsb2FkQWxsb3dlZCA9IHRydWU7XG4vLyAgICAgICAgICAgICB9XG4vLyAgICAgICAgIH0gZWxzZSB7XG4vLyAgICAgICAgICAgICBsb2FkQWxsb3dlZCA9IHRydWU7XG4vLyAgICAgICAgIH1cbi8vICAgICB9KTtcbi8vXG4vLyAgICAgcmV0dXJuIGxvYWRBbGxvd2VkO1xuLy8gfTtcblxuLyoqXG4gKiBpbml0aWFsaXplV2lkZ2V0XG4gKlxuICogQHBhcmFtIGVsZW1lbnRcbiAqIEBwYXJhbSBhdHRyaWJ1dGVzXG4gKi9cbmV4cG9ydCBjb25zdCBpbml0aWFsaXplV2lkZ2V0ID0gKGVsZW1lbnQsIGF0dHJpYnV0ZXMpID0+IHtcbiAgICAvLyBHZXQgc2NyaXB0IHRhZyBkYXRhLSogYXR0cmlidXRlc1xuICAgIGNvbnN0IGRhdGFBdHRyaWJ1dGVzID0gZ2V0U2NyaXB0VGFnRGF0YUF0dHJpYnV0ZXMoYXR0cmlidXRlcyk7XG4gICAgY29uc3QgbG9hZGVyRG9tYWluID0gZXh0cmFjdEhvc3RuYW1lKGVsZW1lbnQuc3JjKTtcblxuICAgIGxldCBmaW5hbENvbmZpZyA9IHt9O1xuXG4gICAgLy8gTG9hZCB3aWRnZXQgY29uZmlnIGpzb24gYW5kIG1lcmdlIHdpdGggc2NyaXB0IHRhZyBkYXRhLSogYXR0cmlidXRlc1xuICAgIGxvYWRXaWRnZXRDb25maWcobG9hZGVyRG9tYWluLCBkYXRhQXR0cmlidXRlcywgKGVycm9yLCBjb25maWcpID0+IHtcbiAgICAgICAgaWYgKGVycm9yICE9PSBudWxsKSB7XG4gICAgICAgICAgICBlbGVtZW50LnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGVycm9yKSwgZWxlbWVudCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBmaW5hbENvbmZpZyA9IG1lcmdlT2JqZWN0KHt9LCBmaW5hbENvbmZpZywgY29uZmlnKTtcblxuICAgICAgICAvLyBMb2FkIHRoZSBpRnJhbWUgd2l0aCByZXR1cm5lZCBjb25maWdcbiAgICAgICAgaWYgKGNvbmZpZy5zcG9ydCAhPT0gdW5kZWZpbmVkICYmIGNvbmZpZy50eXBlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGlmIChjb25maWcudGhlbWUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIGZpbmFsQ29uZmlnLnRoZW1lID0gJ2RlZmF1bHQnO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmaW5hbENvbmZpZy5sb2FkZXJEb21haW4gPSBleHRyYWN0SG9zdG5hbWUoZWxlbWVudC5zcmMpO1xuICAgICAgICAgICAgZmluYWxDb25maWcuYWN0dWFsVmVyc2lvbiA9IGV4dHJhY3RBY3R1YWxWZXJzaW9uKGVsZW1lbnQuc3JjKTtcbiAgICAgICAgICAgIGZpbmFsQ29uZmlnLmJhc2VVUkwgPSBlbGVtZW50LnNyYy5yZXBsYWNlKCdnbnMud2lkZ2V0LmluaXRpYWxpemVyLmpzJywgJycpO1xuXG4gICAgICAgICAgICBsb2FkSWZyYW1lQ2hlY2tlcihlbGVtZW50LCBmaW5hbENvbmZpZyk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIGxvYWRGZWF0dXJlU3dpdGNoZXMoZXh0cmFjdEhvc3RuYW1lKGVsZW1lbnQuc3JjLCB0cnVlKSwgKGVyciwgZmVhdHVyZVN3aXRjaGVyQ29uZmlnKSA9PiB7XG4gICAgICAgIGlmIChlcnIgIT09IG51bGwpIHtcbiAgICAgICAgICAgIGVsZW1lbnQucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoZXJyKSwgZWxlbWVudCk7XG4gICAgICAgIH1cblxuICAgICAgICBmaW5hbENvbmZpZy5mZWF0dXJlU3dpdGNoZXMgPSBmZWF0dXJlU3dpdGNoZXJDb25maWc7XG4gICAgICAgIGxvYWRJZnJhbWVDaGVja2VyKGVsZW1lbnQsIGZpbmFsQ29uZmlnKTtcbiAgICB9KTtcbn07XG5cbmV4cG9ydCBjb25zdCBsb2FkSWZyYW1lQ2hlY2tlciA9IChlbGVtZW50LCBjb25maWcpID0+IHtcbiAgICBpZiAoY29uZmlnLmZlYXR1cmVTd2l0Y2hlcyAmJiBjb25maWcud2lkZ2V0X2lkKSB7XG4gICAgICAgIGxldCB3aWRnZXRVcmwgPSBgd2lkZ2V0cy8ke2NvbmZpZy50aGVtZX0vJHtjb25maWcuc3BvcnR9LyR7Y29uZmlnLnR5cGV9LmpzP3ZpZXdDb3VudD0xYDtcblxuICAgICAgICBpZiAoY29uZmlnLmNsaWVudF9pZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB3aWRnZXRVcmwgPSBgJHt3aWRnZXRVcmx9JmM9JHtjb25maWcuY2xpZW50X2lkfWA7XG4gICAgICAgIH1cbiAgICAgICAgbG9hZElGcmFtZShlbGVtZW50LCB3aWRnZXRVcmwsIGNvbmZpZyk7XG4gICAgfVxufTtcblxuLyoqXG4gKiBnZW5lcmF0ZUlkXG4gKlxuICogQHBhcmFtIGxlbmd0aFxuICogQHJldHVybnMge3N0cmluZ31cbiAqL1xuZXhwb3J0IGNvbnN0IGdlbmVyYXRlSWQgPSAobGVuZ3RoKSA9PiB7XG4gICAgbGV0IHN0cmluZyA9ICcnO1xuICAgIGNvbnN0IGNoYXJzID0gJ0FCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXowMTIzNDU2Nzg5JztcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgc3RyaW5nICs9IGNoYXJzLmNoYXJBdChNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBjaGFycy5sZW5ndGgpKTtcbiAgICB9XG4gICAgcmV0dXJuIHN0cmluZztcbn07XG5cbi8qKlxuICogbWVzc2FnZUV2ZW50Q2FsbGJhY2tcbiAqXG4gKiBAcGFyYW0gZXZlbnRcbiAqL1xuZXhwb3J0IGNvbnN0IG1lc3NhZ2VFdmVudENhbGxiYWNrID0gKGV2ZW50KSA9PiB7XG4gICAgbGV0IG1lc3NhZ2U7XG4gICAgdHJ5IHtcbiAgICAgICAgbWVzc2FnZSA9IEpTT04ucGFyc2UoZXZlbnQuZGF0YSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgbWVzc2FnZSA9IG51bGw7XG4gICAgfVxuXG4gICAgaWYgKG1lc3NhZ2UpIHtcbiAgICAgICAgaWYgKG1lc3NhZ2UuYWN0aW9uKSB7XG4gICAgICAgICAgICBzd2l0Y2ggKG1lc3NhZ2UuYWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnZ25zLnJlc2l6ZUlGcmFtZSc6XG4gICAgICAgICAgICAgICAgICAgIGlmIChtZXNzYWdlLmlGcmFtZUlkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhcmdldElGcmFtZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKG1lc3NhZ2UuaUZyYW1lSWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRhcmdldElGcmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmVudFpvb21MZXZlbCA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdodG1sJylbMF0pLnpvb207XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmVudFpvb21MZXZlbCA8IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0SUZyYW1lLmhlaWdodCA9IE1hdGguY2VpbChtZXNzYWdlLmJvdW5kcy5oZWlnaHQgLyBwYXJlbnRab29tTGV2ZWwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldElGcmFtZS5oZWlnaHQgPSBtZXNzYWdlLmJvdW5kcy5oZWlnaHQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ2ducy5zd2l0Y2hUb1dpZGdldCc6XG4gICAgICAgICAgICAgICAgICAgIGlmIChtZXNzYWdlLnNldHRpbmdzLmlGcmFtZUlkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhcmdldElGcmFtZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKG1lc3NhZ2Uuc2V0dGluZ3MuaUZyYW1lSWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRhcmdldElGcmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB3aWRnZXRVcmwgPSBgd2lkZ2V0cy8ke21lc3NhZ2Uuc2V0dGluZ3MudGhlbWV9LyR7bWVzc2FnZS5zZXR0aW5ncy5zcG9ydH0vJHttZXNzYWdlLnNldHRpbmdzLnR5cGV9LmpzP3ZpZXdDb3VudD0xYDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXNzYWdlLnNldHRpbmdzLmNsaWVudF9pZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZGdldFVybCA9IGAke3dpZGdldFVybH0mYz0ke21lc3NhZ2Uuc2V0dGluZ3MuY2xpZW50X2lkfWA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3dpdGNoSUZyYW1lQ29udGVudCh3aWRnZXRVcmwsIG1lc3NhZ2Uuc2V0dGluZ3MpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59O1xuXG4vKipcbiAqIGxvYWRWZXJzaW9uc1xuICpcbiAqIEBwYXJhbSBsb2FkZXJVUkxcbiAqIEBwYXJhbSBjYWxsYmFja1xuICovXG5leHBvcnQgY29uc3QgbG9hZFZlcnNpb25zID0gKGxvYWRlclVSTCwgY2FsbGJhY2spID0+IHtcbiAgICBsZXQgdmVyc2lvbnMgPSB7fTtcbiAgICBjb25zdCBhbnRpQ2FjaGUgPSBnbG9iYWwuX19HTlNfQU5USV9DQUNIRSB8fCBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICBnbG9iYWwuX19HTlNfQU5USV9DQUNIRSA9IGFudGlDYWNoZTtcbiAgICBjb25zdCBjb25maWdGaWxlID0gYCR7bG9hZGVyVVJMfS92ZXJzaW9ucy5qc29uP3Y9JHthbnRpQ2FjaGV9YDtcblxuICAgIGNvbnN0IHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICAgIHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbiB4aHJSZXNwb25zZSgpIHtcbiAgICAgICAgaWYgKHhoci5yZWFkeVN0YXRlID09PSA0KSB7XG4gICAgICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgdmVyc2lvbnMgPSBKU09OLnBhcnNlKHhoci5yZXNwb25zZVRleHQpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCdFcnJvcjogY291bGQgbm90IGxvYWQgdmVyc2lvbnMuanNvbicsIG51bGwpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygnRXJyb3I6IGNvdWxkIG5vdCBsb2FkIHZlcnNpb25zLmpzb24nLCBudWxsKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYWxsYmFjayhudWxsLCB2ZXJzaW9ucyk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHhoci5vcGVuKCdnZXQnLCBjb25maWdGaWxlKTtcbiAgICB4aHIucmVzcG9uc2VUeXBlID0gJ3RleHQnO1xuICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdBY2NlcHQnLCAnYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOCcpO1xuICAgIHhoci5zZW5kKCk7XG59O1xuXG5leHBvcnQgY29uc3QgbG9hZEZlYXR1cmVTd2l0Y2hlcyA9IChsb2FkZXJVUkwsIGNhbGxiYWNrKSA9PiB7XG4gICAgY29uc3QgYW50aUNhY2hlID0gZ2xvYmFsLl9fR05TX0FOVElfQ0FDSEUgfHwgbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgZ2xvYmFsLl9fR05TX0FOVElfQ0FDSEUgPSBhbnRpQ2FjaGU7XG4gICAgY29uc3QgY29uZmlnRmlsZVVSTCA9IGAke2xvYWRlclVSTH0vZmVhdHVyZV9zd2l0Y2hlcy5qc29uP3Y9JHthbnRpQ2FjaGV9YDtcblxuICAgIGNvbnN0IHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG4gICAgeGhyLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uIHhoclJlc3BvbnNlKCkge1xuICAgICAgICBpZiAoeGhyLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgIGlmICh4aHIuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhudWxsLCBKU09OLnBhcnNlKHhoci5yZXNwb25zZVRleHQpKTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygnRXJyb3I6IGNvdWxkIG5vdCBsb2FkIGZlYXR1cmUgc3dpdGNoZXMgY29uZmlnIGZpbGUnLCBudWxsKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKCdFcnJvcjogY291bGQgbm90IGxvYWQgZmVhdHVyZSBzd2l0Y2hlcyBjb25maWcgZmlsZScsIG51bGwpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcblxuICAgIHhoci5vcGVuKCdnZXQnLCBjb25maWdGaWxlVVJMKTtcbiAgICB4aHIucmVzcG9uc2VUeXBlID0gJ3RleHQnO1xuICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdBY2NlcHQnLCAnYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOCcpO1xuICAgIHhoci5zZW5kKCk7XG59O1xuXG4vKipcbiAqIGV4dHJhY3RIb3N0bmFtZVxuICpcbiAqIEBwYXJhbSB1cmxcbiAqIEBwYXJhbSBrZWVwUHJvdG9jb2xcbiAqIEByZXR1cm5zIHsqfVxuICovXG5leHBvcnQgZnVuY3Rpb24gZXh0cmFjdEhvc3RuYW1lKHVybCwga2VlcFByb3RvY29sID0gZmFsc2UpIHtcbiAgICBsZXQgaG9zdG5hbWU7XG5cbiAgICAvLyBmaW5kICYgcmVtb3ZlIHByb3RvY29sIChodHRwLCBmdHAsIGV0Yy4pIGFuZCBnZXQgaG9zdG5hbWVcbiAgICBpZiAodXJsLmluZGV4T2YoJzovLycpID4gLTEpIHtcbiAgICAgICAgaG9zdG5hbWUgPSB1cmwuc3BsaXQoJy8nKVsyXTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBob3N0bmFtZSA9IHVybC5zcGxpdCgnLycpWzBdO1xuICAgIH1cblxuICAgIC8vIGZpbmQgJiByZW1vdmUgcG9ydCBudW1iZXJcbiAgICBjb25zdCBwb3J0ID0gaG9zdG5hbWUuc3BsaXQoJzonKVsxXTtcbiAgICBob3N0bmFtZSA9IGhvc3RuYW1lLnNwbGl0KCc6JylbMF07XG5cbiAgICAvLyBmaW5kICYgcmVtb3ZlIFwiP1wiXG4gICAgaG9zdG5hbWUgPSBob3N0bmFtZS5zcGxpdCgnPycpWzBdO1xuXG4gICAgaWYgKHVybC5pbmRleE9mKCc6Ly8nKSA+IC0xICYmIGtlZXBQcm90b2NvbCkge1xuICAgICAgICBob3N0bmFtZSA9IGAke3VybC5zcGxpdCgnLycpWzBdfS8vJHtob3N0bmFtZX1gO1xuICAgICAgICBpZiAocG9ydCkge1xuICAgICAgICAgICAgaG9zdG5hbWUgPSBgJHtob3N0bmFtZX06JHtwb3J0fWA7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gaG9zdG5hbWU7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBleHRyYWN0QWN0dWFsVmVyc2lvbih1cmwpIHtcbiAgICBsZXQgYWN0dWFsVmVyc2lvbjtcblxuICAgIC8vIFJlbW92ZSBpbml0aWFsaXplciBwYXJ0XG4gICAgY29uc3QgY2xlYW5VcmwgPSB1cmwucmVwbGFjZSgnL2ducy53aWRnZXQuaW5pdGlhbGl6ZXIuanMnLCAnJyk7XG5cbiAgICAvLyBmaW5kICYgcmVtb3ZlIHByb3RvY29sIChodHRwLCBmdHAsIGV0Yy4pIGFuZCBnZXQgaG9zdG5hbWVcblxuICAgIGlmIChjbGVhblVybC5pbmRleE9mKCc6Ly8nKSA+IC0xKSB7XG4gICAgICAgIGFjdHVhbFZlcnNpb24gPSBjbGVhblVybC5zcGxpdCgnLycpWzNdO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGFjdHVhbFZlcnNpb24gPSBjbGVhblVybC5zcGxpdCgnLycpWzFdO1xuICAgIH1cblxuICAgIGlmICghYWN0dWFsVmVyc2lvbikge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICAvLyBmaW5kICYgcmVtb3ZlIHBvcnQgbnVtYmVyXG4gICAgYWN0dWFsVmVyc2lvbiA9IGFjdHVhbFZlcnNpb24uc3BsaXQoJzonKVswXTtcblxuICAgIC8vIGZpbmQgJiByZW1vdmUgXCI/XCJcbiAgICBhY3R1YWxWZXJzaW9uID0gYWN0dWFsVmVyc2lvbi5zcGxpdCgnPycpWzBdO1xuXG5cbiAgICByZXR1cm4gYWN0dWFsVmVyc2lvbjtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3V0aWxzL2xvYWRlci9sb2FkZXItdXRpbHMuanMiLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZGVmYXVsdDoge1xuICAgICAgICBiYXNlVVJMOiAnaHR0cHM6Ly9vZzIwMTgtZGF0YXRlc3QtYXBpLnNwb3J0cy5ncmFjZW5vdGUuY29tL3N2Yy8nLFxuICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgbW9ja3M6IHtcbiAgICAgICAgICAgIGJhc2VVUkw6ICdodHRwOi8vd2lkZ2V0cy5zcG9ydHMubGFicy5ncmFjZW5vdGUuY29tL21vY2tzLycsXG4gICAgICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgfSxcbiAgICAgICAgY29uZmlnVVJMOiAnaHR0cHM6Ly93aWRnZXRzLnNwb3J0cy5ncmFjZW5vdGUuY29tL2NvbmZpZ3MvJyxcbiAgICB9LFxuICAgICd3aWRnZXQtYnVpbGRlci5kZXYnOiB7XG4gICAgICAgIGJhc2VVUkw6ICdodHRwczovL29nMjAxOC1kYXRhdGVzdC1hcGkuc3BvcnRzLmdyYWNlbm90ZS5jb20vc3ZjLycsXG4gICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICBtb2Nrczoge1xuICAgICAgICAgICAgYmFzZVVSTDogJy8vd2lkZ2V0LWJ1aWxkZXIuZGV2OjMwMDAvbW9ja3MvJyxcbiAgICAgICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICB9LFxuICAgICAgICBjb25maWdVUkw6ICdodHRwczovL3dpZGdldHMuc3BvcnRzLmdyYWNlbm90ZS5jb20vY29uZmlncy8nLFxuICAgIH0sXG4gICAgJ3Rlc3Qud2lkZ2V0cy5zcG9ydHMubGFicy5ncmFjZW5vdGUuY29tJzoge1xuICAgICAgICBiYXNlVVJMOiAnaHR0cHM6Ly9vZzIwMTgtZGF0YXRlc3QtYXBpLnNwb3J0cy5ncmFjZW5vdGUuY29tL3N2Yy8nLFxuICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgbW9ja3M6IHtcbiAgICAgICAgICAgIGJhc2VVUkw6ICcvL3Rlc3Qud2lkZ2V0cy5zcG9ydHMubGFicy5ncmFjZW5vdGUuY29tL21vY2tzLycsXG4gICAgICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgfSxcbiAgICAgICAgY29uZmlnVVJMOiAnaHR0cHM6Ly93aWRnZXRzLnNwb3J0cy5ncmFjZW5vdGUuY29tL2NvbmZpZ3MvJyxcbiAgICB9LFxuICAgICdzdGFnaW5nLndpZGdldHMuc3BvcnRzLmdyYWNlbm90ZS5jb20nOiB7XG4gICAgICAgIGJhc2VVUkw6ICdodHRwczovL29nMjAxOC1hcGkuc3BvcnRzLmdyYWNlbm90ZS5jb20vc3ZjLycsXG4gICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICBtb2Nrczoge1xuICAgICAgICAgICAgYmFzZVVSTDogJy8vc3RhZ2luZy53aWRnZXRzLnNwb3J0cy5ncmFjZW5vdGUuY29tL21vY2tzLycsXG4gICAgICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgfSxcbiAgICAgICAgY29uZmlnVVJMOiAnaHR0cHM6Ly93aWRnZXRzLnNwb3J0cy5ncmFjZW5vdGUuY29tL2NvbmZpZ3MvJyxcbiAgICB9LFxuICAgICd3aWRnZXRzLnNwb3J0cy5ncmFjZW5vdGUuY29tJzoge1xuICAgICAgICBiYXNlVVJMOiAnaHR0cHM6Ly9vZzIwMTgtYXBpLnNwb3J0cy5ncmFjZW5vdGUuY29tL3N2Yy8nLFxuICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgbW9ja3M6IHtcbiAgICAgICAgICAgIGJhc2VVUkw6ICcvL3N0YWdpbmcud2lkZ2V0cy5zcG9ydHMuZ3JhY2Vub3RlLmNvbS9tb2Nrcy8nLFxuICAgICAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIH0sXG4gICAgICAgIGNvbmZpZ1VSTDogJ2h0dHBzOi8vd2lkZ2V0cy5zcG9ydHMuZ3JhY2Vub3RlLmNvbS9jb25maWdzLycsXG4gICAgfSxcbiAgICAnc3RhZ2luZy13aWRnZXRzLm5iY29seW1waWNzLmNvbSc6IHtcbiAgICAgICAgYmFzZVVSTDogJ2h0dHBzOi8vc3RnYXBpLWdyYWNlbm90ZS5uYmNvbHltcGljcy5jb20vc3ZjLycsXG4gICAgICAgIGhlYWRlcnM6IHt9LFxuICAgICAgICBtb2Nrczoge1xuICAgICAgICAgICAgYmFzZVVSTDogJy8vc3RhZ2luZy13aWRnZXRzLm5iY29seW1waWNzLmNvbS9tb2Nrcy8nLFxuICAgICAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIH0sXG4gICAgICAgIGNvbmZpZ1VSTDogJ2h0dHBzOi8vd2lkZ2V0cy5uYmNvbHltcGljcy5jb20vY29uZmlncy8nLFxuICAgIH0sXG4gICAgJ3dpZGdldHMubmJjb2x5bXBpY3MuY29tJzoge1xuICAgICAgICBiYXNlVVJMOiAnaHR0cHM6Ly9hcGktZ3JhY2Vub3RlLm5iY29seW1waWNzLmNvbS9zdmMvJyxcbiAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIG1vY2tzOiB7XG4gICAgICAgICAgICBiYXNlVVJMOiAnLy93aWRnZXRzLm5iY29seW1waWNzLmNvbS9tb2Nrcy8nLFxuICAgICAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgIH0sXG4gICAgICAgIGNvbmZpZ1VSTDogJ2h0dHBzOi8vd2lkZ2V0cy5uYmNvbHltcGljcy5jb20vY29uZmlncy8nLFxuICAgIH0sXG59O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vY29uZmlncy9nYXRld2F5LmpzIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgZGVmaW5lID0gcmVxdWlyZSgnZGVmaW5lLXByb3BlcnRpZXMnKTtcbnZhciBFUyA9IHJlcXVpcmUoJ2VzLWFic3RyYWN0L2VzNicpO1xuXG52YXIgaW1wbGVtZW50YXRpb24gPSByZXF1aXJlKCcuL2ltcGxlbWVudGF0aW9uJyk7XG52YXIgZ2V0UG9seWZpbGwgPSByZXF1aXJlKCcuL3BvbHlmaWxsJyk7XG52YXIgc2hpbSA9IHJlcXVpcmUoJy4vc2hpbScpO1xuXG52YXIgc2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG5cbnZhciBwb2x5ZmlsbCA9IGdldFBvbHlmaWxsKCk7XG5cbnZhciBib3VuZEZpbmRTaGltID0gZnVuY3Rpb24gZmluZChhcnJheSwgcHJlZGljYXRlKSB7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW51c2VkLXZhcnNcblx0RVMuUmVxdWlyZU9iamVjdENvZXJjaWJsZShhcnJheSk7XG5cdHZhciBhcmdzID0gc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuXHRyZXR1cm4gcG9seWZpbGwuYXBwbHkoYXJyYXksIGFyZ3MpO1xufTtcblxuZGVmaW5lKGJvdW5kRmluZFNoaW0sIHtcblx0Z2V0UG9seWZpbGw6IGdldFBvbHlmaWxsLFxuXHRpbXBsZW1lbnRhdGlvbjogaW1wbGVtZW50YXRpb24sXG5cdHNoaW06IHNoaW1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGJvdW5kRmluZFNoaW07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvYXJyYXkucHJvdG90eXBlLmZpbmQvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDE1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG4vLyBtb2RpZmllZCBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9lcy1zaGltcy9lczUtc2hpbVxudmFyIGhhcyA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHk7XG52YXIgdG9TdHIgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xudmFyIHNsaWNlID0gQXJyYXkucHJvdG90eXBlLnNsaWNlO1xudmFyIGlzQXJncyA9IHJlcXVpcmUoJy4vaXNBcmd1bWVudHMnKTtcbnZhciBpc0VudW1lcmFibGUgPSBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlO1xudmFyIGhhc0RvbnRFbnVtQnVnID0gIWlzRW51bWVyYWJsZS5jYWxsKHsgdG9TdHJpbmc6IG51bGwgfSwgJ3RvU3RyaW5nJyk7XG52YXIgaGFzUHJvdG9FbnVtQnVnID0gaXNFbnVtZXJhYmxlLmNhbGwoZnVuY3Rpb24gKCkge30sICdwcm90b3R5cGUnKTtcbnZhciBkb250RW51bXMgPSBbXG5cdCd0b1N0cmluZycsXG5cdCd0b0xvY2FsZVN0cmluZycsXG5cdCd2YWx1ZU9mJyxcblx0J2hhc093blByb3BlcnR5Jyxcblx0J2lzUHJvdG90eXBlT2YnLFxuXHQncHJvcGVydHlJc0VudW1lcmFibGUnLFxuXHQnY29uc3RydWN0b3InXG5dO1xudmFyIGVxdWFsc0NvbnN0cnVjdG9yUHJvdG90eXBlID0gZnVuY3Rpb24gKG8pIHtcblx0dmFyIGN0b3IgPSBvLmNvbnN0cnVjdG9yO1xuXHRyZXR1cm4gY3RvciAmJiBjdG9yLnByb3RvdHlwZSA9PT0gbztcbn07XG52YXIgZXhjbHVkZWRLZXlzID0ge1xuXHQkY29uc29sZTogdHJ1ZSxcblx0JGV4dGVybmFsOiB0cnVlLFxuXHQkZnJhbWU6IHRydWUsXG5cdCRmcmFtZUVsZW1lbnQ6IHRydWUsXG5cdCRmcmFtZXM6IHRydWUsXG5cdCRpbm5lckhlaWdodDogdHJ1ZSxcblx0JGlubmVyV2lkdGg6IHRydWUsXG5cdCRvdXRlckhlaWdodDogdHJ1ZSxcblx0JG91dGVyV2lkdGg6IHRydWUsXG5cdCRwYWdlWE9mZnNldDogdHJ1ZSxcblx0JHBhZ2VZT2Zmc2V0OiB0cnVlLFxuXHQkcGFyZW50OiB0cnVlLFxuXHQkc2Nyb2xsTGVmdDogdHJ1ZSxcblx0JHNjcm9sbFRvcDogdHJ1ZSxcblx0JHNjcm9sbFg6IHRydWUsXG5cdCRzY3JvbGxZOiB0cnVlLFxuXHQkc2VsZjogdHJ1ZSxcblx0JHdlYmtpdEluZGV4ZWREQjogdHJ1ZSxcblx0JHdlYmtpdFN0b3JhZ2VJbmZvOiB0cnVlLFxuXHQkd2luZG93OiB0cnVlXG59O1xudmFyIGhhc0F1dG9tYXRpb25FcXVhbGl0eUJ1ZyA9IChmdW5jdGlvbiAoKSB7XG5cdC8qIGdsb2JhbCB3aW5kb3cgKi9cblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09ICd1bmRlZmluZWQnKSB7IHJldHVybiBmYWxzZTsgfVxuXHRmb3IgKHZhciBrIGluIHdpbmRvdykge1xuXHRcdHRyeSB7XG5cdFx0XHRpZiAoIWV4Y2x1ZGVkS2V5c1snJCcgKyBrXSAmJiBoYXMuY2FsbCh3aW5kb3csIGspICYmIHdpbmRvd1trXSAhPT0gbnVsbCAmJiB0eXBlb2Ygd2luZG93W2tdID09PSAnb2JqZWN0Jykge1xuXHRcdFx0XHR0cnkge1xuXHRcdFx0XHRcdGVxdWFsc0NvbnN0cnVjdG9yUHJvdG90eXBlKHdpbmRvd1trXSk7XG5cdFx0XHRcdH0gY2F0Y2ggKGUpIHtcblx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0gY2F0Y2ggKGUpIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0fVxuXHRyZXR1cm4gZmFsc2U7XG59KCkpO1xudmFyIGVxdWFsc0NvbnN0cnVjdG9yUHJvdG90eXBlSWZOb3RCdWdneSA9IGZ1bmN0aW9uIChvKSB7XG5cdC8qIGdsb2JhbCB3aW5kb3cgKi9cblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09ICd1bmRlZmluZWQnIHx8ICFoYXNBdXRvbWF0aW9uRXF1YWxpdHlCdWcpIHtcblx0XHRyZXR1cm4gZXF1YWxzQ29uc3RydWN0b3JQcm90b3R5cGUobyk7XG5cdH1cblx0dHJ5IHtcblx0XHRyZXR1cm4gZXF1YWxzQ29uc3RydWN0b3JQcm90b3R5cGUobyk7XG5cdH0gY2F0Y2ggKGUpIHtcblx0XHRyZXR1cm4gZmFsc2U7XG5cdH1cbn07XG5cbnZhciBrZXlzU2hpbSA9IGZ1bmN0aW9uIGtleXMob2JqZWN0KSB7XG5cdHZhciBpc09iamVjdCA9IG9iamVjdCAhPT0gbnVsbCAmJiB0eXBlb2Ygb2JqZWN0ID09PSAnb2JqZWN0Jztcblx0dmFyIGlzRnVuY3Rpb24gPSB0b1N0ci5jYWxsKG9iamVjdCkgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG5cdHZhciBpc0FyZ3VtZW50cyA9IGlzQXJncyhvYmplY3QpO1xuXHR2YXIgaXNTdHJpbmcgPSBpc09iamVjdCAmJiB0b1N0ci5jYWxsKG9iamVjdCkgPT09ICdbb2JqZWN0IFN0cmluZ10nO1xuXHR2YXIgdGhlS2V5cyA9IFtdO1xuXG5cdGlmICghaXNPYmplY3QgJiYgIWlzRnVuY3Rpb24gJiYgIWlzQXJndW1lbnRzKSB7XG5cdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignT2JqZWN0LmtleXMgY2FsbGVkIG9uIGEgbm9uLW9iamVjdCcpO1xuXHR9XG5cblx0dmFyIHNraXBQcm90byA9IGhhc1Byb3RvRW51bUJ1ZyAmJiBpc0Z1bmN0aW9uO1xuXHRpZiAoaXNTdHJpbmcgJiYgb2JqZWN0Lmxlbmd0aCA+IDAgJiYgIWhhcy5jYWxsKG9iamVjdCwgMCkpIHtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IG9iamVjdC5sZW5ndGg7ICsraSkge1xuXHRcdFx0dGhlS2V5cy5wdXNoKFN0cmluZyhpKSk7XG5cdFx0fVxuXHR9XG5cblx0aWYgKGlzQXJndW1lbnRzICYmIG9iamVjdC5sZW5ndGggPiAwKSB7XG5cdFx0Zm9yICh2YXIgaiA9IDA7IGogPCBvYmplY3QubGVuZ3RoOyArK2opIHtcblx0XHRcdHRoZUtleXMucHVzaChTdHJpbmcoaikpO1xuXHRcdH1cblx0fSBlbHNlIHtcblx0XHRmb3IgKHZhciBuYW1lIGluIG9iamVjdCkge1xuXHRcdFx0aWYgKCEoc2tpcFByb3RvICYmIG5hbWUgPT09ICdwcm90b3R5cGUnKSAmJiBoYXMuY2FsbChvYmplY3QsIG5hbWUpKSB7XG5cdFx0XHRcdHRoZUtleXMucHVzaChTdHJpbmcobmFtZSkpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdGlmIChoYXNEb250RW51bUJ1Zykge1xuXHRcdHZhciBza2lwQ29uc3RydWN0b3IgPSBlcXVhbHNDb25zdHJ1Y3RvclByb3RvdHlwZUlmTm90QnVnZ3kob2JqZWN0KTtcblxuXHRcdGZvciAodmFyIGsgPSAwOyBrIDwgZG9udEVudW1zLmxlbmd0aDsgKytrKSB7XG5cdFx0XHRpZiAoIShza2lwQ29uc3RydWN0b3IgJiYgZG9udEVudW1zW2tdID09PSAnY29uc3RydWN0b3InKSAmJiBoYXMuY2FsbChvYmplY3QsIGRvbnRFbnVtc1trXSkpIHtcblx0XHRcdFx0dGhlS2V5cy5wdXNoKGRvbnRFbnVtc1trXSk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cdHJldHVybiB0aGVLZXlzO1xufTtcblxua2V5c1NoaW0uc2hpbSA9IGZ1bmN0aW9uIHNoaW1PYmplY3RLZXlzKCkge1xuXHRpZiAoT2JqZWN0LmtleXMpIHtcblx0XHR2YXIga2V5c1dvcmtzV2l0aEFyZ3VtZW50cyA9IChmdW5jdGlvbiAoKSB7XG5cdFx0XHQvLyBTYWZhcmkgNS4wIGJ1Z1xuXHRcdFx0cmV0dXJuIChPYmplY3Qua2V5cyhhcmd1bWVudHMpIHx8ICcnKS5sZW5ndGggPT09IDI7XG5cdFx0fSgxLCAyKSk7XG5cdFx0aWYgKCFrZXlzV29ya3NXaXRoQXJndW1lbnRzKSB7XG5cdFx0XHR2YXIgb3JpZ2luYWxLZXlzID0gT2JqZWN0LmtleXM7XG5cdFx0XHRPYmplY3Qua2V5cyA9IGZ1bmN0aW9uIGtleXMob2JqZWN0KSB7XG5cdFx0XHRcdGlmIChpc0FyZ3Mob2JqZWN0KSkge1xuXHRcdFx0XHRcdHJldHVybiBvcmlnaW5hbEtleXMoc2xpY2UuY2FsbChvYmplY3QpKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRyZXR1cm4gb3JpZ2luYWxLZXlzKG9iamVjdCk7XG5cdFx0XHRcdH1cblx0XHRcdH07XG5cdFx0fVxuXHR9IGVsc2Uge1xuXHRcdE9iamVjdC5rZXlzID0ga2V5c1NoaW07XG5cdH1cblx0cmV0dXJuIE9iamVjdC5rZXlzIHx8IGtleXNTaGltO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBrZXlzU2hpbTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9vYmplY3Qta2V5cy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMTZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNBcmd1bWVudHModmFsdWUpIHtcblx0dmFyIHN0ciA9IHRvU3RyLmNhbGwodmFsdWUpO1xuXHR2YXIgaXNBcmdzID0gc3RyID09PSAnW29iamVjdCBBcmd1bWVudHNdJztcblx0aWYgKCFpc0FyZ3MpIHtcblx0XHRpc0FyZ3MgPSBzdHIgIT09ICdbb2JqZWN0IEFycmF5XScgJiZcblx0XHRcdHZhbHVlICE9PSBudWxsICYmXG5cdFx0XHR0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmXG5cdFx0XHR0eXBlb2YgdmFsdWUubGVuZ3RoID09PSAnbnVtYmVyJyAmJlxuXHRcdFx0dmFsdWUubGVuZ3RoID49IDAgJiZcblx0XHRcdHRvU3RyLmNhbGwodmFsdWUuY2FsbGVlKSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcblx0fVxuXHRyZXR1cm4gaXNBcmdzO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9vYmplY3Qta2V5cy9pc0FyZ3VtZW50cy5qc1xuLy8gbW9kdWxlIGlkID0gMTdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCJcbnZhciBoYXNPd24gPSBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5O1xudmFyIHRvU3RyaW5nID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBmb3JFYWNoIChvYmosIGZuLCBjdHgpIHtcbiAgICBpZiAodG9TdHJpbmcuY2FsbChmbikgIT09ICdbb2JqZWN0IEZ1bmN0aW9uXScpIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignaXRlcmF0b3IgbXVzdCBiZSBhIGZ1bmN0aW9uJyk7XG4gICAgfVxuICAgIHZhciBsID0gb2JqLmxlbmd0aDtcbiAgICBpZiAobCA9PT0gK2wpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgICAgIGZuLmNhbGwoY3R4LCBvYmpbaV0sIGksIG9iaik7XG4gICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICBmb3IgKHZhciBrIGluIG9iaikge1xuICAgICAgICAgICAgaWYgKGhhc093bi5jYWxsKG9iaiwgaykpIHtcbiAgICAgICAgICAgICAgICBmbi5jYWxsKGN0eCwgb2JqW2tdLCBrLCBvYmopO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufTtcblxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2ZvcmVhY2gvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDE4XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgaGFzID0gcmVxdWlyZSgnaGFzJyk7XG52YXIgdG9QcmltaXRpdmUgPSByZXF1aXJlKCdlcy10by1wcmltaXRpdmUvZXM2Jyk7XG5cbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG52YXIgaGFzU3ltYm9scyA9IHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gJ3N5bWJvbCc7XG5cbnZhciAkaXNOYU4gPSByZXF1aXJlKCcuL2hlbHBlcnMvaXNOYU4nKTtcbnZhciAkaXNGaW5pdGUgPSByZXF1aXJlKCcuL2hlbHBlcnMvaXNGaW5pdGUnKTtcbnZhciBNQVhfU0FGRV9JTlRFR0VSID0gTnVtYmVyLk1BWF9TQUZFX0lOVEVHRVIgfHwgTWF0aC5wb3coMiwgNTMpIC0gMTtcblxudmFyIGFzc2lnbiA9IHJlcXVpcmUoJy4vaGVscGVycy9hc3NpZ24nKTtcbnZhciBzaWduID0gcmVxdWlyZSgnLi9oZWxwZXJzL3NpZ24nKTtcbnZhciBtb2QgPSByZXF1aXJlKCcuL2hlbHBlcnMvbW9kJyk7XG52YXIgaXNQcmltaXRpdmUgPSByZXF1aXJlKCcuL2hlbHBlcnMvaXNQcmltaXRpdmUnKTtcbnZhciBwYXJzZUludGVnZXIgPSBwYXJzZUludDtcbnZhciBiaW5kID0gcmVxdWlyZSgnZnVuY3Rpb24tYmluZCcpO1xudmFyIGFycmF5U2xpY2UgPSBiaW5kLmNhbGwoRnVuY3Rpb24uY2FsbCwgQXJyYXkucHJvdG90eXBlLnNsaWNlKTtcbnZhciBzdHJTbGljZSA9IGJpbmQuY2FsbChGdW5jdGlvbi5jYWxsLCBTdHJpbmcucHJvdG90eXBlLnNsaWNlKTtcbnZhciBpc0JpbmFyeSA9IGJpbmQuY2FsbChGdW5jdGlvbi5jYWxsLCBSZWdFeHAucHJvdG90eXBlLnRlc3QsIC9eMGJbMDFdKyQvaSk7XG52YXIgaXNPY3RhbCA9IGJpbmQuY2FsbChGdW5jdGlvbi5jYWxsLCBSZWdFeHAucHJvdG90eXBlLnRlc3QsIC9eMG9bMC03XSskL2kpO1xudmFyIHJlZ2V4RXhlYyA9IGJpbmQuY2FsbChGdW5jdGlvbi5jYWxsLCBSZWdFeHAucHJvdG90eXBlLmV4ZWMpO1xudmFyIG5vbldTID0gWydcXHUwMDg1JywgJ1xcdTIwMGInLCAnXFx1ZmZmZSddLmpvaW4oJycpO1xudmFyIG5vbldTcmVnZXggPSBuZXcgUmVnRXhwKCdbJyArIG5vbldTICsgJ10nLCAnZycpO1xudmFyIGhhc05vbldTID0gYmluZC5jYWxsKEZ1bmN0aW9uLmNhbGwsIFJlZ0V4cC5wcm90b3R5cGUudGVzdCwgbm9uV1NyZWdleCk7XG52YXIgaW52YWxpZEhleExpdGVyYWwgPSAvXlstK10weFswLTlhLWZdKyQvaTtcbnZhciBpc0ludmFsaWRIZXhMaXRlcmFsID0gYmluZC5jYWxsKEZ1bmN0aW9uLmNhbGwsIFJlZ0V4cC5wcm90b3R5cGUudGVzdCwgaW52YWxpZEhleExpdGVyYWwpO1xuXG4vLyB3aGl0ZXNwYWNlIGZyb206IGh0dHA6Ly9lczUuZ2l0aHViLmlvLyN4MTUuNS40LjIwXG4vLyBpbXBsZW1lbnRhdGlvbiBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9lcy1zaGltcy9lczUtc2hpbS9ibG9iL3YzLjQuMC9lczUtc2hpbS5qcyNMMTMwNC1MMTMyNFxudmFyIHdzID0gW1xuXHQnXFx4MDlcXHgwQVxceDBCXFx4MENcXHgwRFxceDIwXFx4QTBcXHUxNjgwXFx1MTgwRVxcdTIwMDBcXHUyMDAxXFx1MjAwMlxcdTIwMDMnLFxuXHQnXFx1MjAwNFxcdTIwMDVcXHUyMDA2XFx1MjAwN1xcdTIwMDhcXHUyMDA5XFx1MjAwQVxcdTIwMkZcXHUyMDVGXFx1MzAwMFxcdTIwMjgnLFxuXHQnXFx1MjAyOVxcdUZFRkYnXG5dLmpvaW4oJycpO1xudmFyIHRyaW1SZWdleCA9IG5ldyBSZWdFeHAoJyheWycgKyB3cyArICddKyl8KFsnICsgd3MgKyAnXSskKScsICdnJyk7XG52YXIgcmVwbGFjZSA9IGJpbmQuY2FsbChGdW5jdGlvbi5jYWxsLCBTdHJpbmcucHJvdG90eXBlLnJlcGxhY2UpO1xudmFyIHRyaW0gPSBmdW5jdGlvbiAodmFsdWUpIHtcblx0cmV0dXJuIHJlcGxhY2UodmFsdWUsIHRyaW1SZWdleCwgJycpO1xufTtcblxudmFyIEVTNSA9IHJlcXVpcmUoJy4vZXM1Jyk7XG5cbnZhciBoYXNSZWdFeHBNYXRjaGVyID0gcmVxdWlyZSgnaXMtcmVnZXgnKTtcblxuLy8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLWFic3RyYWN0LW9wZXJhdGlvbnNcbnZhciBFUzYgPSBhc3NpZ24oYXNzaWduKHt9LCBFUzUpLCB7XG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLWNhbGwtZi12LWFyZ3Ncblx0Q2FsbDogZnVuY3Rpb24gQ2FsbChGLCBWKSB7XG5cdFx0dmFyIGFyZ3MgPSBhcmd1bWVudHMubGVuZ3RoID4gMiA/IGFyZ3VtZW50c1syXSA6IFtdO1xuXHRcdGlmICghdGhpcy5Jc0NhbGxhYmxlKEYpKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKEYgKyAnIGlzIG5vdCBhIGZ1bmN0aW9uJyk7XG5cdFx0fVxuXHRcdHJldHVybiBGLmFwcGx5KFYsIGFyZ3MpO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b3ByaW1pdGl2ZVxuXHRUb1ByaW1pdGl2ZTogdG9QcmltaXRpdmUsXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvYm9vbGVhblxuXHQvLyBUb0Jvb2xlYW46IEVTNS5Ub0Jvb2xlYW4sXG5cblx0Ly8gaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLXRvbnVtYmVyXG5cdFRvTnVtYmVyOiBmdW5jdGlvbiBUb051bWJlcihhcmd1bWVudCkge1xuXHRcdHZhciB2YWx1ZSA9IGlzUHJpbWl0aXZlKGFyZ3VtZW50KSA/IGFyZ3VtZW50IDogdG9QcmltaXRpdmUoYXJndW1lbnQsIE51bWJlcik7XG5cdFx0aWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3N5bWJvbCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Nhbm5vdCBjb252ZXJ0IGEgU3ltYm9sIHZhbHVlIHRvIGEgbnVtYmVyJyk7XG5cdFx0fVxuXHRcdGlmICh0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB7XG5cdFx0XHRpZiAoaXNCaW5hcnkodmFsdWUpKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLlRvTnVtYmVyKHBhcnNlSW50ZWdlcihzdHJTbGljZSh2YWx1ZSwgMiksIDIpKTtcblx0XHRcdH0gZWxzZSBpZiAoaXNPY3RhbCh2YWx1ZSkpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuVG9OdW1iZXIocGFyc2VJbnRlZ2VyKHN0clNsaWNlKHZhbHVlLCAyKSwgOCkpO1xuXHRcdFx0fSBlbHNlIGlmIChoYXNOb25XUyh2YWx1ZSkgfHwgaXNJbnZhbGlkSGV4TGl0ZXJhbCh2YWx1ZSkpIHtcblx0XHRcdFx0cmV0dXJuIE5hTjtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHZhciB0cmltbWVkID0gdHJpbSh2YWx1ZSk7XG5cdFx0XHRcdGlmICh0cmltbWVkICE9PSB2YWx1ZSkge1xuXHRcdFx0XHRcdHJldHVybiB0aGlzLlRvTnVtYmVyKHRyaW1tZWQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHRcdHJldHVybiBOdW1iZXIodmFsdWUpO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b2ludGVnZXJcblx0Ly8gVG9JbnRlZ2VyOiBFUzUuVG9OdW1iZXIsXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvaW50MzJcblx0Ly8gVG9JbnQzMjogRVM1LlRvSW50MzIsXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvdWludDMyXG5cdC8vIFRvVWludDMyOiBFUzUuVG9VaW50MzIsXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvaW50MTZcblx0VG9JbnQxNjogZnVuY3Rpb24gVG9JbnQxNihhcmd1bWVudCkge1xuXHRcdHZhciBpbnQxNmJpdCA9IHRoaXMuVG9VaW50MTYoYXJndW1lbnQpO1xuXHRcdHJldHVybiBpbnQxNmJpdCA+PSAweDgwMDAgPyBpbnQxNmJpdCAtIDB4MTAwMDAgOiBpbnQxNmJpdDtcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG91aW50MTZcblx0Ly8gVG9VaW50MTY6IEVTNS5Ub1VpbnQxNixcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG9pbnQ4XG5cdFRvSW50ODogZnVuY3Rpb24gVG9JbnQ4KGFyZ3VtZW50KSB7XG5cdFx0dmFyIGludDhiaXQgPSB0aGlzLlRvVWludDgoYXJndW1lbnQpO1xuXHRcdHJldHVybiBpbnQ4Yml0ID49IDB4ODAgPyBpbnQ4Yml0IC0gMHgxMDAgOiBpbnQ4Yml0O1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b3VpbnQ4XG5cdFRvVWludDg6IGZ1bmN0aW9uIFRvVWludDgoYXJndW1lbnQpIHtcblx0XHR2YXIgbnVtYmVyID0gdGhpcy5Ub051bWJlcihhcmd1bWVudCk7XG5cdFx0aWYgKCRpc05hTihudW1iZXIpIHx8IG51bWJlciA9PT0gMCB8fCAhJGlzRmluaXRlKG51bWJlcikpIHsgcmV0dXJuIDA7IH1cblx0XHR2YXIgcG9zSW50ID0gc2lnbihudW1iZXIpICogTWF0aC5mbG9vcihNYXRoLmFicyhudW1iZXIpKTtcblx0XHRyZXR1cm4gbW9kKHBvc0ludCwgMHgxMDApO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b3VpbnQ4Y2xhbXBcblx0VG9VaW50OENsYW1wOiBmdW5jdGlvbiBUb1VpbnQ4Q2xhbXAoYXJndW1lbnQpIHtcblx0XHR2YXIgbnVtYmVyID0gdGhpcy5Ub051bWJlcihhcmd1bWVudCk7XG5cdFx0aWYgKCRpc05hTihudW1iZXIpIHx8IG51bWJlciA8PSAwKSB7IHJldHVybiAwOyB9XG5cdFx0aWYgKG51bWJlciA+PSAweEZGKSB7IHJldHVybiAweEZGOyB9XG5cdFx0dmFyIGYgPSBNYXRoLmZsb29yKGFyZ3VtZW50KTtcblx0XHRpZiAoZiArIDAuNSA8IG51bWJlcikgeyByZXR1cm4gZiArIDE7IH1cblx0XHRpZiAobnVtYmVyIDwgZiArIDAuNSkgeyByZXR1cm4gZjsgfVxuXHRcdGlmIChmICUgMiAhPT0gMCkgeyByZXR1cm4gZiArIDE7IH1cblx0XHRyZXR1cm4gZjtcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtdG9zdHJpbmdcblx0VG9TdHJpbmc6IGZ1bmN0aW9uIFRvU3RyaW5nKGFyZ3VtZW50KSB7XG5cdFx0aWYgKHR5cGVvZiBhcmd1bWVudCA9PT0gJ3N5bWJvbCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Nhbm5vdCBjb252ZXJ0IGEgU3ltYm9sIHZhbHVlIHRvIGEgc3RyaW5nJyk7XG5cdFx0fVxuXHRcdHJldHVybiBTdHJpbmcoYXJndW1lbnQpO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b29iamVjdFxuXHRUb09iamVjdDogZnVuY3Rpb24gVG9PYmplY3QodmFsdWUpIHtcblx0XHR0aGlzLlJlcXVpcmVPYmplY3RDb2VyY2libGUodmFsdWUpO1xuXHRcdHJldHVybiBPYmplY3QodmFsdWUpO1xuXHR9LFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b3Byb3BlcnR5a2V5XG5cdFRvUHJvcGVydHlLZXk6IGZ1bmN0aW9uIFRvUHJvcGVydHlLZXkoYXJndW1lbnQpIHtcblx0XHR2YXIga2V5ID0gdGhpcy5Ub1ByaW1pdGl2ZShhcmd1bWVudCwgU3RyaW5nKTtcblx0XHRyZXR1cm4gdHlwZW9mIGtleSA9PT0gJ3N5bWJvbCcgPyBrZXkgOiB0aGlzLlRvU3RyaW5nKGtleSk7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXRvbGVuZ3RoXG5cdFRvTGVuZ3RoOiBmdW5jdGlvbiBUb0xlbmd0aChhcmd1bWVudCkge1xuXHRcdHZhciBsZW4gPSB0aGlzLlRvSW50ZWdlcihhcmd1bWVudCk7XG5cdFx0aWYgKGxlbiA8PSAwKSB7IHJldHVybiAwOyB9IC8vIGluY2x1ZGVzIGNvbnZlcnRpbmcgLTAgdG8gKzBcblx0XHRpZiAobGVuID4gTUFYX1NBRkVfSU5URUdFUikgeyByZXR1cm4gTUFYX1NBRkVfSU5URUdFUjsgfVxuXHRcdHJldHVybiBsZW47XG5cdH0sXG5cblx0Ly8gaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLWNhbm9uaWNhbG51bWVyaWNpbmRleHN0cmluZ1xuXHRDYW5vbmljYWxOdW1lcmljSW5kZXhTdHJpbmc6IGZ1bmN0aW9uIENhbm9uaWNhbE51bWVyaWNJbmRleFN0cmluZyhhcmd1bWVudCkge1xuXHRcdGlmICh0b1N0ci5jYWxsKGFyZ3VtZW50KSAhPT0gJ1tvYmplY3QgU3RyaW5nXScpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ211c3QgYmUgYSBzdHJpbmcnKTtcblx0XHR9XG5cdFx0aWYgKGFyZ3VtZW50ID09PSAnLTAnKSB7IHJldHVybiAtMDsgfVxuXHRcdHZhciBuID0gdGhpcy5Ub051bWJlcihhcmd1bWVudCk7XG5cdFx0aWYgKHRoaXMuU2FtZVZhbHVlKHRoaXMuVG9TdHJpbmcobiksIGFyZ3VtZW50KSkgeyByZXR1cm4gbjsgfVxuXHRcdHJldHVybiB2b2lkIDA7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXJlcXVpcmVvYmplY3Rjb2VyY2libGVcblx0UmVxdWlyZU9iamVjdENvZXJjaWJsZTogRVM1LkNoZWNrT2JqZWN0Q29lcmNpYmxlLFxuXG5cdC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy1pc2FycmF5XG5cdElzQXJyYXk6IEFycmF5LmlzQXJyYXkgfHwgZnVuY3Rpb24gSXNBcnJheShhcmd1bWVudCkge1xuXHRcdHJldHVybiB0b1N0ci5jYWxsKGFyZ3VtZW50KSA9PT0gJ1tvYmplY3QgQXJyYXldJztcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtaXNjYWxsYWJsZVxuXHQvLyBJc0NhbGxhYmxlOiBFUzUuSXNDYWxsYWJsZSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtaXNjb25zdHJ1Y3RvclxuXHRJc0NvbnN0cnVjdG9yOiBmdW5jdGlvbiBJc0NvbnN0cnVjdG9yKGFyZ3VtZW50KSB7XG5cdFx0cmV0dXJuIHR5cGVvZiBhcmd1bWVudCA9PT0gJ2Z1bmN0aW9uJyAmJiAhIWFyZ3VtZW50LnByb3RvdHlwZTsgLy8gdW5mb3J0dW5hdGVseSB0aGVyZSdzIG5vIHdheSB0byB0cnVseSBjaGVjayB0aGlzIHdpdGhvdXQgdHJ5L2NhdGNoIGBuZXcgYXJndW1lbnRgXG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLWlzZXh0ZW5zaWJsZS1vXG5cdElzRXh0ZW5zaWJsZTogZnVuY3Rpb24gSXNFeHRlbnNpYmxlKG9iaikge1xuXHRcdGlmICghT2JqZWN0LnByZXZlbnRFeHRlbnNpb25zKSB7IHJldHVybiB0cnVlOyB9XG5cdFx0aWYgKGlzUHJpbWl0aXZlKG9iaikpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cdFx0cmV0dXJuIE9iamVjdC5pc0V4dGVuc2libGUob2JqKTtcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtaXNpbnRlZ2VyXG5cdElzSW50ZWdlcjogZnVuY3Rpb24gSXNJbnRlZ2VyKGFyZ3VtZW50KSB7XG5cdFx0aWYgKHR5cGVvZiBhcmd1bWVudCAhPT0gJ251bWJlcicgfHwgJGlzTmFOKGFyZ3VtZW50KSB8fCAhJGlzRmluaXRlKGFyZ3VtZW50KSkge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0XHR2YXIgYWJzID0gTWF0aC5hYnMoYXJndW1lbnQpO1xuXHRcdHJldHVybiBNYXRoLmZsb29yKGFicykgPT09IGFicztcblx0fSxcblxuXHQvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtaXNwcm9wZXJ0eWtleVxuXHRJc1Byb3BlcnR5S2V5OiBmdW5jdGlvbiBJc1Byb3BlcnR5S2V5KGFyZ3VtZW50KSB7XG5cdFx0cmV0dXJuIHR5cGVvZiBhcmd1bWVudCA9PT0gJ3N0cmluZycgfHwgdHlwZW9mIGFyZ3VtZW50ID09PSAnc3ltYm9sJztcblx0fSxcblxuXHQvLyBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtaXNyZWdleHBcblx0SXNSZWdFeHA6IGZ1bmN0aW9uIElzUmVnRXhwKGFyZ3VtZW50KSB7XG5cdFx0aWYgKCFhcmd1bWVudCB8fCB0eXBlb2YgYXJndW1lbnQgIT09ICdvYmplY3QnKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXHRcdGlmIChoYXNTeW1ib2xzKSB7XG5cdFx0XHR2YXIgaXNSZWdFeHAgPSBhcmd1bWVudFtTeW1ib2wubWF0Y2hdO1xuXHRcdFx0aWYgKHR5cGVvZiBpc1JlZ0V4cCAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdFx0cmV0dXJuIEVTNS5Ub0Jvb2xlYW4oaXNSZWdFeHApO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRyZXR1cm4gaGFzUmVnRXhwTWF0Y2hlcihhcmd1bWVudCk7XG5cdH0sXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXNhbWV2YWx1ZVxuXHQvLyBTYW1lVmFsdWU6IEVTNS5TYW1lVmFsdWUsXG5cblx0Ly8gaHR0cHM6Ly9wZW9wbGUubW96aWxsYS5vcmcvfmpvcmVuZG9yZmYvZXM2LWRyYWZ0Lmh0bWwjc2VjLXNhbWV2YWx1ZXplcm9cblx0U2FtZVZhbHVlWmVybzogZnVuY3Rpb24gU2FtZVZhbHVlWmVybyh4LCB5KSB7XG5cdFx0cmV0dXJuICh4ID09PSB5KSB8fCAoJGlzTmFOKHgpICYmICRpc05hTih5KSk7XG5cdH0sXG5cblx0LyoqXG5cdCAqIDcuMy4yIEdldFYgKFYsIFApXG5cdCAqIDEuIEFzc2VydDogSXNQcm9wZXJ0eUtleShQKSBpcyB0cnVlLlxuXHQgKiAyLiBMZXQgTyBiZSBUb09iamVjdChWKS5cblx0ICogMy4gUmV0dXJuSWZBYnJ1cHQoTykuXG5cdCAqIDQuIFJldHVybiBPLltbR2V0XV0oUCwgVikuXG5cdCAqL1xuXHRHZXRWOiBmdW5jdGlvbiBHZXRWKFYsIFApIHtcblx0XHQvLyA3LjMuMi4xXG5cdFx0aWYgKCF0aGlzLklzUHJvcGVydHlLZXkoUCkpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IElzUHJvcGVydHlLZXkoUCkgaXMgbm90IHRydWUnKTtcblx0XHR9XG5cblx0XHQvLyA3LjMuMi4yLTNcblx0XHR2YXIgTyA9IHRoaXMuVG9PYmplY3QoVik7XG5cblx0XHQvLyA3LjMuMi40XG5cdFx0cmV0dXJuIE9bUF07XG5cdH0sXG5cblx0LyoqXG5cdCAqIDcuMy45IC0gaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLWdldG1ldGhvZFxuXHQgKiAxLiBBc3NlcnQ6IElzUHJvcGVydHlLZXkoUCkgaXMgdHJ1ZS5cblx0ICogMi4gTGV0IGZ1bmMgYmUgR2V0VihPLCBQKS5cblx0ICogMy4gUmV0dXJuSWZBYnJ1cHQoZnVuYykuXG5cdCAqIDQuIElmIGZ1bmMgaXMgZWl0aGVyIHVuZGVmaW5lZCBvciBudWxsLCByZXR1cm4gdW5kZWZpbmVkLlxuXHQgKiA1LiBJZiBJc0NhbGxhYmxlKGZ1bmMpIGlzIGZhbHNlLCB0aHJvdyBhIFR5cGVFcnJvciBleGNlcHRpb24uXG5cdCAqIDYuIFJldHVybiBmdW5jLlxuXHQgKi9cblx0R2V0TWV0aG9kOiBmdW5jdGlvbiBHZXRNZXRob2QoTywgUCkge1xuXHRcdC8vIDcuMy45LjFcblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eUtleShQKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogSXNQcm9wZXJ0eUtleShQKSBpcyBub3QgdHJ1ZScpO1xuXHRcdH1cblxuXHRcdC8vIDcuMy45LjJcblx0XHR2YXIgZnVuYyA9IHRoaXMuR2V0VihPLCBQKTtcblxuXHRcdC8vIDcuMy45LjRcblx0XHRpZiAoZnVuYyA9PSBudWxsKSB7XG5cdFx0XHRyZXR1cm4gdm9pZCAwO1xuXHRcdH1cblxuXHRcdC8vIDcuMy45LjVcblx0XHRpZiAoIXRoaXMuSXNDYWxsYWJsZShmdW5jKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcihQICsgJ2lzIG5vdCBhIGZ1bmN0aW9uJyk7XG5cdFx0fVxuXG5cdFx0Ly8gNy4zLjkuNlxuXHRcdHJldHVybiBmdW5jO1xuXHR9LFxuXG5cdC8qKlxuXHQgKiA3LjMuMSBHZXQgKE8sIFApIC0gaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLWdldC1vLXBcblx0ICogMS4gQXNzZXJ0OiBUeXBlKE8pIGlzIE9iamVjdC5cblx0ICogMi4gQXNzZXJ0OiBJc1Byb3BlcnR5S2V5KFApIGlzIHRydWUuXG5cdCAqIDMuIFJldHVybiBPLltbR2V0XV0oUCwgTykuXG5cdCAqL1xuXHRHZXQ6IGZ1bmN0aW9uIEdldChPLCBQKSB7XG5cdFx0Ly8gNy4zLjEuMVxuXHRcdGlmICh0aGlzLlR5cGUoTykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBUeXBlKE8pIGlzIG5vdCBPYmplY3QnKTtcblx0XHR9XG5cdFx0Ly8gNy4zLjEuMlxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBJc1Byb3BlcnR5S2V5KFApIGlzIG5vdCB0cnVlJyk7XG5cdFx0fVxuXHRcdC8vIDcuMy4xLjNcblx0XHRyZXR1cm4gT1tQXTtcblx0fSxcblxuXHRUeXBlOiBmdW5jdGlvbiBUeXBlKHgpIHtcblx0XHRpZiAodHlwZW9mIHggPT09ICdzeW1ib2wnKSB7XG5cdFx0XHRyZXR1cm4gJ1N5bWJvbCc7XG5cdFx0fVxuXHRcdHJldHVybiBFUzUuVHlwZSh4KTtcblx0fSxcblxuXHQvLyBodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtc3BlY2llc2NvbnN0cnVjdG9yXG5cdFNwZWNpZXNDb25zdHJ1Y3RvcjogZnVuY3Rpb24gU3BlY2llc0NvbnN0cnVjdG9yKE8sIGRlZmF1bHRDb25zdHJ1Y3Rvcikge1xuXHRcdGlmICh0aGlzLlR5cGUoTykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBUeXBlKE8pIGlzIG5vdCBPYmplY3QnKTtcblx0XHR9XG5cdFx0dmFyIEMgPSBPLmNvbnN0cnVjdG9yO1xuXHRcdGlmICh0eXBlb2YgQyA9PT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdHJldHVybiBkZWZhdWx0Q29uc3RydWN0b3I7XG5cdFx0fVxuXHRcdGlmICh0aGlzLlR5cGUoQykgIT09ICdPYmplY3QnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdPLmNvbnN0cnVjdG9yIGlzIG5vdCBhbiBPYmplY3QnKTtcblx0XHR9XG5cdFx0dmFyIFMgPSBoYXNTeW1ib2xzICYmIFN5bWJvbC5zcGVjaWVzID8gQ1tTeW1ib2wuc3BlY2llc10gOiB2b2lkIDA7XG5cdFx0aWYgKFMgPT0gbnVsbCkge1xuXHRcdFx0cmV0dXJuIGRlZmF1bHRDb25zdHJ1Y3Rvcjtcblx0XHR9XG5cdFx0aWYgKHRoaXMuSXNDb25zdHJ1Y3RvcihTKSkge1xuXHRcdFx0cmV0dXJuIFM7XG5cdFx0fVxuXHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ25vIGNvbnN0cnVjdG9yIGZvdW5kJyk7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtY29tcGxldGVwcm9wZXJ0eWRlc2NyaXB0b3Jcblx0Q29tcGxldGVQcm9wZXJ0eURlc2NyaXB0b3I6IGZ1bmN0aW9uIENvbXBsZXRlUHJvcGVydHlEZXNjcmlwdG9yKERlc2MpIHtcblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eURlc2NyaXB0b3IoRGVzYykpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Rlc2MgbXVzdCBiZSBhIFByb3BlcnR5IERlc2NyaXB0b3InKTtcblx0XHR9XG5cblx0XHRpZiAodGhpcy5Jc0dlbmVyaWNEZXNjcmlwdG9yKERlc2MpIHx8IHRoaXMuSXNEYXRhRGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0aWYgKCFoYXMoRGVzYywgJ1tbVmFsdWVdXScpKSB7XG5cdFx0XHRcdERlc2NbJ1tbVmFsdWVdXSddID0gdm9pZCAwO1xuXHRcdFx0fVxuXHRcdFx0aWYgKCFoYXMoRGVzYywgJ1tbV3JpdGFibGVdXScpKSB7XG5cdFx0XHRcdERlc2NbJ1tbV3JpdGFibGVdXSddID0gZmFsc2U7XG5cdFx0XHR9XG5cdFx0fSBlbHNlIHtcblx0XHRcdGlmICghaGFzKERlc2MsICdbW0dldF1dJykpIHtcblx0XHRcdFx0RGVzY1snW1tHZXRdXSddID0gdm9pZCAwO1xuXHRcdFx0fVxuXHRcdFx0aWYgKCFoYXMoRGVzYywgJ1tbU2V0XV0nKSkge1xuXHRcdFx0XHREZXNjWydbW1NldF1dJ10gPSB2b2lkIDA7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdGlmICghaGFzKERlc2MsICdbW0VudW1lcmFibGVdXScpKSB7XG5cdFx0XHREZXNjWydbW0VudW1lcmFibGVdXSddID0gZmFsc2U7XG5cdFx0fVxuXHRcdGlmICghaGFzKERlc2MsICdbW0NvbmZpZ3VyYWJsZV1dJykpIHtcblx0XHRcdERlc2NbJ1tbQ29uZmlndXJhYmxlXV0nXSA9IGZhbHNlO1xuXHRcdH1cblx0XHRyZXR1cm4gRGVzYztcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1zZXQtby1wLXYtdGhyb3dcblx0U2V0OiBmdW5jdGlvbiBTZXQoTywgUCwgViwgVGhyb3cpIHtcblx0XHRpZiAodGhpcy5UeXBlKE8pICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignTyBtdXN0IGJlIGFuIE9iamVjdCcpO1xuXHRcdH1cblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eUtleShQKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignUCBtdXN0IGJlIGEgUHJvcGVydHkgS2V5Jyk7XG5cdFx0fVxuXHRcdGlmICh0aGlzLlR5cGUoVGhyb3cpICE9PSAnQm9vbGVhbicpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ1Rocm93IG11c3QgYmUgYSBCb29sZWFuJyk7XG5cdFx0fVxuXHRcdGlmIChUaHJvdykge1xuXHRcdFx0T1tQXSA9IFY7XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dHJ5IHtcblx0XHRcdFx0T1tQXSA9IFY7XG5cdFx0XHR9IGNhdGNoIChlKSB7XG5cdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdH1cblx0XHR9XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtaGFzb3ducHJvcGVydHlcblx0SGFzT3duUHJvcGVydHk6IGZ1bmN0aW9uIEhhc093blByb3BlcnR5KE8sIFApIHtcblx0XHRpZiAodGhpcy5UeXBlKE8pICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignTyBtdXN0IGJlIGFuIE9iamVjdCcpO1xuXHRcdH1cblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eUtleShQKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignUCBtdXN0IGJlIGEgUHJvcGVydHkgS2V5Jyk7XG5cdFx0fVxuXHRcdHJldHVybiBoYXMoTywgUCk7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtaGFzcHJvcGVydHlcblx0SGFzUHJvcGVydHk6IGZ1bmN0aW9uIEhhc1Byb3BlcnR5KE8sIFApIHtcblx0XHRpZiAodGhpcy5UeXBlKE8pICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignTyBtdXN0IGJlIGFuIE9iamVjdCcpO1xuXHRcdH1cblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eUtleShQKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignUCBtdXN0IGJlIGEgUHJvcGVydHkgS2V5Jyk7XG5cdFx0fVxuXHRcdHJldHVybiBQIGluIE87XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtaXNjb25jYXRzcHJlYWRhYmxlXG5cdElzQ29uY2F0U3ByZWFkYWJsZTogZnVuY3Rpb24gSXNDb25jYXRTcHJlYWRhYmxlKE8pIHtcblx0XHRpZiAodGhpcy5UeXBlKE8pICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0XHRpZiAoaGFzU3ltYm9scyAmJiB0eXBlb2YgU3ltYm9sLmlzQ29uY2F0U3ByZWFkYWJsZSA9PT0gJ3N5bWJvbCcpIHtcblx0XHRcdHZhciBzcHJlYWRhYmxlID0gdGhpcy5HZXQoTywgU3ltYm9sLmlzQ29uY2F0U3ByZWFkYWJsZSk7XG5cdFx0XHRpZiAodHlwZW9mIHNwcmVhZGFibGUgIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLlRvQm9vbGVhbihzcHJlYWRhYmxlKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0cmV0dXJuIHRoaXMuSXNBcnJheShPKTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1pbnZva2Vcblx0SW52b2tlOiBmdW5jdGlvbiBJbnZva2UoTywgUCkge1xuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdQIG11c3QgYmUgYSBQcm9wZXJ0eSBLZXknKTtcblx0XHR9XG5cdFx0dmFyIGFyZ3VtZW50c0xpc3QgPSBhcnJheVNsaWNlKGFyZ3VtZW50cywgMik7XG5cdFx0dmFyIGZ1bmMgPSB0aGlzLkdldFYoTywgUCk7XG5cdFx0cmV0dXJuIHRoaXMuQ2FsbChmdW5jLCBPLCBhcmd1bWVudHNMaXN0KTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1jcmVhdGVpdGVycmVzdWx0b2JqZWN0XG5cdENyZWF0ZUl0ZXJSZXN1bHRPYmplY3Q6IGZ1bmN0aW9uIENyZWF0ZUl0ZXJSZXN1bHRPYmplY3QodmFsdWUsIGRvbmUpIHtcblx0XHRpZiAodGhpcy5UeXBlKGRvbmUpICE9PSAnQm9vbGVhbicpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IFR5cGUoZG9uZSkgaXMgbm90IEJvb2xlYW4nKTtcblx0XHR9XG5cdFx0cmV0dXJuIHtcblx0XHRcdHZhbHVlOiB2YWx1ZSxcblx0XHRcdGRvbmU6IGRvbmVcblx0XHR9O1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLXJlZ2V4cGV4ZWNcblx0UmVnRXhwRXhlYzogZnVuY3Rpb24gUmVnRXhwRXhlYyhSLCBTKSB7XG5cdFx0aWYgKHRoaXMuVHlwZShSKSAhPT0gJ09iamVjdCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ1IgbXVzdCBiZSBhbiBPYmplY3QnKTtcblx0XHR9XG5cdFx0aWYgKHRoaXMuVHlwZShTKSAhPT0gJ1N0cmluZycpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ1MgbXVzdCBiZSBhIFN0cmluZycpO1xuXHRcdH1cblx0XHR2YXIgZXhlYyA9IHRoaXMuR2V0KFIsICdleGVjJyk7XG5cdFx0aWYgKHRoaXMuSXNDYWxsYWJsZShleGVjKSkge1xuXHRcdFx0dmFyIHJlc3VsdCA9IHRoaXMuQ2FsbChleGVjLCBSLCBbU10pO1xuXHRcdFx0aWYgKHJlc3VsdCA9PT0gbnVsbCB8fCB0aGlzLlR5cGUocmVzdWx0KSA9PT0gJ09iamVjdCcpIHtcblx0XHRcdFx0cmV0dXJuIHJlc3VsdDtcblx0XHRcdH1cblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ1wiZXhlY1wiIG1ldGhvZCBtdXN0IHJldHVybiBgbnVsbGAgb3IgYW4gT2JqZWN0Jyk7XG5cdFx0fVxuXHRcdHJldHVybiByZWdleEV4ZWMoUiwgUyk7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtYXJyYXlzcGVjaWVzY3JlYXRlXG5cdEFycmF5U3BlY2llc0NyZWF0ZTogZnVuY3Rpb24gQXJyYXlTcGVjaWVzQ3JlYXRlKG9yaWdpbmFsQXJyYXksIGxlbmd0aCkge1xuXHRcdGlmICghdGhpcy5Jc0ludGVnZXIobGVuZ3RoKSB8fCBsZW5ndGggPCAwKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBsZW5ndGggbXVzdCBiZSBhbiBpbnRlZ2VyID49IDAnKTtcblx0XHR9XG5cdFx0dmFyIGxlbiA9IGxlbmd0aCA9PT0gMCA/IDAgOiBsZW5ndGg7XG5cdFx0dmFyIEM7XG5cdFx0dmFyIGlzQXJyYXkgPSB0aGlzLklzQXJyYXkob3JpZ2luYWxBcnJheSk7XG5cdFx0aWYgKGlzQXJyYXkpIHtcblx0XHRcdEMgPSB0aGlzLkdldChvcmlnaW5hbEFycmF5LCAnY29uc3RydWN0b3InKTtcblx0XHRcdC8vIFRPRE86IGZpZ3VyZSBvdXQgaG93IHRvIG1ha2UgYSBjcm9zcy1yZWFsbSBub3JtYWwgQXJyYXksIGEgc2FtZS1yZWFsbSBBcnJheVxuXHRcdFx0Ly8gaWYgKHRoaXMuSXNDb25zdHJ1Y3RvcihDKSkge1xuXHRcdFx0Ly8gXHRpZiBDIGlzIGFub3RoZXIgcmVhbG0ncyBBcnJheSwgQyA9IHVuZGVmaW5lZFxuXHRcdFx0Ly8gXHRPYmplY3QuZ2V0UHJvdG90eXBlT2YoT2JqZWN0LmdldFByb3RvdHlwZU9mKE9iamVjdC5nZXRQcm90b3R5cGVPZihBcnJheSkpKSA9PT0gbnVsbCA/XG5cdFx0XHQvLyB9XG5cdFx0XHRpZiAodGhpcy5UeXBlKEMpID09PSAnT2JqZWN0JyAmJiBoYXNTeW1ib2xzICYmIFN5bWJvbC5zcGVjaWVzKSB7XG5cdFx0XHRcdEMgPSB0aGlzLkdldChDLCBTeW1ib2wuc3BlY2llcyk7XG5cdFx0XHRcdGlmIChDID09PSBudWxsKSB7XG5cdFx0XHRcdFx0QyA9IHZvaWQgMDtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblx0XHRpZiAodHlwZW9mIEMgPT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHRyZXR1cm4gQXJyYXkobGVuKTtcblx0XHR9XG5cdFx0aWYgKCF0aGlzLklzQ29uc3RydWN0b3IoQykpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0MgbXVzdCBiZSBhIGNvbnN0cnVjdG9yJyk7XG5cdFx0fVxuXHRcdHJldHVybiBuZXcgQyhsZW4pOyAvLyB0aGlzLkNvbnN0cnVjdChDLCBsZW4pO1xuXHR9LFxuXG5cdENyZWF0ZURhdGFQcm9wZXJ0eTogZnVuY3Rpb24gQ3JlYXRlRGF0YVByb3BlcnR5KE8sIFAsIFYpIHtcblx0XHRpZiAodGhpcy5UeXBlKE8pICE9PSAnT2JqZWN0Jykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogVHlwZShPKSBpcyBub3QgT2JqZWN0Jyk7XG5cdFx0fVxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5S2V5KFApKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBJc1Byb3BlcnR5S2V5KFApIGlzIG5vdCB0cnVlJyk7XG5cdFx0fVxuXHRcdHZhciBvbGREZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihPLCBQKTtcblx0XHR2YXIgZXh0ZW5zaWJsZSA9IG9sZERlc2MgfHwgKHR5cGVvZiBPYmplY3QuaXNFeHRlbnNpYmxlICE9PSAnZnVuY3Rpb24nIHx8IE9iamVjdC5pc0V4dGVuc2libGUoTykpO1xuXHRcdHZhciBpbW11dGFibGUgPSBvbGREZXNjICYmICghb2xkRGVzYy53cml0YWJsZSB8fCAhb2xkRGVzYy5jb25maWd1cmFibGUpO1xuXHRcdGlmIChpbW11dGFibGUgfHwgIWV4dGVuc2libGUpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cdFx0dmFyIG5ld0Rlc2MgPSB7XG5cdFx0XHRjb25maWd1cmFibGU6IHRydWUsXG5cdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuXHRcdFx0dmFsdWU6IFYsXG5cdFx0XHR3cml0YWJsZTogdHJ1ZVxuXHRcdH07XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KE8sIFAsIG5ld0Rlc2MpO1xuXHRcdHJldHVybiB0cnVlO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLWNyZWF0ZWRhdGFwcm9wZXJ0eW9ydGhyb3dcblx0Q3JlYXRlRGF0YVByb3BlcnR5T3JUaHJvdzogZnVuY3Rpb24gQ3JlYXRlRGF0YVByb3BlcnR5T3JUaHJvdyhPLCBQLCBWKSB7XG5cdFx0aWYgKHRoaXMuVHlwZShPKSAhPT0gJ09iamVjdCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IFR5cGUoTykgaXMgbm90IE9iamVjdCcpO1xuXHRcdH1cblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eUtleShQKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogSXNQcm9wZXJ0eUtleShQKSBpcyBub3QgdHJ1ZScpO1xuXHRcdH1cblx0XHR2YXIgc3VjY2VzcyA9IHRoaXMuQ3JlYXRlRGF0YVByb3BlcnR5KE8sIFAsIFYpO1xuXHRcdGlmICghc3VjY2Vzcykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcigndW5hYmxlIHRvIGNyZWF0ZSBkYXRhIHByb3BlcnR5Jyk7XG5cdFx0fVxuXHRcdHJldHVybiBzdWNjZXNzO1xuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLWFkdmFuY2VzdHJpbmdpbmRleFxuXHRBZHZhbmNlU3RyaW5nSW5kZXg6IGZ1bmN0aW9uIEFkdmFuY2VTdHJpbmdJbmRleChTLCBpbmRleCwgdW5pY29kZSkge1xuXHRcdGlmICh0aGlzLlR5cGUoUykgIT09ICdTdHJpbmcnKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBUeXBlKFMpIGlzIG5vdCBTdHJpbmcnKTtcblx0XHR9XG5cdFx0aWYgKCF0aGlzLklzSW50ZWdlcihpbmRleCkpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Fzc2VydGlvbiBmYWlsZWQ6IGxlbmd0aCBtdXN0IGJlIGFuIGludGVnZXIgPj0gMCBhbmQgPD0gKDIqKjUzIC0gMSknKTtcblx0XHR9XG5cdFx0aWYgKGluZGV4IDwgMCB8fCBpbmRleCA+IE1BWF9TQUZFX0lOVEVHRVIpIHtcblx0XHRcdHRocm93IG5ldyBSYW5nZUVycm9yKCdBc3NlcnRpb24gZmFpbGVkOiBsZW5ndGggbXVzdCBiZSBhbiBpbnRlZ2VyID49IDAgYW5kIDw9ICgyKio1MyAtIDEpJyk7XG5cdFx0fVxuXHRcdGlmICh0aGlzLlR5cGUodW5pY29kZSkgIT09ICdCb29sZWFuJykge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQXNzZXJ0aW9uIGZhaWxlZDogVHlwZSh1bmljb2RlKSBpcyBub3QgQm9vbGVhbicpO1xuXHRcdH1cblx0XHRpZiAoIXVuaWNvZGUpIHtcblx0XHRcdHJldHVybiBpbmRleCArIDE7XG5cdFx0fVxuXHRcdHZhciBsZW5ndGggPSBTLmxlbmd0aDtcblx0XHRpZiAoKGluZGV4ICsgMSkgPj0gbGVuZ3RoKSB7XG5cdFx0XHRyZXR1cm4gaW5kZXggKyAxO1xuXHRcdH1cblx0XHR2YXIgZmlyc3QgPSBTLmNoYXJDb2RlQXQoaW5kZXgpO1xuXHRcdGlmIChmaXJzdCA8IDB4RDgwMCB8fCBmaXJzdCA+IDB4REJGRikge1xuXHRcdFx0cmV0dXJuIGluZGV4ICsgMTtcblx0XHR9XG5cdFx0dmFyIHNlY29uZCA9IFMuY2hhckNvZGVBdChpbmRleCArIDEpO1xuXHRcdGlmIChzZWNvbmQgPCAweERDMDAgfHwgc2Vjb25kID4gMHhERkZGKSB7XG5cdFx0XHRyZXR1cm4gaW5kZXggKyAxO1xuXHRcdH1cblx0XHRyZXR1cm4gaW5kZXggKyAyO1xuXHR9XG59KTtcblxuZGVsZXRlIEVTNi5DaGVja09iamVjdENvZXJjaWJsZTsgLy8gcmVuYW1lZCBpbiBFUzYgdG8gUmVxdWlyZU9iamVjdENvZXJjaWJsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IEVTNjtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9lcy1hYnN0cmFjdC9lczIwMTUuanNcbi8vIG1vZHVsZSBpZCA9IDE5XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKiBlc2xpbnQgbm8taW52YWxpZC10aGlzOiAxICovXG5cbnZhciBFUlJPUl9NRVNTQUdFID0gJ0Z1bmN0aW9uLnByb3RvdHlwZS5iaW5kIGNhbGxlZCBvbiBpbmNvbXBhdGlibGUgJztcbnZhciBzbGljZSA9IEFycmF5LnByb3RvdHlwZS5zbGljZTtcbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG52YXIgZnVuY1R5cGUgPSAnW29iamVjdCBGdW5jdGlvbl0nO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGJpbmQodGhhdCkge1xuICAgIHZhciB0YXJnZXQgPSB0aGlzO1xuICAgIGlmICh0eXBlb2YgdGFyZ2V0ICE9PSAnZnVuY3Rpb24nIHx8IHRvU3RyLmNhbGwodGFyZ2V0KSAhPT0gZnVuY1R5cGUpIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihFUlJPUl9NRVNTQUdFICsgdGFyZ2V0KTtcbiAgICB9XG4gICAgdmFyIGFyZ3MgPSBzbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG5cbiAgICB2YXIgYm91bmQ7XG4gICAgdmFyIGJpbmRlciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMgaW5zdGFuY2VvZiBib3VuZCkge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IHRhcmdldC5hcHBseShcbiAgICAgICAgICAgICAgICB0aGlzLFxuICAgICAgICAgICAgICAgIGFyZ3MuY29uY2F0KHNsaWNlLmNhbGwoYXJndW1lbnRzKSlcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBpZiAoT2JqZWN0KHJlc3VsdCkgPT09IHJlc3VsdCkge1xuICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0YXJnZXQuYXBwbHkoXG4gICAgICAgICAgICAgICAgdGhhdCxcbiAgICAgICAgICAgICAgICBhcmdzLmNvbmNhdChzbGljZS5jYWxsKGFyZ3VtZW50cykpXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIHZhciBib3VuZExlbmd0aCA9IE1hdGgubWF4KDAsIHRhcmdldC5sZW5ndGggLSBhcmdzLmxlbmd0aCk7XG4gICAgdmFyIGJvdW5kQXJncyA9IFtdO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYm91bmRMZW5ndGg7IGkrKykge1xuICAgICAgICBib3VuZEFyZ3MucHVzaCgnJCcgKyBpKTtcbiAgICB9XG5cbiAgICBib3VuZCA9IEZ1bmN0aW9uKCdiaW5kZXInLCAncmV0dXJuIGZ1bmN0aW9uICgnICsgYm91bmRBcmdzLmpvaW4oJywnKSArICcpeyByZXR1cm4gYmluZGVyLmFwcGx5KHRoaXMsYXJndW1lbnRzKTsgfScpKGJpbmRlcik7XG5cbiAgICBpZiAodGFyZ2V0LnByb3RvdHlwZSkge1xuICAgICAgICB2YXIgRW1wdHkgPSBmdW5jdGlvbiBFbXB0eSgpIHt9O1xuICAgICAgICBFbXB0eS5wcm90b3R5cGUgPSB0YXJnZXQucHJvdG90eXBlO1xuICAgICAgICBib3VuZC5wcm90b3R5cGUgPSBuZXcgRW1wdHkoKTtcbiAgICAgICAgRW1wdHkucHJvdG90eXBlID0gbnVsbDtcbiAgICB9XG5cbiAgICByZXR1cm4gYm91bmQ7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2Z1bmN0aW9uLWJpbmQvaW1wbGVtZW50YXRpb24uanNcbi8vIG1vZHVsZSBpZCA9IDIwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgaGFzU3ltYm9scyA9IHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gJ3N5bWJvbCc7XG5cbnZhciBpc1ByaW1pdGl2ZSA9IHJlcXVpcmUoJy4vaGVscGVycy9pc1ByaW1pdGl2ZScpO1xudmFyIGlzQ2FsbGFibGUgPSByZXF1aXJlKCdpcy1jYWxsYWJsZScpO1xudmFyIGlzRGF0ZSA9IHJlcXVpcmUoJ2lzLWRhdGUtb2JqZWN0Jyk7XG52YXIgaXNTeW1ib2wgPSByZXF1aXJlKCdpcy1zeW1ib2wnKTtcblxudmFyIG9yZGluYXJ5VG9QcmltaXRpdmUgPSBmdW5jdGlvbiBPcmRpbmFyeVRvUHJpbWl0aXZlKE8sIGhpbnQpIHtcblx0aWYgKHR5cGVvZiBPID09PSAndW5kZWZpbmVkJyB8fCBPID09PSBudWxsKSB7XG5cdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignQ2Fubm90IGNhbGwgbWV0aG9kIG9uICcgKyBPKTtcblx0fVxuXHRpZiAodHlwZW9mIGhpbnQgIT09ICdzdHJpbmcnIHx8IChoaW50ICE9PSAnbnVtYmVyJyAmJiBoaW50ICE9PSAnc3RyaW5nJykpIHtcblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdoaW50IG11c3QgYmUgXCJzdHJpbmdcIiBvciBcIm51bWJlclwiJyk7XG5cdH1cblx0dmFyIG1ldGhvZE5hbWVzID0gaGludCA9PT0gJ3N0cmluZycgPyBbJ3RvU3RyaW5nJywgJ3ZhbHVlT2YnXSA6IFsndmFsdWVPZicsICd0b1N0cmluZyddO1xuXHR2YXIgbWV0aG9kLCByZXN1bHQsIGk7XG5cdGZvciAoaSA9IDA7IGkgPCBtZXRob2ROYW1lcy5sZW5ndGg7ICsraSkge1xuXHRcdG1ldGhvZCA9IE9bbWV0aG9kTmFtZXNbaV1dO1xuXHRcdGlmIChpc0NhbGxhYmxlKG1ldGhvZCkpIHtcblx0XHRcdHJlc3VsdCA9IG1ldGhvZC5jYWxsKE8pO1xuXHRcdFx0aWYgKGlzUHJpbWl0aXZlKHJlc3VsdCkpIHtcblx0XHRcdFx0cmV0dXJuIHJlc3VsdDtcblx0XHRcdH1cblx0XHR9XG5cdH1cblx0dGhyb3cgbmV3IFR5cGVFcnJvcignTm8gZGVmYXVsdCB2YWx1ZScpO1xufTtcblxudmFyIEdldE1ldGhvZCA9IGZ1bmN0aW9uIEdldE1ldGhvZChPLCBQKSB7XG5cdHZhciBmdW5jID0gT1tQXTtcblx0aWYgKGZ1bmMgIT09IG51bGwgJiYgdHlwZW9mIGZ1bmMgIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0aWYgKCFpc0NhbGxhYmxlKGZ1bmMpKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKGZ1bmMgKyAnIHJldHVybmVkIGZvciBwcm9wZXJ0eSAnICsgUCArICcgb2Ygb2JqZWN0ICcgKyBPICsgJyBpcyBub3QgYSBmdW5jdGlvbicpO1xuXHRcdH1cblx0XHRyZXR1cm4gZnVuYztcblx0fVxufTtcblxuLy8gaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLXRvcHJpbWl0aXZlXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIFRvUHJpbWl0aXZlKGlucHV0LCBQcmVmZXJyZWRUeXBlKSB7XG5cdGlmIChpc1ByaW1pdGl2ZShpbnB1dCkpIHtcblx0XHRyZXR1cm4gaW5wdXQ7XG5cdH1cblx0dmFyIGhpbnQgPSAnZGVmYXVsdCc7XG5cdGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuXHRcdGlmIChQcmVmZXJyZWRUeXBlID09PSBTdHJpbmcpIHtcblx0XHRcdGhpbnQgPSAnc3RyaW5nJztcblx0XHR9IGVsc2UgaWYgKFByZWZlcnJlZFR5cGUgPT09IE51bWJlcikge1xuXHRcdFx0aGludCA9ICdudW1iZXInO1xuXHRcdH1cblx0fVxuXG5cdHZhciBleG90aWNUb1ByaW07XG5cdGlmIChoYXNTeW1ib2xzKSB7XG5cdFx0aWYgKFN5bWJvbC50b1ByaW1pdGl2ZSkge1xuXHRcdFx0ZXhvdGljVG9QcmltID0gR2V0TWV0aG9kKGlucHV0LCBTeW1ib2wudG9QcmltaXRpdmUpO1xuXHRcdH0gZWxzZSBpZiAoaXNTeW1ib2woaW5wdXQpKSB7XG5cdFx0XHRleG90aWNUb1ByaW0gPSBTeW1ib2wucHJvdG90eXBlLnZhbHVlT2Y7XG5cdFx0fVxuXHR9XG5cdGlmICh0eXBlb2YgZXhvdGljVG9QcmltICE9PSAndW5kZWZpbmVkJykge1xuXHRcdHZhciByZXN1bHQgPSBleG90aWNUb1ByaW0uY2FsbChpbnB1dCwgaGludCk7XG5cdFx0aWYgKGlzUHJpbWl0aXZlKHJlc3VsdCkpIHtcblx0XHRcdHJldHVybiByZXN1bHQ7XG5cdFx0fVxuXHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ3VuYWJsZSB0byBjb252ZXJ0IGV4b3RpYyBvYmplY3QgdG8gcHJpbWl0aXZlJyk7XG5cdH1cblx0aWYgKGhpbnQgPT09ICdkZWZhdWx0JyAmJiAoaXNEYXRlKGlucHV0KSB8fCBpc1N5bWJvbChpbnB1dCkpKSB7XG5cdFx0aGludCA9ICdzdHJpbmcnO1xuXHR9XG5cdHJldHVybiBvcmRpbmFyeVRvUHJpbWl0aXZlKGlucHV0LCBoaW50ID09PSAnZGVmYXVsdCcgPyAnbnVtYmVyJyA6IGhpbnQpO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9lcy10by1wcmltaXRpdmUvZXM2LmpzXG4vLyBtb2R1bGUgaWQgPSAyMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIid1c2Ugc3RyaWN0JztcblxudmFyIGdldERheSA9IERhdGUucHJvdG90eXBlLmdldERheTtcbnZhciB0cnlEYXRlT2JqZWN0ID0gZnVuY3Rpb24gdHJ5RGF0ZU9iamVjdCh2YWx1ZSkge1xuXHR0cnkge1xuXHRcdGdldERheS5jYWxsKHZhbHVlKTtcblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSBjYXRjaCAoZSkge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fVxufTtcblxudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcbnZhciBkYXRlQ2xhc3MgPSAnW29iamVjdCBEYXRlXSc7XG52YXIgaGFzVG9TdHJpbmdUYWcgPSB0eXBlb2YgU3ltYm9sID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBTeW1ib2wudG9TdHJpbmdUYWcgPT09ICdzeW1ib2wnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGlzRGF0ZU9iamVjdCh2YWx1ZSkge1xuXHRpZiAodHlwZW9mIHZhbHVlICE9PSAnb2JqZWN0JyB8fCB2YWx1ZSA9PT0gbnVsbCkgeyByZXR1cm4gZmFsc2U7IH1cblx0cmV0dXJuIGhhc1RvU3RyaW5nVGFnID8gdHJ5RGF0ZU9iamVjdCh2YWx1ZSkgOiB0b1N0ci5jYWxsKHZhbHVlKSA9PT0gZGF0ZUNsYXNzO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9pcy1kYXRlLW9iamVjdC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMjJcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG52YXIgaGFzU3ltYm9scyA9IHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIFN5bWJvbCgpID09PSAnc3ltYm9sJztcblxuaWYgKGhhc1N5bWJvbHMpIHtcblx0dmFyIHN5bVRvU3RyID0gU3ltYm9sLnByb3RvdHlwZS50b1N0cmluZztcblx0dmFyIHN5bVN0cmluZ1JlZ2V4ID0gL15TeW1ib2xcXCguKlxcKSQvO1xuXHR2YXIgaXNTeW1ib2xPYmplY3QgPSBmdW5jdGlvbiBpc1N5bWJvbE9iamVjdCh2YWx1ZSkge1xuXHRcdGlmICh0eXBlb2YgdmFsdWUudmFsdWVPZigpICE9PSAnc3ltYm9sJykgeyByZXR1cm4gZmFsc2U7IH1cblx0XHRyZXR1cm4gc3ltU3RyaW5nUmVnZXgudGVzdChzeW1Ub1N0ci5jYWxsKHZhbHVlKSk7XG5cdH07XG5cdG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNTeW1ib2wodmFsdWUpIHtcblx0XHRpZiAodHlwZW9mIHZhbHVlID09PSAnc3ltYm9sJykgeyByZXR1cm4gdHJ1ZTsgfVxuXHRcdGlmICh0b1N0ci5jYWxsKHZhbHVlKSAhPT0gJ1tvYmplY3QgU3ltYm9sXScpIHsgcmV0dXJuIGZhbHNlOyB9XG5cdFx0dHJ5IHtcblx0XHRcdHJldHVybiBpc1N5bWJvbE9iamVjdCh2YWx1ZSk7XG5cdFx0fSBjYXRjaCAoZSkge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0fTtcbn0gZWxzZSB7XG5cdG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNTeW1ib2wodmFsdWUpIHtcblx0XHQvLyB0aGlzIGVudmlyb25tZW50IGRvZXMgbm90IHN1cHBvcnQgU3ltYm9scy5cblx0XHRyZXR1cm4gZmFsc2U7XG5cdH07XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvaXMtc3ltYm9sL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAyM1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsInZhciBoYXMgPSBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBhc3NpZ24odGFyZ2V0LCBzb3VyY2UpIHtcblx0aWYgKE9iamVjdC5hc3NpZ24pIHtcblx0XHRyZXR1cm4gT2JqZWN0LmFzc2lnbih0YXJnZXQsIHNvdXJjZSk7XG5cdH1cblx0Zm9yICh2YXIga2V5IGluIHNvdXJjZSkge1xuXHRcdGlmIChoYXMuY2FsbChzb3VyY2UsIGtleSkpIHtcblx0XHRcdHRhcmdldFtrZXldID0gc291cmNlW2tleV07XG5cdFx0fVxuXHR9XG5cdHJldHVybiB0YXJnZXQ7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvYXNzaWduLmpzXG4vLyBtb2R1bGUgaWQgPSAyNFxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNQcmltaXRpdmUodmFsdWUpIHtcblx0cmV0dXJuIHZhbHVlID09PSBudWxsIHx8ICh0eXBlb2YgdmFsdWUgIT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIHZhbHVlICE9PSAnb2JqZWN0Jyk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2hlbHBlcnMvaXNQcmltaXRpdmUuanNcbi8vIG1vZHVsZSBpZCA9IDI1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgJGlzTmFOID0gcmVxdWlyZSgnLi9oZWxwZXJzL2lzTmFOJyk7XG52YXIgJGlzRmluaXRlID0gcmVxdWlyZSgnLi9oZWxwZXJzL2lzRmluaXRlJyk7XG5cbnZhciBzaWduID0gcmVxdWlyZSgnLi9oZWxwZXJzL3NpZ24nKTtcbnZhciBtb2QgPSByZXF1aXJlKCcuL2hlbHBlcnMvbW9kJyk7XG5cbnZhciBJc0NhbGxhYmxlID0gcmVxdWlyZSgnaXMtY2FsbGFibGUnKTtcbnZhciB0b1ByaW1pdGl2ZSA9IHJlcXVpcmUoJ2VzLXRvLXByaW1pdGl2ZS9lczUnKTtcblxudmFyIGhhcyA9IHJlcXVpcmUoJ2hhcycpO1xuXG4vLyBodHRwczovL2VzNS5naXRodWIuaW8vI3g5XG52YXIgRVM1ID0ge1xuXHRUb1ByaW1pdGl2ZTogdG9QcmltaXRpdmUsXG5cblx0VG9Cb29sZWFuOiBmdW5jdGlvbiBUb0Jvb2xlYW4odmFsdWUpIHtcblx0XHRyZXR1cm4gISF2YWx1ZTtcblx0fSxcblx0VG9OdW1iZXI6IGZ1bmN0aW9uIFRvTnVtYmVyKHZhbHVlKSB7XG5cdFx0cmV0dXJuIE51bWJlcih2YWx1ZSk7XG5cdH0sXG5cdFRvSW50ZWdlcjogZnVuY3Rpb24gVG9JbnRlZ2VyKHZhbHVlKSB7XG5cdFx0dmFyIG51bWJlciA9IHRoaXMuVG9OdW1iZXIodmFsdWUpO1xuXHRcdGlmICgkaXNOYU4obnVtYmVyKSkgeyByZXR1cm4gMDsgfVxuXHRcdGlmIChudW1iZXIgPT09IDAgfHwgISRpc0Zpbml0ZShudW1iZXIpKSB7IHJldHVybiBudW1iZXI7IH1cblx0XHRyZXR1cm4gc2lnbihudW1iZXIpICogTWF0aC5mbG9vcihNYXRoLmFicyhudW1iZXIpKTtcblx0fSxcblx0VG9JbnQzMjogZnVuY3Rpb24gVG9JbnQzMih4KSB7XG5cdFx0cmV0dXJuIHRoaXMuVG9OdW1iZXIoeCkgPj4gMDtcblx0fSxcblx0VG9VaW50MzI6IGZ1bmN0aW9uIFRvVWludDMyKHgpIHtcblx0XHRyZXR1cm4gdGhpcy5Ub051bWJlcih4KSA+Pj4gMDtcblx0fSxcblx0VG9VaW50MTY6IGZ1bmN0aW9uIFRvVWludDE2KHZhbHVlKSB7XG5cdFx0dmFyIG51bWJlciA9IHRoaXMuVG9OdW1iZXIodmFsdWUpO1xuXHRcdGlmICgkaXNOYU4obnVtYmVyKSB8fCBudW1iZXIgPT09IDAgfHwgISRpc0Zpbml0ZShudW1iZXIpKSB7IHJldHVybiAwOyB9XG5cdFx0dmFyIHBvc0ludCA9IHNpZ24obnVtYmVyKSAqIE1hdGguZmxvb3IoTWF0aC5hYnMobnVtYmVyKSk7XG5cdFx0cmV0dXJuIG1vZChwb3NJbnQsIDB4MTAwMDApO1xuXHR9LFxuXHRUb1N0cmluZzogZnVuY3Rpb24gVG9TdHJpbmcodmFsdWUpIHtcblx0XHRyZXR1cm4gU3RyaW5nKHZhbHVlKTtcblx0fSxcblx0VG9PYmplY3Q6IGZ1bmN0aW9uIFRvT2JqZWN0KHZhbHVlKSB7XG5cdFx0dGhpcy5DaGVja09iamVjdENvZXJjaWJsZSh2YWx1ZSk7XG5cdFx0cmV0dXJuIE9iamVjdCh2YWx1ZSk7XG5cdH0sXG5cdENoZWNrT2JqZWN0Q29lcmNpYmxlOiBmdW5jdGlvbiBDaGVja09iamVjdENvZXJjaWJsZSh2YWx1ZSwgb3B0TWVzc2FnZSkge1xuXHRcdC8qIGpzaGludCBlcW51bGw6dHJ1ZSAqL1xuXHRcdGlmICh2YWx1ZSA9PSBudWxsKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKG9wdE1lc3NhZ2UgfHwgJ0Nhbm5vdCBjYWxsIG1ldGhvZCBvbiAnICsgdmFsdWUpO1xuXHRcdH1cblx0XHRyZXR1cm4gdmFsdWU7XG5cdH0sXG5cdElzQ2FsbGFibGU6IElzQ2FsbGFibGUsXG5cdFNhbWVWYWx1ZTogZnVuY3Rpb24gU2FtZVZhbHVlKHgsIHkpIHtcblx0XHRpZiAoeCA9PT0geSkgeyAvLyAwID09PSAtMCwgYnV0IHRoZXkgYXJlIG5vdCBpZGVudGljYWwuXG5cdFx0XHRpZiAoeCA9PT0gMCkgeyByZXR1cm4gMSAvIHggPT09IDEgLyB5OyB9XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9XG5cdFx0cmV0dXJuICRpc05hTih4KSAmJiAkaXNOYU4oeSk7XG5cdH0sXG5cblx0Ly8gaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzUuMS8jc2VjLThcblx0VHlwZTogZnVuY3Rpb24gVHlwZSh4KSB7XG5cdFx0aWYgKHggPT09IG51bGwpIHtcblx0XHRcdHJldHVybiAnTnVsbCc7XG5cdFx0fVxuXHRcdGlmICh0eXBlb2YgeCA9PT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdHJldHVybiAnVW5kZWZpbmVkJztcblx0XHR9XG5cdFx0aWYgKHR5cGVvZiB4ID09PSAnZnVuY3Rpb24nIHx8IHR5cGVvZiB4ID09PSAnb2JqZWN0Jykge1xuXHRcdFx0cmV0dXJuICdPYmplY3QnO1xuXHRcdH1cblx0XHRpZiAodHlwZW9mIHggPT09ICdudW1iZXInKSB7XG5cdFx0XHRyZXR1cm4gJ051bWJlcic7XG5cdFx0fVxuXHRcdGlmICh0eXBlb2YgeCA9PT0gJ2Jvb2xlYW4nKSB7XG5cdFx0XHRyZXR1cm4gJ0Jvb2xlYW4nO1xuXHRcdH1cblx0XHRpZiAodHlwZW9mIHggPT09ICdzdHJpbmcnKSB7XG5cdFx0XHRyZXR1cm4gJ1N0cmluZyc7XG5cdFx0fVxuXHR9LFxuXG5cdC8vIGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLXByb3BlcnR5LWRlc2NyaXB0b3Itc3BlY2lmaWNhdGlvbi10eXBlXG5cdElzUHJvcGVydHlEZXNjcmlwdG9yOiBmdW5jdGlvbiBJc1Byb3BlcnR5RGVzY3JpcHRvcihEZXNjKSB7XG5cdFx0aWYgKHRoaXMuVHlwZShEZXNjKSAhPT0gJ09iamVjdCcpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cdFx0dmFyIGFsbG93ZWQgPSB7XG5cdFx0XHQnW1tDb25maWd1cmFibGVdXSc6IHRydWUsXG5cdFx0XHQnW1tFbnVtZXJhYmxlXV0nOiB0cnVlLFxuXHRcdFx0J1tbR2V0XV0nOiB0cnVlLFxuXHRcdFx0J1tbU2V0XV0nOiB0cnVlLFxuXHRcdFx0J1tbVmFsdWVdXSc6IHRydWUsXG5cdFx0XHQnW1tXcml0YWJsZV1dJzogdHJ1ZVxuXHRcdH07XG5cdFx0Ly8ganNjczpkaXNhYmxlXG5cdFx0Zm9yICh2YXIga2V5IGluIERlc2MpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuXHRcdFx0aWYgKGhhcyhEZXNjLCBrZXkpICYmICFhbGxvd2VkW2tleV0pIHtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXHRcdH1cblx0XHQvLyBqc2NzOmVuYWJsZVxuXHRcdHZhciBpc0RhdGEgPSBoYXMoRGVzYywgJ1tbVmFsdWVdXScpO1xuXHRcdHZhciBJc0FjY2Vzc29yID0gaGFzKERlc2MsICdbW0dldF1dJykgfHwgaGFzKERlc2MsICdbW1NldF1dJyk7XG5cdFx0aWYgKGlzRGF0YSAmJiBJc0FjY2Vzc29yKSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdQcm9wZXJ0eSBEZXNjcmlwdG9ycyBtYXkgbm90IGJlIGJvdGggYWNjZXNzb3IgYW5kIGRhdGEgZGVzY3JpcHRvcnMnKTtcblx0XHR9XG5cdFx0cmV0dXJuIHRydWU7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNS4xLyNzZWMtOC4xMC4xXG5cdElzQWNjZXNzb3JEZXNjcmlwdG9yOiBmdW5jdGlvbiBJc0FjY2Vzc29yRGVzY3JpcHRvcihEZXNjKSB7XG5cdFx0aWYgKHR5cGVvZiBEZXNjID09PSAndW5kZWZpbmVkJykge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5RGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignRGVzYyBtdXN0IGJlIGEgUHJvcGVydHkgRGVzY3JpcHRvcicpO1xuXHRcdH1cblxuXHRcdGlmICghaGFzKERlc2MsICdbW0dldF1dJykgJiYgIWhhcyhEZXNjLCAnW1tTZXRdXScpKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHRydWU7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNS4xLyNzZWMtOC4xMC4yXG5cdElzRGF0YURlc2NyaXB0b3I6IGZ1bmN0aW9uIElzRGF0YURlc2NyaXB0b3IoRGVzYykge1xuXHRcdGlmICh0eXBlb2YgRGVzYyA9PT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cblx0XHRpZiAoIXRoaXMuSXNQcm9wZXJ0eURlc2NyaXB0b3IoRGVzYykpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0Rlc2MgbXVzdCBiZSBhIFByb3BlcnR5IERlc2NyaXB0b3InKTtcblx0XHR9XG5cblx0XHRpZiAoIWhhcyhEZXNjLCAnW1tWYWx1ZV1dJykgJiYgIWhhcyhEZXNjLCAnW1tXcml0YWJsZV1dJykpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi81LjEvI3NlYy04LjEwLjNcblx0SXNHZW5lcmljRGVzY3JpcHRvcjogZnVuY3Rpb24gSXNHZW5lcmljRGVzY3JpcHRvcihEZXNjKSB7XG5cdFx0aWYgKHR5cGVvZiBEZXNjID09PSAndW5kZWZpbmVkJykge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5RGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignRGVzYyBtdXN0IGJlIGEgUHJvcGVydHkgRGVzY3JpcHRvcicpO1xuXHRcdH1cblxuXHRcdGlmICghdGhpcy5Jc0FjY2Vzc29yRGVzY3JpcHRvcihEZXNjKSAmJiAhdGhpcy5Jc0RhdGFEZXNjcmlwdG9yKERlc2MpKSB7XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9XG5cblx0XHRyZXR1cm4gZmFsc2U7XG5cdH0sXG5cblx0Ly8gaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNS4xLyNzZWMtOC4xMC40XG5cdEZyb21Qcm9wZXJ0eURlc2NyaXB0b3I6IGZ1bmN0aW9uIEZyb21Qcm9wZXJ0eURlc2NyaXB0b3IoRGVzYykge1xuXHRcdGlmICh0eXBlb2YgRGVzYyA9PT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdHJldHVybiBEZXNjO1xuXHRcdH1cblxuXHRcdGlmICghdGhpcy5Jc1Byb3BlcnR5RGVzY3JpcHRvcihEZXNjKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignRGVzYyBtdXN0IGJlIGEgUHJvcGVydHkgRGVzY3JpcHRvcicpO1xuXHRcdH1cblxuXHRcdGlmICh0aGlzLklzRGF0YURlc2NyaXB0b3IoRGVzYykpIHtcblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdHZhbHVlOiBEZXNjWydbW1ZhbHVlXV0nXSxcblx0XHRcdFx0d3JpdGFibGU6ICEhRGVzY1snW1tXcml0YWJsZV1dJ10sXG5cdFx0XHRcdGVudW1lcmFibGU6ICEhRGVzY1snW1tFbnVtZXJhYmxlXV0nXSxcblx0XHRcdFx0Y29uZmlndXJhYmxlOiAhIURlc2NbJ1tbQ29uZmlndXJhYmxlXV0nXVxuXHRcdFx0fTtcblx0XHR9IGVsc2UgaWYgKHRoaXMuSXNBY2Nlc3NvckRlc2NyaXB0b3IoRGVzYykpIHtcblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdGdldDogRGVzY1snW1tHZXRdXSddLFxuXHRcdFx0XHRzZXQ6IERlc2NbJ1tbU2V0XV0nXSxcblx0XHRcdFx0ZW51bWVyYWJsZTogISFEZXNjWydbW0VudW1lcmFibGVdXSddLFxuXHRcdFx0XHRjb25maWd1cmFibGU6ICEhRGVzY1snW1tDb25maWd1cmFibGVdXSddXG5cdFx0XHR9O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdGcm9tUHJvcGVydHlEZXNjcmlwdG9yIG11c3QgYmUgY2FsbGVkIHdpdGggYSBmdWxseSBwb3B1bGF0ZWQgUHJvcGVydHkgRGVzY3JpcHRvcicpO1xuXHRcdH1cblx0fSxcblxuXHQvLyBodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi81LjEvI3NlYy04LjEwLjVcblx0VG9Qcm9wZXJ0eURlc2NyaXB0b3I6IGZ1bmN0aW9uIFRvUHJvcGVydHlEZXNjcmlwdG9yKE9iaikge1xuXHRcdGlmICh0aGlzLlR5cGUoT2JqKSAhPT0gJ09iamVjdCcpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ1RvUHJvcGVydHlEZXNjcmlwdG9yIHJlcXVpcmVzIGFuIG9iamVjdCcpO1xuXHRcdH1cblxuXHRcdHZhciBkZXNjID0ge307XG5cdFx0aWYgKGhhcyhPYmosICdlbnVtZXJhYmxlJykpIHtcblx0XHRcdGRlc2NbJ1tbRW51bWVyYWJsZV1dJ10gPSB0aGlzLlRvQm9vbGVhbihPYmouZW51bWVyYWJsZSk7XG5cdFx0fVxuXHRcdGlmIChoYXMoT2JqLCAnY29uZmlndXJhYmxlJykpIHtcblx0XHRcdGRlc2NbJ1tbQ29uZmlndXJhYmxlXV0nXSA9IHRoaXMuVG9Cb29sZWFuKE9iai5jb25maWd1cmFibGUpO1xuXHRcdH1cblx0XHRpZiAoaGFzKE9iaiwgJ3ZhbHVlJykpIHtcblx0XHRcdGRlc2NbJ1tbVmFsdWVdXSddID0gT2JqLnZhbHVlO1xuXHRcdH1cblx0XHRpZiAoaGFzKE9iaiwgJ3dyaXRhYmxlJykpIHtcblx0XHRcdGRlc2NbJ1tbV3JpdGFibGVdXSddID0gdGhpcy5Ub0Jvb2xlYW4oT2JqLndyaXRhYmxlKTtcblx0XHR9XG5cdFx0aWYgKGhhcyhPYmosICdnZXQnKSkge1xuXHRcdFx0dmFyIGdldHRlciA9IE9iai5nZXQ7XG5cdFx0XHRpZiAodHlwZW9mIGdldHRlciAhPT0gJ3VuZGVmaW5lZCcgJiYgIXRoaXMuSXNDYWxsYWJsZShnZXR0ZXIpKSB7XG5cdFx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ2dldHRlciBtdXN0IGJlIGEgZnVuY3Rpb24nKTtcblx0XHRcdH1cblx0XHRcdGRlc2NbJ1tbR2V0XV0nXSA9IGdldHRlcjtcblx0XHR9XG5cdFx0aWYgKGhhcyhPYmosICdzZXQnKSkge1xuXHRcdFx0dmFyIHNldHRlciA9IE9iai5zZXQ7XG5cdFx0XHRpZiAodHlwZW9mIHNldHRlciAhPT0gJ3VuZGVmaW5lZCcgJiYgIXRoaXMuSXNDYWxsYWJsZShzZXR0ZXIpKSB7XG5cdFx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ3NldHRlciBtdXN0IGJlIGEgZnVuY3Rpb24nKTtcblx0XHRcdH1cblx0XHRcdGRlc2NbJ1tbU2V0XV0nXSA9IHNldHRlcjtcblx0XHR9XG5cblx0XHRpZiAoKGhhcyhkZXNjLCAnW1tHZXRdXScpIHx8IGhhcyhkZXNjLCAnW1tTZXRdXScpKSAmJiAoaGFzKGRlc2MsICdbW1ZhbHVlXV0nKSB8fCBoYXMoZGVzYywgJ1tbV3JpdGFibGVdXScpKSkge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignSW52YWxpZCBwcm9wZXJ0eSBkZXNjcmlwdG9yLiBDYW5ub3QgYm90aCBzcGVjaWZ5IGFjY2Vzc29ycyBhbmQgYSB2YWx1ZSBvciB3cml0YWJsZSBhdHRyaWJ1dGUnKTtcblx0XHR9XG5cdFx0cmV0dXJuIGRlc2M7XG5cdH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gRVM1O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLWFic3RyYWN0L2VzNS5qc1xuLy8gbW9kdWxlIGlkID0gMjZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciB0b1N0ciA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG5cbnZhciBpc1ByaW1pdGl2ZSA9IHJlcXVpcmUoJy4vaGVscGVycy9pc1ByaW1pdGl2ZScpO1xuXG52YXIgaXNDYWxsYWJsZSA9IHJlcXVpcmUoJ2lzLWNhbGxhYmxlJyk7XG5cbi8vIGh0dHBzOi8vZXM1LmdpdGh1Yi5pby8jeDguMTJcbnZhciBFUzVpbnRlcm5hbFNsb3RzID0ge1xuXHQnW1tEZWZhdWx0VmFsdWVdXSc6IGZ1bmN0aW9uIChPLCBoaW50KSB7XG5cdFx0dmFyIGFjdHVhbEhpbnQgPSBoaW50IHx8ICh0b1N0ci5jYWxsKE8pID09PSAnW29iamVjdCBEYXRlXScgPyBTdHJpbmcgOiBOdW1iZXIpO1xuXG5cdFx0aWYgKGFjdHVhbEhpbnQgPT09IFN0cmluZyB8fCBhY3R1YWxIaW50ID09PSBOdW1iZXIpIHtcblx0XHRcdHZhciBtZXRob2RzID0gYWN0dWFsSGludCA9PT0gU3RyaW5nID8gWyd0b1N0cmluZycsICd2YWx1ZU9mJ10gOiBbJ3ZhbHVlT2YnLCAndG9TdHJpbmcnXTtcblx0XHRcdHZhciB2YWx1ZSwgaTtcblx0XHRcdGZvciAoaSA9IDA7IGkgPCBtZXRob2RzLmxlbmd0aDsgKytpKSB7XG5cdFx0XHRcdGlmIChpc0NhbGxhYmxlKE9bbWV0aG9kc1tpXV0pKSB7XG5cdFx0XHRcdFx0dmFsdWUgPSBPW21ldGhvZHNbaV1dKCk7XG5cdFx0XHRcdFx0aWYgKGlzUHJpbWl0aXZlKHZhbHVlKSkge1xuXHRcdFx0XHRcdFx0cmV0dXJuIHZhbHVlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignTm8gZGVmYXVsdCB2YWx1ZScpO1xuXHRcdH1cblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdpbnZhbGlkIFtbRGVmYXVsdFZhbHVlXV0gaGludCBzdXBwbGllZCcpO1xuXHR9XG59O1xuXG4vLyBodHRwczovL2VzNS5naXRodWIuaW8vI3g5XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIFRvUHJpbWl0aXZlKGlucHV0LCBQcmVmZXJyZWRUeXBlKSB7XG5cdGlmIChpc1ByaW1pdGl2ZShpbnB1dCkpIHtcblx0XHRyZXR1cm4gaW5wdXQ7XG5cdH1cblx0cmV0dXJuIEVTNWludGVybmFsU2xvdHNbJ1tbRGVmYXVsdFZhbHVlXV0nXShpbnB1dCwgUHJlZmVycmVkVHlwZSk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi4vbm9kZV9tb2R1bGVzL2VzLXRvLXByaW1pdGl2ZS9lczUuanNcbi8vIG1vZHVsZSBpZCA9IDI3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgaGFzID0gcmVxdWlyZSgnaGFzJyk7XG52YXIgcmVnZXhFeGVjID0gUmVnRXhwLnByb3RvdHlwZS5leGVjO1xudmFyIGdPUEQgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yO1xuXG52YXIgdHJ5UmVnZXhFeGVjQ2FsbCA9IGZ1bmN0aW9uIHRyeVJlZ2V4RXhlYyh2YWx1ZSkge1xuXHR0cnkge1xuXHRcdHZhciBsYXN0SW5kZXggPSB2YWx1ZS5sYXN0SW5kZXg7XG5cdFx0dmFsdWUubGFzdEluZGV4ID0gMDtcblxuXHRcdHJlZ2V4RXhlYy5jYWxsKHZhbHVlKTtcblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSBjYXRjaCAoZSkge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fSBmaW5hbGx5IHtcblx0XHR2YWx1ZS5sYXN0SW5kZXggPSBsYXN0SW5kZXg7XG5cdH1cbn07XG52YXIgdG9TdHIgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xudmFyIHJlZ2V4Q2xhc3MgPSAnW29iamVjdCBSZWdFeHBdJztcbnZhciBoYXNUb1N0cmluZ1RhZyA9IHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIFN5bWJvbC50b1N0cmluZ1RhZyA9PT0gJ3N5bWJvbCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNSZWdleCh2YWx1ZSkge1xuXHRpZiAoIXZhbHVlIHx8IHR5cGVvZiB2YWx1ZSAhPT0gJ29iamVjdCcpIHtcblx0XHRyZXR1cm4gZmFsc2U7XG5cdH1cblx0aWYgKCFoYXNUb1N0cmluZ1RhZykge1xuXHRcdHJldHVybiB0b1N0ci5jYWxsKHZhbHVlKSA9PT0gcmVnZXhDbGFzcztcblx0fVxuXG5cdHZhciBkZXNjcmlwdG9yID0gZ09QRCh2YWx1ZSwgJ2xhc3RJbmRleCcpO1xuXHR2YXIgaGFzTGFzdEluZGV4RGF0YVByb3BlcnR5ID0gZGVzY3JpcHRvciAmJiBoYXMoZGVzY3JpcHRvciwgJ3ZhbHVlJyk7XG5cdGlmICghaGFzTGFzdEluZGV4RGF0YVByb3BlcnR5KSB7XG5cdFx0cmV0dXJuIGZhbHNlO1xuXHR9XG5cblx0cmV0dXJuIHRyeVJlZ2V4RXhlY0NhbGwodmFsdWUpO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4uL25vZGVfbW9kdWxlcy9pcy1yZWdleC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMjhcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIndXNlIHN0cmljdCc7XG5cbnZhciBkZWZpbmUgPSByZXF1aXJlKCdkZWZpbmUtcHJvcGVydGllcycpO1xudmFyIGdldFBvbHlmaWxsID0gcmVxdWlyZSgnLi9wb2x5ZmlsbCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHNoaW1BcnJheVByb3RvdHlwZUZpbmQoKSB7XG5cdHZhciBwb2x5ZmlsbCA9IGdldFBvbHlmaWxsKCk7XG5cblx0ZGVmaW5lKEFycmF5LnByb3RvdHlwZSwgeyBmaW5kOiBwb2x5ZmlsbCB9LCB7XG5cdFx0ZmluZDogZnVuY3Rpb24gKCkge1xuXHRcdFx0cmV0dXJuIEFycmF5LnByb3RvdHlwZS5maW5kICE9PSBwb2x5ZmlsbDtcblx0XHR9XG5cdH0pO1xuXG5cdHJldHVybiBwb2x5ZmlsbDtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuLi9ub2RlX21vZHVsZXMvYXJyYXkucHJvdG90eXBlLmZpbmQvc2hpbS5qc1xuLy8gbW9kdWxlIGlkID0gMjlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCJpbXBvcnQgeyBpbml0aWFsaXplV2lkZ2V0LCBtZXNzYWdlRXZlbnRDYWxsYmFjayB9IGZyb20gJ3V0aWxzL2xvYWRlci9sb2FkZXItdXRpbHMnO1xuXG4oKGdsb2JhbCkgPT4ge1xuICAgIGNvbnN0IGVtcyA9IGdsb2JhbC5kb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0Jyk7XG4gICAgY29uc3QgcmVnZXhwID0gLy4qZ25zXFwud2lkZ2V0XFwuaW5pdGlhbGl6ZXJcXC4oW14vXStcXC4pP2pzLztcbiAgICBjb25zdCBuRW1zID0gZW1zLmxlbmd0aDtcblxuICAgIGlmIChnbG9iYWwuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcikge1xuICAgICAgICAvLyBHbG9iYWwgR05TIG9iamVjdFxuICAgICAgICBpZiAoIWdsb2JhbC5HTlMpIHtcbiAgICAgICAgICAgIGdsb2JhbC5HTlMgPSB7fTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBHTlMgPSBnbG9iYWwuR05TO1xuXG4gICAgICAgIC8vIEdsb2JhbCBHTlMgZWxlbWVudHMgKCBzY3JpcHQgdGFncyApXG4gICAgICAgIGlmICghR05TLmVsZW1lbnRzKSB7XG4gICAgICAgICAgICBHTlMuZWxlbWVudHMgPSBbXTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBlbGVtZW50cyA9IEdOUy5lbGVtZW50cztcblxuICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgbkVtczsgaW5kZXggKz0gMSkge1xuICAgICAgICAgICAgY29uc3QgZWxlbWVudCA9IGVtc1tpbmRleF07XG5cbiAgICAgICAgICAgIGlmIChlbGVtZW50LnNyYy5tYXRjaChyZWdleHApICYmIGVsZW1lbnRzLmluZGV4T2YoZWxlbWVudCkgPCAwKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudHMucHVzaChlbGVtZW50KTtcbiAgICAgICAgICAgICAgICBpbml0aWFsaXplV2lkZ2V0KGVsZW1lbnQsIGVsZW1lbnQuYXR0cmlidXRlcyk7XG5cbiAgICAgICAgICAgICAgICBjb25zdCBldmVudE1ldGhvZCA9IGdsb2JhbC5hZGRFdmVudExpc3RlbmVyID8gJ2FkZEV2ZW50TGlzdGVuZXInIDogJ2F0dGFjaEV2ZW50JztcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudExpc3RlbmVyID0gZ2xvYmFsW2V2ZW50TWV0aG9kXTtcbiAgICAgICAgICAgICAgICBjb25zdCBtZXNzYWdlRXZlbnQgPSBldmVudE1ldGhvZCA9PT0gJ2F0dGFjaEV2ZW50JyA/ICdvbm1lc3NhZ2UnIDogJ21lc3NhZ2UnO1xuXG4gICAgICAgICAgICAgICAgZXZlbnRMaXN0ZW5lcihtZXNzYWdlRXZlbnQsIG1lc3NhZ2VFdmVudENhbGxiYWNrLCB0cnVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCBuRW1zOyBpbmRleCArPSAxKSB7XG4gICAgICAgICAgICBjb25zdCBlbGVtZW50ID0gZW1zW2luZGV4XTtcblxuICAgICAgICAgICAgaWYgKGVsZW1lbnQuc3JjLm1hdGNoKHJlZ2V4cCkpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhZ3JhcGggPSBnbG9iYWwuZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgICAgICAgICAgICAgIHBhcmFncmFwaC5pZCA9ICdicm93c2VyLW5vdC1zdXBwb3J0ZWQnO1xuICAgICAgICAgICAgICAgIHBhcmFncmFwaC5pbm5lckhUTUwgPSAnVW5mb3J0dW5hdGVseSB0aGUgYnJvd3NlciB5b3UgYXJlIGN1cnJlbnRseSB1c2luZyBpcyBub3Qgc3VwcG9ydGVkIGJ5IG91ciB3aWRnZXRzLiBQbGVhc2UgdXBkYXRlIHlvdXIgYnJvd3Nlci4nO1xuICAgICAgICAgICAgICAgIGVsZW1lbnQucGFyZW50Tm9kZS5hcHBlbmRDaGlsZChwYXJhZ3JhcGgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSkoZ2xvYmFsKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL2ducy53aWRnZXQuaW5pdGlhbGl6ZXIuanMiXSwic291cmNlUm9vdCI6IiJ9